//
//  CJCHttpTool.m
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCHttpTool.h"
#import <AFNetworking.h>
#import "CJCCommon.h"

@implementation CJCHttpTool

+ (void)getWithUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json",@"text/html", @"text/javascript",@"text/plain", nil];
    
    NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:params];
    
    //未登录接口不用传
    //tempParams[@"uid"] = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    
    
    //签名值，服务器端约定签名算法，上线前可以先不传
    //tempParams[@"sign"] = @"";
    //未登录接口不用传
    
    if ([kUSERDEFAULT_STAND objectForKey:kUSERTOKEN]) {
        
        tempParams[@"token"] = [kUSERDEFAULT_STAND objectForKey:kUSERTOKEN];
        tempParams[@"uid"] = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    }
    
    //版本号，上线前传0.1
    //tempParams[@"v"] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    tempParams[@"v"] = @"0.1";
    //当前毫秒数
    tempParams[@"t"] = [CJCTools getDateTimeTOMilliSeconds];
    //来源
    
    if ([kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE]) {
        
        tempParams[@"f"] = [kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE];
    }else{
        
        tempParams[@"f"] = @"测试";
    }
    
    //操作系统版本
    tempParams[@"osv"] =   [[UIDevice currentDevice] systemName];
    //手机型号
    tempParams[@"pinfo"] = [CJCTools iphoneType];
    
    //发送get请求
   [manager GET:url parameters:tempParams success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
       
       if (success) {
           
           success(responseObject);
       }
       
   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       
       if (failure) {
           
           failure(error);
       }
       
   }];
}

+ (void)getWithNoAddParamsUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json",@"text/html", @"text/javascript",@"text/plain", nil];
    
    [manager GET:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        if (success) {
            
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            
            failure(error);
        }
        
    }];
}

+ (void)postWithNoAddParamsUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json",@"text/html", @"text/javascript",@"text/plain", nil];
    
    [manager POST:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        if (success) {
            
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [MBManager hideAlert];
        
        if (failure) {
            
            failure(error);
        }
        
    }];
}

+ (void)postWithUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json",@"text/html", @"text/javascript",@"text/plain", nil];
    
    NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:params];
    
    //未登录接口不用传
    //tempParams[@"uid"] = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    
    
    //签名值，服务器端约定签名算法，上线前可以先不传
    //tempParams[@"sign"] = @"";
    //未登录接口不用传
    
    if ([kUSERDEFAULT_STAND objectForKey:kUSERTOKEN]) {
        
        tempParams[@"token"] = [kUSERDEFAULT_STAND objectForKey:kUSERTOKEN];
        tempParams[@"uid"] = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    }
    
    //版本号，上线前传0.1
    //tempParams[@"v"] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    tempParams[@"v"] = @"0.1";
    //当前毫秒数
    tempParams[@"t"] = [CJCTools getDateTimeTOMilliSeconds];
    //来源
    
    if ([kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE]) {
        
       tempParams[@"f"] = [kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE];
    }else{
    
        tempParams[@"f"] = @"测试";
    }
    
    //操作系统版本
    tempParams[@"osv"] =   [[UIDevice currentDevice] systemName];
    //手机型号
    tempParams[@"pinfo"] = [CJCTools iphoneType];
    
    [manager POST:url parameters:tempParams success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        if (success) {
            
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [MBManager hideAlert];
        
        if (failure) {
            
            failure(error);
        }
        
    }];
}

+ (void)postImagesAndVideosWithUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    //申明返回的结果是json类型
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //申明请求的数据是json类型
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json",@"text/html", @"text/javascript",@"text/plain", nil];
    
    NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:params];
    
    //未登录接口不用传
    //tempParams[@"uid"] = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    
    
    //签名值，服务器端约定签名算法，上线前可以先不传
    //tempParams[@"sign"] = @"";
    //未登录接口不用传
    
    if ([kUSERDEFAULT_STAND objectForKey:kUSERTOKEN]) {
        
        tempParams[@"token"] = [kUSERDEFAULT_STAND objectForKey:kUSERTOKEN];
        tempParams[@"uid"] = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    }
    
    //版本号，上线前传0.1
    //tempParams[@"v"] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    tempParams[@"v"] = @"0.1";
    //当前毫秒数
    tempParams[@"t"] = [CJCTools getDateTimeTOMilliSeconds];
    //来源
    
    if ([kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE]) {
        
        tempParams[@"f"] = [kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE];
    }else{
        
        tempParams[@"f"] = @"测试";
    }
    
    //操作系统版本
    tempParams[@"osv"] =   [[UIDevice currentDevice] systemName];
    //手机型号
    tempParams[@"pinfo"] = [CJCTools iphoneType];
    
    [manager POST:url parameters:tempParams success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        if (success) {
            
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            
            failure(error);
        }
        
    }];
}

+(void)getRoxmVerificationCodeWithPhone:(NSString *)phone success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure{

    NSString *url = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"sms/send/for_reg"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = phone;
    
    [self postWithUrl:url params:params success:^(id responseObject) {
       
        if (success) {
            
            success(responseObject);
        }
        
    } failure:^(NSError *error) {
        
        if (failure) {
            
            failure(error);
        }
        
    }];
}


@end

//
//  CJCDownloadManger.m
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCDownloadManger.h"
#import "CJCCommon.h"
#import <AFNetworking.h>
#import<CommonCrypto/CommonDigest.h>
#import <AVFoundation/AVFoundation.h>

@interface CJCDownloadManger ()

@property (nonatomic ,strong) NSProgress *progress;

@property (nonatomic ,strong) NSURLSessionDownloadTask *downloadTask;

@end

@implementation CJCDownloadManger

- (void)getVideoPreViewImageWithPath:(NSString *)path finishBlock:(void (^)(UIImage * image,NSString *videoLength)) finishBlock
{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //异步函数
    dispatch_async(queue, ^{
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:path] options:nil];
        AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        
        gen.appliesPreferredTrackTransform = YES;
        CMTime time = CMTimeMakeWithSeconds(0.0, 600);
        NSError *error = nil;
        CMTime actualTime;
        CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
        UIImage *img = [[UIImage alloc] initWithCGImage:image];
        CGImageRelease(image);
        
        CMTime videoTime = [asset duration];
        double seconds = ceil(videoTime.value/videoTime.timescale);
        
        NSString *timeStr = [NSString stringWithFormat:@"%0.0f",seconds];
        
        timeStr = [CJCTools getNewTimeFromDurationSecond:timeStr.integerValue];
        
        dispatch_queue_t queue=dispatch_get_main_queue();
        
        dispatch_async(queue, ^{
        
            if (finishBlock) {
                
                finishBlock(img,timeStr);
            }
            
        });
        
        
    });
}

-(void)downloadVideoWithVideoURL:(NSString *)videoURL{
    
    if ([CJCTools isBlankString:videoURL]) {
        
        return;
    }
    
    NSString *md5str = [self getDownLoadPathwith:videoURL];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:md5str]) {
        
        [self notExistsAndDownload:videoURL];
        
    } else {
        
        [self getVideoPreViewImageWithPath:md5str finishBlock:^(UIImage *image, NSString *videoLength) {
            
           
            if (self.successHandle) {
                
                self.successHandle(md5str, image, videoLength);
            }
            
        }];
        
        
    }
}

-(void)notExistsAndDownload:(NSString *)downloadPath{

    //远程地址
    NSURL *URL = [NSURL URLWithString:downloadPath];
    //默认配置
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    //AFN3.0+基于封住URLSession的句柄
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    //请求
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSProgress *progress;
    self.progress = progress;
    
    //下载Task操作
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:&progress destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        
        NSString *path = [self getDownLoadPathwith:downloadPath];
        
        return [NSURL fileURLWithPath:path];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        
        if (error) {
            
            return ;
        }
        
        [self getVideoPreViewImageWithPath:[self getDownLoadPathwith:downloadPath] finishBlock:^(UIImage *image, NSString *videoLength) {
            
            if (self.successHandle) {
                
                self.successHandle([self getDownLoadPathwith:downloadPath], image, videoLength);
            }
        }];
    }];
    
    self.downloadTask = downloadTask;

    [progress addObserver:self
               forKeyPath:@"fractionCompleted"
                  options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                  context:nil];
    
    [downloadTask resume];
    
}

-(void)dealloc{

    if (self.downloadTask.state == NSURLSessionTaskStateRunning) {
        
        [self.downloadTask cancel];
    }
    
    [self.progress removeObserver:self forKeyPath:@"fractionCompleted"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if ([keyPath isEqualToString:@"fractionCompleted"] && [object isKindOfClass:[NSProgress class]]) {
        NSProgress *progress = (NSProgress *)object;
        if (self.progressHandle) {
            
            self.progressHandle(progress.fractionCompleted);
        }
    }
}

-(NSString *)getDownLoadPathwith:(NSString *)videoPath{
    
    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    
    NSString *createPath = [NSString stringWithFormat:@"%@/ROXM_Videos", videoStr];
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    if (![[NSFileManager defaultManager] fileExistsAtPath:createPath]) {
        [fileManager createDirectoryAtPath:createPath withIntermediateDirectories:YES attributes:nil error:nil];
        
    }
    
    NSString *MD5str = [self md5:videoPath];
    
    videoStr = [NSString stringWithFormat:@"%@/%@.mp4",createPath,MD5str];
    
    return videoStr;
}

- (NSString *)md5:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}


@end

//
//  CJCUploadManager.m
//  roxm
//
//  Created by lfy on 2017/8/31.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCUploadManager.h"
#import <QiniuSDK.h>
#import "CJCCommon.h"
#import "CJCAddImageModel.h"
#import <Photos/Photos.h>

@interface CJCUploadManager ()

@property (nonatomic ,strong) QNUploadManager *manager;

@property (atomic ,assign) NSInteger taskNum;

@end

@implementation CJCUploadManager

-(void)uploadGroupImagesAndVideos:(NSArray *)groups successHandle:(groupUploadSuccessBlock)groupSuccess{

    self.taskNum = 0;
    
    NSMutableArray *keysArr = [NSMutableArray array];
    
    for (CJCAddImageModel *model in groups) {
        
        if (model.mediaType == CJCMediaTypePhoto) {
            
            [self uploadImage:model.image successHandle:^(NSString *QiniuKey) {
                
                self.taskNum++;
                
                model.uploadKey = QiniuKey;
                
                [keysArr addObject:QiniuKey];
                
                if (self.taskNum == groups.count) {
                    
                    self.taskNum = 0;
                    
                    if (groupSuccess) {
                        
                        groupSuccess(keysArr.copy);
                    }
                    
                }
            }];
        }else if (model.mediaType == CJCMediaTypeVideo){
        
            [self uploadVideo:model.videoPath successHandle:^(NSString *QiniuKey) {
                
                self.taskNum++;
                
                model.uploadKey = QiniuKey;
                
                [keysArr addObject:QiniuKey];
                
                if (self.taskNum == groups.count) {
                    
                    self.taskNum = 0;
                    
                    if (groupSuccess) {
                        
                        groupSuccess(keysArr.copy);
                    }
                    
                }
            }];
        }else{
        
            [self uploadAsset:model.asset successHandle:^(NSString *QiniuKey) {
                
                self.taskNum++;
                
                model.uploadKey = QiniuKey;
                
                [keysArr addObject:QiniuKey];
                
                if (self.taskNum == groups.count) {
                    
                    self.taskNum = 0;
                    
                    if (groupSuccess) {
                        
                        groupSuccess(keysArr.copy);
                    }
                    
                }
            }];
        }
    }
    
}

-(void)uploadAsset:(PHAsset *)asset successHandle:(uploadSuccessBlock)success{

    NSString *QNToken = [kUSERDEFAULT_STAND objectForKey:kQINIUTOKEN];
    
    NSString *key;
    
    if (asset.mediaType == PHAssetMediaTypeImage) {
        
        key = [self getQINIUUploadKeyWithType:@"images"];
    }else{
    
        key = [self getQINIUUploadKeyWithType:@"videos"];
    }
    
    
    [self.manager putPHAsset:asset key:key token:QNToken complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        
        if (info.ok) {
            
            if (success) {
                
                success(key);
            }
        }
        
    } option:nil];
}

-(void)uploadVideo:(NSString *)videoPath successHandle:(uploadSuccessBlock)success{

    NSString *QNToken = [kUSERDEFAULT_STAND objectForKey:kQINIUTOKEN];

    NSString *key = [self getQINIUUploadKeyWithType:@"videos"];
    
    [self.manager putFile:videoPath key:key token:QNToken complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        
        if (info.ok) {
            
            NSLog(@"请求成功 %@",[CJCTools getDateTimeTOMilliSeconds]);
            
            if (success) {
                
                success(key);
            }
        }
        
    } option:nil];
}

-(void)uploadImage:(UIImage *)uploadImage successHandle:(uploadSuccessBlock)success{

    NSData *uploadData = UIImageJPEGRepresentation(uploadImage, 0.8);
    
    NSString *QNToken = [kUSERDEFAULT_STAND objectForKey:kQINIUTOKEN];

    NSString *key = [self getQINIUUploadKeyWithType:@"images"];
    
    [self.manager putData:uploadData key:key token:QNToken complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        
        if (info.ok) {
            
            NSLog(@"请求成功 %@",[CJCTools getDateTimeTOMilliSeconds]);
            
            if (success) {
                
                success(key);
            }
        }
        
    } option:nil];
}

-(NSString *)getQINIUUploadKeyWithType:(NSString *)type{
    
    NSString *userID = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    
    NSString *timeStamp = [CJCTools getDateTimeTOMilliSeconds];
    
    NSString *backStr;
    if ([type isEqualToString:@"images"]) {
        
        backStr = @"jpg";
    }else{
        
        backStr = @"mp4";
    }
    
    NSString *key = [NSString stringWithFormat:@"user/%@/%@/%@.%@",userID,type,timeStamp,backStr];
    
    return key;
}

-(QNUploadManager *)manager{

    if (_manager == nil) {
        
        QNConfiguration *config = [QNConfiguration build:^(QNConfigurationBuilder *builder) {
            builder.zone = [QNZone zone0];
        }];
        
        _manager = [QNUploadManager sharedInstanceWithConfiguration:config];
    }
    return _manager;
}

@end

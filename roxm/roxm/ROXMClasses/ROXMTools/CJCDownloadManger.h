//
//  CJCDownloadManger.h
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^downloadSuccessBlock)(NSString *loadPath,UIImage *returnImage,NSString *videoLength);
typedef void(^downloadProgressHandle)(CGFloat progress);

@interface CJCDownloadManger : NSObject

@property (nonatomic ,copy) NSString *downloadPath;

@property (nonatomic ,copy) downloadSuccessBlock successHandle;

@property (nonatomic ,copy) downloadProgressHandle progressHandle;

-(void)downloadVideoWithVideoURL:(NSString *)videoURL;

- (NSString *)md5:(NSString *)input;

@end

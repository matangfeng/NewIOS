//
//  UIButton+addCanOpration.h
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (addCanOpration)

//给系统的button类添加一个属性
@property (nonatomic ,assign) BOOL canOpration;

@end

//
//  UIFont+AccodingVersion.m
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "UIFont+AccodingVersion.h"
#import "CJCCommon.h"

@implementation UIFont (AccodingVersion)

+(UIFont *)accodingVersionGetFont_lightWithSize:(CGFloat)size{

    
    if (GREATERIOS9) {
        
        if (SCREEN_WITDH < 375) {
            
            if (size > 19) {
                
                size--;
            }
            
            return kPINGFANGFont_LIGHTWithSize(kAdaptedValue(size));
        }
        
        return kPINGFANGFont_LIGHTWithSize(size);
    }else{
    
        return kSystemFontWithSize(size);
    }
    
    
}

+(UIFont *)accodingVersionGetFont_regularWithSize:(CGFloat)size{
    
    
    if (CJCIOS9) {
        
        if (SCREEN_WITDH < 375) {
            
            if (size > 19) {
                
                size--;
            }
            
            return kPINGFANGFont_REGULARWithSize(kAdaptedValue(size));
        }
        
        return kPINGFANGFont_REGULARWithSize(size);
    }else{
        
        return kSystemFontWithSize(size);
    }
}

+(UIFont *)accodingVersionGetFont_mediumWithSize:(CGFloat)size{

    if (CJCIOS9) {
        
        if (SCREEN_WITDH < 375) {
            
            if (size > 19) {
                
                size--;
            }
            
            return kPINGFANGFont_MEDIUMWithSize(kAdaptedValue(size));
        }
        
        return kPINGFANGFont_MEDIUMWithSize(kAdaptedValue(size));
    }else{
        
        return kSystemFontWithSize(size);
    }
}

@end

//
//  UIButton+addCanOpration.m
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "UIButton+addCanOpration.h"
#import <objc/runtime.h>

@implementation UIButton (addCanOpration)


static int _intFlag;
//根据添加类的不同属性 设置不同的C类型
//string 用 char   基本数据类型用基本数据类型

-(BOOL)canOpration{
    
    return [objc_getAssociatedObject(self, &_intFlag)integerValue];
}

-(void)setCanOpration:(BOOL)canOpration{

    if (canOpration == YES) {
        
        self.alpha = 1.0;
        
        self.userInteractionEnabled = YES;
    }else{
        
        self.alpha = 0.5;
        
        self.userInteractionEnabled = NO;
    }
    
    NSNumber *num = @(canOpration);
    
    objc_setAssociatedObject(self, &_intFlag,num,OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end

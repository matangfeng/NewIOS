//
//  CJCOprationButton.m
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCOprationButton.h"
#import "CJCCommon.h"

@interface CJCOprationButton ()

@property (nonatomic ,strong) UIImage *opratImage;

@end

@implementation CJCOprationButton

-(instancetype)init{

    if (self = [super init]) {
        
        self.frame = CGRectMake(0, 0, kAdaptedValue(200), kAdaptedValue(45));
        
        self.layer.cornerRadius = kAdaptedValue(22.5);
        self.layer.masksToBounds = YES;
        
        self.titleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:16];
        
        self.opratImage = [UIView imageWithColor:[UIColor toUIColorByStr:@"222222"]];
        
        [self setBackgroundImage:self.opratImage forState:UIControlStateNormal];
        
        self.cjc_acceptEventInterval = 1;
    }
    
    return self;
}

- (void)sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event{

    if ([NSDate date].timeIntervalSince1970 - self.cjc_acceptEventTime < self.cjc_acceptEventInterval) {
        return;
    }
    
    if (self.cjc_acceptEventInterval > 0) {
        self.cjc_acceptEventTime = [NSDate date].timeIntervalSince1970;
    }
    
    [super sendAction:action to:target forEvent:event];
}

@end

//
//  CJCOprationButton.h
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

//创建一个固定格式的button
@interface CJCOprationButton : UIButton

// 重复点击的间隔
@property (nonatomic, assign) NSTimeInterval cjc_acceptEventInterval;

//记录上次按钮点击的时间
@property (nonatomic, assign) NSTimeInterval cjc_acceptEventTime;


@end

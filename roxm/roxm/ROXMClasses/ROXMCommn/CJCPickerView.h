//
//  CJCPickerView.h
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CJCPickerViewHandle)(NSString * returnString,NSString *returnInt);

@interface CJCPickerView : UIView

@property (nonatomic ,strong) UIPickerView *pickerView;

@property (nonatomic ,copy) CJCPickerViewHandle returnHandle;

-(void)showPickerView;

-(void)clickConfirmButton;

-(void)dismissPickerView;

@end

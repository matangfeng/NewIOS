//
//  CJCBirthHeiWeightView.m
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBirthHeiWeightView.h"
#import "CJCCommon.h"
#import "CJCInviteGiftModel.h"

@interface CJCBirthHeiWeightView ()<UIPickerViewDelegate,UIPickerViewDataSource>{

    NSInteger selectYearRow;
    
    NSInteger selectMonthRow;
    
    NSInteger selectDayRow;
    
    NSInteger dayNumber;
    
    NSInteger selectYear;
    
    NSMutableArray *dataArray;
    
    NSString *interStr;
}

@end

@implementation CJCBirthHeiWeightView

-(void)clickConfirmButton{

    if (self.returnHandle) {
        
        NSLog(@"%@------%@",self.returnStr,interStr);
        
        self.returnHandle(self.returnStr,interStr);
    }

    [self dismissPickerView];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.pickerView.delegate = self;
        self.pickerView.dataSource = self;
        dataArray = [NSMutableArray array];
        
        dayNumber = 31;
    }
    return self;
}


-(void)setPickerViewType:(CJCPickerViewType)pickerViewType{
    
    _pickerViewType = pickerViewType;

    switch (pickerViewType) {
        case CJCPickerViewTypeBirthDay:{
        
            [self getTypeBirthDayDataSource];
        }
            break;
            
        case CJCPickerViewTypeHeight:{
            
            [self getTypeHeightDataSource];
        }
            break;
            
        case CJCPickerViewTypeWeight:{
            
            [self getTypeWeightDataSource];
        }
            break;
            
        case CJCPickerViewTypeBust:{
            
            [self getTypeBustDataSource];
        }
            break;
            
        case CJCPickerViewTypeHotelLevel:{
            
            [self getTypeHotelLevelDataSource];
        }
            break;
            
        case CJCPickerViewTypeThreeHour:{
            
            [self getGiftList];
        }
            break;
            
        case CJCPickerViewTypeTwelveHoure:{
            
            [self getGiftList];
        }
            break;
            
        case CJCPickerViewTypeDateTime:{
            
            [self getTypeDateTimeDataSource];
        }
            break;
            
        default:
            break;
    }
}

-(void)getTypeDateTimeDataSource{

    NSArray *tempArr = @[@"3小时",@"12小时"];
    
    [dataArray addObjectsFromArray:tempArr];
    
    self.returnStr = @"3小时";
    interStr = @"1";
}

-(void)getTypeWeightDataSource{
    
    if ([self.selectStr isEqualToString:@"选择"]) {
        
        if (self.sexType == CJCSexTypeMan) {
            
            self.returnStr = @"70kg";
            interStr = @"70";
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:35 inComponent:0 animated:NO];
        }else if(self.sexType == CJCSexTypeWoman){
            
            self.returnStr = @"45kg";
            interStr = @"45";
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:10 inComponent:0 animated:NO];
        }else{
        
            self.returnStr = @"60kg";
            interStr = @"60";
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:25 inComponent:0 animated:NO];
        }
    }else{
    
        NSString *subStr = [self.selectStr substringToIndex:2];
        
        self.returnStr = self.selectStr;
        
        if (subStr.integerValue <30) {
            
            subStr = @"100";
        }
        interStr = subStr;
        
        [self.pickerView reloadAllComponents];
        
        [self.pickerView selectRow:subStr.integerValue-35 inComponent:0 animated:NO];
    }
}

-(void)getTypeHeightDataSource{

    if ([self.selectStr isEqualToString:@"选择"]){
    
        if (self.sexType == CJCSexTypeMan) {
            
            self.returnStr = @"175cm";
            interStr = @"175";
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:25 inComponent:0 animated:NO];
        }else  if(self.sexType == CJCSexTypeWoman){
            
            self.returnStr = @"165cm";
            interStr = @"165";
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:15 inComponent:0 animated:NO];
        }else{
        
            self.returnStr = @"170cm";
            interStr = @"170";
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:20 inComponent:0 animated:NO];
        }
    }else{
    
        NSString *subStr = [self.selectStr substringToIndex:3];
        
        self.returnStr = self.selectStr;
        interStr = subStr;
        
        [self.pickerView reloadAllComponents];
        
        [self.pickerView selectRow:subStr.integerValue-150 inComponent:0 animated:NO];
    }
}

-(void)getTypeBirthDayDataSource{
    
    if ([self.selectStr isEqualToString:@"选择"]) {
        
        if (self.sexType == CJCSexTypeMan) {
            
            self.returnStr = @"1987年1月1日";
            interStr = @"1987_1_1";
            
            selectYearRow = 47;
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:47 inComponent:1 animated:NO];
            [self.pickerView reloadComponent:1];
        }else if(self.sexType == CJCSexTypeWoman){
            
            self.returnStr = @"1997年1月1日";
            interStr = @"1997_1_1";
            
            selectYearRow = 57;
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:57 inComponent:1 animated:NO];
        }else{
        
            self.returnStr = @"1987年1月1日";
            interStr = @"1987_1_1";
            
            selectYearRow = 47;
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:47 inComponent:1 animated:NO];
            [self.pickerView reloadComponent:1];
        }
        
        [self.pickerView selectRow:60 inComponent:2 animated:NO];
        [self.pickerView selectRow:dayNumber*5 inComponent:3 animated:NO];
        
    }else{
    
        self.returnStr = self.selectStr;
        
        NSString *leftStr = self.selectStr;
        
        NSMutableString *tempInterStr = [NSMutableString string];
        
        for (int i=0; i<3; i++) {
            
            if (i==0) {
                
                NSRange  range = [leftStr rangeOfString:@"年"];
                
                NSString *substr = [leftStr substringToIndex:range.location];
                
                [tempInterStr appendString:[NSString stringWithFormat:@"%@_",substr]];
                
                leftStr = [leftStr substringFromIndex:range.location+1];
                
                selectYear = substr.integerValue;
                
                selectYearRow = substr.integerValue-1940;
                
                [self.pickerView reloadAllComponents];
                
                [self.pickerView selectRow:substr.integerValue-1940 inComponent:i+1 animated:NO];
            }else if (i == 1){
            
                NSRange  range = [leftStr rangeOfString:@"月"];
                
                NSString *substr = [leftStr substringToIndex:range.location];
                
                [tempInterStr appendString:[NSString stringWithFormat:@"%@_",substr]];
                
                leftStr = [leftStr substringFromIndex:range.location+1];
            
                dayNumber = [self getDaysWithYear:selectYear month:substr.integerValue];
                
                selectMonthRow = substr.integerValue-1;
                
                [self.pickerView reloadAllComponents];
                
                [self.pickerView selectRow:selectMonthRow+60 inComponent:i+1 animated:NO];
            }else{
                
                NSRange  range = [leftStr rangeOfString:@"日"];
                
                NSString *substr = [leftStr substringToIndex:range.location];
                
                [tempInterStr appendString:substr];
                
                [self.pickerView reloadAllComponents];
                
                selectDayRow = substr.integerValue-1;
                
                [self.pickerView selectRow:selectDayRow+dayNumber*5 inComponent:i+1 animated:NO];
            }
        }
        
        interStr = tempInterStr.copy;
    }
}

-(void)getTypeBustDataSource{

    NSArray *tempArr = @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H"];
    
    [dataArray addObjectsFromArray:tempArr];
    
    [self.pickerView reloadAllComponents];
    
    if ([self.selectStr isEqualToString:@"选择"]){
    
        self.returnStr = @"C";
        interStr = @"C";
        
        [self.pickerView selectRow:2 inComponent:0 animated:NO];
    }else{
    
        self.returnStr = self.selectStr;
        interStr = self.selectStr;
        
        [self.pickerView selectRow:[self selectBustRow] inComponent:0 animated:NO];
    }
    
}

-(NSInteger)selectBustRow{

    NSInteger selectRow = 0;
    
    for (NSString *str in dataArray.copy) {
        
        if ([self.selectStr isEqualToString:@"A"]) {
            
            selectRow = 0;
        }
        
        if ([self.selectStr isEqualToString:@"B"]) {
            
            selectRow = 1;
        }
        
        if ([self.selectStr isEqualToString:@"C"]) {
            
            selectRow = 2;
        }
        
        if ([self.selectStr isEqualToString:@"D"]) {
            
            selectRow = 3;
        }
        
        if ([self.selectStr isEqualToString:@"E"]) {
            
            selectRow = 4;
        }
        
        if ([self.selectStr isEqualToString:@"F"]) {
            
            selectRow = 5;
        }
        
        if ([self.selectStr isEqualToString:@"G"]) {
            
            selectRow = 6;
        }
        
        if ([self.selectStr isEqualToString:@"H"]) {
            
            selectRow = 7;
        }
    }
    
    return selectRow;
}

-(void)getTypeHotelLevelDataSource{
    
    NSArray *tempArr = @[@"五星级或以上",@"四星级或以上",@"三星级或以上",@"不限"];
    
    [dataArray addObjectsFromArray:tempArr];
    
    [self.pickerView reloadAllComponents];
    
    if ([self.selectStr isEqualToString:@"选择"]){
        
        self.returnStr = @"四星级或以上";
        interStr = @"4";
        
        [self.pickerView selectRow:1 inComponent:0 animated:NO];
    }else{
        
        self.returnStr = self.selectStr;
        
        [self.pickerView selectRow:[self selectHotelLevelRow] inComponent:0 animated:NO];
    }
    
}

-(NSInteger)selectHotelLevelRow{

    NSInteger selectRow = 0;
    
    for (int i=0; i<dataArray.count; i++) {
        
        if ([self.selectStr isEqualToString:@"五星级或以上"]){
        
            selectRow = 0;
            interStr = @"5";
        }
        
        if ([self.selectStr isEqualToString:@"四星级或以上"]){
            
            selectRow = 1;
            interStr = @"4";
        }
        
        if ([self.selectStr isEqualToString:@"三星级或以上"]){
            
            selectRow = 2;
            interStr = @"3";
        }
        
        if ([self.selectStr isEqualToString:@"不限"]){
            
            selectRow = 3;
            interStr = @"0";
        }
    }
    
    return selectRow;
}

-(void)getGiftList{
    
    [MBManager showLoading];
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/list/system"];
    
    [CJCHttpTool postWithUrl:giftURL params:nil success:^(id responseObject) {
        
        [MBManager hideAlert];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCInviteGiftModel class] json:responseObject[@"data"]];
            
            [dataArray addObjectsFromArray:tempArr];
            
            CJCInviteGiftModel *model = dataArray[0];
            self.returnStr = model.name;
            interStr = model.giftId;
            
            [self.pickerView reloadAllComponents];
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)getTypeThreeHourDataSource{
    
    NSArray *tempArr = @[@"600",@"800",@"1000",@"1500",@"2000",@"2500",@"3000",@"4000",@"5000",@"6000",@"7000",@"8000",@"9000",@"10000"];
    
    [dataArray addObjectsFromArray:tempArr];
    
    [self.pickerView reloadAllComponents];
    
    if ([self.selectStr isEqualToString:@"选择"]){
        
        self.returnStr = @"2000";
        interStr = self.returnStr;
        
        [self.pickerView selectRow:4 inComponent:0 animated:NO];
    }else{
        
        self.returnStr = self.selectStr;
        interStr = self.returnStr;
        
        [self.pickerView selectRow:[self selectHourRow] inComponent:0 animated:NO];
    }
    
}

-(void)getTypeTwelveHourDataSource{
    
    NSArray *tempArr = @[@"600",@"800",@"1000",@"1500",@"2000",@"2500",@"3000",@"4000",@"5000",@"6000",@"7000",@"8000",@"9000",@"10000"];
    
    [dataArray addObjectsFromArray:tempArr];
    
    [self.pickerView reloadAllComponents];
    
    if ([self.selectStr isEqualToString:@"选择"]){
        
        self.returnStr = @"4000";
        interStr = self.returnStr;
        
        [self.pickerView selectRow:7 inComponent:0 animated:NO];
    }else{
        
        self.returnStr = self.selectStr;
        interStr = self.returnStr;
        
        [self.pickerView selectRow:[self selectHourRow] inComponent:0 animated:NO];
    }
    
}

-(NSInteger)selectHourRow{

    NSInteger selectRow = 0;
    
    for (int i=0; i<dataArray.count; i++){
    
        if ([self.selectStr isEqualToString:@"600"]){
        
            selectRow = 0;
        }
        
        if ([self.selectStr isEqualToString:@"800"]){
            
            selectRow = 1;
        }
        
        if ([self.selectStr isEqualToString:@"1000"]){
            
            selectRow = 2;
        }
        
        if ([self.selectStr isEqualToString:@"1500"]){
            
            selectRow = 3;
        }
        
        if ([self.selectStr isEqualToString:@"2000"]){
            
            selectRow = 4;
        }
        
        if ([self.selectStr isEqualToString:@"2500"]){
            
            selectRow = 5;
        }
        
        if ([self.selectStr isEqualToString:@"3000"]){
            
            selectRow = 6;
        }
        
        if ([self.selectStr isEqualToString:@"4000"]){
            
            selectRow = 7;
        }
        
        if ([self.selectStr isEqualToString:@"5000"]){
            
            selectRow = 8;
        }
        
        if ([self.selectStr isEqualToString:@"6000"]){
            
            selectRow = 9;
        }
        
        if ([self.selectStr isEqualToString:@"7000"]){
            
            selectRow = 10;
        }
        
        if ([self.selectStr isEqualToString:@"8000"]){
            
            selectRow = 11;
        }
        
        if ([self.selectStr isEqualToString:@"9000"]){
            
            selectRow = 12;
        }
        
        if ([self.selectStr isEqualToString:@"10000"]){
            
            selectRow = 13;
        }
    }
    
    return selectRow;
}

#pragma mark ======pickerView的delegate和datesource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{

    if (self.pickerViewType == CJCPickerViewTypeBirthDay) {
        
        return 5;
    }
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    if (self.pickerViewType == CJCPickerViewTypeBirthDay) {
        
        if (component == 1) {
            
            return 60;
        }else if (component == 2){
        
            return 12*10;
        }else if(component == 3){
        
            return dayNumber*10;
        }else{
        
            return 0;
        }
        
    }else if (self.pickerViewType == CJCPickerViewTypeHeight){
        
        return 71;
    }if (self.pickerViewType == CJCPickerViewTypeWeight){
    
        return 66;
    }
    
    return dataArray.count;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{

    if (self.pickerViewType == CJCPickerViewTypeBirthDay){
    
        if (component == 0) {
            
            return kAdaptedValue(70);
        }else if (component == 1) {
            
            return kAdaptedValue(80);
        }else if (component == 2){
        
            return kAdaptedValue(70);
        }else if(component ==3){
        
            return kAdaptedValue(60);
        }else{
        
            return SCREEN_WITDH - kAdaptedValue(280);
        }

    }
    
    return SCREEN_WITDH;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{

    return 36;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{

    
    UILabel *titleLabel = [[UILabel alloc] init];
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    titleLabel.font = [UIFont fontWithName:@"Medium" size:16];
    
    titleLabel.textColor = [UIColor toUIColorByStr:@"29292b"];
    
    if (self.pickerViewType == CJCPickerViewTypeBirthDay){
        
        if (component == 1) {
            
            titleLabel.text = [NSString stringWithFormat:@"%ld年",row+1940];
        }else if (component == 2){
            
            titleLabel.text = [NSString stringWithFormat:@"%ld月",row%12+1];
        }else if(component == 3){
            
            titleLabel.text = [NSString stringWithFormat:@"%ld日",row%dayNumber+1];
        }
        
    }else if (self.pickerViewType == CJCPickerViewTypeHeight){
        
        titleLabel.text = [NSString stringWithFormat:@"%ldcm",row+150];
       
    }else if (self.pickerViewType == CJCPickerViewTypeWeight){
    
        titleLabel.text = [NSString stringWithFormat:@"%ldkg",row+35];
    }else if (self.pickerViewType == CJCPickerViewTypeThreeHour){
    
        CJCInviteGiftModel *model = dataArray[row];
        
        titleLabel.text = model.name;
        
    }else if (self.pickerViewType == CJCPickerViewTypeTwelveHoure){
        
        CJCInviteGiftModel *model = dataArray[row];
        
        titleLabel.text = model.name;
        
    }else{
    
        titleLabel.text = dataArray[row];
    }
    
    return titleLabel;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{

    if (self.pickerViewType == CJCPickerViewTypeBirthDay) {
        
        if (component == 1) {
            
            selectYearRow = row;
            
            dayNumber = [self getDaysWithYear:(selectYearRow+1940) month:(selectMonthRow+1)];
            
            [self.pickerView reloadComponent:2];
            
            [self.pickerView selectRow:selectDayRow+dayNumber*5 inComponent:2 animated:NO];
        }else if(component == 2){
        
            selectMonthRow = row%12;
            
            dayNumber = [self getDaysWithYear:(selectYearRow+1940) month:(selectMonthRow+1)];
            
            [self.pickerView reloadComponent:2];
            
            [self.pickerView selectRow:selectDayRow+dayNumber*5 inComponent:2 animated:NO];
        }else if(component == 3){
        
             selectDayRow = row%dayNumber;
        }
        
        self.returnStr = [NSString stringWithFormat:@"%ld年%ld月%ld日",selectYearRow+1940,selectMonthRow+1,selectDayRow+1];
        interStr = [NSString stringWithFormat:@"%ld_%ld_%ld",selectYearRow+1940,selectMonthRow+1,selectDayRow+1];
        
    }else if (self.pickerViewType == CJCPickerViewTypeHeight){
    
        self.returnStr = [NSString stringWithFormat:@"%ldcm",row+150];
        interStr = [NSString stringWithFormat:@"%ld",row+150];
    }else if (self.pickerViewType == CJCPickerViewTypeWeight){
    
        self.returnStr = [NSString stringWithFormat:@"%ldkg",row+35];
        interStr = [NSString stringWithFormat:@"%ld",row+35];
    }else if(self.pickerViewType == CJCPickerViewTypeHotelLevel){
    
        self.returnStr = dataArray[row];
        
        self.selectStr = dataArray[row];
        [self selectHotelLevelRow];
        
    }else if(self.pickerViewType == CJCPickerViewTypeDateTime){
        
        self.returnStr = dataArray[row];
        interStr = [NSString stringWithFormat:@"%ld",row+1];
        
    }else if (self.pickerViewType == CJCPickerViewTypeThreeHour||self.pickerViewType == CJCPickerViewTypeTwelveHoure){
        
        CJCInviteGiftModel *model = dataArray[row];
        self.returnStr = model.name;
        interStr = model.giftId;
        
    }else{
    
        self.returnStr = dataArray[row];
        interStr = self.returnStr;
    }
}


- (NSInteger)getDaysWithYear:(NSInteger)year
                       month:(NSInteger)month
{
    switch (month) {
        case 1:
            return 31;
            break;
        case 2:
            if (year%400==0 || (year%100!=0 && year%4 == 0)) {
                return 29;
            }else{
                return 28;
            }
            break;
        case 3:
            return 31;
            break;
        case 4:
            return 30;
            break;
        case 5:
            return 31;
            break;
        case 6:
            return 30;
            break;
        case 7:
            return 31;
            break;
        case 8:
            return 31;
            break;
        case 9:
            return 30;
            break;
        case 10:
            return 31;
            break;
        case 11:
            return 30;
            break;
        case 12:
            return 31;
            break;
        default:
            return 0;
            break;
    }
}

@end

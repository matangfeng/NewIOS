//
//  CJCErrorAletLabel.h
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

//创建一个固定格式的提供错误信息的label 需要设置frame
@interface CJCErrorAletLabel : UILabel

@end

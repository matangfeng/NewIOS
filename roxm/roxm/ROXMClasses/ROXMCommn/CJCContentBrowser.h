//
//  CJCContentBrowser.h
//  roxm
//
//  Created by lfy on 2017/8/14.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCContentBrowser : UIView

@property (nonatomic ,assign) CGRect OriginalFrame;

@property (nonatomic ,strong) UIImage *photo;

-(void)show;

@end

//
//  CJCBirthHeiWeightView.h
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPickerView.h"
#import "CommonVC.h"

typedef NS_ENUM(NSInteger,CJCPickerViewType){
    
    CJCPickerViewTypeBirthDay               = 0,
    CJCPickerViewTypeHeight                 = 1,
    CJCPickerViewTypeWeight                 = 2,
    CJCPickerViewTypeBust                   = 3,
    CJCPickerViewTypeHotelLevel             = 4,
    CJCPickerViewTypeThreeHour              = 5,
    CJCPickerViewTypeTwelveHoure            = 6,
    CJCPickerViewTypeDateTime               =7,
};


@interface CJCBirthHeiWeightView : CJCPickerView

@property (nonatomic ,assign) CJCPickerViewType pickerViewType;

@property (nonatomic ,assign) CJCSexType sexType;

@property (nonatomic ,copy) NSString *returnStr;

@property (nonatomic ,copy) NSString *selectStr;

@end

//
//  CJCContentBrowser.m
//  roxm
//
//  Created by lfy on 2017/8/14.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCContentBrowser.h"
#import "CJCCommon.h"

@interface CJCContentBrowser ()

@property (nonatomic ,strong) UIImageView *showImageView;

@end

@implementation CJCContentBrowser

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor blackColor];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hid)];
        
        [self addGestureRecognizer:tap];
        
    }
    return self;
}

-(void)setPhoto:(UIImage *)photo{

    self.showImageView.image = photo;
}

-(void)hid{

    
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.frame = self.OriginalFrame;
        
        if (self.showImageView) {
            
            self.showImageView.frame = CGRectMake(0, (self.height-self.width)/2, self.width, self.width);;
        }
        
    } completion:^(BOOL finished) {
        
        [self removeAllSubviews];
        [self removeFromSuperview];
    }];
}

-(void)show{

    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    [window addSubview:self];
    
    self.frame = self.OriginalFrame;
    
    self.center = window.center;
    
    [window bringSubviewToFront:self];
    
    if (self.showImageView) {
        
        self.showImageView.frame = CGRectMake(0, (self.height-self.width)/2, self.width, self.width);;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.frame = [UIApplication sharedApplication].keyWindow.bounds;
        
        if (self.showImageView) {
            
            self.showImageView.frame = CGRectMake(0, (self.height-self.width)/2, self.width, self.width);
        }
    }];
}

-(UIImageView *)showImageView{

    if (_showImageView == nil) {
        
        _showImageView = [[UIImageView alloc] init];
        
        _showImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview:_showImageView];
    }
    
    return _showImageView;
}

@end

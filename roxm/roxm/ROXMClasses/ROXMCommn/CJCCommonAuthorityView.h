//
//  CJCCommonAuthorityView.h
//  roxm
//
//  Created by lfy on 2017/9/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCCommonAuthorityView : UIView

@property (nonatomic ,strong) UIButton *backButton;

@property (nonatomic ,copy) NSString *imageName;

@property (nonatomic ,copy) NSString *hintStr;

@property (nonatomic ,copy) NSString *titleStr;

@property (nonatomic ,strong) UIButton *goSetButton;

@end

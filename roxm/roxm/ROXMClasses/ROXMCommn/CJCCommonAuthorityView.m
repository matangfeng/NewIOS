//
//  CJCCommonAuthorityView.m
//  roxm
//
//  Created by lfy on 2017/9/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCCommonAuthorityView.h"
#import "CJCCommon.h"

@interface CJCCommonAuthorityView ()

@property (nonatomic ,strong) UIButton *smallBackButton;

@property (nonatomic ,strong) UILabel *hintLabel;

@property (nonatomic ,strong) UIImageView *imageView;

@property (nonatomic ,strong) UILabel *titleLabel;

@end

@implementation CJCCommonAuthorityView

-(instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIButton *backButton = [UIView getButtonWithStr:@"取消" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    backButton.frame = CGRectMake(kAdaptedValue(14), kAdaptedValue(33.5), kAdaptedValue(40), kAdaptedValue(16));
    
    backButton.centerY = kNAVIVIEWCENTERY;
    
    [self addSubview:backButton];
    
    UIButton *bigBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    bigBackButton.frame = CGRectMake(0, 0, 64, 64);
    
    self.backButton = bigBackButton;
    [self addSubview:bigBackButton];
    
    UIView *lineView = [[UIView alloc] init];
    
    lineView.frame = CGRectMake(0, 63, SCREEN_WITDH, OnePXLineHeight);
    
    lineView.backgroundColor = [UIColor toUIColorByStr:@"D8D8D8"];
    
    [self addSubview:lineView];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:kGetImage(@"empty_photo")];
    
    imageView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(174), kAdaptedValue(24), kAdaptedValue(20.5));
    
    self.imageView = imageView;
    [self addSubview:imageView];
    
    UILabel *titleLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:28 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(60), kAdaptedValue(170), kAdaptedValue(220), kAdaptedValue(28));
    
    self.titleLabel = titleLabel;
    [self addSubview:titleLabel];
    
    UILabel *tipLabel = [[UILabel alloc] init];
    tipLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(217), self.width - kAdaptedValue(46), kAdaptedValue(72));
    tipLabel.numberOfLines = 0;
    tipLabel.textColor = [UIColor toUIColorByStr:@"888888"];
    
    self.hintLabel = tipLabel;
    [self addSubview:tipLabel];
    
    UIView *bottomView = [[UIView alloc] init];
    
    bottomView.backgroundColor = [UIColor toUIColorByStr:@"1D1D1D"];
    
    bottomView.frame = CGRectMake(kAdaptedValue(108), kAdaptedValue(334), kAdaptedValue(159), kAdaptedValue(49));
    
    bottomView.layer.cornerRadius = kAdaptedValue(25);
    bottomView.layer.masksToBounds = YES;
    
    bottomView.centerX = self.centerX;
    [self addSubview:bottomView];
    
    UILabel *setLabel = [UIView getYYLabelWithStr:@"去设置" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    setLabel.frame = CGRectMake(kAdaptedValue(55.5), kAdaptedValue(13.5), kAdaptedValue(52), kAdaptedValue(22.5));
    
    setLabel.center = bottomView.center;
    [self addSubview:setLabel];
    
    UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:kGetImage(@"arrowright_white")];
    
    arrowImageView.frame = CGRectMake(200, 10, kAdaptedValue(6), kAdaptedValue(11));
    
    arrowImageView.centerY = setLabel.centerY;
    arrowImageView.x = setLabel.right+kAdaptedValue(25);
    
    [self addSubview:arrowImageView];
    
    UIButton *goSetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    goSetButton.frame = bottomView.frame;
    
    self.goSetButton = goSetButton;
    [self addSubview:goSetButton];
}

-(void)setHintStr:(NSString *)hintStr{

    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineSpacing = 9;
    
    NSDictionary *attributes = @{ NSFontAttributeName:[UIFont accodingVersionGetFont_regularWithSize:15], NSParagraphStyleAttributeName:paragraphStyle};
    self.hintLabel.attributedText = [[NSAttributedString alloc]initWithString:hintStr attributes:attributes];
}

-(void)setImageName:(NSString *)imageName{

    self.imageView.image = kGetImage(imageName);
}

-(void)setTitleStr:(NSString *)titleStr{

    self.titleLabel.text = titleStr;
}

@end

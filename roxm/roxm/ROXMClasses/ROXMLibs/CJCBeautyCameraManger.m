//
//  CJCBeautyCameraManger.m
//  roxm
//
//  Created by lfy on 2017/8/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBeautyCameraManger.h"
#import "CJCCommon.h"
#import "KWBeautyFilter.h"
#import "CJCPhotoCropManger.h"

@interface CJCBeautyCameraManger (){

    BOOL isOpenBeauty;
}

@property (nonatomic ,strong) GPUImageMovieWriter *movieWrite;

@property (nonatomic ,strong) KWBeautyFilter *beautyFilter;

@property (nonatomic ,strong) GPUImageFilter *emptFilter;

//录制音频
@property (nonatomic ,strong) AVAudioRecorder *audioRecorder;

@property (nonatomic ,copy) NSString *videoStr;
@property (nonatomic ,copy) NSString *audioStr;

@end

static CJCBeautyCameraManger *manager;
static dispatch_once_t onceToken;

@implementation CJCBeautyCameraManger

+ (instancetype)manager {
    
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
        
        [manager.beautyCamera addTarget:manager.beautyFilter];
        [manager.beautyFilter addTarget:manager.previewView];
        
        [manager startCameraCapture];
    });
    
    return manager;
}

-(void)startCameraCapture{

    isOpenBeauty = YES;
    
    [manager.beautyCamera startCameraCapture];
}

-(void)stopCameraCapture{

    [manager.beautyCamera stopCameraCapture];
}

-(void)openBeauty{

    isOpenBeauty = YES;
    
    [self.beautyCamera removeAllTargets];
    
    [self.beautyCamera addTarget:self.beautyFilter];
    [self.beautyFilter addTarget:self.previewView];
}
-(void)closeBeauty{

    isOpenBeauty = NO;
    
    [self.beautyCamera removeAllTargets];
    
    [self.beautyCamera addTarget:self.emptFilter];
    [self.emptFilter addTarget:self.previewView];
}

-(void)reTakePhoto{
    
    [self startCameraCapture];
}

-(void)takePhotoActionWithSize:(CGSize)size handle:(CJCCameraPhotoHandle)cameraPhotoHandle{

    GPUImageFilter *PhotoFilter;
    if (isOpenBeauty) {
        
        PhotoFilter = self.beautyFilter;
    }else{
    
        PhotoFilter = self.emptFilter;
    }
    
    [self.beautyCamera capturePhotoAsImageProcessedUpToFilter:PhotoFilter withCompletionHandler:^(UIImage *processedImage, NSError *error) {
        
        [self stopCameraCapture];
        
        if (self.sizeType == CJCCameraSizeTypeFull) {
            
            UIImage *cropImage = [CJCPhotoCropManger getFullImage:processedImage];
            
            if (cameraPhotoHandle) {
                
                cameraPhotoHandle(cropImage);
            }
        }else if(self.sizeType == CJCCameraSizeType34){
        
            if (cameraPhotoHandle) {
                
                cameraPhotoHandle(processedImage);
            }
        }else{
        
            UIImage *cropImage = [CJCPhotoCropManger getEqualWidthHeightImage:processedImage];
            
            if (cameraPhotoHandle) {
                
                cameraPhotoHandle(cropImage);
            }
        }
        
    }];
}

-(void)startRecordVideo{

    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    videoStr = [NSString stringWithFormat:@"%@/ceshiMovie.mp4",videoStr];
    
    //[[NSFileManager defaultManager] removeItemAtPath:videoStr error:nil];
    
    unlink([self.videoStr UTF8String]);
    
    [self setUpMovieWrite];
    
    [self setUpAudioRecorder];
}
-(void)stopRecordVideoHandle:(CJCCameraVideoHandle)cameraVideHandle{

    
    [self.movieWrite finishRecordingWithCompletionHandler:^{
        
        if (isOpenBeauty) {
            
            [self.beautyFilter removeTarget:self.movieWrite];
        }else{
            
            [self.emptFilter removeTarget:self.movieWrite];
            
        }
        
        self.beautyCamera.audioEncodingTarget = nil;
        
        if ([self.audioRecorder isRecording]) {
            [self.audioRecorder stop];
        }
        
        if (cameraVideHandle) {
            
            cameraVideHandle();
        }
        
    }];
}


-(void)setUpMovieWrite{

    NSDictionary *videoSettings = @{AVVideoCodecKey: AVVideoCodecH264,
                                    AVVideoWidthKey: @(960),
                                    AVVideoHeightKey: @(1280),
                                    AVVideoScalingModeKey: AVVideoScalingModeResizeAspectFill};
    
    GPUImageMovieWriter *movieWrite = [[GPUImageMovieWriter alloc] initWithMovieURL:[NSURL fileURLWithPath:self.videoStr] size:CGSizeMake(960, 1280) fileType:AVFileTypeQuickTimeMovie outputSettings:videoSettings];
    
    self.movieWrite = movieWrite;
    
    self.movieWrite.encodingLiveVideo = YES;
    
    if (isOpenBeauty) {
        
        [self.beautyFilter addTarget:self.movieWrite];
    }else{
        
        [self.emptFilter addTarget:self.movieWrite];
        
    }
    
    //添加这一行 会引起拍摄后的视频 开头或结尾有黑幕
    self.beautyCamera.audioEncodingTarget = self.movieWrite;
    
    [self.movieWrite startRecording];
}

-(void)setUpAudioRecorder{
    
    AVAudioSession *session =[AVAudioSession sharedInstance];
    NSError *sessionError;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    
    if (session == nil) {
        
        NSLog(@"Error creating session: %@",[sessionError description]);
        
    }else{
        [session setActive:YES error:nil];
        
    }
    
    //设置参数
    NSDictionary *recordSetting = [[NSDictionary alloc] initWithObjectsAndKeys:
                                   //采样率  8000/11025/22050/44100/96000（影响音频的质量）
                                   [NSNumber numberWithFloat: 11025],AVSampleRateKey,
                                   // 音频格式
                                   [NSNumber numberWithInt: kAudioFormatLinearPCM],AVFormatIDKey,
                                   //采样位数  8、16、24、32 默认为16
                                   [NSNumber numberWithInt:16],AVLinearPCMBitDepthKey,
                                   // 音频通道数 1 或 2
                                   [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                   //录音质量
                                   [NSNumber numberWithInt:AVAudioQualityHigh],AVEncoderAudioQualityKey,
                                   nil];
    
    _audioRecorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:self.audioStr] settings:recordSetting error:nil];
    
    if (_audioRecorder) {
        
        _audioRecorder.meteringEnabled = YES;
        [_audioRecorder prepareToRecord];
        [_audioRecorder record];
        
    }else{
        NSLog(@"音频格式和文件存储格式不匹配,无法初始化Recorder");
        
    }
}

-(NSString *)audioStr{

    if (_audioStr == nil) {
        
        NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
        videoStr = [NSString stringWithFormat:@"%@/ceshiAudio.wav",videoStr];
        
        _audioStr = videoStr;
    }
    return _audioStr;
}

-(NSString *)videoStr{

    if (_videoStr == nil) {
        
        NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
        videoStr = [NSString stringWithFormat:@"%@/ceshiMovie.mp4",videoStr];
        
        _videoStr = videoStr;
    }
    return _videoStr;
}

-(GPUImageStillCamera *)beautyCamera{

    if (_beautyCamera == nil) {
        
        GPUImageStillCamera *photoCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:AVCaptureSessionPresetPhoto cameraPosition:AVCaptureDevicePositionFront];
        
        photoCamera.outputImageOrientation = UIDeviceOrientationPortrait;
        
        //[photoCamera addAudioInputsAndOutputs];
        
        //设置后置摄像头是否显示镜像
        //self.videoCamera.horizontallyMirrorRearFacingCamera = YES;
        
        //设置前置摄像头是否显示镜像
        photoCamera.horizontallyMirrorFrontFacingCamera =YES;
        
        _beautyCamera = photoCamera;
    }
    return _beautyCamera;
}

-(GPUImageView *)previewView{
    
    if (_previewView == nil) {
        
        _previewView = [[GPUImageView alloc] init];
        
        _previewView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    }
    return _previewView;
}

-(KWBeautyFilter *)beautyFilter{

    if (_beautyFilter == nil) {
        
        KWBeautyFilter *kwFilter = [[KWBeautyFilter alloc] init];
        
        [kwFilter setParam:30 withType:KW_NEWBEAUTY_TYPE_SKINWHITENING];
        
        [kwFilter setParam:100 withType:KW_NEWBEAUTY_TYPE_BLEMISHREMOVAL];
        
        [kwFilter setParam:52 withType:KW_NEWBEAUTY_TYPE_SKINSATURATION];
        
        [kwFilter setParam:70 withType:KW_NEWBEAUTY_TYPE_SKINTENDERNESS];
        
        _beautyFilter = kwFilter;
    }
    return _beautyFilter;
}

-(GPUImageFilter *)emptFilter{

    if (_emptFilter == nil) {
        
        _emptFilter = [[GPUImageFilter alloc] init];
    }
    return _emptFilter;
}

@end

//
//  CJCVideoOprationCommon.m
//  roxm
//
//  Created by lfy on 2017/8/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCVideoOprationCommon.h"

NSString* const AVSEEditCommandCompletionNotification = @"AVSEEditCommandCompletionNotification";
NSString* const AVSEExportCommandCompletionNotification = @"AVSEExportCommandCompletionNotification";

@implementation CJCVideoOprationCommon

- (id)initWithComposition:(AVMutableComposition *)composition videoComposition:(AVMutableVideoComposition *)videoComposition audioMix:(AVMutableAudioMix *)audioMix
{
    self = [super init];
    if(self != nil) {
        self.mutableComposition = composition;
        self.mutableVideoComposition = videoComposition;
        self.mutableAudioMix = audioMix;
    }
    return self;
}

- (void)performWithAsset:(AVAsset*)asset
{
    [self doesNotRecognizeSelector:_cmd];
}

@end

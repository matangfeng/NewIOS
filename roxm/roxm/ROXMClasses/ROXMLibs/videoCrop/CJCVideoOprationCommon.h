//
//  CJCVideoOprationCommon.h
//  roxm
//
//  Created by lfy on 2017/8/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

extern NSString* const AVSEEditCommandCompletionNotification;
extern NSString* const AVSEExportCommandCompletionNotification;

@interface CJCVideoOprationCommon : NSObject

@property AVMutableComposition *mutableComposition;
@property AVMutableVideoComposition *mutableVideoComposition;
@property AVMutableAudioMix *mutableAudioMix;
@property CALayer *watermarkLayer;

- (id)initWithComposition:(AVMutableComposition*)composition videoComposition:(AVMutableVideoComposition*)videoComposition audioMix:(AVMutableAudioMix*)audioMix;
- (void)performWithAsset:(AVAsset*)asset;

@end

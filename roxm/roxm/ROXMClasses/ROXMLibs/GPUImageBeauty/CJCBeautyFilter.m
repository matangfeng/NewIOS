//
//  CJCBeautyFilter.m
//  roxm
//
//  Created by lfy on 2017/8/11.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBeautyFilter.h"
#import "LFGPUImageBeautyFilter.h"
#import "CJCCommon.h"

@implementation CJCBeautyFilter

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.lfBeautyFilter = [[LFGPUImageBeautyFilter alloc] init];
        self.lfBeautyFilter.beautyLevel = 0.5;
        [self addGPUImageFilter:self.lfBeautyFilter];
        
        self.brightFilter = [[GPUImageBrightnessFilter alloc] init];
        self.brightFilter.brightness = 0.18;
        [self addGPUImageFilter:self.brightFilter];
        
        //self.exposureFilter = [[GPUImageExposureFilter alloc] init];
        //[self addGPUImageFilter:self.exposureFilter];
        
        //self.contrastFilter = [[GPUImageContrastFilter alloc] init];
        //[self addGPUImageFilter:self.contrastFilter];
        
        //self.saturationFilter = [[GPUImageSaturationFilter alloc] init];
        //[self addGPUImageFilter:self.saturationFilter];
        
        self.whiteBalanceFilter = [[GPUImageWhiteBalanceFilter alloc] init];
        self.whiteBalanceFilter.temperature = 4900;
        self.whiteBalanceFilter.tint = 8.0;
        [self addGPUImageFilter:self.whiteBalanceFilter];
        
        self.sharpenFilter = [[GPUImageSharpenFilter alloc] init];
        [self addGPUImageFilter:self.sharpenFilter];
    }
    return self;
}

- (void)addGPUImageFilter:(GPUImageOutput<GPUImageInput> *)filter
{
    [self addFilter:filter];
    
    GPUImageOutput<GPUImageInput> *newTerminalFilter = filter;
    
    NSInteger count = self.filterCount;
    
    if (count == 1)
    {
        self.initialFilters = @[newTerminalFilter];
        self.terminalFilter = newTerminalFilter;
        
    } else
    {
        GPUImageOutput<GPUImageInput> *terminalFilter    = self.terminalFilter;
        NSArray *initialFilters                          = self.initialFilters;
        
        [terminalFilter addTarget:newTerminalFilter];
        
        self.initialFilters = @[initialFilters[0]];
        self.terminalFilter = newTerminalFilter;
    }
}

@end

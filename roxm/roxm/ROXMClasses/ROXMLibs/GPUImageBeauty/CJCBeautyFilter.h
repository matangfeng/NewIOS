//
//  CJCBeautyFilter.h
//  roxm
//
//  Created by lfy on 2017/8/11.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <GPUImage/GPUImage.h>

@class LFGPUImageBeautyFilter;

@interface CJCBeautyFilter : GPUImageFilterGroup{

    GPUImageCannyEdgeDetectionFilter *cannyEdgeFilter;
}
///双边模糊
@property (nonatomic ,strong) GPUImageBilateralFilter *bilateralFilter;

///双边模糊
@property (nonatomic ,strong) LFGPUImageBeautyFilter *lfBeautyFilter;
///亮度调节
@property (nonatomic ,strong) GPUImageBrightnessFilter *brightFilter;
///曝光度调节
@property (nonatomic ,strong) GPUImageExposureFilter *exposureFilter;
///对比度调节
@property (nonatomic ,strong) GPUImageContrastFilter *contrastFilter;
///饱和度调节
@property (nonatomic ,strong) GPUImageSaturationFilter *saturationFilter;
///色阶调节
@property (nonatomic ,strong) GPUImageLevelsFilter *levelsFilter;
///白平衡
@property (nonatomic ,strong) GPUImageWhiteBalanceFilter *whiteBalanceFilter;
///锐化
@property (nonatomic ,strong) GPUImageSharpenFilter *sharpenFilter;

@property (nonatomic ,strong) GPUImageCropFilter *cropFilter;

@end

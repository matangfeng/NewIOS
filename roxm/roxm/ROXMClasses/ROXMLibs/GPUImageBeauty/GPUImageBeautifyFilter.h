//
//  GPUImageBeautifyFilter.h
//  BeautifyFaceDemo
//
//  Created by guikz on 16/4/28.
//  Copyright © 2016年 guikz. All rights reserved.
//

#import <GPUImage/GPUImage.h>

@class GPUImageCombinationFilter;

@interface GPUImageBeautifyFilter : GPUImageFilterGroup {
    
    GPUImageCannyEdgeDetectionFilter *cannyEdgeFilter;
    GPUImageCombinationFilter *combinationFilter;
    GPUImageHSBFilter *hsbFilter;
    
}

@property (nonatomic ,strong) GPUImageBilateralFilter *bilateralFilter;;

@property (nonatomic ,strong) GPUImageWhiteBalanceFilter *whiteBalanceFilter;

@property (nonatomic ,strong) GPUImageLevelsFilter *levelsFilter;

-(instancetype)initWithBright:(CGFloat)bright Saturation:(CGFloat)saturation andIntensity:(CGFloat)intensity;

@end

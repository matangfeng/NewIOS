//
//  CJCVideoHandleManger.m
//  roxm
//
//  Created by lfy on 2017/8/28.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCVideoHandleManger.h"
#import "CJCCommon.h"
#import <AssetsLibrary/AssetsLibrary.h>


@interface CJCVideoHandleManger (){

    BOOL isFinishOpration;
}

@property AVMutableComposition *mutableComposition;

@property AVMutableVideoComposition *mutableVideoComposition;

@property AVMutableAudioMix *mutableAudioMix;

@end

@implementation CJCVideoHandleManger

-(void)mergeVideostoOnevideoWithFullRatio{

    [self mergeVideostoOnevideo];
    
    CGFloat margin = SCREEN_WITDH/SCREEN_HEIGHT*1280;
    
    [self videoCropToExpectedSize:CGSizeMake(margin, 1280)];
    
    [self exportVideo];
}

-(void)mergeVideostoOnevideoWith34Ratio{

    [self mergeVideostoOnevideo];
    
    [self exportVideo];
}

-(void)mergeVideostoOnevideoWith11Ratio{

    [self mergeVideostoOnevideo];
    
    [self videoCropToExpectedSize:CGSizeMake(960, 960)];
    
    [self exportVideo];
}

-(void)mergeVideostoOnevideo{
    
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    AVMutableCompositionTrack *video_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVMutableCompositionTrack *audio_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    Float64 tmpDuration =0.0f;
    
    for (NSInteger i=0; i<self.videoPaths.count; i++)
    {
        
        NSURL *url = [NSURL fileURLWithPath:self.videoPaths[i]];
        
        AVURLAsset *videoAsset = [[AVURLAsset alloc]initWithURL:url options:nil];
        
        CMTimeRange video_timeRange = CMTimeRangeMake(kCMTimeZero,videoAsset.duration);
        
        NSURL *audioURL = [NSURL fileURLWithPath:self.audioPaths[i]];
        
        AVURLAsset *audioAsset = [[AVURLAsset alloc] initWithURL:audioURL options:nil];
        
        /**
         *  依次加入每个asset
         *
         *  @param TimeRange 加入的asset持续时间
         *  @param Track     加入的asset类型,这里都是video
         *  @param Time      从哪个时间点加入asset,这里用了CMTime下面的CMTimeMakeWithSeconds(tmpDuration, 0),timesacle为0
         *
         */
        NSError *error1,*error2;
        BOOL tbool = [video_compositionVideoTrack insertTimeRange:video_timeRange ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:CMTimeMakeWithSeconds(tmpDuration, 0) error:&error1];
        
        if (!tbool) {
            
            NSLog(@"拼接视频错误：%@",error1);
        }
        
        BOOL bbool = [audio_compositionVideoTrack insertTimeRange:video_timeRange ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:CMTimeMakeWithSeconds(tmpDuration, 0) error:&error1];
        
        if (!bbool) {
            
            NSLog(@"拼接音频错误：%@",error2);

        }
        
        tmpDuration += CMTimeGetSeconds(videoAsset.duration);
        
        NSFileManager *manager = [NSFileManager defaultManager];
        
        [manager removeItemAtPath:self.videoPaths[i] error:nil];
        [manager removeItemAtPath:self.audioPaths[i] error:nil];
    }
    
    self.mutableComposition = mixComposition;
}

-(void)videoCropToExpectedSize:(CGSize)asize{

    AVMutableVideoCompositionInstruction *instruction = nil;
    AVMutableVideoCompositionLayerInstruction *layerInstruction = nil;
    CGAffineTransform t1;
    
    if (!self.mutableVideoComposition){
    
        // Create a new video composition
        self.mutableVideoComposition = [AVMutableVideoComposition videoComposition];
        
        // 需要剪裁的区域 在原视频区域上的size
        self.mutableVideoComposition.renderSize = asize;
        self.mutableVideoComposition.frameDuration = CMTimeMake(1, 30);
        
        // The crop transform is set on a layer instruction
        instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [self.mutableComposition duration]);
        layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:(self.mutableComposition.tracks)[0]];
        
        // Crop transformation (translation to move the bottom right half into the view)
        
        if (self.sizeType == CJCCameraSizeTypeFull) {
            
            CGFloat x = self.mutableComposition.naturalSize.width - asize.width;
            //
            t1 = CGAffineTransformMakeTranslation(-x/2, 0);
            [layerInstruction setTransform:t1 atTime:kCMTimeZero];
        }else if (self.sizeType == CJCCameraSizeType11){
        
            CGFloat y = self.mutableComposition.naturalSize.height - self.mutableComposition.naturalSize.width;
            //
            t1 = CGAffineTransformMakeTranslation(0, -y/2);
            [layerInstruction setTransform:t1 atTime:kCMTimeZero];
        }
        
        // Step 3
        // Add the instructions to the video composition
        instruction.layerInstructions = @[layerInstruction];
        self.mutableVideoComposition.instructions = @[instruction];
    
    }
}

-(void)exportVideo{

    NSFileManager *manager = [NSFileManager defaultManager];
    
    [manager removeItemAtPath:self.exportPath error:nil];
    
    //修改存到本地的视频
    self.exportSession = [[AVAssetExportSession alloc] initWithAsset:[self.mutableComposition copy] presetName:AVAssetExportPresetMediumQuality];
    
    self.exportSession.videoComposition = self.mutableVideoComposition;
    self.exportSession.audioMix = self.mutableAudioMix;
    self.exportSession.outputURL = [NSURL fileURLWithPath:self.exportPath];
    self.exportSession.outputFileType=AVFileTypeQuickTimeMovie;
    
    [self.exportSession exportAsynchronouslyWithCompletionHandler:^(void){
        switch (self.exportSession.status) {
            case AVAssetExportSessionStatusCompleted:
                //[self writeVideoToPhotoLibrary:[NSURL fileURLWithPath:outputURL]];
                
                if ([self.delegate respondsToSelector:@selector(videoOprationDidFinish)]) {
                    
                    [self.delegate videoOprationDidFinish];
                }
                
                break;
            case AVAssetExportSessionStatusFailed:
                NSLog(@"Failed:%@",self.exportSession.error);
                
                [self isFinishExport:NO];
                
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Canceled:%@",self.exportSession.error);
                
                [self isFinishExport:NO];
                
                break;
            default:
                break;
        }
    }];

}

-(void)compressVideo:(NSURL *)path andVideoName:(NSString *)name andSave:(BOOL)saveState
     successCompress:(void(^)(NSData *))successCompress  //saveState 是否保存视频到相册
{
    
    AVURLAsset *avAsset = [[AVURLAsset alloc] initWithURL:path options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetLowQuality]) {
        
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:avAsset presetName:AVAssetExportPreset640x480];
        exportSession.outputURL = [NSURL fileURLWithPath:self.exportPath];//设置压缩后视频流导出的路径
        exportSession.shouldOptimizeForNetworkUse = true;
        //转换后的格式
        exportSession.outputFileType = AVFileTypeMPEG4;
        //异步导出
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            // 如果导出的状态为完成
            if ([exportSession status] == AVAssetExportSessionStatusCompleted) {
                //NSLog(@"视频压缩成功,压缩后大小 %f MB",[self fileSize:[self compressedURL]]);
                
            }else{
                //压缩失败的回调
                successCompress(nil);
            }
        }];
    }
}

-(BOOL)isFinishExport:(BOOL)finish{

    return finish;
}

- (void)writeVideoToPhotoLibrary:(NSURL *)url
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library writeVideoAtPathToSavedPhotosAlbum:url completionBlock:^(NSURL *assetURL, NSError *error){
        if (error) {
            NSLog(@"Video could not be saved");
        }
    }];
}

@end

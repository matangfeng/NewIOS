//
//  CJCVideoHandleManger.h
//  roxm
//
//  Created by lfy on 2017/8/28.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "CJCBeautyCameraManger.h"

@class  AVAssetExportSession;

typedef void(^CJCVideoOprationHandle)(BOOL Success);

@protocol CJCVideoHandleMangerDelegate <NSObject>

-(void)videoOprationDidFinish;

@end

@interface CJCVideoHandleManger : NSObject

@property (nonatomic ,copy) NSArray *videoPaths;
@property (nonatomic ,copy) NSArray *audioPaths;

@property (nonatomic ,copy) NSString *exportPath;

@property (nonatomic ,assign) CJCCameraSizeType sizeType;

@property (nonatomic ,weak) id<CJCVideoHandleMangerDelegate> delegate;

@property AVAssetExportSession *exportSession;

//获得全屏拍摄时的视频
-(void)mergeVideostoOnevideoWithFullRatio;

//获得3：4比例是的视频
-(void)mergeVideostoOnevideoWith34Ratio;

//获得1：1比例时的视频
-(void)mergeVideostoOnevideoWith11Ratio;

@end

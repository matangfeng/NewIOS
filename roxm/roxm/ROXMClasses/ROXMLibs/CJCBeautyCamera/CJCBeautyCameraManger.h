//
//  CJCBeautyCameraManger.h
//  roxm
//
//  Created by lfy on 2017/8/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPUImageBeautifyFilter.h"

typedef NS_ENUM(NSInteger,CJCCameraSizeType){
    
    CJCCameraSizeTypeFull               = 0,
    CJCCameraSizeType11                 = 1,
    CJCCameraSizeType34                 = 2,
};

@class GPUImageView;

typedef void(^CJCCameraPhotoHandle)(UIImage * returnImage);

typedef void(^CJCCameraVideoHandle)();

@interface CJCBeautyCameraManger : NSObject

@property (nonatomic ,strong) GPUImageStillCamera *beautyCamera;

@property (nonatomic ,strong) GPUImageView *previewView;

@property (nonatomic ,assign) CJCCameraSizeType sizeType;

+ (instancetype)manager;

-(void)startCameraCapture;
-(void)stopCameraCapture;

-(void)openBeauty;
-(void)closeBeauty;

-(void)reTakePhoto;

-(void)startRecordVideo;
-(void)stopRecordVideoHandle:(CJCCameraVideoHandle)cameraVideHandle;

-(void)takePhotoActionWithSize:(CGSize)size handle:(CJCCameraPhotoHandle)cameraPhotoHandle;

@end

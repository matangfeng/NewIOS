//
//  CJCTurnOutSuccessVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCTurnOutSuccessVC.h"
#import "CJCCommon.h"
#import "CJCWalletVC.h"

@interface CJCTurnOutSuccessVC ()

@end

@implementation CJCTurnOutSuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    
    [self setUpUI];
}

-(void)setUpUI{

    UIImageView *topImageView = [[UIImageView alloc] init];
    
    topImageView.image = kGetImage(@"profile_wollet_success");
    
    topImageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    topImageView.frame = CGRectMake(166, kAdaptedValue(120), kAdaptedValue(43), kAdaptedValue(43));
    topImageView.centerX = self.view.centerX;
    
    [self.view addSubview:topImageView];
    
    UILabel *hintLabel = [UIView getSystemLabelWithStr:@"提现申请已提交" fontName:kFONTNAMEMEDIUM size:19 color:[UIColor toUIColorByStr:@"222222"]];
    
    hintLabel.textAlignment = NSTextAlignmentCenter;
    
    hintLabel.frame = CGRectMake(0, topImageView.bottom+kAdaptedValue(20), SCREEN_WITDH, kAdaptedValue(20));
    
    [self.view addSubview:hintLabel];
    
    UILabel *amontLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"222222"]];
    
    amontLabel.textAlignment = NSTextAlignmentCenter;
    
    amontLabel.frame = CGRectMake(0, hintLabel.bottom+kAdaptedValue(25), SCREEN_WITDH, kAdaptedValue(16));
    
    amontLabel.text = [NSString stringWithFormat:@"提现金额 ¥%@",self.disCountStr];
    
    [self.view addSubview:amontLabel];
    
    UILabel *timeLabel = [UIView getSystemLabelWithStr:@"预计1-2个工作日内到账" fontName:kFONTNAMEREGULAR size:12 color:[UIColor toUIColorByStr:@"AFAFAF"]];
    
    timeLabel.textAlignment = NSTextAlignmentCenter;
    
    timeLabel.frame = CGRectMake(0, amontLabel.bottom+kAdaptedValue(8), SCREEN_WITDH, kAdaptedValue(13));
    
    [self.view addSubview:timeLabel];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B" andAlpha:0.5]] forState:UIControlStateSelected];
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B"]] forState:UIControlStateNormal];

    
    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];
    [buyButton setTitle:@"完成" forState:UIControlStateNormal];
    
    buyButton.frame = CGRectMake(kAdaptedValue(23), timeLabel.bottom+kAdaptedValue(87), SCREEN_WITDH-kAdaptedValue(46), kAdaptedValue(49));
    
    //    buyButton.canOpration = NO;
    
    buyButton.layer.cornerRadius = kAdaptedValue(3);
    buyButton.layer.masksToBounds = YES;
    
    [buyButton addTarget:self action:@selector(buyButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:buyButton];
}

-(void)buyButtonClickHandle{
    
    [self naviViewBackBtnDidClick];
}

-(void)naviViewBackBtnDidClick{

    CJCWalletVC *preVC = self.navigationController.childViewControllers[1];
    [preVC banlanceChanged];
    
    [self.navigationController popToViewController:self.navigationController.childViewControllers[1] animated:YES];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"余额转出" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"余额转出" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

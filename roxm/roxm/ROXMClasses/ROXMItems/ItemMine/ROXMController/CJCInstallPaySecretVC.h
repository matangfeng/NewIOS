//
//  CJCInstallPaySecretVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef NS_ENUM(NSInteger,CJCInstallPaySecretVCType){
    
    CJCInstallPaySecretVCTypeDefault              = 0,//第一次输入密码
    CJCInstallPaySecretVCTypeConfirm              = 1,//确认密码
};

typedef NS_ENUM(NSInteger,CJCpushVCType){
    
    CJCpushVCTypeDefault              = 0,//第二次输入密码之后跳转到钱包界面
    CJCpushVCTypeConfirm              = 1,//第二次输入密码之后向后跳转
};

@interface CJCInstallPaySecretVC : CommonVC

@property (nonatomic ,assign) CJCInstallPaySecretVCType installType;

@property (nonatomic ,assign) CJCpushVCType pushType;

@property (nonatomic ,copy) NSString *secret;

@property (nonatomic ,copy) NSString *phoneCode;

@end

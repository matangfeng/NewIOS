//
//  CJCBalanceTurnOutVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@class CJCBanlanceModel;

@interface CJCBalanceTurnOutVC : CommonVC

@property (nonatomic ,assign) CGFloat amont;

@property (nonatomic ,strong) CJCBanlanceModel *banlanceModel;

@end

//
//  CJCWalletVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCWalletVC.h"
#import "CJCCommon.h"
#import "CJCIncomeAndExpensesDetailVC.h"
#import "CJCBalanceTurnOutVC.h"
#import "CJCBanlanceModel.h"
#import <ReactiveObjC.h>

@interface CJCWalletVC (){

    CGFloat amont;
    
    CJCBanlanceModel *banlanceModel;
}

@property (nonatomic ,strong) UILabel *balanceLabel;

@property (nonatomic ,strong) UIButton *turnOutButton;

@end

@implementation CJCWalletVC

-(void)banlanceChanged{

    [self getWalletBanlance];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    
    [self setUpUI];
    
    [self getWalletBanlance];
}

-(void)getWalletBanlance{

    [self showWithLabelAnimation];
    
    NSString *banlanceURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/balance/get"];
    
    [CJCHttpTool postWithUrl:banlanceURL params:nil success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            banlanceModel = [CJCBanlanceModel modelWithDictionary:responseObject[@"data"][@"balance"]];
            
            amont = banlanceModel.amount;
            
            if (amont>0) {
                
                self.turnOutButton.canOpration = YES;
            }else{
            
                self.turnOutButton.canOpration = YES;
            }
            
            self.balanceLabel.text = [NSString stringWithFormat:@"¥%.2f",amont];
            
        }else{
        
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)setUpUI{

    UIImageView *imageView = [[UIImageView alloc] init];
    
    imageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    imageView.image = kGetImage(@"profile_wollet_my");
    
    imageView.frame = CGRectMake(100, kAdaptedValue(144), kAdaptedValue(43), kAdaptedValue(43));
    imageView.centerX = self.view.centerX;
    
    [self.view addSubview:imageView];
    
    UILabel *balanceHintLabel = [UIView getSystemLabelWithStr:@"我的余额" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"7E8489"]];
    
    balanceHintLabel.textAlignment = NSTextAlignmentCenter;
    
    balanceHintLabel.frame = CGRectMake(0, imageView.bottom+kAdaptedValue(47), SCREEN_WITDH, kAdaptedValue(21));
    
    [self.view addSubview:balanceHintLabel];
    
    UILabel *balanceLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:28 color:[UIColor toUIColorByStr:@"222222"]];
    
    balanceLabel.textAlignment = NSTextAlignmentCenter;
    
    balanceLabel.frame = CGRectMake(0, balanceHintLabel.bottom+kAdaptedValue(4), SCREEN_WITDH, kAdaptedValue(33));
    
    balanceLabel.text = @"¥0";
    
    self.balanceLabel = balanceLabel;
    [self.view addSubview:balanceLabel];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B"]] forState:0];
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B" andAlpha:0.5]] forState:UIControlStateSelected];

    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];

    [buyButton setTitle:@"转出" forState:UIControlStateNormal];
    
    buyButton.frame = CGRectMake(kAdaptedValue(43), balanceLabel.bottom+kAdaptedValue(47), SCREEN_WITDH-kAdaptedValue(86), 49);
    
    //根据用户的余额判断 是否打开用户交互
//    buyButton.userInteractionEnabled = NO;
    
    buyButton.layer.cornerRadius = kAdaptedValue(3);
    buyButton.layer.masksToBounds = YES;
    
    [[buyButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        CJCBalanceTurnOutVC *nextVC = [[CJCBalanceTurnOutVC alloc] init];
        nextVC.banlanceModel = banlanceModel;
        [self.navigationController pushViewController:nextVC animated:YES];
    }];
    self.turnOutButton = buyButton;
    [self.view addSubview:buyButton];
    
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"我的钱包" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"我的钱包" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
    
    UIButton *shopButton = [UIView getButtonWithStr:@"明细" fontName:kFONTNAMEMEDIUM size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    CGFloat buttonWidth = [CJCTools getShortStringLength:@"明细" withFont:[UIFont accodingVersionGetFont_mediumWithSize:15]];
    
    shopButton.size = CGSizeMake(buttonWidth,kAdaptedValue(23));
    shopButton.centerY = kNAVIVIEWCENTERY;
    shopButton.right = SCREEN_WITDH - kAdaptedValue(18);
    
    [[shopButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        CJCIncomeAndExpensesDetailVC *nextVC = [[CJCIncomeAndExpensesDetailVC alloc] init];
        [self.navigationController pushViewController:nextVC animated:YES];
    }];
    
    [self.naviView addSubview:shopButton];
}

-(void)cancleButtonClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

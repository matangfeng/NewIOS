//
//  CJCIncomeAndExpensesDetailVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCIncomeAndExpensesDetailVC.h"
#import "CJCCommon.h"
#import "CJCIncomeDetailCell.h"
#import <NSObject+YYModel.h>
#import "CJCInviteOrderDetailVC.h"
#import "CJCIncomeDetailModel.h"

#define kInviteRecordCell       @"CJCIncomeDetailCell"

@interface CJCIncomeAndExpensesDetailVC ()<UITableViewDelegate,UITableViewDataSource>{
    
    NSMutableArray *dataArray;
    
    NSInteger pageSize;
    BOOL isRefresh;
    
    BOOL isDataReady;
}

@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation CJCIncomeAndExpensesDetailVC

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (!isDataReady) {
        
        [self getInviteRecord];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataArray = [NSMutableArray array];
    pageSize = 1;
    isRefresh = NO;
    isDataReady = NO;
    
    [self setUpNaviView];
    
    [self configTableView];
}

-(void)getInviteRecord{
    
    [self showWithLabelAnimation];
    
    NSString *recordURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/bill/list"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"page"] = [NSString stringWithFormat:@"%ld",(long)pageSize];
    params[@"pageSize"] = [NSString stringWithFormat:@"25"];
    
    [CJCHttpTool postWithUrl:recordURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        isDataReady = YES;
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCIncomeDetailModel class] json:responseObject[@"data"]];
            
            if (isRefresh) {
                
                [dataArray removeAllObjects];
            }
            
            [dataArray addObjectsFromArray:tempArr];
            
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
            [self.tableView reloadData];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)configTableView{
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCIncomeDetailCell class] forCellReuseIdentifier:kInviteRecordCell];
    
    __weak __typeof(self) weakSelf = self;
    
    [weakSelf loadNewData];

//    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//
//        [weakSelf loadNewData];
//    }];
//
//    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//
//        [weakSelf loadMoreData];
//    }];
    
    self.tableView.mj_footer.automaticallyHidden = YES;
}

-(void)loadNewData{
    
    pageSize = 1;
    isRefresh = YES;
    
    [self getInviteRecord];
}

-(void)loadMoreData{
    
    pageSize++;
    
    isRefresh = NO;
    
    [self getInviteRecord];
}

#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kAdaptedValue(92);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCIncomeDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteRecordCell];
    
    CJCIncomeDetailModel *model = dataArray[indexPath.row];
    
    cell.detailModel = model;
    
    return cell;
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)setUpNaviView{

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"收支明细" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"收支明细" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

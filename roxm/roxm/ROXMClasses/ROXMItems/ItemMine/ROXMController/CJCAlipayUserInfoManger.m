//
//  CJCAlipayUserInfoManger.m
//  roxm
//
//  Created by 陈建才 on 2017/10/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCAlipayUserInfoManger.h"
#import <AlipaySDK/AlipaySDK.h>
#import "APAuthV2Info.h"
#import "RSADataSigner.h"
#import "CJCCommon.h"

@interface CJCAlipayUserInfoManger (){
    
    NSString *saveAuthCode;
    
    NSString *saveAccess_token;
}

@end

@implementation CJCAlipayUserInfoManger

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self doAlipayAuth];
    }
    return self;
}

-(void)bindingAlipayWith:(NSString *)alipayUid andName:(NSString *)alipayName{

    NSString *bindingURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/bind/alipay"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"alipayUid"] = alipayUid;
    params[@"alipayName"] = alipayName;
    
    [CJCHttpTool postWithUrl:bindingURL params:params success:^(id responseObject) {
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            if (self.infoHandle) {
                self.infoHandle(alipayUid, alipayName);
            }
        }else{
            if (self.failHandle) {
                self.failHandle(@"授权失败");
            }
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        if (self.failHandle) {
            self.failHandle(@"授权失败");
        }
        [MBManager showBriefAlert:@"授权失败"];
    }];
}

-(void)getAlipayInfo{
    
    NSString *url = @"https://openapi.alipay.com/gateway.do";
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"app_id"] = kALIPAYAPPID;
    params[@"method"] = @"alipay.user.info.share";
    params[@"version"] = @"1.0";
    
    params[@"sign_type"] = @"RSA2";
    params[@"charset"] = @"GBK";
    params[@"auth_token"] = saveAccess_token;
    
    NSString *stamp =  [CJCTools getDateStrTOMilliSeconds];
    
    params[@"timestamp"] = stamp;
    
    RSADataSigner* signer = [[RSADataSigner alloc] initWithPrivateKey:kALIPAYSIYAO];
    
    NSMutableDictionary *tmpDict = [NSMutableDictionary new];
    [tmpDict addEntriesFromDictionary:@{@"app_id":kALIPAYAPPID,
                                        @"method":@"alipay.user.info.share",
                                        @"version":@"1.0",
                                        @"timestamp":stamp,
                                        @"charset":@"GBK",
                                        @"sign_type":@"RSA2",
                                        @"auth_token":saveAccess_token}];
    
    
    // NOTE: 排序，得出最终请求字串
    NSArray* sortedKeyArray = [[tmpDict allKeys] sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    NSMutableArray *tmpArray = [NSMutableArray new];
    for (NSString* key in sortedKeyArray) {
        NSString* orderItem = [self itemWithKey:key andValue:[tmpDict objectForKey:key]];
        if (orderItem.length > 0) {
            [tmpArray addObject:orderItem];
        }
    }
    NSString *authInfoStr = [tmpArray componentsJoinedByString:@"&"];
    NSString *sign = [signer signString:authInfoStr withRSA2:YES];
    params[@"sign"] = [self decodeString:sign];
    [CJCHttpTool postWithNoAddParamsUrl:url params:params success:^(id responseObject) {
        NSDictionary *tempDict = responseObject[@"alipay_user_info_share_response"];
        if (![CJCTools isBlankString:tempDict[@"user_id"]]) {
            [self bindingAlipayWith:tempDict[@"user_id"] andName:tempDict[@"nick_name"]];
        }else{
            if (self.failHandle) {
                
                self.failHandle(@"获取access_token失败");
            }
            [MBManager showBriefAlert:@"获取access_token失败"];
        }
    } failure:^(NSError *error) {
        
        if (self.failHandle) {
            
            self.failHandle(@"获取access_token失败");
        }
        
    }];
}

-(void)getAlipayAccess{
    
    NSString *url = @"https://openapi.alipay.com/gateway.do";
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"grant_type"] = @"authorization_code";
    params[@"code"] = saveAuthCode;
    params[@"app_id"] = kALIPAYAPPID;
    params[@"method"] = @"alipay.system.oauth.token";
    params[@"version"] = @"1.0";
    
    params[@"sign_type"] = @"RSA2";
    params[@"charset"] = @"GBK";
    
    NSString *stamp =  [CJCTools getDateStrTOMilliSeconds];
    
    params[@"timestamp"] = stamp;
    
    RSADataSigner* signer = [[RSADataSigner alloc] initWithPrivateKey:kALIPAYSIYAO];
    
    NSMutableDictionary *tmpDict = [NSMutableDictionary new];
    [tmpDict addEntriesFromDictionary:@{@"app_id":kALIPAYAPPID,
                                        @"code":saveAuthCode,
                                        @"grant_type":@"authorization_code",
                                        @"method":@"alipay.system.oauth.token",
                                        @"version":@"1.0",
                                        @"timestamp":stamp,
                                        @"charset":@"GBK",
                                        @"sign_type":@"RSA2"}];
    
    
    // NOTE: 排序，得出最终请求字串
    NSArray* sortedKeyArray = [[tmpDict allKeys] sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    NSMutableArray *tmpArray = [NSMutableArray new];
    for (NSString* key in sortedKeyArray) {
        NSString* orderItem = [self itemWithKey:key andValue:[tmpDict objectForKey:key]];
        if (orderItem.length > 0) {
            [tmpArray addObject:orderItem];
        }
    }
    NSString *authInfoStr = [tmpArray componentsJoinedByString:@"&"];
    
    NSString *sign = [signer signString:authInfoStr withRSA2:YES];
    
    params[@"sign"] = [self decodeString:sign];
    
    [CJCHttpTool postWithNoAddParamsUrl:url params:params success:^(id responseObject) {
        
        /*"alipay_system_oauth_token_response" =     {
         "access_token" = kuaijieBacd3d89b46444f6da5388f9f3b887X32;
         "alipay_user_id" = 20881043913562554582412550015332;
         "expires_in" = 1209600;
         "re_expires_in" = 15552000;
         "refresh_token" = kuaijieB58c2e9688c59465194cf27ea747e9X32;
         "user_id" = 2088422199406324;
         };*/
        
        NSDictionary *tempDict = responseObject[@"alipay_system_oauth_token_response"];
        
        if (![CJCTools isBlankString:tempDict[@"access_token"]]) {
            
            saveAccess_token = tempDict[@"access_token"];
            
            [self getAlipayInfo];
        }else{
            
            if (self.failHandle) {
                
                self.failHandle(@"获取access_token失败");
            }
            [MBManager showBriefAlert:@"获取access_token失败"];
        }
    } failure:^(NSError *error) {
        
        if (self.failHandle) {
            self.failHandle(@"获取access_token失败");
        }
        
    }];
}

-(NSString*)decodeString:(NSString*)encodedString

{
    
    //NSString *decodedString = [encodedString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    
    NSString*decodedString=(__bridge_transfer NSString*)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                                                
                                                                                                                (__bridge CFStringRef)encodedString,
                                                                                                                
                                                                                                                CFSTR(""),
                                                                                                                
                                                                                                                CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
    return decodedString;
    
}

- (NSString*)itemWithKey:(NSString*)key andValue:(NSString*)value
{
    if (key.length > 0 && value.length > 0) {
        return [NSString stringWithFormat:@"%@=%@", key, value];
    }
    return nil;
}

- (void)doAlipayAuth
{
    
    NSString *pid = kALIPAYPID;
    NSString *appID = kALIPAYAPPID;
    
    NSString *rsa2PrivateKey = kALIPAYSIYAO;
    NSString *rsaPrivateKey = @"";
    
    //pid和appID获取失败,提示
    if ([pid length] == 0 ||
        [appID length] == 0 ||
        ([rsa2PrivateKey length] == 0 && [rsaPrivateKey length] == 0))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"缺少pid或者appID或者私钥。"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    //生成 auth info 对象
    APAuthV2Info *authInfo = [APAuthV2Info new];
    authInfo.pid = pid;
    authInfo.appID = appID;
    
    //auth type
    authInfo.authType = @"AUTHACCOUNT";
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = kALIPAYSCHEME;
    
    // 将授权信息拼接成字符串
    NSString *authInfoStr = [authInfo description];
    NSLog(@"authInfoStr = %@",authInfoStr);
    
    //    APayAuthInfo *info = [[APayAuthInfo alloc] initWithAppID:appID pid:pid redirectUri:[NSString stringWithFormat:@"%@//auth",appScheme]];
    
    // 获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    NSString *signedString = nil;
    RSADataSigner* signer = [[RSADataSigner alloc] initWithPrivateKey:((rsa2PrivateKey.length > 1)?rsa2PrivateKey:rsaPrivateKey)];
    if ((rsa2PrivateKey.length > 1)) {
        signedString = [signer signString:authInfoStr withRSA2:YES];
    } else {
        signedString = [signer signString:authInfoStr withRSA2:NO];
    }
    
    // 将签名成功字符串格式化为订单字符串,请严格按照该格式
    if (signedString.length > 0) {
        authInfoStr = [NSString stringWithFormat:@"%@&sign=%@&sign_type=%@", authInfoStr, signedString, ((rsa2PrivateKey.length > 1)?@"RSA2":@"RSA")];
        
        [[AlipaySDK defaultService] auth_V2WithInfo:authInfoStr
                                         fromScheme:appScheme
                                           callback:^(NSDictionary *resultDic) {
                                               NSLog(@"result = %@",resultDic);
                                               // 解析 auth code
                                               NSString *result = resultDic[@"result"];
                                               NSString *authCode = nil;
                                               if (result.length>0) {
                                                   NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                                                   for (NSString *subResult in resultArr) {
                                                       if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                                                           authCode = [subResult substringFromIndex:10];
                                                           break;
                                                       }
                                                   }
                                               }
                                               NSLog(@"授权结果 authCode = %@", authCode?:@"");
                                               
                                               if (![CJCTools isBlankString:authCode]) {
                                                   
                                                   saveAuthCode = authCode;
                                                   
                                                   [self getAlipayAccess];
                                               }else{
                                                   
                                                   if (self.failHandle) {
                                                       
                                                       self.failHandle(@"授权失败");
                                                   }
                                                   
                                                   [MBManager showBriefAlert:@"授权失败"];
                                               }
                                               
                                           }];
    }
    
}

@end

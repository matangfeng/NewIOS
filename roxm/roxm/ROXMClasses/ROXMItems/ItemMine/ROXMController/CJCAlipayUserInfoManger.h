//
//  CJCAlipayUserInfoManger.h
//  roxm
//
//  Created by 陈建才 on 2017/10/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^alipayUserInfoHandle)(NSString *alipayUid,NSString *alipayName);

typedef void(^alipayUserInfoFailHandle)(NSString *errorStr);

@interface CJCAlipayUserInfoManger : NSObject

@property (nonatomic ,copy) alipayUserInfoHandle infoHandle;

@property (nonatomic ,copy) alipayUserInfoFailHandle failHandle;

@end

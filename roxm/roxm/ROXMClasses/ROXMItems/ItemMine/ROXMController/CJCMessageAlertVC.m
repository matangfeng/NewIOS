//
//  CJCMessageAlertVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/18.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageAlertVC.h"
#import "CJCCommon.h"
#import "CJCBigTopTitleCell.h"
#import "CJCSettingOprationCell.h"
#import "CJCClickSecretVC.h"

#define kBigTopTitleCell            @"CJCBigTopTitleCell"
#define kSettingOprationCell        @"CJCSettingOprationCell"

@interface CJCMessageAlertVC ()<UITableViewDelegate,UITableViewDataSource,CJCSettingOprationCellDelegate>{
    
    
}

@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation CJCMessageAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNaviView];
    [self configTableView];
}

-(void)configTableView{
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCBigTopTitleCell class] forCellReuseIdentifier:kBigTopTitleCell];
    [tableView registerClass:[CJCSettingOprationCell class] forCellReuseIdentifier:kSettingOprationCell];
}

-(void)switchChangeHandle:(BOOL)isOpen andIndex:(CJCSettingOprationCell *)cell{

    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    switch (index.row) {
        case 1:{
        
            if (isOpen) {
                
                [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kCHATMESSAGENOTICE];
            }else{
                
                [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kCHATMESSAGENOTICE];
            }
        }
            break;
            
        case 2:{
            
            if (isOpen) {
                
                [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kYAOYUEMESSAGENOTICE];
            }else{
                
                [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kYAOYUEMESSAGENOTICE];
            }
        }
            break;
            
        case 3:{
            
            if (isOpen) {
                
                [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kMESSAGEISSHOWDETAIL];
            }else{
                
                [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kMESSAGEISSHOWDETAIL];
            }
        }
            break;
            
        case 4:{
            
            if (isOpen) {
                
                [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kNOTICEAUDIO];
            }else{
                
                [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kNOTICEAUDIO];
            }
        }
            break;
            
        case 5:{
            
            if (isOpen) {
                
                [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kNOTICEVIBRATION];
            }else{
                
                [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kNOTICEVIBRATION];
            }
        }
            break;
            
        default:
            break;
    }
}


#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        return kAdaptedValue(83);
    }else if (indexPath.row == 2 || indexPath.row == 3||indexPath.row == 5){
        
        return kAdaptedValue(122);
    }else{
        
        return kAdaptedValue(61);
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        CJCBigTopTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:kBigTopTitleCell];
        
        cell.title = @"通知";
        
        return cell;
    }else{
        
        CJCSettingOprationCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingOprationCell];
        
        cell.delegate = self;
        
        switch (indexPath.row) {
            case 1:{
                
                cell.cellType = CJCSettingOprationCellTypeSwitch;
                cell.title = @"对话消息通知";
                cell.bottomLineView.hidden = YES;
                
                if (![kUSERDEFAULT_STAND objectForKey:kCHATMESSAGENOTICE]) {
                    
                    cell.settingSwitch.on = YES;
                    
                }else if ([[kUSERDEFAULT_STAND objectForKey:kCHATMESSAGENOTICE] isEqualToString:kSWITCHON]){
                    
                    cell.settingSwitch.on = YES;
                }else if ([[kUSERDEFAULT_STAND objectForKey:kCHATMESSAGENOTICE] isEqualToString:kSWITCHOFF]){
                    
                    cell.settingSwitch.on = NO;
                }
            }
                break;
                
            case 2:{
                
                cell.cellType = CJCSettingOprationCellTypeSwitchHigher;
                cell.title = @"邀约消息通知";
                cell.detail = @"关闭后，收到对话或邀约消息时不再有app或短信提示";
                
                if (![kUSERDEFAULT_STAND objectForKey:kYAOYUEMESSAGENOTICE]) {
                    
                    cell.settingSwitch.on = YES;
                    
                }else if ([[kUSERDEFAULT_STAND objectForKey:kYAOYUEMESSAGENOTICE] isEqualToString:kSWITCHON]){
                    
                    cell.settingSwitch.on = YES;
                }else if ([[kUSERDEFAULT_STAND objectForKey:kYAOYUEMESSAGENOTICE] isEqualToString:kSWITCHOFF]){
                    
                    cell.settingSwitch.on = NO;
                }
            }
                break;
                
            case 3:{
                
                cell.cellType = CJCSettingOprationCellTypeSwitchHigher;
                cell.title = @"通知显示消息详情";
                cell.detail = @"关闭后，新消息通知将不显示发送人和具体内容";
                
                if (![kUSERDEFAULT_STAND objectForKey:kMESSAGEISSHOWDETAIL]) {
                    
                    cell.settingSwitch.on = YES;
                    
                }else if ([[kUSERDEFAULT_STAND objectForKey:kMESSAGEISSHOWDETAIL] isEqualToString:kSWITCHON]){
                    
                    cell.settingSwitch.on = YES;
                }else if ([[kUSERDEFAULT_STAND objectForKey:kMESSAGEISSHOWDETAIL] isEqualToString:kSWITCHOFF]){
                    
                    cell.settingSwitch.on = NO;
                }
            }
                break;
                
            case 4:{
                
                cell.cellType = CJCSettingOprationCellTypeSwitch;
                cell.title = @"声音";
                cell.bottomLineView.hidden = YES;
                
                if (![kUSERDEFAULT_STAND objectForKey:kNOTICEAUDIO]) {
                    
                    cell.settingSwitch.on = YES;
                    
                }else if ([[kUSERDEFAULT_STAND objectForKey:kNOTICEAUDIO] isEqualToString:kSWITCHON]){
                    
                    cell.settingSwitch.on = YES;
                }else if ([[kUSERDEFAULT_STAND objectForKey:kNOTICEAUDIO] isEqualToString:kSWITCHOFF]){
                    
                    cell.settingSwitch.on = NO;
                }
            }
                break;
                
            case 5:{
                
                cell.cellType = CJCSettingOprationCellTypeSwitchHigher;
                cell.title = @"震动";
                cell.detail = @"当你正在使用roxm时，新消息的提醒是否需要声音或振动";
                
                if (![kUSERDEFAULT_STAND objectForKey:kNOTICEVIBRATION]) {
                    
                    cell.settingSwitch.on = YES;
                    
                }else if ([[kUSERDEFAULT_STAND objectForKey:kNOTICEVIBRATION] isEqualToString:kSWITCHON]){
                    
                    cell.settingSwitch.on = YES;
                }else if ([[kUSERDEFAULT_STAND objectForKey:kNOTICEVIBRATION] isEqualToString:kSWITCHOFF]){
                    
                    cell.settingSwitch.on = NO;
                }
            }
                break;
                
            default:
                break;
        }
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 1) {
        
        CJCClickSecretVC *nextVC = [[CJCClickSecretVC alloc] init];
        
        [self.navigationController pushViewController:nextVC animated:YES];
    }
    
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    self.lineView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CJCClickSecretSetSuccessVC.h
//  roxm
//
//  Created by 陈建才 on 2017/11/3.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@interface CJCClickSecretSetSuccessVC : CommonVC

@property (nonatomic ,assign) CGPoint secretPoint;

@end

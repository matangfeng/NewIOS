//
//  CJCPrivacySettingVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPrivacySettingVC.h"
#import "CJCCommon.h"
#import "CJCBigTopTitleCell.h"
#import "CJCSettingOprationCell.h"
#import "CJCClickSecretVC.h"
#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>
#import "CJCBlacklistVC.h"
#import "CJCPrivacyModelVC.h"

#define kBigTopTitleCell            @"CJCBigTopTitleCell"
#define kSettingOprationCell        @"CJCSettingOprationCell"

@interface CJCPrivacySettingVC ()<UITableViewDelegate,UITableViewDataSource,CJCSettingOprationCellDelegate>{
    
    NSString *mailListStr;
}

@property (nonatomic ,strong) UITableView *tableView;

@property (nonatomic ,strong) UISwitch *contactSwitch;

@end

@implementation CJCPrivacySettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNaviView];
    [self configTableView];
}

-(void)configTableView{
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCBigTopTitleCell class] forCellReuseIdentifier:kBigTopTitleCell];
    [tableView registerClass:[CJCSettingOprationCell class] forCellReuseIdentifier:kSettingOprationCell];
}

-(void)switchChangeHandle:(BOOL)isOpen andIndex:(CJCSettingOprationCell *)cell{

    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    if (index.row == 2) {
        
        [self mailListAuthority];
    }
}

-(void)mailListAuthority{
    
    if (GREATERIOS9) {
        
        [self mailListAuthorityIOS9Later];
        
    }else{
        
        [self mailListAuthorityIOS9Befor];
    }
}

-(void)mailListAuthorityIOS9Later{
    
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    
    if (status == CNAuthorizationStatusAuthorized) {
        
        if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
            
            [self uploadContactToROXM];
        }else{
            
            [self updateContactStatus];
        }
    }else{
        
        CNContactStore *contactStore = [[CNContactStore alloc] init]; [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted) {
                
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
                        
                        [self uploadContactToROXM];
                    }else{
                        
                        [self updateContactStatus];
                    }
                });
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.contactSwitch.on = NO;
                    
                    [self alertVCShowWith:@"屏蔽手机联系人功能需要开启通讯录权限"];
                });
            }
        }];
        
    }
}

//如果上传完通讯录  只需要更新通讯录的开关状态就可
-(void)updateContactStatus{
    
    [self showWithLabelAnimation];
    
    NSString *statusStr,*alertStr,*saveStr;
    if (self.contactSwitch.on) {
        
        statusStr = @"1";
        alertStr = @"开启屏蔽联系人";
        saveStr = @"YES";
    }else{
        
        statusStr = @"0";
        alertStr = @"关闭屏蔽联系人";
        saveStr = @"NO";
    }
    
    NSString *cantactURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/contact/shield"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"contactShield"] = statusStr;
    
    [CJCHttpTool postWithUrl:cantactURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [kUSERDEFAULT_STAND setObject:saveStr forKey:kUSERCONTACTUPLOAD];
            
            [kUSERDEFAULT_STAND synchronize];
            
            [self showWithLabel:alertStr];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
            
            self.contactSwitch.on = NO;
        }
        
    } failure:^(NSError *error) {
        
    }];
}

//如果之前没有上传过通讯录  需要先上传通讯录
-(void)uploadContactToROXM{
    
    [self showWithLabelAnimation];
    
    if (GREATERIOS9){
        
        [self getMailListDataIOS9Later];
    }else{
        
        [self getMailListDataIOS9Befor];
    }
    
    NSString *cantactURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/contact/upload"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"contact"] = mailListStr;
    
    [CJCHttpTool postWithUrl:cantactURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [kUSERDEFAULT_STAND setObject:@"YES" forKey:kUSERCONTACTUPLOAD];
            
            [kUSERDEFAULT_STAND synchronize];
            
            [MBManager showBriefAlert:@"屏蔽联系人成功"];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
            
            self.contactSwitch.on = NO;
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)mailListAuthorityIOS9Befor{
    
    ABAuthorizationStatus ABstatus = ABAddressBookGetAuthorizationStatus();
    
    if (ABstatus == kABAuthorizationStatusAuthorized) {
        
        if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
            
            [self uploadContactToROXM];
        }else{
            
            [self updateContactStatus];
        }
        
    }else{
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted){
                
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
                        
                        [self uploadContactToROXM];
                    }else{
                        
                        [self updateContactStatus];
                    }
                });
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.contactSwitch.on = NO;
                    
                    [self alertVCShowWith:@"拍摄照片需要开启手机的相机权限"];
                });
            }
        });
        
    }
}

-(void)getMailListDataIOS9Later{
    
    // 3.获取联系人
    // 3.1.创建联系人仓库
    CNContactStore *store = [[CNContactStore alloc] init];
    
    // 3.2.创建联系人的请求对象
    // keys决定这次要获取哪些信息,比如姓名/电话
    NSArray *fetchKeys = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey];
    CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:fetchKeys];
    
    NSMutableString *contactStr = [NSMutableString string];
    
    // 3.3.请求联系人
    NSError *error = nil;
    [store enumerateContactsWithFetchRequest:request error:&error usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
        
        // stop是决定是否要停止
        // 1.获取姓名
        NSString *firstname = contact.givenName;
        NSString *lastname = contact.familyName;
        
        // 2.获取电话号码
        NSArray *phones = contact.phoneNumbers;
        
        NSMutableString *tempStr = [NSMutableString string];
        
        // 3.遍历电话号码
        for (CNLabeledValue *labelValue in phones) {
            CNPhoneNumber *phoneNumber = labelValue.value;
            
            NSString *phone = [phoneNumber.stringValue stringByReplacingOccurrencesOfString:@"-" withString:@""];
            
            [tempStr appendFormat:@"%@%@:%@",firstname,lastname,phone];
        }
        
        [contactStr appendFormat:@"%@;",tempStr.copy];
    }];
    
    mailListStr = contactStr.copy;
}

-(void)getMailListDataIOS9Befor{
    
    NSMutableString *contactStr = [NSMutableString string];
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    CFArrayRef allLinkPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex num = ABAddressBookGetPersonCount(addressBook);
    for (NSInteger i = 0; i < num; i++) {
        ABRecordRef  people = CFArrayGetValueAtIndex(allLinkPeople, i);
        
        //读取firstname
        NSString *personName = (__bridge NSString*)ABRecordCopyValue(people, kABPersonFirstNameProperty);
        //读取lastname
        NSString *lastname = (__bridge NSString*)ABRecordCopyValue(people, kABPersonLastNameProperty);
        
        NSMutableString *tempStr = [NSMutableString string];
        
        ABMultiValueRef phones = ABRecordCopyValue(people, kABPersonPhoneProperty);
        
        for (int k = 0; k<ABMultiValueGetCount(phones); k++)
        {
            //获取該Label下的电话值
            NSString * personPhone = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, k);
            
            NSString *newP = [personPhone stringByReplacingOccurrencesOfString:@"-" withString:@""];
            
            if ([CJCTools isBlankString:personName]) {
                
                [tempStr appendFormat:@"%@:%@",lastname,newP];
            }else{
                
                [tempStr appendFormat:@"%@%@:%@",personName,lastname,newP];
            }
            
        }
        
        [contactStr appendFormat:@"%@;",tempStr.copy];
    }
    
    mailListStr = contactStr.copy;
}

#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        return kAdaptedValue(83);
    }else if (indexPath.row == 2 || indexPath.row == 3){
        
        return kAdaptedValue(122);
    }else{
        
        return kAdaptedValue(61);
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        CJCBigTopTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:kBigTopTitleCell];
        
        cell.title = @"隐私设置";
        
        return cell;
    }else{
        
        CJCSettingOprationCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingOprationCell];
        
        cell.delegate = self;
        
        switch (indexPath.row) {
            case 1:{
                
                cell.cellType = CJCSettingOprationCellTypeDefault;
                cell.title = @"开启指纹屏保";
                cell.bottomLineView.hidden = YES;
            }
                break;
                
            case 2:{
                
                cell.cellType = CJCSettingOprationCellTypeSwitchHigher;
                cell.title = @"屏蔽手机联系人";
                cell.detail = @"开启后，你手机通讯录中的联系人将不会看到你，你也看不到他们";
                
                if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
                    
                    cell.settingSwitch.on = NO;
                    
                }else if ([[kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD] isEqualToString:kSWITCHON]){
                    
                    cell.settingSwitch.on = YES;
                }else if ([[kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD] isEqualToString:kSWITCHOFF]){
                    
                    cell.settingSwitch.on = NO;
                }
                
                self.contactSwitch = cell.settingSwitch;
            }
                break;
                
            case 3:{
                
                cell.cellType = CJCSettingOprationCellTypeDefaultHigher;
                cell.title = @"黑名单";
                cell.detail = @"加入黑名单后对方将无法看到你、关注你或给你发消息";
            }
                break;
                
            default:
                break;
        }
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 1) {
        
        CJCPrivacyModelVC *nextVC = [[CJCPrivacyModelVC alloc] init];
        
        [self.navigationController pushViewController:nextVC animated:YES];
        
    }else if (indexPath.row == 3){
    
        CJCBlacklistVC *nextVC = [[CJCBlacklistVC alloc] init];
    
        [self.navigationController pushViewController:nextVC animated:YES];
    }
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    self.lineView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

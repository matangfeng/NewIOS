//
//  CJCInstallPaySecretVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInstallPaySecretVC.h"
#import "CJCCommon.h"
#import <ReactiveObjC.h>
#import "FQ_CodeTextView.h"

@interface CJCInstallPaySecretVC (){

    //记录手机号的长度 用于判断是输入 还是删除
    NSInteger phoneNUmLengh;
}

@property (nonatomic ,strong) UITextField *hidTF;

@property (nonatomic ,strong) UILabel *firstTF;

@property (nonatomic ,strong) UILabel *secondTF;

@property (nonatomic ,strong) UILabel *thirdTF;

@property (nonatomic ,strong) UILabel *fourthTF;

@property (nonatomic ,strong) UILabel *fifthTF;

@property (nonatomic ,strong) UILabel *sixTF;

@property (nonatomic ,strong) CJCErrorAletLabel *errorLabel;

@property (nonatomic , strong) FQ_CodeTextView * textView;
@end

@implementation CJCInstallPaySecretVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.textView becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor toUIColorByStr:@"FFFFFF"];
    [self setUpNaviView];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"设置提现密码" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"设置提现密码" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
    NSString *middleStr;
    if (self.installType == CJCInstallPaySecretVCTypeConfirm) {
        middleStr = @"再次输入6位提现密码";
    }else{
        middleStr = @"请设置6位提现密码";
    }

    UILabel *middleLabel = [UIView getSystemLabelWithStr:middleStr fontName:kFONTNAMEMEDIUM size:19 color:[UIColor toUIColorByStr:@"222222"]];
    middleLabel.textAlignment = NSTextAlignmentCenter;
    middleLabel.frame = CGRectMake(0, kAdaptedValue(172), SCREEN_WITDH, kAdaptedValue(26));
    [self.view addSubview:middleLabel];
    
    CGFloat sizeW = (self.view.frame.size.width * 1.0f - 57 - (6 - 1) * 6)/ 6;
    for (NSInteger i = 0; i < 6; i ++) {
        UIView * big = [[UIView alloc] initWithFrame:CGRectMake(57/2 + (i * (sizeW + 6) ), CGRectGetMaxY(middleLabel.frame) + 34.5, sizeW - 1, sizeW - 1)];
        big.layer.cornerRadius = 4;
        big.layer.masksToBounds = YES;
        big.backgroundColor = [UIColor toUIColorByStr:@"FCFCFC"];
        [self.view addSubview:big];
    }
    
    self.textView = [[FQ_CodeTextView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(middleLabel.frame) + 33.5,  self.view.bounds.size.width, 100)];
    self.textView.isSelectStatus = YES;
    @weakify(self);
    self.textView.completeBlock = ^{
        @strongify(self);
        [self makeSureSecretIsCorrect];
    };
    [self.view addSubview:self.textView];
}

-(void)makeSureSecretIsCorrect{

    if (self.installType == CJCInstallPaySecretVCTypeConfirm) {
        NSLog(@"inset ：%@",self.secret);
        NSLog(@"value ：%@",self.textView.textTot);
        if ([self.secret isEqualToString:self.textView.textTot]) {
            [self getPhoneCode];
        }else{
            [self showWithLabel:@"请输入正确的密码"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                phoneNUmLengh = self.textView.textTot.length;
                [self.firstTF.layer setBorderColor:[UIColor toUIColorByStr:@"429CF0"].CGColor];
                [self.sixTF.layer setBorderColor:[UIColor toUIColorByStr:@"D5D5D5"].CGColor];
            });
        }
    }else if (self.installType == CJCInstallPaySecretVCTypeDefault){
        CJCInstallPaySecretVC *nextVC = [[CJCInstallPaySecretVC alloc]init];
        nextVC.installType = CJCInstallPaySecretVCTypeConfirm;
        nextVC.secret = self.textView.textTot;
        nextVC.phoneCode = self.phoneCode;
        nextVC.pushType = self.pushType;
        [self.navigationController pushViewController:nextVC animated:YES];
    }
}

-(void)getPhoneCode{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/set/pay_password"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"code"] = self.phoneCode;
    params[@"password"] = self.textView.textTot;
    
    [CJCHttpTool postWithUrl:urlStr params:params success:^(id responseObject) {
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0){
            [self.view endEditing:YES];
            [MBManager showBriefAlert:@"设置提现密码成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                NSInteger VCIndex = self.navigationController.childViewControllers.count;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"inputSecretViewShow" object:nil];
                
                [self.navigationController popToViewController:self.navigationController.childViewControllers[VCIndex-2] animated:YES];
                
            });
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(CJCErrorAletLabel *)errorLabel{
    
    if (_errorLabel == nil) {
        
        _errorLabel = [[CJCErrorAletLabel alloc] init];
        
        _errorLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(300), kAdaptedValue(134), kAdaptedValue(12));
        
        [self.view addSubview:_errorLabel];
    }
    
    return _errorLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CJCInputSecretView.m
//  roxm
//
//  Created by 陈建才 on 2017/10/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInputSecretView.h"
#import "CJCCommon.h"
#import <ReactiveObjC.h>
#import "FQ_CodeTextView.h"

@interface CJCInputSecretView (){
    
    //记录手机号的长度 用于判断是输入 还是删除
    NSInteger phoneNUmLengh;
}

@property (nonatomic ,strong) UITextField *hidTF;

@property (nonatomic ,strong) UILabel *firstTF;

@property (nonatomic ,strong) UILabel *secondTF;

@property (nonatomic ,strong) UILabel *thirdTF;

@property (nonatomic ,strong) UILabel *fourthTF;

@property (nonatomic ,strong) UILabel *fifthTF;

@property (nonatomic ,strong) UILabel *sixTF;

@property (nonatomic ,strong) CJCErrorAletLabel *errorLabel;

@property (nonatomic ,strong) UIView *containView;

    @property (nonatomic , strong) FQ_CodeTextView * textView;
@end

@implementation CJCInputSecretView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.bounds = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        self.layer.opacity = 0.0;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{
    
    UIView *topView = [[UIView alloc] init];
    
    topView.backgroundColor = [UIColor clearColor];
    
    topView.frame = CGRectMake(0, 0, SCREEN_WITDH, SCREEN_HEIGHT-kAdaptedValue(319));
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPickerView)];
    
    [topView addGestureRecognizer:tapGesture];
    
    [self addSubview:topView];
    
    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WITDH, kAdaptedValue(383))];
    
    containView.backgroundColor = [UIColor whiteColor];
    
    self.containView = containView;
    
    [self addSubview:containView];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"请输入支付密码" fontName:kFONTNAMEMEDIUM size:19 color:[UIColor toUIColorByStr:@"222222"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"请输入支付密码" withFont:[UIFont accodingVersionGetFont_mediumWithSize:19]];
    
    titleLabel.frame = CGRectMake(20, kAdaptedValue(23), width, kAdaptedValue(22));
    titleLabel.centerX = SCREEN_WITDH/2;
    
    [containView addSubview:titleLabel];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:kGetImage(@"profile_wollet_close") forState:UIControlStateNormal];
    [backButton setImage:[self imageByApplyingAlpha:0.5 image:kGetImage(@"profile_wollet_close")] forState:1];

    backButton.size = CGSizeMake(kAdaptedValue(44), kAdaptedValue(44));
    backButton.x = kAdaptedValue(5);
    backButton.centerY = titleLabel.centerY;
    
    [[backButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self dismissPickerView];
    }];
    [containView addSubview:backButton];
    
    CGFloat sizeW = (self.frame.size.width * 1.0f - 57 - (6 - 1) * 6)/ 6;
    for (NSInteger i = 0; i < 6; i ++) {
        UIView * big = [[UIView alloc] initWithFrame:CGRectMake(57/2 + (i * (sizeW + 6) ), kAdaptedValue(79), sizeW - 1, sizeW - 1)];
        big.layer.cornerRadius = 4;
        big.layer.masksToBounds = YES;
        big.backgroundColor = [UIColor toUIColorByStr:@"fcfcfc"];
        [self.containView addSubview:big];
    }
    
    self.textView = [[FQ_CodeTextView alloc]initWithFrame:CGRectMake(0, kAdaptedValue(79),  SCREEN_WITDH, 100)];
    self.textView.isSelectStatus = YES;
    @weakify(self);
    self.textView.completeBlock = ^{
        @strongify(self);
        [self makeSureSecretIsCorrect];
    };
    [self.containView addSubview:self.textView];
     [self.textView becomeFirstResponder];
}

    - (UIImage*)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image {
        
        UIGraphicsBeginImageContextWithOptions(image.size,NO,0.0f);
        CGContextRef ctx =UIGraphicsGetCurrentContext();
        CGRect area =CGRectMake(0,0, image.size.width, image.size.height);
        CGContextScaleCTM(ctx,1, -1);
        CGContextTranslateCTM(ctx,0, -area.size.height);
        CGContextSetBlendMode(ctx,kCGBlendModeMultiply);
        CGContextSetAlpha(ctx, alpha);
        CGContextDrawImage(ctx, area, image.CGImage);
        UIImage*newImage =UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
//-(void)creatFourTextFieldAndLine{
//
//    for (int i=0; i<6; i++) {
//
//        UILabel *codeTf = [[UILabel alloc] init];
//
//        codeTf.tag = i+1;
//
//        codeTf.frame = CGRectMake(kAdaptedValue(28)+i*kAdaptedValue(54), kAdaptedValue(79), kAdaptedValue(48), kAdaptedValue(48));
//
//        codeTf.font = [UIFont accodingVersionGetFont_lightWithSize:24];
//
//        codeTf.textColor = [UIColor toUIColorByStr:@"222222"];
//
//        codeTf.textAlignment = NSTextAlignmentCenter;
//
//        codeTf.layer.cornerRadius = kAdaptedValue(6);
//        [codeTf.layer setBorderColor:[UIColor toUIColorByStr:@"D5D5D5"].CGColor];
//        [codeTf.layer setBorderWidth:1];
//        [codeTf.layer setMasksToBounds:YES];
//
//        codeTf.backgroundColor = [UIColor toUIColorByStr:@"FCFCFC"];
//
//        [self.containView addSubview:codeTf];
//
//        switch (i+1) {
//            case 1:{
//
//                self.firstTF = codeTf;
//                [self.firstTF.layer setBorderColor:[UIColor toUIColorByStr:@"429CF0"].CGColor];
//            }
//                break;
//
//            case 2:{
//
//                self.secondTF = codeTf;
//
//            }
//                break;
//
//            case 3:{
//
//                self.thirdTF = codeTf;
//            }
//                break;
//
//            case 4:{
//
//                self.fourthTF = codeTf;
//            }
//                break;
//
//            case 5:{
//
//                self.fifthTF = codeTf;
//            }
//                break;
//
//            case 6:{
//
//                self.sixTF = codeTf;
//            }
//                break;
//
//            default:
//                break;
//        }
//    }
//
//}

-(void)makeSureSecretIsCorrect{

    [self verifyPassWord];
}

-(void)verifyPassWord{

    NSString *verifyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/verify/pay_password"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"password"] = self.textView.textTot;
    
    [CJCHttpTool postWithUrl:verifyURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSNumber *dataNum = responseObject[@"data"];
            
            if (dataNum.integerValue == 1) {
                
                CGRect frameContent =  self.containView.frame;
                frameContent.origin.y += self.containView.frame.size.height;
                
                [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    
                    [self.layer setOpacity:0.0];
                    self.containView.frame = frameContent;
                } completion:^(BOOL finished) {
                    
                    if (self.confirmHandle) {
                        
                        self.confirmHandle();
                    }
                    
                    [self.containView endEditing:YES];
                    [self removeFromSuperview];
                }];
            }else{
            
                [MBManager showBriefAlert:@"请输入正确的密码"];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    self.firstTF.text = @"";
                    self.secondTF.text = @"";
                    self.thirdTF.text = @"";
                    self.fourthTF.text = @"";
                    self.fifthTF.text = @"";
                    self.sixTF.text = @"";
                    
                    self.hidTF.text = @"";
                    
                    phoneNUmLengh = self.hidTF.text.length;
                    
                    [self.firstTF.layer setBorderColor:[UIColor toUIColorByStr:@"429CF0"].CGColor];
                    [self.sixTF.layer setBorderColor:[UIColor toUIColorByStr:@"D5D5D5"].CGColor];
                });
            }
            
        }else{
        
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
       
        
    }];
}

-(void)showPickerView{
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    delegate.keyBoardType = CJCKeyboardTypeOnlySystem;
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self setCenter:[UIApplication sharedApplication].keyWindow.center];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self];
    
    CGRect frameContent =  self.containView.frame;
    
    frameContent.origin.y -= self.containView.frame.size.height;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.layer setOpacity:1.0];
        self.containView.frame = frameContent;
        
    } completion:^(BOOL finished) {
        
        [self.hidTF becomeFirstResponder];
    }];
    
}


-(void)dismissPickerView{
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    delegate.keyBoardType = CJCKeyboardTypeDefault;
    
    [self.containView endEditing:YES];
    
    CGRect frameContent =  self.containView.frame;
    frameContent.origin.y += self.containView.frame.size.height;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.layer setOpacity:0.0];
        self.containView.frame = frameContent;
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
    }];
}


@end

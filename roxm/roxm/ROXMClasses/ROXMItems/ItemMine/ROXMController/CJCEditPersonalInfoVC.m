//
//  CJCEditPersonalInfoVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCEditPersonalInfoVC.h"
#import "CJCCommon.h"
#import "CJCEditInfoTopCell.h"
#import "CJCEditInfoBottomCell.h"
#import "CJCChangeNickNameVC.h"
#import "CJCBirthHeiWeightView.h"
#import "CJCBeforeTakePhotoVC.h"
#import "CJCMoreInfoVC.h"
#import "CJCPhotoShowModel.h"
#import "CJCPhotoLibraryVC.h"

#define kEditInfoTopCell        @"CJCEditInfoTopCell"
#define kEditInfoBottomCell     @"CJCEditInfoBottomCell"

@interface CJCEditPersonalInfoVC ()<UITableViewDelegate,UITableViewDataSource,CJCEditInfoTopCellDelegate>{

    NSDictionary *changeInfoDict;
    
    NSMutableArray *imagesArray;
    
    NSMutableArray *uploadImageArray;
}

@property (nonatomic ,strong) UITableView *mineTableView;

@end

@implementation CJCEditPersonalInfoVC

-(void)viewWillAppear:(BOOL)animated{

    [self getPersonalImages];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    imagesArray = [NSMutableArray array];
    uploadImageArray = [NSMutableArray array];
    
    [self setUpNaviView];
}

//获得图片  视频 列表
- (void)getPersonalImages{
    
    [MBManager showLoadingInView:self.view];
    
    NSString *imagesURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/list/photo"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"otherUserId"] = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    
    [CJCHttpTool postWithUrl:imagesURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [imagesArray removeAllObjects];
            [uploadImageArray removeAllObjects];
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCPhotoShowModel class] json:responseObject[@"data"]];
            NSArray *tempUploadArr = responseObject[@"data"];
            
            [imagesArray addObjectsFromArray:tempArr];
            [uploadImageArray addObjectsFromArray:tempUploadArr];
            
            [MBManager hideAlert];
            
            [self.mineTableView reloadRow:3 inSection:0 withRowAnimation:NO];
        }else{
            
            [MBManager hideAlert];
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

//更换头像
-(void)changeIconHandle{

    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCSexType sexType;
    if (model.sex == 0) {
        
        sexType = CJCSexTypeWoman;
    }else{
        
        sexType = CJCSexTypeMan;
    }
    
    CJCBeforeTakePhotoVC *nextVC = [[CJCBeforeTakePhotoVC alloc] init];
    
    nextVC.takePhotoType = CJCTakePhotoTypeChangeCamera;
    
    nextVC.sexType = sexType;
    
    nextVC.imagePath = model.imageUrl;
    
    nextVC.changeHandle = ^(NSDictionary *infoDict) {
        
        model.imageUrl = infoDict[@"imageUrl"];
        NSLog(@"wo kao :%@",infoDict[@"imageUrl"]);
        NSString * str = [CJCpersonalInfoModel infoModel].loginUserDict[@"imageUrl"];
        
        [self.mineTableView reloadRow:0 inSection:0 withRowAnimation:NO];
    };
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

//    CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCSexType sexType;
    if (model.sex == 0) {
        
        sexType = CJCSexTypeWoman;
    }else{
        
        sexType = CJCSexTypeMan;
    }
    
    switch (indexPath.row) {
        case 1:{
            //昵称
            CJCChangeNickNameVC *nextVC = [[CJCChangeNickNameVC alloc] init];
            
            nextVC.typeName = @"昵称";
            nextVC.nickName = [CJCpersonalInfoModel infoModel].loginUserDict[@"nickname"];
            
            nextVC.changeHandle = ^(NSDictionary *infoDict) {
                
                changeInfoDict = infoDict;
//                [model.loginUserDict setValue:[NSString stringWithFormat:@"%@",infoDict[@"nickname"]] forKey:@"nickname"];
//                [CJCpersonalInfoModel infoModel].loginUserDict = model.loginUserDict;
                
                [self.mineTableView reloadRow:1 inSection:0 withRowAnimation:NO];
            };
            [self.navigationController pushViewController:nextVC animated:YES];
        
        }
            break;
            
        case 2:{
            //视频介绍
            CJCBeforeTakePhotoVC *nextVC = [[CJCBeforeTakePhotoVC alloc] init];
            
            nextVC.takePhotoType = CJCTakePhotoTypeChangeVideo;
            
            nextVC.sexType = sexType;
            
            nextVC.videoPath = [CJCpersonalInfoModel infoModel].loginUserDict[@"videoUrl"];
            
            nextVC.changeHandle = ^(NSDictionary *infoDict) {
                
//                [model.loginUserDict setValue:infoDict[@"videoUrl"] forKey:@"videoUrl"];
//                [CJCpersonalInfoModel infoModel].loginUserDict = model.loginUserDict;
                
                [self.mineTableView reloadRow:2 inSection:0 withRowAnimation:NO];
            };
            
            [self.navigationController pushViewController:nextVC animated:YES];
            
        }
            break;
            
        case 3:{
            //相册
            
            CJCPhotoLibraryVC *nextVC = [[CJCPhotoLibraryVC alloc] init];
            
            nextVC.imageArray = imagesArray.copy;
            nextVC.uploadImageArray = uploadImageArray.copy;
            nextVC.infoModel = model;
            nextVC.infoVCType = CJCPersonalInfoVCTypeOwn;
            
            [self.navigationController pushViewController:nextVC animated:YES];
        }
            break;
            
        case 4:{
            //出生日期
            
            NSArray *tempArr = [[CJCpersonalInfoModel infoModel].loginUserDict[@"birthDay"] componentsSeparatedByString:@"_"];
            NSString *newStr = [NSString stringWithFormat:@"%@年%@月%@日",tempArr[0],tempArr[1],tempArr[2]];
            
            [self creatDarePickerWithPickerType:CJCPickerViewTypeBirthDay andSelectStr:newStr];
        }
            break;
            
        case 5:{
            //身高
            
            if (model.height == 0) {
                
                model.height = 175;
            }
            
            NSString *height = [NSString stringWithFormat:@"%@",[[CJCpersonalInfoModel infoModel].loginUserDict[@"height"] stringValue]];
            
            [self creatDarePickerWithPickerType:CJCPickerViewTypeHeight andSelectStr:height];
            
        }
            break;
            
        case 6:{
            //体重
            if (model.weight == 0) {
                
                model.weight = 65;
            }
            
            NSString *height = [NSString stringWithFormat:@"%@",[[CJCpersonalInfoModel infoModel].loginUserDict[@"weight"] stringValue]];
            NSLog(@"我了操场：%@",height);
            
            [self creatDarePickerWithPickerType:CJCPickerViewTypeWeight andSelectStr:height];
        }
            break;
            
        case 7:{
            //个人签名
            CJCChangeNickNameVC *nextVC = [[CJCChangeNickNameVC alloc] init];
            
            nextVC.typeName = @"个人签名";
            nextVC.nickName =  [CJCpersonalInfoModel infoModel].loginUserDict[@"brief"];
            
            nextVC.changeHandle = ^(NSDictionary *infoDict) {
                
                NSLog(@"sing :%@",[NSString stringWithFormat:@"%@",infoDict[@"brief"]]);
//                [model.loginUserDict setValue:[NSString stringWithFormat:@"%@",infoDict[@"brief"]] forKey:@"brief"];
//                [CJCpersonalInfoModel infoModel].loginUserDict = model.loginUserDict;
                
                [self.mineTableView reloadRow:7 inSection:0 withRowAnimation:NO];
            };
            
            [self.navigationController pushViewController:nextVC animated:YES];
        }
            break;
            
        case 8:{
            //职业
            CJCChangeNickNameVC *nextVC = [[CJCChangeNickNameVC alloc] init];
            
            nextVC.typeName = @"职业";
            nextVC.nickName = [CJCpersonalInfoModel infoModel].loginUserDict[@"career"];
            
            nextVC.changeHandle = ^(NSDictionary *infoDict) {
                
                changeInfoDict = infoDict;
                

                
                [self.mineTableView reloadRow:8 inSection:0 withRowAnimation:NO];
            };
            
            [self.navigationController pushViewController:nextVC animated:YES];
        }
            break;
            
        case 9:{
            
            if (model.sex == 0) {
                //更多资料
                
                CJCMoreInfoVC *nextVC = [[CJCMoreInfoVC alloc] init];
                
                nextVC.moreInfoHandle = ^(NSDictionary *infoDict) {
                  
                    changeInfoDict = infoDict;
                };
                
                [self.navigationController pushViewController:nextVC animated:YES];
                
            }else{
                //收入
                
            }
        }
            break;
            
        default:
            break;
    }

}

-(void)creatDarePickerWithPickerType:(CJCPickerViewType)pickType andSelectStr:(NSString *)selectStr{

//    CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCBirthHeiWeightView *pick = [[CJCBirthHeiWeightView alloc] init];
    
    CJCSexType sexType;
    if (model.sex == 0) {
        
        sexType = CJCSexTypeWoman;
    }else{
        
        sexType = CJCSexTypeMan;
    }
    
    pick.sexType = sexType;
    
    pick.selectStr = selectStr;
    
    pick.pickerViewType = pickType;
    
    [pick showPickerView];
    
    pick.returnHandle = ^(NSString *returnString, NSString *returnInt){
        
        [self updateInfoWith:pickType andValue:returnInt];
    };
}

-(void)updateInfoWith:(CJCPickerViewType)takeType andValue:(NSString *)value{

    [self showWithLabelAnimation];
    
    NSString *updateURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/update"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    switch (takeType) {
        case CJCPickerViewTypeBirthDay:{
        
            params[@"birthDay"] = value;
            
            
        }
            break;
            
        case CJCPickerViewTypeHeight:{
            
            params[@"height"] = value;
        }
            break;
            
        case CJCPickerViewTypeWeight:{
            
            params[@"weight"] = value;
        }
            break;
            
        default:
            break;
    }
    
    [CJCHttpTool postWithUrl:updateURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSDictionary *infoDict = responseObject[@"data"][@"user"];
              [CJCpersonalInfoModel infoModel].loginUserDict = infoDict;
//            CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
//            CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

            changeInfoDict = infoDict;
            
            switch (takeType) {
                case CJCPickerViewTypeBirthDay:{
                    
//                    [model.loginUserDict setValue:infoDict[@"birthDay"] forKey:@"birthDay"];
//                    [CJCpersonalInfoModel infoModel].loginUserDict = model.loginUserDict;
                    
                    [self.mineTableView reloadRow:4 inSection:0 withRowAnimation:NO];
                }
                    break;
                    
                case CJCPickerViewTypeHeight:{
                    
//                    NSNumber *heightNum = infoDict[@"height"];
//                    NSLog(@"rile:%@",heightNum);
//                    model.height = heightNum.integerValue;
                    
//                    [model.loginUserDict setValue:infoDict[@"height"] forKey:@"height"];
//                    [CJCpersonalInfoModel infoModel].loginUserDict = model.loginUserDict;
//
                    [self.mineTableView reloadRow:5 inSection:0 withRowAnimation:NO];
                }
                    break;
                    
                case CJCPickerViewTypeWeight:{
                    
                    NSNumber *heightNum = [NSNumber numberWithInt:[infoDict[@"weight"] intValue]] ;
                
//                    NSLog(@"rile :%@",infoDict[@"weight"]);
//                    NSLog(@"rile32323:%@",heightNum);
                    
//                    [model.loginUserDict setValue:heightNum forKey:@"weight"];
//                    [CJCpersonalInfoModel infoModel].loginUserDict = model.loginUserDict;
                    
//                    model.weight = heightNum.integerValue;
                    
                    [self.mineTableView reloadRow:6 inSection:0 withRowAnimation:NO];
                }
                    break;
                    
                default:
                    break;
            }
        
        }
        
        
        
    } failure:^(NSError *error) {
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        return kAdaptedValue(174);
    }else{
        
        return kAdaptedValue(62);
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    CJCpersonalInfoModel * model = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (indexPath.row == 0) {
        
        CJCEditInfoTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kEditInfoTopCell];
        
        cell.delegate = self;
        cell.iconURL = model.imageUrl;
        
        return cell;
    }else{
    
        CJCEditInfoBottomCell *cell = [tableView dequeueReusableCellWithIdentifier:kEditInfoBottomCell];
        
        switch (indexPath.row) {
            case 1:{
            
                cell.title = @"昵称";
                cell.detail = model.nickname;
            }
                break;
             
            case 2:{
                
                cell.title = @"视频介绍";
                cell.detail = @"重新拍摄";
            }
                break;
                
            case 3:{
                
                cell.title = @"相册";
                cell.detail = [NSString stringWithFormat:@"%lu张",(unsigned long)imagesArray.count];
            }
                break;
                
            case 4:{
                
                cell.title = @"出生日期";
                cell.detail = model.birthDay;
            }
                break;
                
            case 5:{
                
                cell.title = @"身高";
                cell.detail = [NSString stringWithFormat:@"%ldcm",(long)model.height];
            }
                break;
                
            case 6:{
                
                cell.title = @"体重";
                cell.detail = [NSString stringWithFormat:@"%ldkg",(long)model.weight];
            }
                break;
                
            case 7:{
                
                cell.title = @"个人签名";
                cell.detail = model.brief;
            }
                break;
                
            case 8:{
                
                cell.title = @"职业";
                cell.detail = model.career;
            }
                break;
                
            case 9:{
                
                if (model.sex == 0) {
                    
                    cell.title = @"更多资料";
                    cell.detail = @"";
                }else{
                
                    cell.title = @"收入";
                    cell.detail = @"";
                }
            }
                break;
                
            default:
                break;
        }
        
        return cell;
    }
}


-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    [self setNaviViewBottom:64];
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT - 64);
    
    [tableView registerClass:[CJCEditInfoTopCell class] forCellReuseIdentifier:kEditInfoTopCell];
    [tableView registerClass:[CJCEditInfoBottomCell class] forCellReuseIdentifier:kEditInfoBottomCell];
    
    self.mineTableView = tableView;
    [self.view addSubview:tableView];
    
}

-(void)naviViewBackBtnDidClick{
    
    if (changeInfoDict) {
        
//        [CJCpersonalInfoModel updateInfoJSONTolocal:changeInfoDict];
        
        if (self.iconHandle) {
            
            self.iconHandle(changeInfoDict);
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

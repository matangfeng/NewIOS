//
//  CJCInviteRecordCell.h
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYSideslipCell.h"

@class CJCInviteRecordModel;

@interface CJCInviteRecordCell : LYSideslipCell

@property (nonatomic ,strong) CJCInviteRecordModel *recordModel;

@end

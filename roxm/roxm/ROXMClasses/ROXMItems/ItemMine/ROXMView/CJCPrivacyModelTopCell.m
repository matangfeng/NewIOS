//
//  CJCPrivacyModelTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/11/2.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPrivacyModelTopCell.h"
#import "CJCCommon.h"

@implementation CJCPrivacyModelTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"隐私模式" fontName:kFONTNAMEREGULAR size:28 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(65), kAdaptedValue(32), kAdaptedValue(150), kAdaptedValue(28));
    
    [self.contentView addSubview:titleLabel];
    
    UIImageView *yuechi = [[UIImageView alloc] initWithImage:kGetImage(@"set_icon_yinsi")];
    
    yuechi.frame = CGRectMake(kAdaptedValue(23), 32, kAdaptedValue(27), kAdaptedValue(29));
    
    yuechi.centerY = titleLabel.centerY;
    
    [self.contentView addSubview:yuechi];
    
    UILabel *hintLabel = [UIView getSystemLabelWithStr:@"开启后，每次打开roxm濡沫都会出现黑屏，长按屏幕即可进入首页" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"636363"]];
    
    hintLabel.frame = CGRectMake(kAdaptedValue(23), titleLabel.bottom+kAdaptedValue(18), SCREEN_WITDH-kAdaptedValue(46), kAdaptedValue(46));
    
    [self.contentView addSubview:hintLabel];
}

@end

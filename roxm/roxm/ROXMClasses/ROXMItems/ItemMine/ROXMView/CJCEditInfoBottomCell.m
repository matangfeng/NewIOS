//
//  CJCEditInfoBottomCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCEditInfoBottomCell.h"
#import "CJCCommon.h"

@interface CJCEditInfoBottomCell ()

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UILabel *detailLabel;

@end

@implementation CJCEditInfoBottomCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(20.5), 64, kAdaptedValue(21));
    titleLabel.centerY = kAdaptedValue(31);
    
    self.titleLabel = titleLabel;
    [self.contentView addSubview:titleLabel];
    
    UIImageView *rightImageView = [[UIImageView alloc] initWithImage:kGetImage(@"profile_icon_arrowright")];
    
    rightImageView.size = CGSizeMake(kAdaptedValue(22), kAdaptedValue(22));
    rightImageView.centerY = kAdaptedValue(31);
    rightImageView.right = SCREEN_WITDH - kAdaptedValue(15);
    
    [self.contentView addSubview:rightImageView];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(17.5), kAdaptedValue(61), SCREEN_WITDH - kAdaptedValue(35), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
    
    UILabel *detailLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"999999"]];
    
    self.detailLabel = detailLabel;
    [self.contentView addSubview:detailLabel];
}

-(void)setTitle:(NSString *)title{
    
    CGFloat width = [CJCTools getShortStringLength:title withFont:[UIFont accodingVersionGetFont_regularWithSize:16]];
    
    self.titleLabel.text = title;
    self.titleLabel.width = width;
}

-(void)setDetail:(NSString *)detail{

    CGFloat maxDetailWidth = SCREEN_WITDH - kAdaptedValue(190);
    
    CGFloat width = [CJCTools getShortStringLength:detail withFont:[UIFont accodingVersionGetFont_regularWithSize:16]];
    
    if (width>maxDetailWidth) {
        
        width = maxDetailWidth;
    }
    
    self.detailLabel.text = detail;
    self.detailLabel.frame = CGRectMake(100, 22, width, kAdaptedValue(22));
    
    self.detailLabel.centerY = kAdaptedValue(31);
    self.detailLabel.right = SCREEN_WITDH - kAdaptedValue(41);
}

@end

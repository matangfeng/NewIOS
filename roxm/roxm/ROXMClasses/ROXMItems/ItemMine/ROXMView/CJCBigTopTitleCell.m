//
//  CJCBigTopTitleCell.m
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBigTopTitleCell.h"
#import "CJCCommon.h"

@interface CJCBigTopTitleCell ()

@property (nonatomic ,strong) UILabel *bigTitleLabel;

@end

@implementation CJCBigTopTitleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:32 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(16), SCREEN_WITDH-kAdaptedValue(40), kAdaptedValue(32));
    
    self.bigTitleLabel = titleLabel;
    [self.contentView addSubview:titleLabel];
}

-(void)setTitle:(NSString *)title{

    self.bigTitleLabel.text = title;
}

@end

//
//  CJCEditInfoTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCEditInfoTopCell.h"
#import "CJCCommon.h"

@interface CJCEditInfoTopCell ()

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UIImageView *iconImageView;

@end

@implementation CJCEditInfoTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"编辑个人资料" fontName:kFONTNAMEMEDIUM size:32 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), 26, 250, kAdaptedValue(32));
    
    [self.contentView addSubview:titleLabel];
    
    UIImageView *iconImageView = [[UIImageView alloc] init];
    
    iconImageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    iconImageView.frame = CGRectMake(kAdaptedValue(23), 86, kAdaptedValue(60), kAdaptedValue(60));
    
    iconImageView.layer.cornerRadius = kAdaptedValue(30);
    iconImageView.layer.masksToBounds = YES;
    
    iconImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconImageTapHandle)];
    
    [iconImageView addGestureRecognizer:imageTap];
    
    self.iconImageView = iconImageView;
    [self.contentView addSubview:iconImageView];
    
    UIImageView *cameraImage = [[UIImageView alloc] initWithImage:kGetImage(@"profile_edit_photo")];
    
    cameraImage.frame = CGRectMake(kAdaptedValue(61), kAdaptedValue(38), kAdaptedValue(22), kAdaptedValue(22));
    
    cameraImage.bottom = iconImageView.bottom;
    
    [self.contentView addSubview:cameraImage];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), 173, SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

-(void)iconImageTapHandle{

    if ([self.delegate respondsToSelector:@selector(changeIconHandle)]) {
        
        [self.delegate changeIconHandle];
    }
}

-(void)setIconURL:(NSString *)iconURL{

    [self.iconImageView setImageURL:[NSURL URLWithString:[CJCpersonalInfoModel infoModel].loginUserDict[@"imageUrl"]]];
}

@end

//
//  CJCSettingOprationCell.h
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCSettingOprationCell;

typedef NS_ENUM(NSInteger,CJCSettingOprationCellType){
    
    CJCSettingOprationCellTypeDefault               = 0,//默认样式:箭头 title
    CJCSettingOprationCellTypeDefaultDetail        = 1,//带箭头 title 描述在一行
    CJCSettingOprationCellTypeDefaultHigher        = 2,//箭头 title 带底部描述view
    CJCSettingOprationCellTypeSwitchHigher         = 3,//title switch 带有底部描述view
    CJCSettingOprationCellTypeSwitch               = 4,//title switch
    CJCSettingOprationCellTypeNoArrow              = 5,//没有箭头的样式
};

@protocol CJCSettingOprationCellDelegate <NSObject>

-(void)switchChangeHandle:(BOOL)isOpen andIndex:(CJCSettingOprationCell *)cell;

@end

@interface CJCSettingOprationCell : UITableViewCell

@property (nonatomic ,weak) id<CJCSettingOprationCellDelegate> delegate;

@property (nonatomic ,assign) CJCSettingOprationCellType cellType;

@property (nonatomic ,assign) BOOL isShow;

@property (nonatomic ,copy) NSString *title;

@property (nonatomic ,copy) NSString *detail;

@property (nonatomic ,copy) NSString *middleDetail;

@property (nonatomic ,strong) UIImageView *arrowImageView;

@property (nonatomic ,strong) UIView *bottomLineView;

@property (nonatomic ,strong) UISwitch *settingSwitch;

@end

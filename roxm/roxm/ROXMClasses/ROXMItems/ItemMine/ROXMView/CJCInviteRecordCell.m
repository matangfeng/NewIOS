//
//  CJCInviteRecordCell.m
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteRecordCell.h"
#import "CJCCommon.h"
#import "CJCInviteRecordModel.h"
#import "CJCpersonalInfoModel.h"

@interface CJCInviteRecordCell ()

@property (nonatomic ,strong) UIImageView *iconImageView;

@property (nonatomic ,strong) UILabel *nameLabel;

@property (nonatomic ,strong) UILabel *creatTimeLabel;

@property (nonatomic ,strong) UILabel *hotelNameLabel;

@property (nonatomic ,strong) UILabel *hotelAddressLabel;

@property (nonatomic ,strong) UILabel *orderStatusLabel;

@end

@implementation CJCInviteRecordCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{

//    UIView *topView = [[UIView alloc] init];
//
//    topView.backgroundColor = [UIColor groupTableViewBackgroundColor];
//
//    topView.frame = CGRectMake(0, 0, SCREEN_WITDH, kAdaptedValue(10));
//
//    [self.contentView addSubview:topView];
    
//    UIView *topLineView = [UIView getLineView];
//
//    topLineView.frame = CGRectMake(0, 0, SCREEN_WITDH, OnePXLineHeight);
//
//    [self.contentView addSubview:topLineView];
    
    UIImageView *iconView = [[UIImageView alloc] init];
    
    iconView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    iconView.frame = CGRectMake(kAdaptedValue(16), 11, kAdaptedValue(99), kAdaptedValue(99));
    iconView.centerY = kAdaptedValue(75);
    
    self.iconImageView = iconView;
    [self.contentView addSubview:iconView];
    
    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:18 color:[UIColor toUIColorByStr:@"4A4A4A"]];
    
    nameLabel.frame = CGRectMake(iconView.right+kAdaptedValue(22), kAdaptedValue(26), SCREEN_WITDH-kAdaptedValue(200), kAdaptedValue(19));
    
    self.nameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UILabel *timeLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"666666"]];
    
    timeLabel.frame = CGRectMake(nameLabel.x, nameLabel.bottom+kAdaptedValue(8), nameLabel.width, kAdaptedValue(15));
    
    self.creatTimeLabel = timeLabel;
    [self.contentView addSubview:timeLabel];
    
    UIView *middleLineView = [UIView getLineView];
    
    middleLineView.frame = CGRectMake(nameLabel.x, timeLabel.bottom+kAdaptedValue(11), SCREEN_WITDH - kAdaptedValue(155), OnePXLineHeight);
    
    [self.contentView addSubview:middleLineView];
    
    UILabel *hotelName = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"666666"]];
    
    hotelName.frame = CGRectMake(nameLabel.x, middleLineView.bottom+kAdaptedValue(11), nameLabel.width, kAdaptedValue(15));
    
    self.hotelNameLabel = hotelName;
    [self.contentView addSubview:hotelName];
    
    UILabel *addressLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:11 color:[UIColor toUIColorByStr:@"666666"]];
    
    addressLabel.frame = CGRectMake(nameLabel.x, hotelName.bottom+kAdaptedValue(8), nameLabel.width, kAdaptedValue(12));
    
    self.hotelAddressLabel = addressLabel;
    [self.contentView addSubview:addressLabel];
    
    UILabel *statusLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:13 color:[UIColor toUIColorByStr:@"666666"]];
    
    statusLabel.textAlignment = NSTextAlignmentRight;
    
    statusLabel.frame = CGRectMake(200, kAdaptedValue(26), kAdaptedValue(50), kAdaptedValue(14));
    statusLabel.right = SCREEN_WITDH - kAdaptedValue(16);
    
    self.orderStatusLabel = statusLabel;
    [self.contentView addSubview:statusLabel];
    
//    UIView *bottomLineView = [UIView getLineView];
    
//    bottomLineView.frame = CGRectMake(0, kAdaptedValue(140), SCREEN_WITDH, OnePXLineHeight);
    
//    [self.contentView addSubview:bottomLineView];
}

-(void)setRecordModel:(CJCInviteRecordModel *)recordModel{
    
//    [self getPartnerInfoWithPartnerID:recordModel.partnerId];
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (localInfoModel.sex == 0) {
        
        self.nameLabel.text = recordModel.userTrueName;
        
        [self.iconImageView setImageURL:[NSURL URLWithString:recordModel.userImageUrl]];
    }else{
    
        self.nameLabel.text = recordModel.partnerTrueName;
        
        [self.iconImageView setImageURL:[NSURL URLWithString:recordModel.partnerImageUrl]];
    }
    
    NSString *timeStr = [CJCTools getDateTimeStrFromMilliSeconds:recordModel.createTime];
    
    self.creatTimeLabel.text = timeStr;
    
    self.hotelNameLabel.text = recordModel.hotelName;
    
    self.hotelAddressLabel.text = recordModel.hotelName;
    
    switch (recordModel.status) {
        case 0:{
            
            self.orderStatusLabel.text = @"待支付";
            
        }
            break;
            
        case 10:{
            
            self.orderStatusLabel.text = @"待回应";
            
        }
            break;
            
        case 20:{
            
            self.orderStatusLabel.text = @"已应邀";
    
        }
            break;
            
        case 30:{
            
            self.orderStatusLabel.text = @"已到达";
            
        }
            break;
            
        case 40:{
            
            if (localInfoModel.sex == 0) {
                
                if (recordModel.partnerCommentId==0) {
                    
                    self.orderStatusLabel.text = @"待评价";
                    
                }else{
                    
                    self.orderStatusLabel.text = @"已结束";
        
                }
                
            }else{
                
                if (recordModel.userCommentId==0) {
                    
                    self.orderStatusLabel.text = @"待评价";
                    
                }else{
                    
                    self.orderStatusLabel.text = @"已结束";
    
                }
                
            }
            
//            self.orderStatusLabel.text = @"待评价";
            
        }
            break;
            
        case 50:{
            
            self.orderStatusLabel.text = @"已结束";
    
        }
            break;
            
        case 110:{
            
            self.orderStatusLabel.text = @"已取消";
        }
            break;
            
        case 120:{
            
            self.orderStatusLabel.text = @"已取消";
        
        }
            break;
            
        default:
            break;
    }
}

@end

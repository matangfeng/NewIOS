//
//  CJCEditInfoTopCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CJCEditInfoTopCellDelegate <NSObject>

-(void)changeIconHandle;

@end

@interface CJCEditInfoTopCell : UITableViewCell

@property (nonatomic ,weak) id<CJCEditInfoTopCellDelegate> delegate;

@property (nonatomic ,copy) NSString *iconURL;

@end

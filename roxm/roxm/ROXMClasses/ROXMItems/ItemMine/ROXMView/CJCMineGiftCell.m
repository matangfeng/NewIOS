//
//  CJCMineGiftCell.m
//  roxm
//
//  Created by 陈建才 on 2017/10/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMineGiftCell.h"
#import "CJCCommon.h"
#import "CJCInviteGiftModel.h"

@interface CJCMineGiftCell ()

@property (nonatomic ,strong) UIImageView *giftImageView;

@property (nonatomic ,strong) UILabel *giftNameLabel;

@property (nonatomic ,strong) UIView *shadowView;

@property (nonatomic ,strong) UILabel *statusLabel;

@end

@implementation CJCMineGiftCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{
    
    UIImageView *giftView = [[UIImageView alloc] init];
    
    giftView.frame = CGRectMake(kAdaptedValue(14), kAdaptedValue(14), kAdaptedValue(64), kAdaptedValue(64));
    
    self.giftImageView = giftView;
    [self.contentView addSubview:giftView];
    
    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    nameLabel.textAlignment = NSTextAlignmentCenter;
    
    nameLabel.frame = CGRectMake(0, giftView.bottom+kAdaptedValue(12), SCREEN_WITDH/4, kAdaptedValue(19));
    
    self.giftNameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
}

-(void)setGiftModel:(CJCInviteGiftModel *)giftModel{
    
//    NSString *thumnailStr = [NSString stringWithFormat:@"?imageView2/2/w/%d/q/50",(int)SCREEN_WITDH/4];
    
//    NSString *thumbleImage = [NSString stringWithFormat:@"%@%@",giftModel.url,thumnailStr];
    
    [self.giftImageView setImageURL:[NSURL URLWithString:giftModel.url]];
    self.giftNameLabel.text = giftModel.name;
    
    if (giftModel.status == 0) {
        
        self.shadowView.hidden = YES;
    }else{
    
        self.shadowView.hidden = NO;
        
        if (giftModel.status == 10) {
            
            self.statusLabel.text = @"冻结中";
        }else if (giftModel.status == 20){
        
            self.statusLabel.text = @"已转出";
        }
    }
}

-(UIView *)shadowView{

    if (_shadowView == nil) {
        
        UIView *view = [[UIView alloc] init];
        
        view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        ;
        
        view.frame = self.giftImageView.frame;
        
        [self.contentView addSubview:view];
        
        _shadowView = view;
        
        UILabel *status = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:15 color:[UIColor whiteColor]];
        
        status.textAlignment = NSTextAlignmentCenter;
        
        status.size = CGSizeMake(view.width, kAdaptedValue(18));
        
        status.center = CGPointMake(view.width/2, view.height/2);
        
        self.statusLabel = status;
        [view addSubview:status];
    }
    return _shadowView;
}

@end

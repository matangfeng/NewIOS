//
//  CJCBanlanceModel.h
//  roxm
//
//  Created by 陈建才 on 2017/10/24.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CJCBanlanceModel : NSObject

@property (nonatomic ,copy) NSString *alipay;

@property (nonatomic ,copy) NSString *alipayUid;

@property (nonatomic ,assign) CGFloat amount;

@property (nonatomic ,assign) NSInteger status;

@property (nonatomic ,copy) NSString *trueName;

@property (nonatomic ,copy) NSString *uid;

@property (nonatomic ,copy) NSString *weixin;

@end

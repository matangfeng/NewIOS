//
//  CJCIncomeDetailModel.h
//  roxm
//
//  Created by 陈建才 on 2017/10/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CJCIncomeDetailModel : NSObject

@property (nonatomic ,copy) NSString *id;

@property (nonatomic ,copy) NSString *userId;

@property (nonatomic ,copy) NSString *name;

@property (nonatomic ,assign) CGFloat price;

@property (nonatomic ,copy) NSString *balance;

@property (nonatomic ,assign) long createTime;

@property (nonatomic ,assign) NSInteger status;

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,copy) NSString *imageUrl;

@end

//
//  CJCInviteRecordModel.h
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CJCInviteRecordModel : NSObject

@property (nonatomic ,copy) NSString *IDd;

@property (nonatomic ,copy) NSString *userId;

@property (nonatomic ,copy) NSString *partnerId;

@property (nonatomic ,copy) NSString *hotelName;

@property (nonatomic ,copy) NSString *lng;

@property (nonatomic ,copy) NSString *lat;

@property (nonatomic ,assign) long long createTime;

@property (nonatomic ,assign) NSInteger status;

@property (nonatomic ,assign) NSInteger userCommentId;

@property (nonatomic ,assign) NSInteger partnerCommentId;

///男士
@property (nonatomic ,copy) NSString *userImageUrl;

@property (nonatomic ,copy) NSString *userTrueName;

///女士
@property (nonatomic ,copy) NSString *partnerImageUrl;

@property (nonatomic ,copy) NSString *partnerTrueName;

@end

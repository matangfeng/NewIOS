//
//  CJCToolBarMoreView.h
//  roxm
//
//  Created by 陈建才 on 2017/9/9.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CJCToolBarMoreViewDelegate <NSObject>

-(void)buttonInMoreViewClick:(NSInteger)buttonIndx;

@end

@interface CJCToolBarMoreView : UIView

@property (nonatomic ,weak) id<CJCToolBarMoreViewDelegate> delegate;

@end

//
//  CJCToolBarMoreButton.m
//  roxm
//
//  Created by 陈建才 on 2017/9/10.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCToolBarMoreButton.h"
#import "CJCCommon.h"

#define   kImageLabelRatio   0.5

@implementation CJCToolBarMoreButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.imageView.contentMode = UIViewContentModeCenter;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        self.titleLabel.font = [UIFont accodingVersionGetFont_lightWithSize:13];
        [self setTitleColor:[UIColor toUIColorByStr:@"4F4F4F"] forState:UIControlStateNormal];
    }
    return self;
}

//重新布局button的imageview 和label
-(CGRect)imageRectForContentRect:(CGRect)contentRect{
    
    CGFloat imageWidth = contentRect.size.width;
    CGFloat imageHeight = contentRect.size.height*kImageLabelRatio;
    
    CGRect newImageRect = CGRectMake(0, 0, imageWidth, imageHeight);
    
    return newImageRect;
}

-(CGRect)titleRectForContentRect:(CGRect)contentRect{
    
    CGFloat titleY = contentRect.size.height*kImageLabelRatio + 4;
    CGFloat titleWidth = contentRect.size.width;
    CGFloat titleHeight = 14;
    
    CGRect newImageRect = CGRectMake(0, titleY, titleWidth, titleHeight);
    
    return newImageRect;
}

@end

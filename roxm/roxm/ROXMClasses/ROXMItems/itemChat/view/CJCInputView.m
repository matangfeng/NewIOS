//
//  CJCInputView.m
//  roxm
//
//  Created by 陈建才 on 2017/9/8.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInputView.h"
#import "CJCCommon.h"
#import "CJCToolBarAudioView.h"
#import "CJCToolBarMoreView.h"
#import "CJCChatMessageHandle.h"
#import "CJCEmojiView.h"
#import "CJCEmojiHandle.h"
#import "CJCMessageHandle.h"
#import <EaseUI.h>

#define kAUDIOVIEWHEIGHT    161
#define kMAXAUDIOLENGTH     60
#define kMaxStringLength    140
#define kMinInputBarHeight  45.0f
#define kNaviHeight         64.0f

@interface CJCInputView ()<UITextFieldDelegate,EMFaceDelegate,UITextViewDelegate,CJCEmojiViewDelegate>{

    CGFloat keyboardHeight;
    
    NSTimer *takeAudioTimer;
    
    CGFloat audioLength;
    
    NSInteger _textInputMaxHeight;
    
    ZInputbarPosition _barPositionType;
    
    NSDictionary * _textAttributDic;
    
    CGFloat _currentInputHeight;
    
    CGFloat oldInputViewHeight;
    
    BOOL  isTypeText;
}

@property (nonatomic ,strong) UIView *containView;

@property (nonatomic ,strong) CJCToolBarAudioView *audioView;

@property (nonatomic ,strong) UIView *hintView;

@property (nonatomic ,strong) UILabel *leftHintLabel;

@property (nonatomic ,strong) UILabel *rightHintLable;

@property (nonatomic ,strong) UIView *showingView;

@property (strong, nonatomic) UITextView *inputTextView;

@property (strong, nonatomic) UILabel *placeHolderLabel;        // 无输入时 默认提示

@property (nonatomic ,strong) CJCEmojiView *emojiView;

@property (nonatomic ,strong) UIButton *moreButton;

@property (nonatomic ,strong) UIButton *audioButton;

@property (nonatomic ,strong) UIButton *emojButton;

@end

@implementation CJCInputView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor toUIColorByStr:@"FFFFFF"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)name:UIKeyboardWillHideNotification object:nil];
        
        _textAttributDic = [NSDictionary dictionaryWithObjectsAndKeys:
                            [UIFont accodingVersionGetFont_regularWithSize:15], NSFontAttributeName,
                            [UIColor toUIColorByStr:@"3C3C3C"], NSForegroundColorAttributeName,nil];
        
        audioLength = 0.0;
        isTypeText = YES;
        
        [self setUpUI];
    }
    return self;
}

-(void)inputViewShrink{
    
    [self endEditing:YES];
    
    self.audioButton.selected = NO;
    self.audioView.y = 261+_currentInputHeight;
    self.emojButton.selected = NO;
    self.emojiView.y = 261+_currentInputHeight;
    self.moreButton.selected = NO;
    self.moreView.y = 261+_currentInputHeight;
    
    if (_currentInputHeight > 60) {
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT -_currentInputHeight- 64, SCREEN_WITDH, _currentInputHeight) ;
    }else{
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT -60- 64, SCREEN_WITDH, 60) ;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [self.delegate chatToolbarDidChangeFrameToHeight:self.frame.size.height];
    }
}

-(void)setUpUI{

    UIView *containView = [[UIView alloc] init];
    
    containView.frame = CGRectMake(0, 0, SCREEN_WITDH, 60);
    
    self.containView = containView;
    [self addSubview:containView];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(0, 0, SCREEN_WITDH, OnePXLineHeight);
    
    [containView addSubview:lineView];
    
    UIButton *audioButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [audioButton setImage:kGetImage(@"message_sentvoice") forState:UIControlStateNormal];
    
    audioButton.frame = CGRectMake(10, 10, 40, 40);
    
    [audioButton addTarget:self action:@selector(audioButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.audioButton = audioButton;
    [containView addSubview:audioButton];
    
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [moreButton setImage:kGetImage(@"message_add") forState:UIControlStateNormal];
    
    moreButton.frame = CGRectMake(SCREEN_WITDH - 48, 10, 40, 40);
    
    [moreButton addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.moreButton = moreButton;
    [containView addSubview:moreButton];
    
    UIButton *emojButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    emojButton.accessibilityIdentifier = @"face";
    
    [emojButton setImage:kGetImage(@"message_face") forState:UIControlStateNormal];
    
    emojButton.frame = CGRectMake(moreButton.left-42, 10, 40, 40);
    
    [emojButton addTarget:self action:@selector(emojButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.emojButton = emojButton;
    [containView addSubview:emojButton];
    
    _inputTextView = [[UITextView alloc]initWithFrame:CGRectMake(59.5, 7.5, SCREEN_WITDH-159.5, 45)];
    _inputTextView.font = [UIFont accodingVersionGetFont_regularWithSize:15];
    _inputTextView.textColor = [UIColor toUIColorByStr:@"3C3C3C"];
    
//    _inputTextView.attributedText = [[NSAttributedString alloc]initWithString:@"" attributes:_textAttributDic];
    
    _inputTextView.returnKeyType = UIReturnKeySend;
    _inputTextView.enablesReturnKeyAutomatically = YES;
    _inputTextView.delegate = self;
    
    _inputTextView.backgroundColor = [UIColor toUIColorByStr:@"F8F8F8"];
    
    _inputTextView.textContainerInset = UIEdgeInsetsMake(14, 14, 14, 10);  // 光标 上下位置
    
//    [_inputTextView addSubview:self.placeHolderLabel];
    [containView addSubview:_inputTextView];
    
    _textInputMaxHeight = ceil(self.inputTextView.font.lineHeight * 4 + self.inputTextView.textContainerInset.top + self.inputTextView.textContainerInset.bottom);
}

- (UILabel *)placeHolderLabel {
    if (!_placeHolderLabel) {
        
        _placeHolderLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.inputTextView.width - 20, 40)];
        
        _placeHolderLabel.adjustsFontSizeToFitWidth = YES;
        _placeHolderLabel.font = [UIFont accodingVersionGetFont_lightWithSize:13];
        _placeHolderLabel.minimumScaleFactor = 0.9;
        _placeHolderLabel.textColor = [UIColor toUIColorByStr:@"333333"];
        _placeHolderLabel.userInteractionEnabled = NO;
        _placeHolderLabel.text = @"说点什么...";
    }
    return _placeHolderLabel;
}

#pragma mark - DXFaceDelegate
- (void)selectedFacialView:(NSString *)str isDelete:(BOOL)isDelete
{
    
    isTypeText = NO;
    
    NSString *chatText = self.inputTextView.text;
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithAttributedString:self.inputTextView.attributedText];
    
    if (!isDelete && str.length > 0) {
        NSRange range = [self.inputTextView selectedRange];
        [attr insertAttributedString:[[EaseEmotionEscape sharedInstance] attStringFromTextForInputView:str textFont:self.inputTextView.font] atIndex:range.location];
        self.inputTextView.attributedText = attr;
    }
    else {
        if (chatText.length > 0) {
            NSInteger length = 1;
            if (chatText.length >= 2) {
                NSString *subStr = [chatText substringFromIndex:chatText.length-2];
                if ([EaseEmoji stringContainsEmoji:subStr]) {
                    length = 2;
                }
            }
            self.inputTextView.attributedText = [self backspaceText:attr length:length];
        }
    }
    
    [self textViewDidChange:self.inputTextView];
}

/*!
 @method
 @brief 删除文本光标前长度为length的字符串
 @discussion
 @param attr   待修改的富文本
 @param length 字符串长度
 @result   修改后的富文本
 */
-(NSMutableAttributedString*)backspaceText:(NSMutableAttributedString*) attr length:(NSInteger)length
{
    NSRange range = [self.inputTextView selectedRange];
    if (range.location == 0) {
        return attr;
    }
    [attr deleteCharactersInRange:NSMakeRange(range.location - length, length)];
    return attr;
}

- (void)sendFace
{
    NSString *chatText = self.inputTextView.text;
    if (chatText.length > 0) {
        if ([self.delegate respondsToSelector:@selector(didSendText:)]) {
            
            if (![_inputTextView.text isEqualToString:@""]) {
                
                //转义回来
                NSMutableString *attStr = [[NSMutableString alloc] initWithString:self.inputTextView.attributedText.string];
                [_inputTextView.attributedText enumerateAttribute:NSAttachmentAttributeName
                                                          inRange:NSMakeRange(0, self.inputTextView.attributedText.length)
                                                          options:NSAttributedStringEnumerationReverse
                                                       usingBlock:^(id value, NSRange range, BOOL *stop)
                 {
                     if (value) {
                         EMTextAttachment* attachment = (EMTextAttachment*)value;
                         NSString *str = [NSString stringWithFormat:@"%@",attachment.imageName];
                         [attStr replaceCharactersInRange:range withString:str];
                     }
                 }];
                [self.delegate didSendText:attStr];
                self.inputTextView.text = @"";
                
            }
        }
    }
}

- (void)sendFaceWithEmotion:(EaseEmotion *)emotion
{
    if (emotion) {
        if ([self.delegate respondsToSelector:@selector(didSendText:withExt:)]) {
            [self.delegate didSendText:emotion.emotionTitle withExt:@{EASEUI_EMOTION_DEFAULT_EXT:emotion}];
            
        }
    }
}

#pragma mark  CJCEmojiViewDelegate
-(void)emojiItemDidClick:(NSString *)emojiStr{

    isTypeText = NO;
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithAttributedString:self.inputTextView.attributedText];
    
    if (attr.length > kMaxStringLength)
    {
        return;
    }
    
    if (emojiStr.length > 0) {
        NSRange range = [self.inputTextView selectedRange];
        
        NSAttributedString *strCodeValue_Mut = [[NSAttributedString alloc]initWithString:emojiStr attributes:_textAttributDic];
        
        [attr insertAttributedString:strCodeValue_Mut atIndex:range.location];
        self.inputTextView.attributedText = attr.copy;
    }
    
    [self textViewDidChange:self.inputTextView];
}

-(void)deleteItemDidClick{

    NSString *chatText = self.inputTextView.text;
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithAttributedString:self.inputTextView.attributedText];
    
    if (chatText.length > 0) {
        NSInteger length = 1;
        if (chatText.length >= 2) {
            NSString *subStr = [chatText substringFromIndex:chatText.length-2];
            
            if ([CJCEmojiHandle stringContainsEmoji:subStr]) {
                length = 2;
            }
            
//            if ([EaseEmoji stringContainsEmoji:subStr]) {
//                length = 2;
//            }
        }
        self.inputTextView.attributedText = [self backspaceText:attr length:length];
    }
    
    [self textViewDidChange:self.inputTextView];
}

-(void)sendItemDidClick{

    if ([self.delegate respondsToSelector:@selector(didSendText:withExt:)]) {
        
        NSDictionary *ext = [CJCMessageHandle getIconAndNameMessageExpand];
        
        [self.delegate didSendText:self.inputTextView.text withExt:ext];
        
        self.inputTextView.text = @"";
//        self.placeHolderLabel.hidden = NO;
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT-180 - 64-60, SCREEN_WITDH, 60 + 180);
        
        _currentInputHeight = 60;
        
        [self textViewDidChange:self.inputTextView];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
            [self.delegate chatToolbarDidChangeFrameToHeight:self.height];
        }
    }
}

#pragma mark textView
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    isTypeText = YES;
    
    self.audioButton.selected = NO;
    self.audioView.y = 261+_currentInputHeight;
    self.emojButton.selected = NO;
    self.emojiView.y = 261+_currentInputHeight;
    self.moreButton.selected = NO;
    self.moreView.y = 261+_currentInputHeight;
}

- (void)textViewDidChange:(UITextView *)textView
{
    
    if (textView.text.length > kMaxStringLength) {
        textView.text = [textView.text substringToIndex:kMaxStringLength];
    }
    
    // placeHolder
//    self.placeHolderLabel.hidden = textView.text.length ? YES : NO ;
    
    float textInputHeight = ceilf([self.inputTextView sizeThatFits:CGSizeMake(self.inputTextView.bounds.size.width, MAXFLOAT)].height);
    
    if (oldInputViewHeight != 0) {
        
        if (oldInputViewHeight < textInputHeight) {
            
            _inputTextView.textContainerInset = UIEdgeInsetsMake(7, 14, 7, 10);
            
            textInputHeight = ceilf([self.inputTextView sizeThatFits:CGSizeMake(self.inputTextView.bounds.size.width, MAXFLOAT)].height);
        }
    }
    
    oldInputViewHeight = textInputHeight;
    
    self.inputTextView.scrollEnabled = textInputHeight > _textInputMaxHeight && _textInputMaxHeight > 0;
    
    textInputHeight = textInputHeight > kMinInputBarHeight ? textInputHeight : kMinInputBarHeight;
    textInputHeight = textInputHeight < _textInputMaxHeight ? textInputHeight : _textInputMaxHeight;
    
    
    self.inputTextView.height = textInputHeight;
    
    
    if (isTypeText) {
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT - keyboardHeight - 84-textInputHeight, SCREEN_WITDH, textInputHeight+20 + keyboardHeight);
        
        _currentInputHeight = textInputHeight+20;
    }else{
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT - 180 - 84-textInputHeight, SCREEN_WITDH, textInputHeight+20 + 180);
        
        _currentInputHeight = textInputHeight+20;
    
        self.emojiView.y = _currentInputHeight;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [self.delegate chatToolbarDidChangeFrameToHeight:self.height];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //默认键盘删除键
    if ([text isEqualToString:@""]) {
        //确定光标位置并且删除
        NSInteger location = self.inputTextView.selectedRange.location;
        NSString *string = [self.inputTextView.text substringToIndex:location];
        if ([string hasSuffix:@"]"]) {
            for (NSInteger i = string.length - 1; i >= 0; i --) {
                char c = [string characterAtIndex:i];
                if (c == '[') {
                    break;
                }
                [self.inputTextView deleteBackward];
            }
        } else {
            //[self.inputTextView deleteBackward];
        }
        [self textViewDidChange:self.inputTextView];
    }//键盘默认发送键
    else if ([text isEqualToString:@"\n"]) {
        
        if ([self.delegate respondsToSelector:@selector(didSendText:withExt:)]) {
            
            NSDictionary *ext = [CJCMessageHandle getIconAndNameMessageExpand];
            
            [self.delegate didSendText:textView.text withExt:ext];
            self.inputTextView.text = @"";
//            self.placeHolderLabel.hidden = NO;
            
            self.frame = CGRectMake(0, SCREEN_HEIGHT - keyboardHeight - 64-60, SCREEN_WITDH, 60 + keyboardHeight);
            
            _currentInputHeight = 60;
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
                [self.delegate chatToolbarDidChangeFrameToHeight:self.height];
            }
        }
        return NO;
    }
    
    return YES;
}

#pragma mark =====更多按钮的点击
-(void)moreButtonClick:(UIButton *)button{
    
    [self endEditing:YES];
    
    self.moreView.hidden = NO;
    
    self.audioButton.selected = NO;
    self.audioView.y = 261+_currentInputHeight;
    self.audioView.hidden = YES;
    self.emojButton.selected = NO;
    self.emojiView.y = 261+_currentInputHeight;
    self.emojiView.hidden = YES;
    
    button.selected = !button.selected;
    
    if (button.selected) {
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.frame = CGRectMake(0, SCREEN_HEIGHT -60- 64-93, SCREEN_WITDH, 60 + 93) ;
            
            self.moreView.y = 60;
        }];
    }else{
        
        [self.inputTextView becomeFirstResponder];
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.moreView.y = 261+_currentInputHeight;
        }];
        
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [self.delegate chatToolbarDidChangeFrameToHeight:self.frame.size.height];
    }
}

#pragma mark =====表情按钮的点击
-(void)emojButtonClick:(UIButton *)button{
    
    [self endEditing:YES];
    
    self.emojiView.hidden = NO;
    
    self.moreButton.selected = NO;
    self.moreView.y = 261+_currentInputHeight;
    self.moreView.hidden = YES;
    self.audioButton.selected = NO;
    self.audioView.y = 261+_currentInputHeight;
    self.audioView.hidden = YES;
    
    button.selected = !button.selected;
    
    if (button.selected) {
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.frame = CGRectMake(0, SCREEN_HEIGHT -60- 64-180, SCREEN_WITDH, 60 + 180) ;
            
            self.emojiView.y = 60;
        }];
        
    }else{
        
        [self.inputTextView becomeFirstResponder];
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.emojiView.y = 261+_currentInputHeight;
        }];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [self.delegate chatToolbarDidChangeFrameToHeight:self.frame.size.height];
    }
}

#pragma mark ====语音按钮的点击
-(void)audioButtonClick:(UIButton *)button{
    
    [self endEditing:YES];
    
    self.audioView.hidden = NO;
    
    self.moreButton.selected = NO;
    self.moreView.y = 261+_currentInputHeight;
    self.moreView.hidden = YES;
    self.emojButton.selected = NO;
    self.emojiView.y = 261+_currentInputHeight;
    self.emojiView.hidden = YES;
    
    button.selected = !button.selected;
    
    if (button.selected) {
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.frame = CGRectMake(0, SCREEN_HEIGHT -60- 64-kAUDIOVIEWHEIGHT, SCREEN_WITDH, 60 + kAUDIOVIEWHEIGHT) ;
            
            self.audioView.y = 60;
        }];
        
    }else{
        
        [self.inputTextView becomeFirstResponder];
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.audioView.y = 261+_currentInputHeight;
        }];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [self.delegate chatToolbarDidChangeFrameToHeight:self.frame.size.height];
    }
}

#pragma mark  =====监听键盘的弹起 收回
- (void)keyboardWillShow:(NSNotification *)aNotification{
    //获取键盘的高度
    NSValue *aValue = [[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    if (keyboardRect.size.height > keyboardHeight) {
        keyboardHeight = keyboardRect.size.height;
    }
    
    if (_currentInputHeight > 60) {
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT -_currentInputHeight- 64- keyboardHeight, SCREEN_WITDH, _currentInputHeight+ keyboardHeight) ;
    }else{
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT - keyboardHeight - 64-60, SCREEN_WITDH, 60 + keyboardHeight);
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [self.delegate chatToolbarDidChangeFrameToHeight:self.frame.size.height];
    }
}

- (void)keyboardWillHide:(NSNotification *)aNotification{
    
    if (_currentInputHeight > 60) {
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT -_currentInputHeight- 64, SCREEN_WITDH, _currentInputHeight) ;
    }else{
    
        self.frame = CGRectMake(0, SCREEN_HEIGHT -60- 64, SCREEN_WITDH, 60) ;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [self.delegate chatToolbarDidChangeFrameToHeight:self.frame.size.height];
    }
}

#pragma mark ======底部操作view的懒加载
-(CJCToolBarMoreView *)moreView{

    if (_moreView == nil) {
        
        CJCToolBarMoreView *view = [[CJCToolBarMoreView alloc] init];
        
        view.frame = CGRectMake(0, 261+_currentInputHeight, SCREEN_WITDH, 100);
        
        [self addSubview:view];
        _moreView = view;
    }
    return _moreView;
}

-(CJCEmojiView *)emojiView{

    if (_emojiView == nil) {
        
        CJCEmojiView *emoji = [[CJCEmojiView alloc] initWithFrame:CGRectMake(0, 261+_currentInputHeight, SCREEN_WITDH, 180)];
        
        emoji.delegate = self;
        [self addSubview:emoji];
        _emojiView = emoji;
    }
    return _emojiView;
}

-(UIView *)hintView{

    if (_hintView == nil) {
        
        UIView *view = [[UIView alloc] init];
        
        view.backgroundColor = [UIColor whiteColor];
        
        view.frame = CGRectMake(0, 0, SCREEN_WITDH, 60);
        
        [self addSubview:view];
        _hintView = view;
        
        UILabel *leftHintLabel = [UIView getYYLabelWithStr:@"上滑取消" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"838383"]];
        
        leftHintLabel.textAlignment = NSTextAlignmentRight;
        
        leftHintLabel.frame = CGRectMake(0, 20, SCREEN_WITDH/2+kAdaptedValue(25), 20);
        
        self.leftHintLabel = leftHintLabel;
        [view addSubview:leftHintLabel];
        
        UILabel *rightHintLabel = [UIView getYYLabelWithStr:@"0\"" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"1D0000"]];
        
        rightHintLabel.textAlignment = NSTextAlignmentLeft;
        
        rightHintLabel.frame = CGRectMake(SCREEN_WITDH/2+kAdaptedValue(27), 20, SCREEN_WITDH, 20);
        
        self.rightHintLable = rightHintLabel;
        [view addSubview:rightHintLabel];
    }
    return _hintView;
}

-(CJCToolBarAudioView *)audioView{

    if (_audioView == nil) {
        
        CJCToolBarAudioView *audio = [[CJCToolBarAudioView alloc] init];
        
        audio.frame = CGRectMake(0, 261+_currentInputHeight, SCREEN_WITDH, kAUDIOVIEWHEIGHT);
        
        [audio.MicrophoneButton addTarget:self action:@selector(beginTakeAudio) forControlEvents:UIControlEventTouchDown];
        [audio.MicrophoneButton addTarget:self action:@selector(cancaleAudio) forControlEvents:UIControlEventTouchDragOutside];
        [audio.MicrophoneButton addTarget:self action:@selector(finishTakeAudio) forControlEvents:UIControlEventTouchUpInside];
        [audio.MicrophoneButton addTarget:self action:@selector(cancleAndFinishAudio) forControlEvents:UIControlEventTouchUpOutside];
//        [audio.MicrophoneButton addTarget:self action:@selector(beginTakeAudio) forControlEvents:UIControlEventTouchDragInside];
        
        [self addSubview:audio];
        _audioView = audio;
    }
    return _audioView;
}


-(void)audioAuthorityIsOpen{
    
    AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if (authStatus == AVAuthorizationStatusAuthorized)
    {
        
        [self authorityOpenBeginTakeAudio];
        
    }else{
        
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            
            if (granted) {
                
            } else {
                
                // 用户不同意获取麦克风
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"发送语音需要开启手机的麦克风权限" message:nil preferredStyle:  UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        
                        if([[UIApplication sharedApplication] canOpenURL:url]) {
                            
                            NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                            [[UIApplication sharedApplication] openURL:url];
                            
                        }
                        
                    }]];
                    
                    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        
                    }]];
                    
                    UIWindow *mainWindow = [UIApplication sharedApplication].keyWindow;
                    
                    [mainWindow.rootViewController presentViewController:alert animated:true completion:nil];
                    
                });
            }
            
        }];
    }
}

-(void)beginTakeAudio{

    [self audioAuthorityIsOpen];
}

-(void)authorityOpenBeginTakeAudio{

    if (takeAudioTimer == nil) {
        
        takeAudioTimer = [NSTimer timerWithTimeInterval:0.1 target:self selector:@selector(takingAudio) userInfo:nil repeats:YES];
        
        [[NSRunLoop mainRunLoop] addTimer:takeAudioTimer forMode:NSRunLoopCommonModes];
    }
    
    self.hintView.hidden = NO;
}

-(void)takingAudio{
    
    audioLength = audioLength + 0.1;
    
    NSString *leftStr = @"上滑取消";
    NSString *rightStr = [NSString stringWithFormat:@"%.0f\"",audioLength];
    
    self.leftHintLabel.text = leftStr;
    self.rightHintLable.text = rightStr;
    
    if ([self.delegate respondsToSelector:@selector(didStartRecordingVoiceAction:)]) {
        
        [self.delegate didStartRecordingVoiceAction:nil];
    }
}

-(void)cancaleAudio{

    if (takeAudioTimer!= nil) {
        
        [takeAudioTimer invalidate];
        takeAudioTimer = nil;
        audioLength = 0;
    }
    
    self.leftHintLabel.textColor = [UIColor toUIColorByStr:@"EC4E4E"];
    self.rightHintLable.textColor = [UIColor toUIColorByStr:@"EC4E4E"];
}

-(void)cancleAndFinishAudio{

    self.hintView.hidden = YES;
    self.leftHintLabel.textColor = [UIColor toUIColorByStr:@"838383"];
    self.rightHintLable.textColor = [UIColor toUIColorByStr:@"1D0000"];
    
    if ([self.delegate respondsToSelector:@selector(didCancelRecordingVoiceAction:)]) {
        
        [self.delegate didCancelRecordingVoiceAction:nil];
    }
}

-(void)finishTakeAudio{

    if (takeAudioTimer!= nil) {
        
        [takeAudioTimer invalidate];
        takeAudioTimer = nil;
        audioLength = 0;
        
        NSString *leftStr = @"上滑取消";
        NSString *rightStr = [NSString stringWithFormat:@"%.0f\"",audioLength];
        
        self.leftHintLabel.text = leftStr;
        self.rightHintLable.text = rightStr;
    }
    
    self.hintView.hidden = YES;
    
    [self finishRecoingVoice];
}

-(void)finishRecoingVoice{

    __weak typeof(self) weakSelf = self;
    [[EMCDDeviceManager sharedInstance] asyncStopRecordingWithCompletion:^(NSString *recordPath, NSInteger aDuration, NSError *error) {
        if (!error) {
            
            EMMessage *message = [CJCMessageHandle CJC_HuanxinVoiceMessage:recordPath andDurio:aDuration andTo:self.conversationId];
            
            EaseMessageViewController *messageVC = (EaseMessageViewController *)weakSelf.delegate;
            
            SEL testFunc = NSSelectorFromString(@"_sendMessage:");
            
            ((void(*)(id,SEL, id,id))objc_msgSend)(messageVC, testFunc, message, nil);
        }
    }];

}

-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

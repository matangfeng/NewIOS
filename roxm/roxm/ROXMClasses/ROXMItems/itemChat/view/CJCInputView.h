//
//  CJCInputView.h
//  roxm
//
//  Created by 陈建才 on 2017/9/8.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EaseUI.h>

typedef NS_ENUM(NSUInteger, ZInputbarPosition) {
    ZInputbarPosition_ScreenOut,
    ZInputbarPosition_ScreenBottom,
};

@class CJCToolBarMoreView;

@interface CJCInputView : UIView

@property (nonatomic ,copy) NSString *conversationId;

@property (nonatomic ,weak) id<EMChatToolbarDelegate> delegate;

@property (nonatomic ,strong) CJCToolBarMoreView *moreView;

-(void)inputViewShrink;

@end

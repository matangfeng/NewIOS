//
//  CJCEmojiCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCEmojiCell.h"
#import "CJCTools.h"
#import "CJCCommon.h"

@interface CJCEmojiCell ()

@property (nonatomic ,strong) UIButton *itemButton;

@end

@implementation CJCEmojiCell

-(instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)emojiButtonClick{

    if ([self.delegate respondsToSelector:@selector(emojiButtonClick:)]) {
        
        [self.delegate emojiButtonClick:self.emojiStr];
    }
}

-(void)setUpUI{

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.titleLabel.font = [UIFont fontWithName:@"AppleColorEmoji" size:34];
    
    self.itemButton = button;
    [self.contentView addSubview:button];
    
    [button addTarget:self action:@selector(emojiButtonClick) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setEmojiStr:(NSString *)emojiStr{

    _emojiStr = emojiStr;
    
    self.itemButton.frame = self.contentView.bounds;
    
    [self.itemButton setTitle:emojiStr forState:UIControlStateNormal];
    [self.itemButton setImage:nil forState:UIControlStateNormal];
}

-(void)setImageName:(NSString *)imageName{

    _emojiStr = nil;
    self.itemButton.frame = self.contentView.bounds;
    [self.itemButton setTitle:nil forState:UIControlStateNormal];
    [self.itemButton setImage:kGetImage(imageName) forState:UIControlStateNormal];
}

@end

//
//  CJCEmojiSendCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CJCEmojiSendCellDelegate <NSObject>

-(void)sendButtonClick;

@end

@interface CJCEmojiSendCell : UICollectionViewCell

@property (nonatomic ,copy) NSString *imageName;

@property (nonatomic ,weak) id<CJCEmojiSendCellDelegate> delegate;

@end

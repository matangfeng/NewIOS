//
//  CJCEmojiFlowLayout.m
//  roxm
//
//  Created by 陈建才 on 2017/9/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCEmojiFlowLayout.h"
#import "CJCCommon.h"

@implementation CJCEmojiFlowLayout

-(void)prepareLayout{
    [super prepareLayout];
    
    self.minimumLineSpacing = 0;
    self.minimumInteritemSpacing = 0;
    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
}

-(CGSize)collectionViewContentSize{
    
    CGFloat height = 150;
    
    CGFloat width = 2*SCREEN_WITDH;
    
    return CGSizeMake(width, height);
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{

    return YES;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    
    // 获得super已经计算好的布局属性
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    
    for (UICollectionViewLayoutAttributes *attribute in array) {
        
        NSIndexPath *index = attribute.indexPath;
        
        NSInteger row = index.row/8;
        NSInteger column = index.row%8;
        
        CGFloat width = SCREEN_WITDH/8;
        CGFloat height = 150/3;
        
        if (index.row<22) {
            
            CGRect newFrame = CGRectMake(column*width, row*height, width, height);
            
            attribute.frame = newFrame;
        }else if (index.row==22){
            
            CGRect newFrame = CGRectMake(column*width, row*height, SCREEN_WITDH/4, height);
            
            attribute.frame = newFrame;
        }else if(index.row < 42){
            
            NSInteger newIndex = index.row - 23;
            
            NSInteger row1 = newIndex/8;
            NSInteger column1 = newIndex%8;
            
            CGRect newFrame = CGRectMake(SCREEN_WITDH+column1*width, row1*height, width, height);
            
            attribute.frame = newFrame;
            
        }else if(index.row == 42){
            
            CGRect newFrame = CGRectMake(SCREEN_WITDH+5*width, 2*height, width, height);
            
            attribute.frame = newFrame;
        }else if(index.row == 43){
            
            CGRect newFrame = CGRectMake(SCREEN_WITDH+6*width, 2*height, SCREEN_WITDH/4, height);
            
            attribute.frame = newFrame;
        }
    }
    
    return array;
}

@end

//
//  CJCEmojiView.m
//  roxm
//
//  Created by 陈建才 on 2017/9/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCEmojiView.h"
#import "CJCCommon.h"
#import "CJCEmojiCell.h"
#import "CJCEmojiFlowLayout.h"
#import "CJCEmojiSendCell.h"

#define kEmojColletionHeight         150
#define kPageControlHeight           10
#define kEmojKindContainerHeight     45

#define kEmojiCell                  @"CJCEmojiCell"
#define kEmojiSendCell              @"CJCEmojiSendCell"

@interface CJCEmojiView ()< UICollectionViewDelegate, UICollectionViewDataSource,CJCEmojiSendCellDelegate,CJCEmojiCellDelegate,UIScrollViewDelegate>{

    NSMutableArray *emojis;
    
    CGPoint beginOffset;
}

@property (nonatomic,   strong)     UICollectionView * collectionView;

@property (nonatomic,   strong)     UIPageControl * pageControl;

@end

@implementation CJCEmojiView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString* strFileName = [[NSBundle mainBundle] pathForResource:@"CJC_Emoji" ofType:@"plist"];
        
        NSArray *tempArr = [NSArray arrayWithContentsOfFile:strFileName];
        emojis = [NSMutableArray array];
        
        [emojis addObjectsFromArray:tempArr];
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    CJCEmojiFlowLayout *fLayout = [[CJCEmojiFlowLayout alloc] init];
    
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), kEmojColletionHeight)collectionViewLayout:fLayout];
    collectionView.backgroundColor = [UIColor whiteColor];
    
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.pagingEnabled = YES;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.userInteractionEnabled = YES;
    
    [collectionView registerClass:[CJCEmojiCell class] forCellWithReuseIdentifier:kEmojiCell];
    [collectionView registerClass:[CJCEmojiSendCell class] forCellWithReuseIdentifier:kEmojiSendCell];
    
    [self addSubview:collectionView];
    
    UIPageControl *control = [[UIPageControl alloc] init];
    
    control.pageIndicatorTintColor = [UIColor toUIColorByStr:@"CBCBCB"];
    control.currentPageIndicatorTintColor = [UIColor toUIColorByStr:@"222222"];
    
    control.frame = CGRectMake(100, collectionView.bottom+5, 100, 19);
    control.centerX = SCREEN_WITDH/2;
    
    control.numberOfPages = 2;
    
    self.pageControl = control;
    [self addSubview:control];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    beginOffset = scrollView.contentOffset;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat xOffset = scrollView.contentOffset.x;
    
    if (xOffset - beginOffset.x>0 && xOffset - beginOffset.x<SCREEN_WITDH) {
        
        self.pageControl.currentPage = xOffset/SCREEN_WITDH+1;
    }else{
        
        self.pageControl.currentPage = xOffset/SCREEN_WITDH;
    }
}

#pragma mark ======collection的DataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return emojis.count+4;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    CGFloat width = SCREEN_WITDH/8;
    CGFloat height = 150/3;
    
    if (indexPath.item<21){
        
        return CGSizeMake(width, height);
        
    }else if(indexPath.item == 21){
        
        return CGSizeMake(width, height);
        
    }else if(indexPath.item == 22){
        
        return CGSizeMake(width*2, height);
        
    }else if (indexPath.item < 42){
        
        return CGSizeMake(width, height);
        
    }else if(indexPath.item == 42){
        
        return CGSizeMake(width, height);
        
    }else if(indexPath.item == 43){
        
        return CGSizeMake(width*2, height);
        
    }
    
    return CGSizeMake(width, height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.item<21){
    
        CJCEmojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiCell forIndexPath:indexPath];
        
        cell.emojiStr = emojis[indexPath.item];
        
        cell.delegate = self;
        return cell;
        
    }else if(indexPath.item == 21){
    
        CJCEmojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiCell forIndexPath:indexPath];
        
        cell.imageName = @"message_deleteface";
        
         cell.delegate = self;
        return cell;
        
    }else if(indexPath.item == 22){
    
        CJCEmojiSendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiSendCell forIndexPath:indexPath];
        
        cell.imageName = @"message_send_word";
        
         cell.delegate = self;
        return cell;
        
    }else if (indexPath.item < 42){
    
        CJCEmojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiCell forIndexPath:indexPath];
        
        cell.emojiStr = emojis[indexPath.item-2];
        
         cell.delegate = self;
        return cell;
        
    }else if(indexPath.item == 42){
        
        CJCEmojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiCell forIndexPath:indexPath];
        
        cell.imageName = @"message_deleteface";
        
        cell.delegate = self;
        return cell;
        
    }else if(indexPath.item == 43){
        
        CJCEmojiSendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEmojiSendCell forIndexPath:indexPath];
        
        cell.imageName = @"message_send_word";
        
         cell.delegate = self;
        return cell;
        
    }
    
    UICollectionViewCell *cell = [[UICollectionViewCell alloc] init];
    return cell;
}

-(void)emojiButtonClick:(NSString *)emojiStr{

    if (emojiStr) {
        
        if ([self.delegate respondsToSelector:@selector(emojiItemDidClick:)]) {
            
            [self.delegate emojiItemDidClick:emojiStr];
        }
    }else{
    
        if ([self.delegate respondsToSelector:@selector(deleteItemDidClick)]) {
            
            [self.delegate deleteItemDidClick];
        }
        
    }
}

-(void)sendButtonClick{

    if ([self.delegate respondsToSelector:@selector(sendItemDidClick)]) {
        
        [self.delegate sendItemDidClick];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.item<21){
    
        if ([self.delegate respondsToSelector:@selector(emojiItemDidClick:)]) {
            
            [self.delegate emojiItemDidClick:emojis[indexPath.item]];
        }
        
    }else if(indexPath.item == 21){
    
        if ([self.delegate respondsToSelector:@selector(deleteItemDidClick)]) {
            
            [self.delegate deleteItemDidClick];
        }
        
    }else if(indexPath.item == 22){
    
        if ([self.delegate respondsToSelector:@selector(sendItemDidClick)]) {
            
            [self.delegate sendItemDidClick];
        }
        
    }else if (indexPath.item < 42){
    
        if ([self.delegate respondsToSelector:@selector(emojiItemDidClick:)]) {
            
            [self.delegate emojiItemDidClick:emojis[indexPath.item-2]];
        }
        
    }else if(indexPath.item == 42){
    
        if ([self.delegate respondsToSelector:@selector(deleteItemDidClick)]) {
            
            [self.delegate deleteItemDidClick];
        }
        
    }else{
    
        if ([self.delegate respondsToSelector:@selector(sendItemDidClick)]) {
            
            [self.delegate sendItemDidClick];
        }
        
    }
}

@end

//
//  CJCToolBarAudioView.h
//  roxm
//
//  Created by 陈建才 on 2017/9/8.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCToolBarAudioView : UIView

@property (nonatomic ,strong) UIButton *MicrophoneButton;

@end

//
//  CJCToolBarMoreView.m
//  roxm
//
//  Created by 陈建才 on 2017/9/9.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCToolBarMoreView.h"
#import "CJCToolBarMoreButton.h"
#import "CJCCommon.h"

@implementation CJCToolBarMoreView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    CGFloat buttonWidth = kAdaptedValue(40);
    CGFloat margin = (SCREEN_WITDH - 4*buttonWidth)/5;
    
    for (int i=0; i<3; i++) {
        
        CJCToolBarMoreButton *button = [[CJCToolBarMoreButton alloc] init];
        
        button.frame = CGRectMake(margin+(buttonWidth+margin)*i, 15, buttonWidth, 80);
        
        button.tag = i+1;
        
        [self addSubview:button];
        
        [button addTarget:self action:@selector(moreViewButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        switch (i) {
            case 0:{
            
                [button setTitle:@"相册" forState:UIControlStateNormal];
                
                [button setImage:kGetImage(@"message_morefunction_xiangce") forState:UIControlStateNormal];
            }
                break;
                
            case 1:{
                
                [button setTitle:@"拍摄" forState:UIControlStateNormal];
                [button setImage:kGetImage(@"message_morefunction_paishe") forState:UIControlStateNormal];
            }
                break;
                
            case 2:{
                
                [button setTitle:@"位置" forState:UIControlStateNormal];
                [button setImage:kGetImage(@"message_morefunction_map") forState:UIControlStateNormal];
            }
                break;
                
            case 3:{
                
                [button setTitle:@"红包" forState:UIControlStateNormal];
                [button setImage:kGetImage(@"message_morefunction_mihongbao") forState:UIControlStateNormal];
            }
                break;
                
            default:
                break;
        }
    }
    
}

-(void)moreViewButtonClick:(UIButton *)button{

    if ([self.delegate respondsToSelector:@selector(buttonInMoreViewClick:)]) {
        
        [self.delegate buttonInMoreViewClick:button.tag];
    }
}

@end

//
//  CJCInviteDetailPhoneCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/30.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CJCInviteDetailPhoneCellDelegate <NSObject>

-(void)callHandle;

-(void)locationHandle;

@end

@interface CJCInviteDetailPhoneCell : UITableViewCell

@property (nonatomic ,weak) id<CJCInviteDetailPhoneCellDelegate> delegate;

@property (nonatomic ,assign) BOOL canOpration;

@property (nonatomic ,assign) BOOL isShow;

@end

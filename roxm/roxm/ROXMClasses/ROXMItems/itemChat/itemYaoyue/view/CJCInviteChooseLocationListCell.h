//
//  CJCInviteChooseLocationListCell.h
//  roxm
//
//  Created by 陈建才 on 2017/10/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AMapPOI;

@interface CJCInviteChooseLocationListCell : UITableViewCell

@property (nonatomic ,strong) AMapPOI *hotelPOI;

@end

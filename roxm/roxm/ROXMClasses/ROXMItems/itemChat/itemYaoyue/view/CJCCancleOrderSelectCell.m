//
//  CJCCancleOrderSelectCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCCancleOrderSelectCell.h"
#import "CJCCommon.h"

@interface CJCCancleOrderSelectCell ()

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UIButton *selectButton;

@end

@implementation CJCCancleOrderSelectCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), 10, 200 , 17);
    titleLabel.centerY = 61/2;
    
    self.titleLabel = titleLabel;
    [self.contentView addSubview:titleLabel];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(300, 12, kAdaptedValue(20), kAdaptedValue(20));
    button.right = SCREEN_WITDH - kAdaptedValue(21);
    button.centerY = titleLabel.centerY;
    
    [button setImage:kGetImage(@"invite_sel_unchoose") forState:UIControlStateNormal];
    [button setImage:kGetImage(@"invite_sel_choose") forState:UIControlStateSelected];
    
    button.userInteractionEnabled = NO;
    
    self.selectButton = button;
    [self.contentView addSubview:button];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), 60, SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

-(void)setTitle:(NSString *)title{

    self.titleLabel.text = title;
}

-(void)setIsSelect:(BOOL)isSelect{

    self.selectButton.selected = isSelect;
}

@end

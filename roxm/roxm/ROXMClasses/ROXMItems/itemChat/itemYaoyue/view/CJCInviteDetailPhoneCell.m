//
//  CJCInviteDetailPhoneCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/30.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteDetailPhoneCell.h"
#import "CJCCommon.h"
#import "CJCpersonalInfoModel.h"

@interface CJCInviteDetailPhoneCell (){

    CGFloat viewWidth;
}

@property (nonatomic ,strong) UIView *leftView;

@property (nonatomic ,strong) UIView *rightView;

@property (nonatomic ,strong) UIView *lineView;

@end

@implementation CJCInviteDetailPhoneCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        viewWidth = (SCREEN_WITDH - kAdaptedValue(51))/2;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIView *leftView = [[UIView alloc] init];
    
    leftView.frame = CGRectMake(kAdaptedValue(23), 18, viewWidth, 74);
    
    leftView.layer.cornerRadius = kAdaptedValue(4);
    leftView.layer.masksToBounds = YES;
    
    self.leftView = leftView;
    [self.contentView addSubview:leftView];
    
    UIView *rightView = [[UIView alloc] init];
    
    rightView.frame = CGRectMake(leftView.right+kAdaptedValue(5), 18, viewWidth, 74);
    
    rightView.layer.cornerRadius = kAdaptedValue(4);
    rightView.layer.masksToBounds = YES;
    
    self.rightView = rightView;
    [self.contentView addSubview:rightView];
    
//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (infoModel.sex == 0) {
        
        leftView.backgroundColor = [UIColor toUIColorByStr:@"4BD3C5"];
        
        rightView.backgroundColor = [UIColor toUIColorByStr:@"4BD3C5"];
    }else{
    
        leftView.backgroundColor = [UIColor toUIColorByStr:@"1F1C1B"];
        
        rightView.backgroundColor = [UIColor toUIColorByStr:@"B39B6B"];
    }
    
    UIImageView *phoneImageView = [[UIImageView alloc] initWithImage:kGetImage(@"invite_icon_phone")];
    
    phoneImageView.userInteractionEnabled = YES;
    
    phoneImageView.frame = CGRectMake(93, 15, 22, 22);
    phoneImageView.centerX = viewWidth/2;
    
    [leftView addSubview:phoneImageView];
    
    UILabel *phoneLabel = [UIView getSystemLabelWithStr:@"联系" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    phoneLabel.textAlignment = NSTextAlignmentCenter;
    
    phoneLabel.userInteractionEnabled = YES;
    
    phoneLabel.frame = CGRectMake(0, phoneImageView.bottom+7, viewWidth, kAdaptedValue(21));
    
    [leftView addSubview:phoneLabel];
    
    UIImageView *locationImageView = [[UIImageView alloc] initWithImage:kGetImage(@"invite_icon_location")];
    
    locationImageView.frame = CGRectMake(93, 15, 22, 22);
    locationImageView.centerX = viewWidth/2;
    
    locationImageView.userInteractionEnabled = YES;
    
    [rightView addSubview:locationImageView];
    
    UILabel *locationLabel = [UIView getSystemLabelWithStr:@"位置" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    locationLabel.textAlignment = NSTextAlignmentCenter;
    
    locationLabel.frame = CGRectMake(0, locationImageView.bottom+7, viewWidth, kAdaptedValue(21));
    
    locationLabel.userInteractionEnabled = YES;
    
    [rightView addSubview:locationLabel];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), 109, SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    self.lineView = lineView;
    [self.contentView addSubview:lineView];
    
    UITapGestureRecognizer *leftViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftViewTapGestureHandle)];
    
    [leftView addGestureRecognizer:leftViewTap];
    
    UITapGestureRecognizer *rightViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightViewTapGestureHandle)];
    
    [rightView addGestureRecognizer:rightViewTap];
}

-(void)setIsShow:(BOOL)isShow{

    self.leftView.hidden = !isShow;
    self.rightView.hidden = !isShow;
    self.lineView.hidden = !isShow;
}

-(void)setCanOpration:(BOOL)canOpration{

    self.leftView.alpha = 0.3;
}

-(void)leftViewTapGestureHandle{

    if ([self.delegate respondsToSelector:@selector(callHandle)]) {
        
        [self.delegate callHandle];
    }
}

-(void)rightViewTapGestureHandle{
    
    if ([self.delegate respondsToSelector:@selector(locationHandle)]) {
        
        [self.delegate locationHandle];
    }
}

@end

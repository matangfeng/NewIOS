//
//  CJCinviteOrderTopCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCpersonalInfoModel;

@interface CJCinviteOrderTopCell : UITableViewCell

@property (nonatomic ,copy) NSArray *imagesArray;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@end

//
//  CJCCancleOrderTopCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCCancleOrderTopCell : UITableViewCell

@property (nonatomic ,copy) NSString *title;

@end

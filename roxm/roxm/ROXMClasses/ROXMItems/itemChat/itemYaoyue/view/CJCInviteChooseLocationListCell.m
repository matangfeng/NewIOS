//
//  CJCInviteChooseLocationListCell.m
//  roxm
//
//  Created by 陈建才 on 2017/10/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteChooseLocationListCell.h"
#import "CJCCommon.h"
#import <AMapSearchKit/AMapSearchKit.h>

@interface CJCInviteChooseLocationListCell ()

@property (nonatomic ,strong) UILabel *hotelNameLabel;

@property (nonatomic ,strong) UILabel *hotelAddressLabel;

@property (nonatomic ,strong) UILabel *hotelDistanceLabel;

@end

@implementation CJCInviteChooseLocationListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), 14, SCREEN_WITDH-kAdaptedValue(110), kAdaptedValue(18));
    
    self.hotelNameLabel = titleLabel;
    [self.contentView addSubview:titleLabel];
    
    UILabel *detailLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:12 color:[UIColor toUIColorByStr:@"333333"]];
    
    detailLabel.frame = CGRectMake(kAdaptedValue(23), 14, SCREEN_WITDH-kAdaptedValue(110), kAdaptedValue(15));
    
    detailLabel.top = titleLabel.bottom+7;
    
    self.hotelAddressLabel = detailLabel;
    [self.contentView addSubview:detailLabel];
    
    UILabel *distanceLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"A4A4A4"]];
    
    distanceLabel.textAlignment = NSTextAlignmentRight;
    
    distanceLabel.frame = CGRectMake(kAdaptedValue(23), 14, kAdaptedValue(70), kAdaptedValue(20));
    
    distanceLabel.centerY = 33;
    distanceLabel.right = SCREEN_WITDH - kAdaptedValue(23);
    
    self.hotelDistanceLabel = distanceLabel;
    [self.contentView addSubview:distanceLabel];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), 65, SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

-(void)setHotelPOI:(AMapPOI *)hotelPOI{

    self.hotelNameLabel.text = hotelPOI.name;
    self.hotelAddressLabel.text = hotelPOI.address;
    
    CGFloat distance = (CGFloat)hotelPOI.distance/1000.0;
    
    if (distance < 0.1) {
        
        distance = 0.1;
    }
    
    self.hotelDistanceLabel.text = [NSString stringWithFormat:@"%.1fkm",distance];
}

@end

//
//  CJCInviteChooseLocationTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/10/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteChooseLocationTopCell.h"
#import "CJCCommon.h"

@implementation CJCInviteChooseLocationTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"附近酒店" fontName:kFONTNAMELIGHT size:13 color:[UIColor toUIColorByStr:@"727272"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), 10, SCREEN_WITDH, kAdaptedValue(18));
    
    titleLabel.bottom = 52;
    
    [self.contentView addSubview:titleLabel];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), 59, SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

@end

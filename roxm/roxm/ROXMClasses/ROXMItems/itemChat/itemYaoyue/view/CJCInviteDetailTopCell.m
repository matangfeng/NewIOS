//
//  CJCInviteDetailTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteDetailTopCell.h"
#import "CJCCommon.h"
#import "CJCpersonalInfoModel.h"

@interface CJCInviteDetailTopCell ()

@property (nonatomic ,strong) UILabel *nameAndAgeLabel;

@property (nonatomic ,strong) UILabel *distanceAndTimeLabel;

@property (nonatomic ,strong) UIImageView *iconImageView;

@end

@implementation CJCInviteDetailTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:20 color:[UIColor toUIColorByStr:@"222222"]];
    
    nameLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(23), SCREEN_WITDH-kAdaptedValue(130), kAdaptedValue(20));
    
    self.nameAndAgeLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UILabel *distanceLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"888888"]];
    
    distanceLabel.frame = CGRectMake(kAdaptedValue(23), nameLabel.bottom+kAdaptedValue(12), nameLabel.width, kAdaptedValue(14));
    
    self.distanceAndTimeLabel = distanceLabel;
    [self.contentView addSubview:distanceLabel];
    
    UIImageView *iconView = [[UIImageView alloc] init];
    
    iconView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    iconView.size = CGSizeMake(kAdaptedValue(70), kAdaptedValue(70));
    iconView.centerY = kAdaptedValue(46);
    iconView.right = SCREEN_WITDH - kAdaptedValue(23);
    
    iconView.layer.cornerRadius = kAdaptedValue(35);
    iconView.layer.masksToBounds = YES;
    
    self.iconImageView = iconView;
    [self.contentView addSubview:iconView];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(91), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

-(void)setInfoModel:(CJCpersonalInfoModel *)infoModel{

    self.nameAndAgeLabel.text = [CJCTools nameAndAgeStrFromInfoModel:infoModel];
    
    self.distanceAndTimeLabel.text = @"1.1km·15分钟前";
    
    [self.iconImageView setImageURL:[NSURL URLWithString:infoModel.imageUrl]];
}

@end

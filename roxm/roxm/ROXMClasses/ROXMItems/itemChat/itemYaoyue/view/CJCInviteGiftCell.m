//
//  CJCInviteGiftCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/28.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteGiftCell.h"
#import "CJCCommon.h"
#import "CJCInviteGiftModel.h"

@interface CJCInviteGiftCell ()

@property (nonatomic ,strong) UIImageView *giftImageView;

@property (nonatomic ,strong) UILabel *giftNameLabel;

@property (nonatomic ,strong) UILabel *giftPriceLabel;

@property (nonatomic ,strong) UIView *selectView;

@end

@implementation CJCInviteGiftCell

-(instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIImageView *giftView = [[UIImageView alloc] init];
    
    giftView.frame = CGRectMake(kAdaptedValue(14), kAdaptedValue(14), kAdaptedValue(64), kAdaptedValue(64));
    
    self.giftImageView = giftView;
    [self.contentView addSubview:giftView];
    
    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"333333"]];
    
    nameLabel.textAlignment = NSTextAlignmentCenter;
    
    nameLabel.frame = CGRectMake(0, giftView.bottom+kAdaptedValue(12), SCREEN_WITDH/4, kAdaptedValue(19));
    
    self.giftNameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UILabel *priceLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:13 color:[UIColor toUIColorByStr:@"BFBFBF"]];
    
    priceLabel.textAlignment = NSTextAlignmentCenter;
    
    priceLabel.frame = CGRectMake(0, nameLabel.bottom+kAdaptedValue(4), SCREEN_WITDH/4, kAdaptedValue(14));
    
    self.giftPriceLabel = priceLabel;
    [self.contentView addSubview:priceLabel];
}

-(void)setGiftModel:(CJCInviteGiftModel *)giftModel{

    [self.giftImageView setImageURL:[NSURL URLWithString:giftModel.url]];
    self.giftNameLabel.text = giftModel.name;
    self.giftPriceLabel.text = [NSString stringWithFormat:@"价值¥%.2f",giftModel.price];
    
    if (giftModel.isSelect) {
        
        self.selectView.hidden = NO;
    }else{
    
        self.selectView.hidden = YES;
    }
}

-(UIView *)selectView{

    if (_selectView == nil) {
        
        UIView *view = [[UIView alloc] init];
        
        view.backgroundColor = [UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.72];
        
        view.frame = self.giftImageView.bounds;
        
        [self.giftImageView addSubview:view];
        _selectView = view;
        
        UIImageView *selectImage = [[UIImageView alloc] initWithImage:kGetImage(@"gift_tick")];
        
        selectImage.size = CGSizeMake(kAdaptedValue(18), kAdaptedValue(18));
        selectImage.center = CGPointMake(view.width/2, view.height/2);
        
        [view addSubview:selectImage];
    }
    return _selectView;
}

@end

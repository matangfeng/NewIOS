//
//  CJCDateLocationModel.h
//  roxm
//
//  Created by 陈建才 on 2017/10/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CJCDateLocationModel : NSObject

@property (nonatomic ,copy) NSString *displayAddress;

@property (nonatomic ,assign) CGFloat lat;

@property (nonatomic ,assign) CGFloat lng;

@end

//
//  CJCInviteOrderModel.h
//  roxm
//
//  Created by 陈建才 on 2017/9/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CJCInviteOrderModel : NSObject

@property (nonatomic ,copy) NSString *id;

@property (nonatomic ,assign) NSInteger status;

@property (nonatomic ,copy) NSString *hotelName;

@property (nonatomic ,copy) NSString *hotelAddress;

@property (nonatomic ,assign) NSInteger hotelLevel;

@property (nonatomic ,assign) double lng;

@property (nonatomic ,assign) double lat;

@property (nonatomic ,copy) NSString *userVirtualPhone;

@property (nonatomic ,copy) NSString *partnerVirtualPhone;

@property (nonatomic ,copy) NSString *giftName;

@property (nonatomic ,copy) NSString *giftUrl;

@property (nonatomic ,assign) CGFloat price;

@property (nonatomic ,assign) NSInteger timeType;

@property (nonatomic ,copy) NSString *createTime;

@property (nonatomic ,copy) NSString *updateTime;

@property (nonatomic ,copy) NSString *userId;

@property (nonatomic ,copy) NSString *partnerId;

///男士
@property (nonatomic ,copy) NSString *userImageUrl;

@property (nonatomic ,copy) NSString *userTrueName;

///女士
@property (nonatomic ,copy) NSString *partnerImageUrl;

@property (nonatomic ,copy) NSString *partnerTrueName;

///男评女id，大于0表示已经评论
@property (nonatomic ,assign) NSInteger userCommentId;

///女评男id，大于0表示已经评论
@property (nonatomic ,assign) NSInteger partnerCommentId;

@end

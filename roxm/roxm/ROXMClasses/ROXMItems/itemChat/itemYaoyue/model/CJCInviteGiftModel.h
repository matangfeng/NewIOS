//
//  CJCInviteGiftModel.h
//  roxm
//
//  Created by 陈建才 on 2017/9/28.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CJCInviteGiftModel : NSObject

@property (nonatomic ,copy) NSString *id;

@property (nonatomic ,copy) NSString *giftId;

@property (nonatomic ,copy) NSString *name;

@property (nonatomic ,copy) NSString *url;

@property (nonatomic ,assign) CGFloat price;

@property (nonatomic ,assign) NSInteger status;

@property (nonatomic ,assign) BOOL isSelect;

@end

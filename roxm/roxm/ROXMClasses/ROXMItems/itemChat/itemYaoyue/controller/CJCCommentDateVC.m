//
//  CJCCommentDateVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/25.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCCommentDateVC.h"
#import "CJCCommon.h"
#import "CJCReportReasonVC.h"

@interface CJCCommentDateVC ()<UITextViewDelegate>{

    NSInteger starCount;
}

@property (nonatomic ,strong) UILabel *commentLabel;

@property (nonatomic ,strong) UIButton *reportButton;

@property (nonatomic ,strong) UITextView *commentTextView;

@property (nonatomic ,strong) UIView *topLineView;

@property (nonatomic ,strong) UIView *bottomLineView;

@property (nonatomic ,strong) UIButton *commitButton;

@property (nonatomic ,strong) UILabel *placeHoldLabel;

@end

@implementation CJCCommentDateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    
    [self setUpUI];
}

-(void)commitButtonClickHandle{

    [self showWithLabelAnimation];
    
    NSString *content;
    
    if (self.commentTextView.text.length>0) {
        
        content = self.commentTextView.text;
    }else{
    
        content = @"(这个家伙很懒,什么也没留下)";
    }
    
    NSString *commitURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/comment/publish"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"orderId"] = self.orderID;
    params[@"score"] = @(starCount);
    params[@"content"] = content;
    
    [CJCHttpTool postWithUrl:commitURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            if (self.commentHandle) {
                
                self.commentHandle();
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)bottomContentShow:(BOOL)show{

    self.commentLabel.hidden = !show;
    self.topLineView.hidden = !show;
    self.commentTextView.hidden = !show;
    self.bottomLineView.hidden = !show;
}

-(void)reportButtonClickHandle{

    CJCReportReasonVC *nextVC = [[CJCReportReasonVC alloc]init];
    
    nextVC.preVC = self;
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)reportSuccess{

    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"已举报"];
    [str addAttributes:@{NSFontAttributeName:[UIFont accodingVersionGetFont_regularWithSize:14],NSForegroundColorAttributeName:[UIColor toUIColorByStr:@"838383"]
                         }
                 range:NSMakeRange(0, str.length)];
    
    [self.reportButton setAttributedTitle:str forState:UIControlStateNormal];
}

-(void)starButtonClickHandle:(UIButton *)button{

    [self.view endEditing:YES];
    
    [self bottomContentShow:YES];
    
    self.commitButton.canOpration = YES;
    
    starCount = button.tag;
    
    for (int i=0; i<5; i++) {
        
        UIButton *starButton = (UIButton *)[self.view viewWithTag:i+1];
        
        starButton.selected = NO;
    }
    
    NSInteger tagIndex = button.tag;
    for (int i=0; i<tagIndex; i++) {
        
        UIButton *starButton = (UIButton *)[self.view viewWithTag:i+1];
        
        starButton.selected = YES;
    }
    
    switch (tagIndex) {
        case 1:{
        
            self.commentLabel.text = @"体验很糟糕，";
            self.reportButton.hidden = NO;
        }
            break;
            
        case 2:{
        
            self.commentLabel.text = @"不太满意";
            self.reportButton.hidden = YES;
        }
            break;
            
        case 3:{
            
            self.commentLabel.text = @"一般";
            self.reportButton.hidden = YES;
        }
            break;
            
        case 4:{
            
            self.commentLabel.text = @"较满意";
            self.reportButton.hidden = YES;
        }
            break;
            
        case 5:{
            
            self.commentLabel.text = @"很满意";
            self.reportButton.hidden = YES;
        }
            break;
            
        default:
            break;
    }

}

-(void)setUpUI{

    UILabel *titleLbael = [UIView getSystemLabelWithStr:@"为这次约会打分" fontName:kFONTNAMEMEDIUM size:28 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLbael.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(95), SCREEN_WITDH - kAdaptedValue(46), kAdaptedValue(28));
    
    [self.view addSubview:titleLbael];
    
    UIImage *grayImage = kGetImage(@"dingdan_pingjia_star2");
    UIImage *yellowImage = kGetImage(@"dingdan_pingjia_star1");
    
    for (int i =0; i<5; i++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button setImage:grayImage forState:UIControlStateNormal];
        
        [button setImage:yellowImage forState:UIControlStateSelected];
        
        button.tag = i+1;
        
        button.frame = CGRectMake(kAdaptedValue(23)+i*kAdaptedValue(60), titleLbael.bottom+kAdaptedValue(24), kAdaptedValue(50), kAdaptedValue(50));
        
        [button addTarget:self action:@selector(starButtonClickHandle:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:button];
    }
    
    UILabel *commentLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"838383"]];
    
    commentLabel.frame = CGRectMake(kAdaptedValue(23), titleLbael.bottom+kAdaptedValue(82), SCREEN_WITDH, kAdaptedValue(15));
    
    self.commentLabel = commentLabel;
    [self.view addSubview:commentLabel];
    
    CGFloat width = [CJCTools getShortStringLength:@"体验很糟糕，" withFont:[UIFont accodingVersionGetFont_regularWithSize:14]];
    
    UIButton *reportButton = [UIView getButtonWithStr:@"去举报" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"去举报"];
    NSRange strRange = {0,[str length]};
    [str addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:strRange];
    [str addAttributes:@{NSFontAttributeName:[UIFont accodingVersionGetFont_regularWithSize:14],NSForegroundColorAttributeName:[UIColor toUIColorByStr:@"429CF0"]
                               }
                       range:NSMakeRange(0, str.length)];
    
    [reportButton setAttributedTitle:str forState:UIControlStateNormal];
    
    reportButton.frame = CGRectMake(kAdaptedValue(23)+width, commentLabel.y, kAdaptedValue(50), kAdaptedValue(15));
    
    reportButton.hidden = YES;
    
    [reportButton addTarget:self action:@selector(reportButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    self.reportButton = reportButton;
    [self.view addSubview:reportButton];
    
    UIView *topLineView = [UIView getLineView];
    
    topLineView.frame = CGRectMake(kAdaptedValue(23), commentLabel.bottom+kAdaptedValue(27), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    self.topLineView = topLineView;
    [self.view addSubview:topLineView];
    
    UITextView *textView = [[UITextView alloc] init];
    
    textView.frame = CGRectMake(kAdaptedValue(23), topLineView.bottom+kAdaptedValue(10), SCREEN_WITDH-kAdaptedValue(46), kAdaptedValue(72));
    
    textView.font = [UIFont accodingVersionGetFont_regularWithSize:16];
    textView.textColor = [UIColor toUIColorByStr:@"222222"];
    
    textView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    textView.returnKeyType = UIReturnKeyDone;
    textView.enablesReturnKeyAutomatically = YES;
    textView.delegate = self;
    
    self.commentTextView = textView;
    [self.view addSubview:textView];
    
    UILabel *placeLabel = [UIView getSystemLabelWithStr:@"对方符合你的期待吗？这次约会是否愉快" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"BCBCBC"]];
    
    placeLabel.frame = CGRectMake(5, 0, textView.width, kAdaptedValue(16));
    
    self.placeHoldLabel = placeLabel;
    [textView addSubview:placeLabel];
    
    UIView *bottomView = [UIView getLineView];
    
    bottomView.frame = CGRectMake(kAdaptedValue(23), topLineView.bottom+kAdaptedValue(92), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    self.bottomLineView = bottomView;
    [self.view addSubview:bottomView];
    
    UIView *bottomView1 = [UIView getLineView];
    
    bottomView1.frame = CGRectMake(0, SCREEN_HEIGHT-kAdaptedValue(69), SCREEN_WITDH, OnePXLineHeight);
    
    [self.view addSubview:bottomView1];
    
    UIButton *button = [UIView getButtonWithStr:@"提交" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    button.frame = CGRectMake(kAdaptedValue(23), SCREEN_HEIGHT-kAdaptedValue(57), SCREEN_WITDH-kAdaptedValue(46), 44);
    
    button.layer.cornerRadius = kAdaptedValue(4);
    [button.layer setMasksToBounds:YES];
    button.backgroundColor = [UIColor toUIColorByStr:@"282828"];
    
    [button addTarget:self action:@selector(commitButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    button.canOpration = NO;
    
    self.commitButton = button;
    [self.view addSubview:button];

    [self bottomContentShow:NO];
}

- (void)textViewDidChange:(UITextView *)textView{

    if (textView.text.length>0) {
        
        self.placeHoldLabel.hidden = YES;
    }else{
    
        self.placeHoldLabel.hidden = NO;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{

    if ([text isEqualToString:@"\n"]){
    
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CJCBuyInviteGiftVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/28.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBuyInviteGiftVC.h"
#import "CJCCommon.h"
#import "CJCInviteGiftCell.h"
#import "CJCInviteGiftModel.h"
#import <EaseUI.h>
#import "CJCMessageHandle.h"
#import "CJCpersonalInfoModel.h"
#import "CJCPersonalInfoVC.h"
#import <StoreKit/StoreKit.h>
#import "Order.h"
#import "RSADataSigner.h"
#import <AlipaySDK/AlipaySDK.h>
#import "CJCSingleChatVC.h"
#import "CJCBanlanceModel.h"
#import "CJCInviteOrderModel.h"
#import <ReactiveObjC.h>

#define kInviteGiftCell          @"CJCInviteGiftCell"

@interface CJCBuyInviteGiftVC ()<UICollectionViewDelegate,UICollectionViewDataSource,SKProductsRequestDelegate,SKPaymentTransactionObserver>{

    NSMutableArray *dataArray;
    
    NSInteger selectIndex;
    
    CJCBanlanceModel *banlanceModel;
    
    int buyType;
}

@property (nonatomic ,strong) UICollectionView *giftCollectionView;

@property (nonatomic ,strong) UIButton *buyButton;

@end

@implementation CJCBuyInviteGiftVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = [NSMutableArray array];
    selectIndex = -1;
    
    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    handle.nickName = self.infoModel.nickname;
    handle.iconImageUrl = self.infoModel.imageUrl;
    handle.infoModel = self.infoModel;
    
    [self setUpNaviView];
    
    [self setUpUI];
    
//    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    [self getGiftList];
//    [self RequestProductData];
}

-(void)getGiftList{

    [self showWithLabelAnimation];
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/list/system"];
    
    [CJCHttpTool postWithUrl:giftURL params:nil success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCInviteGiftModel class] json:responseObject[@"data"]];
            
            [dataArray addObjectsFromArray:tempArr];
            
            [self.giftCollectionView reloadData];
        }else{
        
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (self.buyGiftVCType == CJCBuyInviteGiftVCTypedefault||self.buyGiftVCType == CJCBuyInviteGiftVCTypePushType) {
        CJCInviteGiftModel *model = dataArray[indexPath.item];
        if (selectIndex < 0) {
            selectIndex = indexPath.item;
            model.isSelect = YES;
            [self.giftCollectionView reloadItemsAtIndexPaths:@[indexPath]];
        }else{
            if (selectIndex != indexPath.item) {
                model.isSelect = YES;
                CJCInviteGiftModel *oldModel = dataArray[selectIndex];
                oldModel.isSelect = NO;
                NSIndexPath *oldIndex = [NSIndexPath indexPathForRow:selectIndex inSection:0];
                [self.giftCollectionView reloadItemsAtIndexPaths:@[indexPath,oldIndex]];
                selectIndex = indexPath.item;
            }
        }
        
        self.buyButton.userInteractionEnabled = YES;
        [self.buyButton setTitle:[NSString stringWithFormat:@"¥ %.2f 购买",model.price] forState:UIControlStateNormal];
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCInviteGiftCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kInviteGiftCell forIndexPath:indexPath];
    
    CJCInviteGiftModel *model = dataArray[indexPath.item];
    
    if ([model.giftId isEqualToString:self.giftId]) {
        
        model.isSelect = YES;
        
        selectIndex = indexPath.item;
        
        self.buyButton.userInteractionEnabled = YES;
        [self.buyButton setTitle:[NSString stringWithFormat:@"¥ %.2f 购买",model.price] forState:UIControlStateNormal];
    }
    
    cell.giftModel = model;
    
    return cell;
}

-(void)setUpUI{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    CGFloat itemWidth = (SCREEN_WITDH)/4;
    
    flowLayout.itemSize = CGSizeMake(itemWidth, kAdaptedValue(136));
    flowLayout.minimumInteritemSpacing = 0;
    
    flowLayout.minimumLineSpacing = 0;
    
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64-70) collectionViewLayout:flowLayout];
    
    collectionView.backgroundColor = [UIColor whiteColor];
    
    collectionView.showsVerticalScrollIndicator = NO;
    
    collectionView.alwaysBounceVertical = YES;
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    [collectionView registerClass:[CJCInviteGiftCell class] forCellWithReuseIdentifier:kInviteGiftCell];
    
    self.giftCollectionView = collectionView;
    [self.view addSubview:collectionView];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B"]] forState:0];
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B" andAlpha:0.9]] forState:1];

    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.6] forState:1];

    [buyButton setTitle:@"购买" forState:UIControlStateNormal];
    

    buyButton.frame = CGRectMake(kAdaptedValue(18), collectionView.bottom+1, SCREEN_WITDH-kAdaptedValue(36), 49);
    
    buyButton.userInteractionEnabled = NO;
    buyButton.layer.cornerRadius = kAdaptedValue(3);
    buyButton.layer.masksToBounds = YES;
    
    [[buyButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self getWalletBanlance];
    }];
    
    self.buyButton = buyButton;
    [self.view addSubview:buyButton];
}

-(void)buyGift{

    [self showWithLabelAnimation];
    
    CJCInviteGiftModel *model = dataArray[selectIndex];
    
    NSString *buyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/gift/to_alipay"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"giftId"] = model.id;
    
    [CJCHttpTool postWithUrl:buyURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            [self hidHUD];

            [self doAlipayPayWithOrderInfo:responseObject[@"data"][@"bodyString"]];
        }else{
            [self hidHUD];

            
        }
        
    } failure:^(NSError *error) {
       
        [self hidHUD];

    }];
}

-(void)getWalletBanlance{
    
    NSString *banlanceURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/balance/get"];
    
    [CJCHttpTool postWithUrl:banlanceURL params:nil success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            banlanceModel = [CJCBanlanceModel modelWithDictionary:responseObject[@"data"][@"balance"]];
            
            if (self.buyGiftVCType == CJCBuyInviteGiftVCTypedefault||self.buyGiftVCType == CJCBuyInviteGiftVCTypePushType){
                
                CJCInviteGiftModel *model = dataArray[selectIndex];
                
                if (banlanceModel.amount >= model.price) {
                    
                    CJCInviteGiftModel *model = dataArray[selectIndex];
                    
                    [self useBanlanceBuyGiftWithGiftId:model.id];
                }else{
                    
                    [self buyGift];
                }
                
            }else{
                
                if (banlanceModel.amount >= self.orderModel.price) {
                    
                    [self useBanlancePayOrderWithOrderId:self.orderId];
                }else{
                    
                    [self useAliPayPayOrderWithOrderId:self.orderId];
                }
            }
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        [self hidHUD];

    }];
}

-(void)useBanlanceBuyGiftWithGiftId:(NSString *)giftId{
    
    NSString *buyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/pay/by_balance"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = giftId;
    
    [CJCHttpTool postWithUrl:buyURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            [MBManager showBriefAlert:@"余额购买成功"];
            
            if (self.popHandle) {
                
                self.popHandle();
            }
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        [self hidHUD];

    }];
}

-(void)useBanlancePayOrderWithOrderId:(NSString *)orderId{
    
    NSString *buyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/pay/by_balance"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = orderId;
    
    [CJCHttpTool postWithUrl:buyURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
//            [MBManager showBriefAlert:@"余额支付订单成功"];
            
            [self yaoyueSuccess];
        }else{
        
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)useAliPayPayOrderWithOrderId:(NSString *)orderId{

    NSString *buyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/to_alipay"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"orderId"] = orderId;
    
    [CJCHttpTool postWithUrl:buyURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            [self doAlipayPayWithOrderInfo:responseObject[@"data"][@"bodyString"]];
        }else{
        
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)doAlipayPayWithOrderInfo:(NSString *)backOrderInfo
{

    // NOTE: 调用支付结果开始支付
    [[AlipaySDK defaultService] payOrder:backOrderInfo fromScheme:kALIPAYSCHEME callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
        
        NSInteger resultStatus=[[resultDic objectForKey:@"resultStatus"] integerValue];
        NSString *resultStr=[NSString string];
        switch (resultStatus)
        {
            case 4000:
                resultStr=@"订单支付失败";
                break;
            case 6001:
                resultStr=@"用户取消支付";
                break;
            case 6002:
                resultStr=@"网络连接出错";
                break;
            case 8000:
                resultStr=@"正在处理中";
                break;
            case 9000:
                resultStr=@"订单支付成功";
                break;
            default:
                break;
        }
        
        [self hidHUD];
        
        [MBManager showBriefAlert:resultStr];
        
        if (resultStatus == 9000) {
            
            if (self.buyGiftVCType == CJCBuyInviteGiftVCTypedefault||self.buyGiftVCType == CJCBuyInviteGiftVCTypePushType){
                
                if (self.popHandle) {
                    
                    self.popHandle();
                }
                
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                
                 [self yaoyueSuccess];
            }
    
        }
        
    }];
}

-(void)yaoyueSuccess{
    NSLog(@"邀约成功");
    EMMessage *message = [CJCMessageHandle CJC_HuanxinYaoyueMessageWithAmapPOI:self.selectPOI andOrderId:self.orderId andTo:self.infoModel.uid];
    
    [[EMClient sharedClient].chatManager sendMessage:message progress:nil completion:^(EMMessage *aMessage, EMError *aError) {
        
        if (aError == nil) {
            
            [MBManager showBriefAlert:@"邀约成功"];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                //                        [self.navigationController popViewControllerAnimated:YES];
                
                CJCSingleChatVC *chatController = [[CJCSingleChatVC alloc] init];
                
                chatController.conversationId = self.infoModel.emId;
                chatController.location = self.location;
                chatController.qunjuId = self.infoModel.uid;
                chatController.infoModel = self.infoModel;
                chatController.nickName = self.infoModel.nickname;
                chatController.iconImageUrl = self.infoModel.imageUrl;
                chatController.popType = CJCSingleChatPopTypePopToInfoVC;
                
                [self.navigationController pushViewController:chatController animated:YES];
                
            });
            
        }else{
            
            [MBManager showBriefAlert:aError.errorDescription];
        }
    }];
}

-(void)payOrderWithOrderID:(NSString *)orderId{
    
    NSString *buyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/pay"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = orderId;
    
    [CJCHttpTool postWithUrl:buyURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0) {
            
            [self yaoyueSuccess];
            
        }else{
            
            [self hidHUD];
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)setUpNaviView{

    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    UILabel *leftLabel = [UIView getYYLabelWithStr:@"取消" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    leftLabel.frame = CGRectMake(18, 32, 44, 22.5);
    
    leftLabel.centerY = kNAVIVIEWCENTERY;
    
    [self.view addSubview:leftLabel];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    leftButton.frame = CGRectMake(8, 8, 50, 50);
    
    leftButton.centerY = kNAVIVIEWCENTERY;
    
    [leftButton addTarget:self action:@selector(cancleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:leftButton];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(0, 63, SCREEN_WITDH, OnePXLineHeight);
    
    [self.view addSubview:lineView];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"选择礼物" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"选择礼物" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.view addSubview:titleLabel];
    
//    UIView *secondView = [[UIView alloc] init];
//    
//    secondView.frame = CGRectMake(0, 64, SCREEN_WITDH, 64);
//    
//    [self.view addSubview:secondView];
//    
//    UIImageView *iconImageView = [[UIImageView alloc] initWithImage:kGetImage(@"Icon-60")];
//    
//    iconImageView.frame = CGRectMake(17, 10, 43, 43);
//    
//    iconImageView.centerY = 32;
//    
//    [secondView addSubview:iconImageView];
//    
//    UILabel *hintLabel = [UIView getSystemLabelWithStr:@"关注roxm濡沫微信公众号，在公众号内购买更优惠" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"3C3C3C"]];
//    
//    hintLabel.numberOfLines = 2;
//    
//    hintLabel.frame = CGRectMake(iconImageView.right+17, 10, SCREEN_WITDH - 95, 40);
//    
//    hintLabel.centerY = 32;
//    
//    [secondView addSubview:hintLabel];
//    
//    UIView *marginView = [[UIView alloc] init];
//    
//    marginView.backgroundColor = [UIColor groupTableViewBackgroundColor];
//    
//    marginView.frame = CGRectMake(0, secondView.bottom, SCREEN_WITDH, 13);
//    
//    [self.view addSubview:marginView];
}

-(void)cancleButtonClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)RequestProductData
{
    NSLog(@"---------请求对应的产品信息------------");
    NSArray *product = nil;
    
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request=[[SKProductsRequest alloc] initWithProductIdentifiers: nsset];
    request.delegate=self;
    [request start];
    
}

//<SKProductsRequestDelegate> 请求协议
//收到的产品信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    
    NSLog(@"-----------收到产品反馈信息--------------");
    NSArray *myProduct = response.products;
    NSLog(@"产品Product ID:%@",response.invalidProductIdentifiers);
    NSLog(@"产品付费数量: %d", (int)[myProduct count]);
    // populate UI
    for(SKProduct *product in myProduct){
        NSLog(@"product info");
        NSLog(@"SKProduct 描述信息%@", [product description]);
        NSLog(@"产品标题 %@" , product.localizedTitle);
        NSLog(@"产品描述信息: %@" , product.localizedDescription);
        NSLog(@"价格: %@" , product.price);
        NSLog(@"Product id: %@" , product.productIdentifier);
    }
    SKPayment *payment = nil;
    
    NSLog(@"---------发送购买请求------------");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}
- (void)requestProUpgradeProductData
{
    NSLog(@"------请求升级数据---------");
    NSSet *productIdentifiers = [NSSet setWithObject:@"com.productid"];
    SKProductsRequest* productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
    
}
//弹出错误信息
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"-------弹出错误信息----------");
    UIAlertView *alerView =  [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",NULL) message:[error localizedDescription]
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Close",nil) otherButtonTitles:nil];
    [alerView show];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

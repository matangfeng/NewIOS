//
//  CJCCancleOrderVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef void(^cancleOrderHandle)(void);

@class EaseMessageViewController;

@interface CJCCancleOrderVC : CommonVC

@property (nonatomic ,copy) NSString *orderId;

@property (nonatomic ,copy) NSString *otherUID;

@property (nonatomic ,strong) EaseMessageViewController *messageVC;

@property (nonatomic ,copy) cancleOrderHandle cancleHandle;

@end

//
//  CJCInviteChooseHotelVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteChooseHotelVC.h"
#import "CJCCommon.h"
#import <AMapSearchKit/AMapSearchKit.h>
#import "CJCInviteChooseLocationTopCell.h"
#import "CJCInviteChooseLocationListCell.h"
#import "CJCSearchNearbyHotelView.h"

#define kInviteChooseLocationTopCell    @"CJCInviteChooseLocationTopCell"
#define kInviteChooseLocationListCell   @"CJCInviteChooseLocationListCell"

@interface CJCInviteChooseHotelVC ()<UITableViewDelegate,UITableViewDataSource,AMapSearchDelegate>{

    NSMutableArray *dataArray;
    
    BOOL isRefreshData;
    
    NSArray *searchResult;
}

@property (nonatomic ,strong) UITableView *hotelListTB;

@property (nonatomic ,strong) AMapSearchAPI *search;

@property (nonatomic ,strong) UIView *searchView;

@property (nonatomic ,strong) UILabel *searchHintLabel;

@end

@implementation CJCInviteChooseHotelVC

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (isRefreshData) {
        
        [self searchNearbyHotelList];
    }
    
}

#pragma mark =======获取周边 宾馆酒店 的列表
-(void)searchNearbyHotelList{
    
    //    [MBManager showLoadingInView:self.view];
    
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    
    request.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
    //    request.keywords            = @"宾馆酒店";
    
    //    if (self.locationType == CJCChooseLocationTypeInvite) {
    //
    //        request.keywords            = @"三星级宾馆";
    //    }else{
    //
    //        request.keywords            = @"";
    //    }
    
    request.keywords            = @"宾馆酒店";
    
    /* 按照距离排序. */
    request.sortrule            = 0;
    request.requireExtension    = YES;
    
    [self.search AMapPOIAroundSearch:request];
}

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    
    [MBManager hideAlert];
    
    if (response.pois.count == 0)
    {
        return;
    }
    
    isRefreshData = NO;
    
    //解析response获取POI信息，具体解析见 Demo
    searchResult = response.pois;
    
    [dataArray removeAllObjects];
    
    [dataArray addObjectsFromArray:searchResult];
    
    [self.hotelListTB reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = [NSMutableArray array];
    isRefreshData = YES;
    
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    
    [self setUpNaviView];
    
    [self setUpUI];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
    }else{
    
        AMapPOI *hotelPOI = dataArray[indexPath.row-1];
        
        if (self.chooseHandle) {
            
            self.chooseHandle(hotelPOI);
            
            [self naviViewBackBtnDidClick];
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (dataArray.count == 0) {
        
        return 0;
    }else{
    
        return dataArray.count+1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        return 60;
    }else{
    
        return 66;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        CJCInviteChooseLocationTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteChooseLocationTopCell];
        
        return cell;
    }
    
    CJCInviteChooseLocationListCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteChooseLocationListCell];
    
    cell.hotelPOI = dataArray[indexPath.row - 1];
    
    return cell;
}

-(void)setUpUI{

    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 124, SCREEN_WITDH, SCREEN_HEIGHT - 124) style:UITableViewStylePlain];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [tableView registerClass:[CJCInviteChooseLocationTopCell class] forCellReuseIdentifier:kInviteChooseLocationTopCell];
    [tableView registerClass:[CJCInviteChooseLocationListCell class] forCellReuseIdentifier:kInviteChooseLocationListCell];
    
    self.hotelListTB = tableView;
    [self.view addSubview:tableView];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UIView *searchView = [[UIView alloc] init];
    
    searchView.backgroundColor = [UIColor whiteColor];
    
    searchView.frame = CGRectMake(0, 64, SCREEN_WITDH, 60);
    
    UITapGestureRecognizer *searchTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchTapHandle)];
    [searchView addGestureRecognizer:searchTap];
    
    self.searchView = searchView;
    [self.view addSubview:searchView];
    
    UILabel *searchLabel = [UIView getSystemLabelWithStr:@"搜索酒店" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"AEAEAE"]];
    
    searchLabel.frame = CGRectMake(kAdaptedValue(23), 10, SCREEN_WITDH, kAdaptedValue(20));
    
    searchLabel.centerY = 30;
    
    self.searchHintLabel = searchLabel;
    [searchView addSubview:searchLabel];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), 59, SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [searchView addSubview:lineView];
}

-(void)searchTapHandle{

    CJCSearchNearbyHotelView *searchView = [[CJCSearchNearbyHotelView alloc] init];
    
    searchView.searchKeys = @"宾馆酒店";
    searchView.showDistance = NO;
    
    searchView.searchHandle = ^(AMapPOI *POI) {
        
        if (self.chooseHandle) {
            
            self.chooseHandle(POI);
            
            [self naviViewBackBtnDidClick];
        }
    };
    
    searchView.cancleHandle = ^{
        
        self.searchHintLabel.hidden = NO;
    };
    
    self.searchHintLabel.hidden = YES;
    
    [searchView showSearchView];
}


-(void)naviViewBackBtnDidClick{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

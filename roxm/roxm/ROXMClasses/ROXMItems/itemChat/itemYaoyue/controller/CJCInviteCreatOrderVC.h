//
//  CJCInviteCreatOrderVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@class CJCPersonalInfoVC;

@class CJCpersonalInfoModel,CLLocation;

@interface CJCInviteCreatOrderVC : CommonVC

@property (nonatomic ,copy) NSArray *images;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@property (nonatomic ,strong) CLLocation *location;

@property (nonatomic ,strong) CJCPersonalInfoVC *infoVC;

@end

//
//  CJCMessageLocationCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/19.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageLocationCell.h"
#import "CJCCommon.h"

@interface CJCMessageLocationCell ()

@property (nonatomic ,strong) UIImageView *showImageView;

@property (nonatomic ,strong) UILabel *locationLabel;

@property (nonatomic ,strong) UILabel *addressLabel;

@end

@implementation CJCMessageLocationCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    
    self.bubbleImageView.hidden = YES;
    
    UIImageView *showView = [[UIImageView alloc] init];
    
    self.showImageView = showView;
    [self.messageContainView insertSubview:showView aboveSubview:self.bubbleImageView];
    
    UILabel *locationLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"000000"]];
    
    self.locationLabel = locationLabel;
    [self.messageContainView addSubview:locationLabel];
    
    UILabel *addressLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:12 color:[UIColor toUIColorByStr:@"A5A5A5"]];
    
    self.addressLabel = addressLabel;
    [self.messageContainView addSubview:addressLabel];
}

-(void)updateUIWithModel:(id<IMessageModel>)messageModel{
    
    [super updateUIWithModel:messageModel];
    
    UIView *leftView = [UIView getLineView];
    
    leftView.frame = CGRectMake(0, 0, OnePXLineHeight, 176);
    [self.messageContainView addSubview:leftView];
    
    UIView *rightView = [UIView getLineView];
    
    rightView.frame = CGRectMake(199, 0, OnePXLineHeight, 176);
    [self.messageContainView addSubview:rightView];
    
    UIView *topView = [UIView getLineView];
    
    topView.frame = CGRectMake(0, 0, 200, OnePXLineHeight);
    [self.messageContainView addSubview:topView];
    
    UIView *bottpmView = [UIView getLineView];
    
    bottpmView.frame = CGRectMake(0, 175, 200, OnePXLineHeight);
    [self.messageContainView addSubview:bottpmView];
    
    UIView *bottomView = [UIView getLineView];
    
    bottomView.frame = CGRectMake(0, 125, 200, OnePXLineHeight);
    [self.messageContainView addSubview:bottomView];
    
    NSDictionary *typeDict = messageModel.message.ext;
    
    NSString *imagePath = typeDict[kHUANXINLOCATIONIMAGE];
    
    [self.showImageView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:messageModel.failImageName]];
    
    self.showImageView.frame = CGRectMake(0, 0, 200, 125);
    
    UIImageView *locationImageView = [[UIImageView alloc] initWithImage:kGetImage(@"message_place2")];
    
    locationImageView.frame = CGRectMake(100, 100, kAdaptedValue(68), kAdaptedValue(37));
    
    locationImageView.centerX = 100;
    locationImageView.centerY = 125/2-kAdaptedValue(37/2);
    
    [self.showImageView addSubview:locationImageView];
    
    NSNumber *distanceNum = typeDict[kHUANXINLOCATIONDISTANCE];
    
    if (distanceNum.floatValue > 300) {
        
        self.locationLabel.text =typeDict[kHUANXINLOCATIONNAME];
    }else{
    
        self.locationLabel.text = @"我的当前位置";
    }
    
    
    self.locationLabel.frame = CGRectMake(9, 9, self.messageContainView.width-18, 16);
    self.locationLabel.y = self.showImageView.bottom+9;
    
    self.addressLabel.text = messageModel.address;
    self.addressLabel.frame = CGRectMake(9, 31, self.locationLabel.width, 13);
    self.addressLabel.y = self.locationLabel.bottom+9;
}


@end

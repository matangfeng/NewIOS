//
//  CJCMessageTextCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/13.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EaseUI.h>
#import "CJCMessageBaseCell.h"

@class CJCBaseMessageContentView;

@interface CJCMessageTextCell : CJCMessageBaseCell

@end

//
//  CJCMessageOrderStatusCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/30.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageOrderStatusCell.h"
#import "CJCCommon.h"
#import <EaseUI.h>
#import "CJCChatMessageHandle.h"

@interface CJCMessageOrderStatusCell ()

@property (nonatomic ,strong) UILabel *orderStatusLabel;

@property (nonatomic ,strong) UILabel *orderHintLabel;

@end

@implementation CJCMessageOrderStatusCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    self.orderStatusLabel = titleLabel;
    [self.messageContainView insertSubview:titleLabel aboveSubview:self.bubbleImageView];
    
    UILabel *detailLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:13 color:[UIColor toUIColorByStr:@""]];
    
    self.orderHintLabel = detailLabel;
    [self.messageContainView insertSubview:detailLabel aboveSubview:self.bubbleImageView];
    
}

-(void)updateUIWithModel:(id<IMessageModel>)messageModel{
    
    [super updateUIWithModel:messageModel];
    
    self.orderStatusLabel.frame = CGRectMake(8, 9, self.messageContainView.width-16, kAdaptedValue(21));
    
    self.orderStatusLabel.text = [CJCChatMessageHandle getTitleFromOrderStatus:messageModel];
    
    self.orderHintLabel.frame = CGRectMake(8, self.orderStatusLabel.bottom+8, self.messageContainView.width-16, kAdaptedValue(13));
    
    self.orderHintLabel.text = [CJCChatMessageHandle getHintFromOrderStatus:messageModel];
    
    if (messageModel.isSender) {
        
        self.orderStatusLabel.textColor = [UIColor toUIColorByStr:@"FFFFFF"];
        self.orderHintLabel.textColor = [UIColor toUIColorByStr:@"FFFFFF"];
        
        self.orderHintLabel.textAlignment = NSTextAlignmentLeft;
        self.orderStatusLabel.textAlignment = NSTextAlignmentLeft;
    }else{
        
        self.orderStatusLabel.textColor = [UIColor toUIColorByStr:@"333333"];
        self.orderHintLabel.textColor = [UIColor toUIColorByStr:@"41B2A3 "];
        
        self.orderHintLabel.textAlignment = NSTextAlignmentRight;
        self.orderStatusLabel.textAlignment = NSTextAlignmentRight;
    }

}

@end

//
//  CJCMessageLocationCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/14.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageYaoyueCell.h"
#import "CJCCommon.h"

@interface CJCMessageYaoyueCell ()

@property (nonatomic ,strong) UILabel *hotelNameLabel;

@property (nonatomic ,strong) UIImageView *hotelImageView;

@property (nonatomic ,strong) UILabel *hotelAddressLabel;

@property (nonatomic ,strong) UILabel *showDetailButton;

@property (nonatomic ,strong) UIView *clickView;

@end

@implementation CJCMessageYaoyueCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    
    UILabel *nameLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"444444"]];
    
    self.hotelNameLabel = nameLabel;
    [self.messageContainView insertSubview:nameLabel aboveSubview:self.bubbleImageView];
    
    UILabel *addressLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:12 color:[UIColor toUIColorByStr:@""]];
    
    addressLabel.numberOfLines = 2;
    
    self.hotelAddressLabel = addressLabel;
    [self.messageContainView insertSubview:addressLabel aboveSubview:self.bubbleImageView];
    
    UIImageView *showView = [[UIImageView alloc] init];
    
    showView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    showView.contentMode = UIViewContentModeScaleAspectFill;
    
    showView.layer.masksToBounds = YES;
    
    self.hotelImageView = showView;
    [self.messageContainView insertSubview:showView aboveSubview:self.bubbleImageView];
    
    UILabel *detailLabel = [UIView getSystemLabelWithStr:@"点击查看邀约详情" fontName:kFONTNAMELIGHT size:12 color:[UIColor toUIColorByStr:@"41B2A3"]];
    
    detailLabel.textAlignment = NSTextAlignmentLeft;
    self.showDetailButton = detailLabel;
    [self.messageContainView insertSubview:detailLabel aboveSubview:self.bubbleImageView];
    
}

-(void)updateUIWithModel:(id<IMessageModel>)messageModel{
    
    [super updateUIWithModel:messageModel];
    
    if (messageModel.isSender) {
        
        self.hotelNameLabel.textColor = [UIColor toUIColorByStr:@"FFFFFF"];
        self.hotelAddressLabel.textColor = [UIColor toUIColorByStr:@"FFFFFF"];
        self.showDetailButton.textColor = [UIColor toUIColorByStr:@"FFFFFF"];
    }else{
    
        self.hotelNameLabel.textColor = [UIColor toUIColorByStr:@"444444"];
        self.hotelAddressLabel.textColor = [UIColor toUIColorByStr:@"333333"];
        self.showDetailButton.textColor = [UIColor toUIColorByStr:@"41B2A3"];
    }
    
    NSDictionary *tempDict = messageModel.message.ext;
    
    if (!tempDict) {
        
        return;
    }
    
    NSString *nameStr = [NSString stringWithFormat:@"我在%@等你哦",tempDict[kHXYAOYUEHOTELNAME]];
    
    self.hotelNameLabel.origin = CGPointMake(kAdaptedValue(14), kAdaptedValue(12));
    self.hotelNameLabel.size = CGSizeMake(self.messageContainView.width-28, kAdaptedValue(15));
    self.hotelNameLabel.text = nameStr;
    
    self.hotelImageView.x = kAdaptedValue(14);
    self.hotelImageView.y = self.hotelNameLabel.bottom+kAdaptedValue(11);
    self.hotelImageView.size = CGSizeMake(54, 54);
    
    NSString *imageStr = tempDict[kHXYAOYUEHOTELIMAGE];
    NSURL *imageURL = [NSURL URLWithString:imageStr];
    [self.hotelImageView setImageURL:imageURL];
    
    self.hotelAddressLabel.x = self.hotelImageView.right+kAdaptedValue(8);
    self.hotelAddressLabel.y = self.hotelImageView.y;
    self.hotelAddressLabel.size = CGSizeMake(self.messageContainView.width-kAdaptedValue(88), kAdaptedValue(38));
    self.hotelAddressLabel.text = tempDict[kHXYAOYUEHOTELADDRESS];
    
    self.showDetailButton.x = self.hotelAddressLabel.x;
    self.showDetailButton.y = self.hotelAddressLabel.bottom+kAdaptedValue(2);
    self.showDetailButton.size = CGSizeMake(self.messageContainView.width-kAdaptedValue(88), kAdaptedValue(12));

}

@end

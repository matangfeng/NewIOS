//
//  CJCChatMessageHandle.h
//  roxm
//
//  Created by 陈建才 on 2017/9/13.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <EaseUI.h>

@class CJCInviteOrderModel;

@interface CJCChatMessageHandle : NSObject

+(CGSize)CJC_getScaleImageSize:(CGSize)imageSize;

+(CGSize)sizeWithString:(NSString *)string font:(UIFont *)font;

+(UIImage *)getLashenImage:(NSString *)imageName;

+(NSString *)getTitleFromOrderStatus:(id<IMessageModel>)messageModel;

+(NSString *)getHintFromOrderStatus:(id<IMessageModel>)messageModel;

@end

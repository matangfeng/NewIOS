//
//  CJCMessageHandle.h
//  roxm
//
//  Created by 陈建才 on 2017/9/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AMapSearchKit/AMapSearchKit.h>

typedef NS_ENUM(NSUInteger, CJCImageSourceType) {
    CJCImageSourceTypeCamera     = 0,
    CJCImageSourceTypeLibrary    = 1,
};

@class EMMessage,CJCpersonalInfoModel,AMapPOI,CLLocation,CJCInviteOrderModel;

@interface CJCMessageHandle : NSObject

@property (nonatomic ,strong) CLLocation *location;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@property (nonatomic ,copy) NSString *nickName;

@property (nonatomic ,copy) NSString *iconImageUrl;

+(instancetype)standMessageHandle;

+(NSDictionary *)getIconAndNameMessageExpand;

///邀约订单状态的消息
+(EMMessage *)CJC_HuanxinYaoyueStatusMessageWithAmapPOI:(CJCInviteOrderModel *)orderModel andTo:(NSString *)toWho;

//发出邀约的消息
+(EMMessage *)CJC_HuanxinYaoyueMessageWithAmapPOI:(AMapPOI *)poi andOrderId:(NSString *)OrderId andTo:(NSString *)toWho;

+(EMMessage *)CJC_HuanxinTextMessage:(NSString *)text andTo:(NSString *)toWho;

///图片信息的拓展信息  是否是相机拍摄的
+(EMMessage *)CJC_HuanxinImageMessage:(UIImage *)image andType:(CJCImageSourceType)sourceType andTo:(NSString *)toWho;

+(EMMessage *)CJC_HuanxinVoiceMessage:(NSString *)voicePath andDurio:(CGFloat)duration andTo:(NSString *)toWho;

+(EMMessage *)CJC_HuanxinVideoMessage:(NSString *)voicePath andTo:(NSString *)toWho;

///位置消息的拓展信息  高德地图的返回信息(经纬度)  高德地图的截图  发送点距离用户的距离
+(EMMessage *)CJC_HuanxinLocationMessage:(AMapPOI *)poi andImage:(NSString *)imagePath andDistance:(double)distance andTo:(NSString *)toWho;

+(EMMessage *)CJC_HuanxinLocationMessage:(CLLocationCoordinate2D)location andAddress:(NSString *)address andImage:(NSString *)imagePath andDistance:(double)distance andTo:(NSString *)toWho;

@end

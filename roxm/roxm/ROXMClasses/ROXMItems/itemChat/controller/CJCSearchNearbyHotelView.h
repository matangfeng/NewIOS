//
//  CJCSearchNearbyHotelView.h
//  roxm
//
//  Created by 陈建才 on 2017/9/12.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,CJCSearchNearbyType){
    
    CJCSearchNearbyTypedefault             = 0,//搜索没有限制
    CJCSearchNearbyTypeHotel               = 1,//搜索酒店
};

@class AMapPOI;

typedef void(^CJCSearchLocationHandle)(AMapPOI *POI);

typedef void(^CJCCancleChooseHande)(void);

@interface CJCSearchNearbyHotelView : UIView

@property (nonatomic ,copy) CJCSearchLocationHandle searchHandle;

@property (nonatomic ,copy) CJCCancleChooseHande cancleHandle;

@property (nonatomic ,copy) NSString *searchKeys;

@property (nonatomic ,assign) BOOL showDistance;

-(void)showSearchView;

@end

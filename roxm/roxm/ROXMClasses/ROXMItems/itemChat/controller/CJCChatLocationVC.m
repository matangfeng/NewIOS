//
//  CJCChatLocationVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCChatLocationVC.h"
#import "CJCCommon.h"
#import <MAMapKit/MAMapKit.h>

@interface CJCChatLocationVC ()<MAMapViewDelegate>

@property (nonatomic ,strong) MAMapView *aMapView;

@end

@implementation CJCChatLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    [self setUpUI];
    [self setUpBottomView];
}

-(void)setUpBottomView{

    UIView *bottomView = [[UIView alloc] init];
    
    bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-kAdaptedValue(66), SCREEN_WITDH, kAdaptedValue(66));
    
    [self.view addSubview:bottomView];
    
    UILabel *nameLable = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSDictionary *tempDict = self.messageModel.message.ext;
    
    NSString *name;
    if (tempDict) {
        
        name = tempDict[kHUANXINLOCATIONNAME];
    }else{
        
        name = @"未知";
    }
    
    if (self.locationVCType == CJCChatLocationVCTypeYaoyue) {
        
        name = self.hotelName;
    }
    
    nameLable.text = name;
    
    nameLable.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(14), SCREEN_WITDH-kAdaptedValue(100), kAdaptedValue(16));
    
    [bottomView addSubview:nameLable];
    
    UILabel *addressLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:12 color:[UIColor toUIColorByStr:@"333333"]];
    
    addressLabel.text = self.messageModel.address;
    
    if (self.locationVCType == CJCChatLocationVCTypeYaoyue) {
        
        addressLabel.text = self.hotelAddress;
    }
    
    addressLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(38), SCREEN_WITDH-kAdaptedValue(100), kAdaptedValue(16));
    
    [bottomView addSubview:addressLabel];
    
    UIButton *followButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [followButton setImage:kGetImage(@"message_go@2x") forState:UIControlStateNormal];
    
    followButton.frame = CGRectMake(SCREEN_WITDH - kAdaptedValue(50), 10, kAdaptedValue(38.5), kAdaptedValue(38.5));
    followButton.centerY = kAdaptedValue(33);
    followButton.right = SCREEN_WITDH - kAdaptedValue(18);
    
    [followButton addTarget:self action:@selector(followButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [bottomView addSubview:followButton];
}

-(void)followButtonDidClick{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"跳转苹果地图导航" message:nil preferredStyle:  UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"跳转" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self gotoMap];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
    
}

-(void)gotoMap{
    
    CLLocationCoordinate2D coords2 = CLLocationCoordinate2DMake(self.messageModel.latitude,self.messageModel.longitude);
    
    //起点
    
    NSDictionary *tempDict = self.messageModel.message.ext;
    
    NSString *name;
    if (tempDict) {
        
        name = tempDict[kHUANXINLOCATIONNAME];
    }else{
        
        name = @"未知";
    }
    
    if (self.locationVCType == CJCChatLocationVCTypeYaoyue) {
        
        coords2 = CLLocationCoordinate2DMake(self.lat,self.lng);
        name = self.hotelName;
    }
    
    MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
    
    //目的地的位置
    
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:coords2 addressDictionary:nil]];
    
    toLocation.name = name;
    
    NSArray *items = [NSArray arrayWithObjects:currentLocation, toLocation, nil];
    
    NSDictionary *options = @{ MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsMapTypeKey: [NSNumber numberWithInteger:MKMapTypeStandard], MKLaunchOptionsShowsTrafficKey:@YES }; //打开苹果自身地图应用，并呈现特定的item
    
    [MKMapItem openMapsWithItems:items launchOptions:options];
    
}

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay{
    
    return nil;
}

-(void)setUpUI{

    MAMapView *_mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64-kAdaptedValue(66))];
    
    ///把地图添加至view
    self.aMapView = _mapView;
    [self.view addSubview:_mapView];
    
    _mapView.showsUserLocation = YES;
    _mapView.userTrackingMode = MAUserTrackingModeFollow;
    [_mapView setZoomLevel:16.1 animated:YES];
    
    _mapView.showsCompass= NO;
    _mapView.showsScale= NO;
    _mapView.delegate = self;
    _mapView.rotateEnabled = NO;
    _mapView.rotateCameraEnabled = NO;
    _mapView.skyModelEnable = NO;
    _mapView.showsBuildings = NO;
    _mapView.showsLabels = YES;
    
    _mapView.customizeUserLocationAccuracyCircleRepresentation = YES;
    
    CLLocationCoordinate2D coor;
    coor.latitude = self.messageModel.latitude;
    coor.longitude = self.messageModel.longitude;
    
    if (self.locationVCType == CJCChatLocationVCTypeYaoyue){
        
        coor.latitude = self.lat;
        coor.longitude = self.lng;
    }
    
    _mapView.centerCoordinate = coor;
    
    MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
    
    pointAnnotation.coordinate = coor;
    //设置地图的定位中心点坐标
    _mapView.centerCoordinate = coor;
    //将点添加到地图上，即所谓的大头针
    [_mapView addAnnotation:pointAnnotation];
    
    UIButton *originCenterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [originCenterButton setImage:kGetImage(@"message_place1") forState:UIControlStateNormal];
    
    originCenterButton.frame = CGRectMake(14, 14, kAdaptedValue(44), kAdaptedValue(44));
    
    originCenterButton.bottom = _mapView.height - 14;
    
    [originCenterButton addTarget:self action:@selector(mapBackToOrginLocation) forControlEvents:UIControlEventTouchUpInside];
    
    [_mapView addSubview:originCenterButton];
    
//    UIImageView *locationImageView = [[UIImageView alloc] initWithImage:kGetImage(@"message_place2@2x")];
//    
//    locationImageView.frame = CGRectMake(100, 100, kAdaptedValue(68), kAdaptedValue(37));
//    
//    locationImageView.center = CGPointMake(_mapView.width/2, _mapView.height/2-kAdaptedValue(37/2));
//    
//    [_mapView addSubview:locationImageView];
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation{
    
    if (![annotation isKindOfClass:[MAUserLocation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        MAAnnotationView *annotationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:reuseIndetifier];
        }
        
        annotationView.image = [UIImage imageNamed:@"message_place2"];
        //设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -kAdaptedValue(37/2));
        return annotationView;
    }
    return nil;
}

-(void)mapBackToOrginLocation{
    
    self.aMapView.centerCoordinate = self.aMapView.userLocation.location.coordinate;
    
}

-(void)setUpNaviView{
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CJCChatLocationVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"
#import <EaseUI.h>

typedef NS_ENUM(NSInteger,CJCChatLocationVCType){
    
    CJCChatLocationVCTypeDefault              = 0,//位置消息的展示
    CJCChatLocationVCTypeYaoyue               = 1,//邀约消息的展示
};

@interface CJCChatLocationVC : CommonVC

@property (nonatomic ,assign) CJCChatLocationVCType locationVCType;

@property (nonatomic ,strong) id<IMessageModel> messageModel;

@property (nonatomic ,copy) NSString *hotelName;

@property (nonatomic ,copy) NSString *hotelAddress;

@property (nonatomic ,assign) CGFloat lat;

@property (nonatomic ,assign) CGFloat lng;

@end

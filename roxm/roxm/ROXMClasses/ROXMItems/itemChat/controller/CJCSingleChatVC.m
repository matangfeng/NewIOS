//
//  CJCSingleChatVC.m
//  roxm
//
//  Created by lfy on 2017/9/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <objc/runtime.h>
#import <objc/message.h>
#import "CJCSingleChatVC.h"
#import "CJCCommon.h"
#import <Hyphenate/Hyphenate.h>
#import <EaseUI.h>
#import "CJCInputView.h"
#import "CJCToolBarMoreView.h"
#import "CJCChooseLocationVC.h"
#import <AMapSearchKit/AMapSearchKit.h>
#import "TZImageManager.h"
#import "CJCBeautyCameraVC.h"
#import "TZImagePickerController.h"
#import "CJCNavigationController.h"
#import "CJCChatMessageHandle.h"
#import "CJCMessageTextCell.h"
#import "CJCMessageImageCell.h"
#import "CJCMessageAudioCell.h"
#import "CJCMessageYaoyueCell.h"
#import "CJCMessageVideoCell.h"
#import "CJCPersonalInfoVC.h"
#import <MWPhotoBrowser.h>
#import "CJCChatLocationVC.h"
#import "CJCpersonalInfoModel.h"
#import "CJCMessageHandle.h"
#import "CJCMessageLocationCell.h"
#import "CJCInviteOrderDetailVC.h"
#import "CJCMessageOrderStatusCell.h"

#define kMessageTextCell        @"CJCMessageTextCell"
#define kMessageImageCell       @"CJCMessageImageCell"
#define kMessageAudioCell       @"CJCMessageAudioCell"
#define kMessageYaoyueCell      @"CJCMessageYaoyueCell"
#define kMessageVideoCell       @"CJCMessageVideoCell"
#define kMessageLocationCell    @"CJCMessageLocationCell"
#define kMessageOrderStatusCell @"CJCMessageOrderStatusCell"

@interface CJCSingleChatVC ()<EaseMessageViewControllerDelegate,EaseMessageViewControllerDataSource,CJCToolBarMoreViewDelegate,TZImagePickerControllerDelegate,CJCMessageBaseCellDelegate>{

    EMConversation *conversation;
    
}

@property (nonatomic ,strong) EaseMessageViewController *messageVC;

@property (nonatomic ,strong) CJCInputView *inputBar;

@property (nonatomic ,strong) UILabel *titleLabel;

@end

@implementation CJCSingleChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self configNaviView];
    [self configTableView];
    [self configBaseSetting];
}

#pragma mark ======环信的EasyUI代理
//具体创建自定义Cell的样例：
- (UITableViewCell *)messageViewController:(UITableView *)tableView cellForMessageModel:(id<IMessageModel>)model
{
    
    if (model.bodyType == EMMessageBodyTypeText) {
        
        if ([model.text isEqualToString:kHXYAOYUEIDENTIGYCATION]) {
            
            CJCMessageYaoyueCell *cell = [tableView dequeueReusableCellWithIdentifier:kMessageYaoyueCell];
            
            if (cell == nil) {
                
                cell = [[CJCMessageYaoyueCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMessageYaoyueCell];
            }
            
            cell.delegate = self;
            [cell updateUIWithModel:model];
            
            return cell;
        }
        
        if ([model.text isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
        
            CJCMessageOrderStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:kMessageOrderStatusCell];
            
            if (cell == nil) {
                
                cell = [[CJCMessageOrderStatusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMessageOrderStatusCell];
            }
            
            cell.delegate = self;
            [cell updateUIWithModel:model];
            
            return cell;
        }
        
        CJCMessageTextCell *cell = [tableView dequeueReusableCellWithIdentifier:kMessageTextCell];
        
        if (cell == nil) {
            
            cell = [[CJCMessageTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMessageTextCell];
        }
        
        cell.delegate = self;
        [cell updateUIWithModel:model];
        return cell;
        
    }else if (model.bodyType == EMMessageBodyTypeImage) {
        
        CJCMessageImageCell *cell = [tableView dequeueReusableCellWithIdentifier:kMessageImageCell];
        
        if (cell == nil) {
            
            cell = [[CJCMessageImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMessageImageCell];
        }
        
        cell.delegate = self;
        [cell updateUIWithModel:model];
        return cell;
        
    }else if (model.bodyType == EMMessageBodyTypeVoice) {
        
        CJCMessageAudioCell *cell = [tableView dequeueReusableCellWithIdentifier:kMessageAudioCell];
        
        if (cell == nil) {
            
            cell = [[CJCMessageAudioCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMessageAudioCell];
        }
        
        cell.delegate = self;
        [cell updateUIWithModel:model];
        return cell;
        
    }else if (model.bodyType == EMMessageBodyTypeLocation) {
        
        CJCMessageLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:kMessageLocationCell];
        
        if (cell == nil) {
            
            cell = [[CJCMessageLocationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMessageLocationCell];
        }
        
        cell.delegate = self;
        [cell updateUIWithModel:model];
        return cell;
        
    }else if (model.bodyType == EMMessageBodyTypeVideo) {
        
        CJCMessageVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:kMessageVideoCell];
        
        if (cell == nil) {
            
            cell = [[CJCMessageVideoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMessageVideoCell];
        }
        
        cell.delegate = self;
        [cell updateUIWithModel:model];
        return cell;
        
    }
    
    return nil;
}

- (CGFloat)messageViewController:(EaseMessageViewController *)viewController
           heightForMessageModel:(id<IMessageModel>)messageModel
                   withCellWidth:(CGFloat)cellWidth
{
    
    //样例为如果消息是文本消息使用用户自定义cell的高度
    if (messageModel.bodyType == EMMessageBodyTypeText) {
        //CustomMessageCell为用户自定义cell,继承了EaseBaseMessageCell
        
        if ([messageModel.text isEqualToString:kHXYAOYUEIDENTIGYCATION]) {
            
            return 110+16;
        }
        
        if ([messageModel.text isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
            
            return 70+16;
        }
        
        CGSize cellHeight = [CJCChatMessageHandle sizeWithString:messageModel.text font:[UIFont accodingVersionGetFont_lightWithSize:15]];
        
        return cellHeight.height+22+16;
    }else if (messageModel.bodyType == EMMessageBodyTypeImage) {
        
        NSDictionary *ext = messageModel.message.ext;
        
        if (ext) {
            
            NSNumber *widthNum = ext[kHUANXINIMAGEWIDTH];
            NSNumber *heightNum = ext[kHUANXINIMAGEHEIGHT];
            
            CGSize imageSize1 = CGSizeMake(widthNum.floatValue, heightNum.floatValue);
            
            CGSize imageSize = [CJCChatMessageHandle CJC_getScaleImageSize:imageSize1];
            
            NSString *typeStr = ext[kHUANXINIMAGESOURCE];
            
            if ([typeStr isEqualToString:kHUANXINIMAGECAMERA]){
            
                return imageSize.height+18+16;
            }
            
            return imageSize.height+16;
        }
        
        CGSize imageSize = [CJCChatMessageHandle CJC_getScaleImageSize:messageModel.imageSize];
        
        return imageSize.height+30+10;
    }else if (messageModel.bodyType == EMMessageBodyTypeVoice) {
        
        return 45+16;
    }else if (messageModel.bodyType == EMMessageBodyTypeLocation) {
        
        return 176+16;
    }else if (messageModel.bodyType == EMMessageBodyTypeVideo) {
        
        return 100+16;
    }
    return 0.f;
}

- (id<IMessageModel>)messageViewController:(EaseMessageViewController *)viewController modelForMessage:(EMMessage *)message{

//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    id<IMessageModel> model = nil;
    
    model = [[EaseMessageModel alloc] initWithMessage:message];
    
    if (model.isSender) {
        
        model.avatarURLPath = infoModel.imageUrl;
        
    }else{
    
        model.avatarURLPath = self.iconImageUrl;
    }
    
    return model;
}

#pragma mark =========自定义cell的点击事件
-(void)iconImageViewClickHandle:(id<IMessageModel>)messageModel{

    [self iconImageViewDidClick:messageModel];
}

-(void)messageClickHandle:(id<IMessageModel>)messageModel{

    [self tapGestureAction];
    
    switch (messageModel.bodyType) {
        case EMMessageBodyTypeText:{
        
            if ([messageModel.text isEqualToString:kHXYAOYUEIDENTIGYCATION]){
            
                NSDictionary *extDict = messageModel.message.ext;
                
                CJCInviteOrderDetailVC *nextVC = [[CJCInviteOrderDetailVC alloc]init];
                
                nextVC.otherPersonUID = self.conversationId;
                nextVC.orderId = extDict[kHXYAOYUEORDERID];
                nextVC.messageVC = self.messageVC;
                
                [self.navigationController pushViewController:nextVC animated:YES];
            }
            
            if ([messageModel.text isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
                
                NSDictionary *extDict = messageModel.message.ext;
                
                CJCInviteOrderDetailVC *nextVC = [[CJCInviteOrderDetailVC alloc]init];
                
                nextVC.otherPersonUID = self.conversationId;
                nextVC.orderId = extDict[kHXYAOYUEORDERID];
                nextVC.messageVC = self.messageVC;
                
                [self.navigationController pushViewController:nextVC animated:YES];
            }
        }
            break;
            
        case EMMessageBodyTypeImage:{
            
            SEL testFunc = NSSelectorFromString(@"_imageMessageCellSelected:");
            
            ((void(*)(id,SEL, id,id))objc_msgSend)(self.messageVC, testFunc, messageModel, nil);
        }
            break;
            
        case EMMessageBodyTypeVoice:{
            
            SEL testFunc = NSSelectorFromString(@"messageCellSelected:");
            
            ((void(*)(id,SEL, id,id))objc_msgSend)(self.messageVC, testFunc, messageModel, nil);
        }
            break;
            
        case EMMessageBodyTypeVideo:{
            
            SEL testFunc = NSSelectorFromString(@"messageCellSelected:");
            
            ((void(*)(id,SEL, id,id))objc_msgSend)(self.messageVC, testFunc, messageModel, nil);
        }
            break;
            
        case EMMessageBodyTypeLocation:{
            
            CJCChatLocationVC *nextVC = [[CJCChatLocationVC alloc] init];
            
            nextVC.messageModel = messageModel;
            
            [self.navigationController pushViewController:nextVC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

-(void)resendMessageHandle:(id<IMessageModel>)messageModel{

    [self messageResend:messageModel];
}

-(void)messageResend:(id<IMessageModel>)messageModel{

    [[[EMClient sharedClient] chatManager] resendMessage:messageModel.message progress:nil completion:^(EMMessage *message, EMError *error) {
        if (!error) {
            
            [self.messageVC.tableView reloadData];
        }
        
    }];

}

-(void)iconImageViewDidClick:(id<IMessageModel>)messageModel{

    NSString *infoUID;
    
    if (messageModel.isSender) {
        
        infoUID = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
        
    }else{
    
        infoUID = self.qunjuId;
    }
    
    CJCPersonalInfoVC *nextVC = [[CJCPersonalInfoVC alloc] init];
    
    nextVC.sexType = CJCSexTypeWoman;
    nextVC.otherUID = infoUID;
    nextVC.location = self.location;
    nextVC.infoVCType = CJCPersonalInfoVCTypeChat;
    
    nextVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:nextVC animated:YES];
}



-(void)buttonInMoreViewClick:(NSInteger)buttonIndx{

    switch (buttonIndx) {
        case 1:{
        //相册
            
            PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
            if (authStatus == PHAuthorizationStatusAuthorized)
            {
                
                [self openPhotoLibrary];
                
            }else{
                
                [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                    
                    if (status == PHAuthorizationStatusAuthorized)
                    {
                        
                        // 用户同意获取麦克风
                        //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self openPhotoLibrary];
                        });
                        
                    } else {
                        
                        // 用户不同意获取麦克风
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self alertVCShowWith:@"选择相册图片需要开启手机的相册权限"];
                        });
                    }
                }];
                
            }

        }
            break;
            
        case 2:{
            //拍摄
            
            AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (authStatus == AVAuthorizationStatusAuthorized)
            {
                
                [self openCamera];
                
            }else{
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {//相机权限
                    if (granted) {
                        
                        //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self openCamera];
                        });
                        
                    }else{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self alertVCShowWith:@"拍摄照片需要开启手机的相机权限"];
                        });
                        
                    }
                }];
                
            }

        }
            break;
            
        case 3:{
            //定位
            
            CJCChooseLocationVC *nextVC = [[CJCChooseLocationVC alloc] init];
            
            nextVC.location = self.location;
            
            nextVC.locationHandle = ^(AMapPOI *POI, NSString *imagePath, double distance) {
                
                EMMessage *message = [CJCMessageHandle CJC_HuanxinLocationMessage:POI andImage:imagePath andDistance:distance andTo:self.conversationId];
                
                [self useHuanxinVCSendMessage:message];
            };
            
            nextVC.addressLocationHandle = ^(CLLocationCoordinate2D returnLocation, NSString *address, NSString *imagePath, double distance) {
                
                EMMessage *message = [CJCMessageHandle CJC_HuanxinLocationMessage:returnLocation andAddress:address andImage:imagePath andDistance:distance andTo:self.conversationId];
                
                [self useHuanxinVCSendMessage:message];
                
            };
            
            CJCNavigationController *naviVC = [[CJCNavigationController alloc] initWithRootViewController:nextVC];
            
            [self presentViewController:naviVC animated:YES completion:nil];
        }
            break;
            
        case 4:{
            //红包
            
        }
            break;
            
        default:
            break;
    }
    
    
}

-(void)openPhotoLibrary{

    TZImagePickerController *nextVC = [[TZImagePickerController alloc] initWithMaxImagesCount:10 columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    
    nextVC.allowTakePicture = NO;
    nextVC.allowPickingMultipleVideo = YES;
    nextVC.allowPickingOriginalPhoto = NO;
    nextVC.allowPreview = NO;
    nextVC.oKButtonTitleColorNormal = [UIColor toUIColorByStr:@"429CF0"];
    nextVC.oKButtonTitleColorDisabled = [UIColor toUIColorByStr:@"429CF0"];
    
    [self presentViewController:nextVC animated:YES completion:nil];
}

-(void)openCamera{

    CJCBeautyCameraVC *cameraVC = [[CJCBeautyCameraVC alloc] init];
    
    cameraVC.takePhotoHandle = ^(UIImage *returnImage) {
        
        EMMessage *message = [CJCMessageHandle CJC_HuanxinImageMessage:returnImage andType:CJCImageSourceTypeCamera andTo:self.conversationId];
        
        [self useHuanxinVCSendMessage:message];
    };
    
    cameraVC.takeVideoHandle = ^(UIImage *returnImage, NSString *videoPath) {
        
        EMMessage *message = [CJCMessageHandle CJC_HuanxinVideoMessage:videoPath andTo:self.conversationId];
        
        [self useHuanxinVCSendMessage:message];
        
    };
    
    [self.navigationController pushViewController:cameraVC animated:YES];
}

#pragma mark  调用messageVC的私有方法  发送消息
-(void)useHuanxinVCSendMessage:(EMMessage *)message{

    SEL testFunc = NSSelectorFromString(@"_sendMessage:");
    
    ((void(*)(id,SEL, id,id))objc_msgSend)(self.messageVC, testFunc, message, nil);
    
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    
    [MBManager showLoading];
    
    for (int i=0; i<photos.count; i++) {
        
        PHAsset *tempAsset = assets[i];
        
        if (tempAsset.mediaType == PHAssetMediaTypeImage) {
         
            [MBManager hideAlert];
            UIImage *sendImage = photos[i];
            
            EMMessage *message = [CJCMessageHandle CJC_HuanxinImageMessage:sendImage andType:CJCImageSourceTypeLibrary andTo:self.conversationId];;
            
            [self useHuanxinVCSendMessage:message];
            
        }else if (tempAsset.mediaType == PHAssetMediaTypeVideo){
        
            PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
            options.version = PHImageRequestOptionsVersionCurrent;
            options.deliveryMode = PHVideoRequestOptionsDeliveryModeAutomatic;
            
            PHImageManager *manager = [PHImageManager defaultManager];
            [manager requestAVAssetForVideo:tempAsset options:options resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                AVURLAsset *urlAsset = (AVURLAsset *)asset;
                
                NSURL *url = urlAsset.URL;
                
                NSString *movieURL = [CJCTools _convert2Mp4:url];
                
                [MBManager hideAlert];
                
                EMMessage *message = [CJCMessageHandle CJC_HuanxinVideoMessage:movieURL andTo:self.conversationId];
                
                [self useHuanxinVCSendMessage:message];
                
            }];
        
        }
    }
    
}

#pragma mark ====设置基本属性  及创建UI
-(void)configBaseSetting{

    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    handle.nickName = self.nickName;
    handle.iconImageUrl = self.iconImageUrl;
    handle.infoModel = self.infoModel;
}

-(void)configTableView{

    EaseMessageViewController *messageVC = [[EaseMessageViewController alloc] initWithConversationChatter:self.conversationId conversationType:EMConversationTypeChat];
    
    messageVC.view.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    
    messageVC.delegate = self;
    messageVC.dataSource =self;
    
    messageVC.deleteConversationIfNull = YES;
    
    CJCInputView *inputView = [[CJCInputView alloc] init];
    
    inputView.frame = CGRectMake(0, SCREEN_HEIGHT - 60-64, SCREEN_WITDH, 60);
    
    inputView.delegate = messageVC;
    inputView.moreView.delegate = self;
    inputView.conversationId = self.conversationId;
    
    messageVC.chatToolbar = inputView;
    
    self.inputBar = inputView;
    
    [inputView addObserver:self forKeyPath:@"frame" options: NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    
    messageVC.messageCountOfPage = 100;
    messageVC.showRefreshHeader = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction)];
    
    [messageVC.tableView addGestureRecognizer:tapGesture];
    
    self.messageVC = messageVC;
    
    [self addChildViewController:messageVC];
    [self.view addSubview:messageVC.view];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    CGRect newFrame = [[change objectForKey:NSKeyValueChangeNewKey] CGRectValue];
    
    CGRect rect = self.messageVC.tableView.frame;
    rect.origin.y = 0;
    rect.size.height = self.view.frame.size.height - newFrame.size.height;
    self.messageVC.tableView.frame = rect;
    
    if (self.messageVC.tableView.contentSize.height > self.messageVC.tableView.frame.size.height)
    {
        CGPoint offset = CGPointMake(0, self.messageVC.tableView.contentSize.height - self.messageVC.tableView.frame.size.height);
        [self.messageVC.tableView setContentOffset:offset animated:NO];
    }
}

-(void)tapGestureAction{

    CJCInputView *putView = (CJCInputView *)self.messageVC.chatToolbar;
    
    if (putView.height > kAdaptedValue(60)) {
        
        [putView inputViewShrink];
    }
}

-(void)configNaviView{

    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backButton.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(33.5), kAdaptedValue(10.5), kAdaptedValue(17.5));
    
    backButton.centerY = kNAVIVIEWCENTERY;
    
    [backButton setImage:kGetImage(@"nav_icon_back") forState:UIControlStateNormal];
    
    [self.view addSubview:backButton];
    
    UIButton *bigBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    bigBackButton.frame = CGRectMake(0, 0, 64, 64);
    
    [self.view addSubview:bigBackButton];
    
    [bigBackButton addTarget:self action:@selector(naviViewBackBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *lineView = [[UIView alloc] init];
    
    lineView.frame = CGRectMake(0, 63, SCREEN_WITDH, OnePXLineHeight);
    
    lineView.backgroundColor = [UIColor toUIColorByStr:@"D8D8D8"];
    
    [self.view addSubview:lineView];
    
    UILabel *titleLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.text = self.nickName;
    
    CGFloat width = [CJCTools getShortStringLength:self.nickName withFont:[UIFont accodingVersionGetFont_regularWithSize:16]];
    
    titleLabel.size = CGSizeMake(width, 20);
    titleLabel.centerX = self.view.centerX;
    titleLabel.centerY = kNAVIVIEWCENTERY;
    
    [self.view addSubview:titleLabel];
}

-(void)naviViewBackBtnDidClick{
    
    EMMessage *message = [self.messageVC.conversation latestMessage];
    if (message == nil) {
        [[EMClient sharedClient].chatManager deleteConversation:self.conversationId isDeleteMessages:YES completion:nil];
    }
    
    if (self.popType == CJCSingleChatPopTypePopToInfoVC) {
        
        [self.navigationController popToViewController:self.navigationController.childViewControllers[1] animated:YES];
    }else{
    
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)dealloc{

    [self.inputBar removeObserver:self forKeyPath:@"frame"];
}

//系统权限提示
-(void)alertVCShowWith:(NSString *)alertStr{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertStr message:nil preferredStyle:  UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        
        if([[UIApplication sharedApplication] canOpenURL:url]) {
            
            NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url];
            
        }
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CJCSingleChatVC.h
//  roxm
//
//  Created by lfy on 2017/9/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef NS_ENUM(NSInteger,CJCSingleChatPopType){
    
    CJCSingleChatPopTypeDefault               = 0,//直接pop
    CJCSingleChatPopTypePopToInfoVC           = 1,//popToViewcontroller

};

@class CLLocation,CJCpersonalInfoModel;

@interface CJCSingleChatVC : CommonVC

@property (nonatomic ,copy) NSString *conversationId;

@property (nonatomic ,strong) CLLocation *location;

@property (nonatomic ,copy) NSString *qunjuId;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@property (nonatomic ,copy) NSString *nickName;

@property (nonatomic ,copy) NSString *iconImageUrl;

@property (nonatomic ,assign) CJCSingleChatPopType popType;

@end

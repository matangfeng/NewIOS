//
//  CJCChooseLocationVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/9.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCChooseLocationVC.h"
#import "CJCCommon.h"
#import <MAMapKit/MAMapKit.h>
#import "CJCHotelListCell.h"
#import "CJCCustomAmapPOI.h"
#import "CJCSearchNearbyHotelView.h"
#import "CJCMessageHandle.h"
#import "CJCUploadManager.h"
#import "CJCLocationCell.h"

#define kMAXLISTCOUNT   50
#define kHotelListCell      @"CJCHotelListCell"
#define kLocationCell       @"CJCLocationCell"

@interface CJCChooseLocationVC ()<MAMapViewDelegate,AMapSearchDelegate,UITableViewDelegate,UITableViewDataSource>{

    ///记录地图移动之后的经纬度
    CLLocationCoordinate2D orginLocation;
    
    ///记录选择下方地理位置的经纬度
    CLLocationCoordinate2D selectLocation;
    
    NSArray *searchResult;
    
    NSMutableArray *dataArray;
    
    CJCCustomAmapPOI *locationPOI;
    
    NSIndexPath *selectIndexPath;
    
    NSTimer *mapMoveTimer;
}

@property (nonatomic ,strong) UIView *mapContainView;

@property (nonatomic ,strong) MAMapView *aMapView;

@property (nonatomic ,strong) UIImageView *locationImageView;

@property (nonatomic ,strong) UILabel *searchHintLabel;

@property (nonatomic ,strong) UIView *nowLocationView;

@property (nonatomic ,strong) AMapSearchAPI *search;

@property (nonatomic ,strong) UITableView *listTableView;

@property (nonatomic ,strong) UIButton *sendButton;
@property (nonatomic ,strong) UILabel *sendLabel;

@end

@implementation CJCChooseLocationVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    [self locationImageViewAnimation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    [self setUpUI];
    [self configAmapView];
    
    dataArray = [NSMutableArray array];
    selectIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];;
    
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    
    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    
    orginLocation = handle.location.coordinate;
    
    [self searchLocation];
    [self searchNearbyHotelList];
}

#pragma mark =======tableView 的代理方法和数据源
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (section == 0) {
        
        return 1;
    }
    
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return kAdaptedValue(54);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        
        CJCLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:kLocationCell];
        
        cell.customAmapPOI = locationPOI;
        
        return cell;
    }
    
    CJCHotelListCell *cell = [tableView dequeueReusableCellWithIdentifier:kHotelListCell];
    
    CJCCustomAmapPOI *POI = dataArray[indexPath.row];
    
    cell.customAmapPOI = POI;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == selectIndexPath.section && indexPath.row == selectIndexPath.row) {
        
        
    }else{
    
        CJCCustomAmapPOI *newPOI;
        
        if (indexPath.section == 0) {
            
            newPOI = locationPOI;
            newPOI.isSelect = YES;
            [tableView reloadRow:0 inSection:0 withRowAnimation:NO];
            
            CJCCustomAmapPOI *selectPOI = dataArray[selectIndexPath.row];
            selectPOI.isSelect = NO;
            
            [tableView reloadRow:selectIndexPath.row inSection:1 withRowAnimation:NO];
            
            selectLocation = orginLocation;
            
            self.aMapView.centerCoordinate = selectLocation;
            
        }else{
        
            newPOI = dataArray[indexPath.row];
            newPOI.isSelect = YES;
            [tableView reloadRow:indexPath.row inSection:indexPath.section withRowAnimation:NO];
            
            CJCCustomAmapPOI *selectPOI;
            
            if (selectIndexPath.section == 0) {
                
                selectPOI = locationPOI;
            }else{
            
                selectPOI = dataArray[selectIndexPath.row];
            }
            selectPOI.isSelect = NO;
            
            [tableView reloadRow:selectIndexPath.row inSection:selectIndexPath.section withRowAnimation:NO];
            
            AMapPOI *tempPOI = newPOI.gaodePOI;
            
            selectLocation.latitude = tempPOI.location.latitude;
            selectLocation.longitude = tempPOI.location.longitude;
            
            self.aMapView.centerCoordinate = selectLocation;
            
        }
        
        selectIndexPath = indexPath;
    
    }

}

#pragma mark =========逆地理编码
-(void)searchLocation{

    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    regeo.location = [AMapGeoPoint locationWithLatitude:orginLocation.latitude longitude:orginLocation.longitude];
    regeo.requireExtension = YES;
    //发起逆地理编码
    [self.search AMapReGoecodeSearch:regeo];
}

- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response{

    if (response.regeocode) {
        
        CJCCustomAmapPOI *customPOI = [[CJCCustomAmapPOI alloc] init];
        customPOI.reGeocode = response.regeocode;
        customPOI.isSelect = YES;
        
        locationPOI = customPOI;
        
        selectLocation = orginLocation;
        
        [self.listTableView reloadRow:0 inSection:0 withRowAnimation:NO];
    }
}

#pragma mark =======获取周边 宾馆酒店 的列表
-(void)searchNearbyHotelList{
    
//    [MBManager showLoadingInView:self.view];
    
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    
    request.location            = [AMapGeoPoint locationWithLatitude:orginLocation.latitude longitude:orginLocation.longitude];
//    request.keywords            = @"宾馆酒店";
    
//    if (self.locationType == CJCChooseLocationTypeInvite) {
//        
//        request.keywords            = @"三星级宾馆";
//    }else{
//    
//        request.keywords            = @"";
//    }
    
    request.keywords            = @"";
    
    /* 按照距离排序. */
    request.sortrule            = 0;
    request.requireExtension    = YES;
    
    [self.search AMapPOIAroundSearch:request];
}

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    
    [MBManager hideAlert];
    
    if (response.pois.count == 0)
    {
        return;
    }
    
    mapMoveTimer = nil;
    
    self.sendLabel.textColor = [UIColor toUIColorByStr:@"429CF0"];
    self.sendButton.userInteractionEnabled = YES;
    
    //解析response获取POI信息，具体解析见 Demo
    searchResult = response.pois;
    
    [dataArray removeAllObjects];
    
    for (int i=0; i<searchResult.count; i++) {
        
        AMapPOI *tempPOI = searchResult[i];
        
        CJCCustomAmapPOI *customPOI = [[CJCCustomAmapPOI alloc] init];
        customPOI.gaodePOI = tempPOI;
        customPOI.isSelect = NO;

        [dataArray addObject:customPOI];
    }
    
    [self.listTableView reloadSection:1 withRowAnimation:NO];
}


#pragma mark ========高德地图的代理方法
//- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation{
//
//    if (![annotation isKindOfClass:[MAUserLocation class]])
//    {
//        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
//        MAAnnotationView *annotationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
//        if (annotationView == nil)
//        {
//            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation
//                                                          reuseIdentifier:reuseIndetifier];
//        }
//        
//        annotationView.image = [UIImage imageNamed:@"message_place2"];
//        //设置中心点偏移，使得标注底部中间点成为经纬度对应点
//        annotationView.centerOffset = CGPointMake(0, -kAdaptedValue(37/2));
//        return annotationView;
//    }
//    return nil;
//}

-(void)mapView:(MAMapView *)mapView mapWillMoveByUser:(BOOL)wasUserAction{
    
    if (wasUserAction) {

        [dataArray removeAllObjects];
        
        selectIndexPath = 0;
        
        [self.listTableView reloadSection:1 withRowAnimation:NO];
    }

}

- (void)mapView:(MAMapView *)mapView mapDidMoveByUser:(BOOL)wasUserAction{

    //如果是用户移动了地图
    if (wasUserAction) {
        
        orginLocation = mapView.centerCoordinate;
        
        MAMapPoint point1 = MAMapPointForCoordinate(self.aMapView.userLocation.location.coordinate);
        
        MAMapPoint point2 = MAMapPointForCoordinate(orginLocation);
        
        CLLocationDistance distance = MAMetersBetweenMapPoints(point1,point2);
        
        if (distance > 300) {
            
            self.nowLocationView.hidden = YES;
        }else{
        
            self.nowLocationView.hidden = NO;
        }
        
        [self searchLocation];
        
        if (mapMoveTimer == nil) {
            
            [self timerOpenFire];
        }else{
        
            if ([mapMoveTimer isValid]) {
                
                [mapMoveTimer invalidate];
                mapMoveTimer = nil;
                [self timerOpenFire];
            }
        }
        
        
//        [self locationImageViewAnimation];
    }
}

-(void)timerOpenFire{

    __weak __typeof(self) weakSelf = self;
    mapMoveTimer = [NSTimer timerWithTimeInterval:0.5 block:^(NSTimer * _Nonnull timer) {
        
        [weakSelf searchNearbyHotelList];
        
    } repeats:NO];
    
    [[NSRunLoop mainRunLoop] addTimer:mapMoveTimer forMode:NSRunLoopCommonModes];
}

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay{

    return nil;
}

-(void)configAmapView{

    MAMapView *_mapView = [[MAMapView alloc] initWithFrame:self.mapContainView.bounds];
    
    ///把地图添加至view
    [self.mapContainView addSubview:_mapView];
    self.aMapView = _mapView;
    
    _mapView.showsUserLocation = YES;
    _mapView.userTrackingMode = MAUserTrackingModeFollow;
    [_mapView setZoomLevel:16.1 animated:YES];
    
    _mapView.showsCompass= NO;
    _mapView.showsScale= NO;
    _mapView.delegate = self;
    _mapView.rotateEnabled = NO;
    _mapView.rotateCameraEnabled = NO;
    _mapView.skyModelEnable = NO;
    _mapView.showsBuildings = NO;
//    _mapView.showsLabels = NO;
    _mapView.customizeUserLocationAccuracyCircleRepresentation = YES;
    
    UIButton *originCenterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [originCenterButton setImage:kGetImage(@"message_place1") forState:UIControlStateNormal];
    
    originCenterButton.frame = CGRectMake(14, 14, kAdaptedValue(44), kAdaptedValue(44));
    
    originCenterButton.bottom = _mapView.height - 14;
    
    [originCenterButton addTarget:self action:@selector(mapBackToOrginLocation) forControlEvents:UIControlEventTouchUpInside];
    
    [_mapView addSubview:originCenterButton];
    
    UIImageView *locationImageView = [[UIImageView alloc] initWithImage:kGetImage(@"message_place2")];
    
    locationImageView.frame = CGRectMake(100, 100, kAdaptedValue(68), kAdaptedValue(37));
    
    locationImageView.centerX = _mapView.centerX;
    locationImageView.centerY = _mapView.centerY-kAdaptedValue(37/2);
    
    self.locationImageView = locationImageView;
    [_mapView addSubview:locationImageView];
    
    UIView *nowLocationView = [[UIView alloc] init];
    
    nowLocationView.backgroundColor = [UIColor toUIColorByStr:@"000000" andAlpha:0.6];
    
    nowLocationView.frame = CGRectMake(100, 100, kAdaptedValue(80), kAdaptedValue(28));
    nowLocationView.centerX = self.view.centerX;
    nowLocationView.bottom = locationImageView.top-kAdaptedValue(15);
    
    self.nowLocationView = nowLocationView;
    [_mapView addSubview:nowLocationView];
    
    UILabel *nowLocationLable = [UIView getYYLabelWithStr:@"当前位置" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    nowLocationLable.frame = CGRectMake(10, 10, kAdaptedValue(60), kAdaptedValue(20));
    nowLocationLable.centerX = kAdaptedValue(40);
    nowLocationLable.centerY = kAdaptedValue(14);
    
    [nowLocationView addSubview:nowLocationLable];
}

-(void)locationImageViewAnimation{

    [UIView animateWithDuration:0.2 animations:^{
       
        self.locationImageView.centerY = self.aMapView.centerY-kAdaptedValue(37/2)-kAdaptedValue(4);
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.locationImageView.centerY = self.aMapView.centerY-kAdaptedValue(37/2);
        }];
    }];
}

-(void)mapBackToOrginLocation{

    self.nowLocationView.hidden = NO;
    
    orginLocation = self.aMapView.userLocation.location.coordinate;
    
    self.aMapView.centerCoordinate = self.aMapView.userLocation.location.coordinate;
    
    [self searchLocation];
    [self searchNearbyHotelList];
}

#pragma mark  =======创建界面
-(void)setUpUI{

    UIView *searchView = [[UIView alloc] init];
    
    searchView.backgroundColor = [UIColor whiteColor];
    
    searchView.frame = CGRectMake(8, 70, SCREEN_WITDH-16, kAdaptedValue(39));
    
    [self.view addSubview:searchView];
    
    UILabel *searchHintLable = [UIView getYYLabelWithStr:@"搜索位置" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"939393"]];
    
    searchHintLable.textAlignment = NSTextAlignmentCenter;
    
    searchHintLable.frame = CGRectMake(0, kAdaptedValue(10), SCREEN_WITDH, kAdaptedValue(20));
    
    self.searchHintLabel = searchHintLable;
    [searchView addSubview:searchHintLable];
    
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    searchButton.frame = searchView.bounds;
    
    [searchButton addTarget:self action:@selector(searchButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [searchView addSubview:searchButton];
    
    UIView *mapView = [[UIView alloc] init];
    
    mapView.backgroundColor = [UIColor whiteColor];
    
    mapView.frame = CGRectMake(0, searchView.bottom+6, SCREEN_WITDH, kAdaptedValue(280));
    
    self.mapContainView = mapView;
    [self.view addSubview:mapView];
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    tableView.frame = CGRectMake(0, mapView.bottom, SCREEN_WITDH, SCREEN_HEIGHT - mapView.bottom);
    
    [tableView registerClass:[CJCHotelListCell class] forCellReuseIdentifier:kHotelListCell];
    [tableView registerClass:[CJCLocationCell class] forCellReuseIdentifier:kLocationCell];
    
    self.listTableView = tableView;
    [self.view addSubview:tableView];
}

-(void)setUpNaviView{

    self.view.backgroundColor = [UIColor toUIColorByStr:@"F3F3F3"];
    
    UIView *naviView = [[UIView alloc] init];
    
    naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    naviView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:naviView];
    
    [self setKeyBoardHid:YES];
    
    UILabel *leftLabel = [UIView getYYLabelWithStr:@"取消" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"8A8A8A"]];
    
    leftLabel.frame = CGRectMake(18, 32, 44, 22.5);
    
    leftLabel.centerY = kNAVIVIEWCENTERY;
    
    [naviView addSubview:leftLabel];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    leftButton.frame = CGRectMake(8, 8, 50, 50);
    
    leftButton.centerY = kNAVIVIEWCENTERY;
    
    [leftButton addTarget:self action:@selector(cancleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [naviView addSubview:leftButton];
    
    UILabel *rightLabel = [UIView getYYLabelWithStr:@"发送" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor lightGrayColor]];
    
    rightLabel.textAlignment = NSTextAlignmentRight;
    
    rightLabel.frame = CGRectMake(325, 32, 44, 22.5);
    rightLabel.right = SCREEN_WITDH - 18;
    rightLabel.centerY = kNAVIVIEWCENTERY;
    
    self.sendLabel = rightLabel;
    [naviView addSubview:rightLabel];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    rightButton.frame = CGRectMake(8, 8, 50, 50);
    
    rightButton.centerY = kNAVIVIEWCENTERY;
    rightButton.right = SCREEN_WITDH - 8;
    rightButton.userInteractionEnabled = NO;
    
    [rightButton addTarget:self action:@selector(sendButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.sendButton = rightButton;
    [naviView addSubview:rightButton];
}

-(void)searchButtonDidClick{

    CJCSearchNearbyHotelView *searchView = [[CJCSearchNearbyHotelView alloc] init];
    
    searchView.showDistance = NO;
    
    searchView.searchHandle = ^(AMapPOI *POI) {
        
        self.searchHintLabel.hidden = NO;
        
        orginLocation.latitude = POI.location.latitude;
        orginLocation.longitude = POI.location.longitude;
        
        self.aMapView.centerCoordinate = orginLocation;
        
        [self searchLocation];
        [self searchNearbyHotelList];
    };
    
    searchView.cancleHandle = ^{
        
         self.searchHintLabel.hidden = NO;
    };
    
    self.searchHintLabel.hidden = YES;
    
    [searchView showSearchView];
}

-(void)sendButtonDidClick{
    
    if (self.locationType == CJCChooseLocationTypeInvite) {
        
        if (selectIndexPath.section == 0) {
            
            [MBManager showBriefAlert:@"请选择酒店"];
            
            return;
        }
    }
    
    MAMapPoint point1 = MAMapPointForCoordinate(self.aMapView.userLocation.location.coordinate);
    
    MAMapPoint point2 = MAMapPointForCoordinate(selectLocation);
    
    CLLocationDistance distance = MAMetersBetweenMapPoints(point1,point2);
    
    UIImage *mapImage = [self.aMapView takeSnapshotInRect:self.aMapView.bounds];
    
    [self showWithLabelAnimation];
    
    CJCUploadManager *manger = [[CJCUploadManager alloc] init];
    
    [manger uploadImage:mapImage successHandle:^(NSString *QiniuKey) {
        
        NSString *domain = [kUSERDEFAULT_STAND objectForKey:kQINIUDOMAIN];
        
        NSString *returnStr = [NSString stringWithFormat:@"%@/%@",domain,QiniuKey];
        
        if (selectIndexPath.section == 0) {
            
            AMapReGeocode *code = locationPOI.reGeocode;
            
            if (self.addressLocationHandle) {
                
                self.addressLocationHandle(selectLocation, code.formattedAddress, returnStr, distance);
            }
            
        }else{
        
            CJCCustomAmapPOI *tempPOI = dataArray[selectIndexPath.row];
            
            if (self.locationHandle) {
                
                self.locationHandle(tempPOI.gaodePOI, returnStr, distance);
            }
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];

}

-(void)cancleButtonClick{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  CJCSearchNearbyHotelView.m
//  roxm
//
//  Created by 陈建才 on 2017/9/12.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCSearchNearbyHotelView.h"
#import "CJCCommon.h"
#import <AMapSearchKit/AMapSearchKit.h>
#import "CJCSearchHotelListCell.h"

#define kSearchHotelListCell @"CJCSearchHotelListCell"

@interface CJCSearchNearbyHotelView ()<AMapSearchDelegate,UITableViewDelegate,UITableViewDataSource>{

    NSMutableArray *dataArray;
}

@property (nonatomic ,strong) AMapSearchAPI *search;

@property (nonatomic ,strong) UIView *containView;

@property (nonatomic ,strong) UITextField *searchTF;

@property (nonatomic ,strong) UITableView *hotelListTB;

@end

@implementation CJCSearchNearbyHotelView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return kAdaptedValue(66);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    CJCSearchHotelListCell *cell = [tableView dequeueReusableCellWithIdentifier:kSearchHotelListCell];
    
    cell.showDistance = self.showDistance;
    
    cell.keys = self.searchTF.text;
    
    AMapPOI *tempPOI = dataArray[indexPath.row];
    cell.AmapPOI = tempPOI;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    AMapPOI *tempPOI = dataArray[indexPath.row];
    
    if (self.searchHandle) {
        
        self.searchHandle(tempPOI);
    }
    
    [self dismissSearchView];
}

-(void)phoneNumTFTextChanged:(UITextField *)textField{

    [self searchNearbyHotelListWith:textField.text];
}

-(void)searchNearbyHotelListWith:(NSString *)key{
    
//    [MBManager showLoadingInView:self];
    
    AMapPOIKeywordsSearchRequest *request = [[AMapPOIKeywordsSearchRequest alloc] init];
    
    request.keywords            = key;
    request.city                = [kUSERDEFAULT_STAND objectForKey:kUSERCITY];
//    request.types            = @"宾馆酒店";
    request.types               = self.searchKeys;
    request.requireExtension    = YES;
    
    [self.search AMapPOIKeywordsSearch:request];
}

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if (response.pois.count == 0)
    {
        return;
    }
    
    //解析response获取POI信息，具体解析见 Demo
    
//    [MBManager hideAlert];
    
    [dataArray removeAllObjects];
    
    [dataArray addObjectsFromArray:response.pois];
    
    self.hotelListTB.hidden = NO;
    [self.hotelListTB reloadData];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.bounds = CGRectMake(0, 20, SCREEN_WITDH, SCREEN_HEIGHT);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.layer.opacity = 0.0;
        
        [self setUpUI];
        
        dataArray = [NSMutableArray array];
        
        self.search = [[AMapSearchAPI alloc] init];
        self.search.delegate = self;
    }
    return self;
}

-(void)setUpUI{

    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(0, 70, SCREEN_WITDH, kAdaptedValue(80))];
    
    containView.backgroundColor = [UIColor whiteColor];
    
    self.containView = containView;
    
    [self addSubview:containView];
    
    UITextField *searchTF = [[UITextField alloc] init];
    
    searchTF.placeholder = @"搜索酒店";
    
    searchTF.frame = CGRectMake(kAdaptedValue(23), 19, SCREEN_WITDH-kAdaptedValue(60), kAdaptedValue(21));
    
    searchTF.centerY = 20+kAdaptedValue(30);
    
    searchTF.font = [UIFont accodingVersionGetFont_lightWithSize:15];
    
    [searchTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    self.searchTF = searchTF;
    [containView addSubview:searchTF];
    
    UILabel *cancleLabel = [UIView getYYLabelWithStr:@"取消" fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    cancleLabel.frame = CGRectMake(SCREEN_WITDH - kAdaptedValue(52), 19, kAdaptedValue(30), kAdaptedValue(21));
    
    cancleLabel.centerY = searchTF.centerY;
    
    [containView addSubview:cancleLabel];
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    cancleButton.frame = CGRectMake(SCREEN_WITDH - kAdaptedValue(60), 0, kAdaptedValue(60), kAdaptedValue(60));
    
    [cancleButton addTarget:self action:@selector(dismissSearchView) forControlEvents:UIControlEventTouchUpInside];
    
    [containView addSubview:cancleButton];
    
}

-(UITableView *)hotelListTB{

    if (_hotelListTB == nil) {
        
        UITableView *tableView = [[UITableView alloc] init];
        
        tableView.delegate = self;
        tableView.dataSource = self;
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.showsVerticalScrollIndicator = NO;
        
        tableView.frame = CGRectMake(0, self.containView.bottom, SCREEN_WITDH, SCREEN_HEIGHT - self.containView.bottom);
        
        [tableView registerClass:[CJCSearchHotelListCell class] forCellReuseIdentifier:kSearchHotelListCell];
        
        self.hotelListTB = tableView;
        [self addSubview:tableView];
        
        UIView *lineView = [UIView getLineView];
        
        lineView.frame = CGRectMake(kAdaptedValue(23), 0, SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
        
        [tableView addSubview:lineView];
    }
    return _hotelListTB;
}

-(void)showSearchView{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self setCenter:[UIApplication sharedApplication].keyWindow.center];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.layer setOpacity:1.0];
        self.containView.y = 20;
        
        [self.searchTF becomeFirstResponder];
        
    } completion:^(BOOL finished) {
    }];
    
}

-(void)dismissSearchView{
    
    [self.searchTF resignFirstResponder];
    
    CGRect frameContent =  self.containView.frame;
    frameContent.origin.y += self.containView.frame.size.height;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.layer setOpacity:0.0];
        self.containView.frame = frameContent;
    } completion:^(BOOL finished) {
        
        if (self.cancleHandle) {
            
            self.cancleHandle();
        }
        
        [self removeFromSuperview];
    }];
}

@end

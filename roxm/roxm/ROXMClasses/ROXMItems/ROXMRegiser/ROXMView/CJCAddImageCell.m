//
//  CJCAddImageCell.m
//  roxm
//
//  Created by lfy on 2017/8/24.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCAddImageCell.h"
#import "CJCCommon.h"

@interface CJCAddImageCell ()

@end

@implementation CJCAddImageCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor toUIColorByStr:@"B6B8C2"];
    
        self.layer.cornerRadius = kAdaptedValue(3);
        self.layer.masksToBounds = YES;
        
        CGFloat imageViewX = frame.size.width - kAdaptedValue(28);
        CGFloat imageViewY = frame.size.height - kAdaptedValue(28);
        
        UIImageView *addImageView = [[UIImageView alloc] initWithImage:kGetImage(@"login_icon_photo_addmore")];
        
        addImageView.userInteractionEnabled = NO;
        
        self.addImageView = addImageView;
        
        addImageView.frame = CGRectMake(imageViewX/2, imageViewY/2, kAdaptedValue(28), kAdaptedValue(28));
        
        [self.contentView addSubview:addImageView];
    }
    return self;
}

#pragma mark - 是否处于编辑状态

- (void)setInEditState:(BOOL)inEditState
{
    self.button.hidden = !inEditState;
}

-(UIImageView *)selectImageView{

    if (_selectImageView == nil) {
        
        _selectImageView = [[UIImageView alloc] init];
        
        _selectImageView.frame = self.bounds;
        
        _selectImageView.userInteractionEnabled = YES;
        
        _selectImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [self.contentView addSubview:_selectImageView];
        
    }
    return _selectImageView;
}

-(void)setSelectImage:(UIImage *)selectImage{

    self.selectImageView.hidden = NO;
    
    self.addImageView.hidden = YES;
    
    self.selectImageView.image = selectImage;
}

- (UIButton *)button
{
    if (!_button) {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.layer.cornerRadius = 7.5;
        [self addSubview:_button];
        
        _button.frame = CGRectMake(self.width-kAdaptedValue(15), 0, kAdaptedValue(15), kAdaptedValue(15));
        
        [_button setBackgroundImage:[UIImage imageNamed:@"life_reduce"] forState:UIControlStateNormal];
        _button.userInteractionEnabled = YES;
        _button.hidden = YES;
    }
    return _button;
}

-(UIView *)playImageView{

    if (_playImageView == nil) {
        
        _playImageView = [[UIView alloc] init];
        
        _playImageView.frame = self.contentView.bounds;
        
        _playImageView.backgroundColor = [UIColor colorWithDisplayP3Red:0 green:0 blue:0 alpha:0.64];
        
        [self.selectImageView addSubview:_playImageView];
        
        UILabel *timeLengthLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor whiteColor]];
        
        timeLengthLabel.frame = CGRectMake(6, self.height - 20, self.width, 17);
        
        self.timeLengthLabel = timeLengthLabel;
        [_playImageView addSubview:timeLengthLabel];
    }
    return _playImageView;
}

@end

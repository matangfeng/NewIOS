//
//  CJCAddImageCell.h
//  roxm
//
//  Created by lfy on 2017/8/24.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UILabel;

@interface CJCAddImageCell : UICollectionViewCell

@property (nonatomic, assign) BOOL inEditState; //是否处于编辑状态

@property (nonatomic ,strong) UIImage *selectImage;

@property (nonatomic ,strong) UIImageView *selectImageView;

@property (nonatomic, strong) UIButton *button;

@property (nonatomic ,strong) UIImageView *addImageView;

@property (nonatomic ,strong) UIView *playImageView;

@property (nonatomic ,strong) UILabel *timeLengthLabel;

@end

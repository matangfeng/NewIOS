//
//  CJCGetPhoneCodeVC.m
//  roxm
//
//  Created by lfy on 2017/8/5.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCGetPhoneCodeVC.h"
#import "CJCCommon.h"
#import "CJCFillInPersonalInfoVC.h"
#import "CJCLoginAndRegisterVC.h"
#import "CJCNavigationController.h"
#import "ROXMRootController.h"

@interface CJCGetPhoneCodeVC ()<UITextFieldDelegate>{
    
    //记录手机号的长度 用于判断是输入 还是删除
    NSInteger phoneNUmLengh;
}

@property (nonatomic ,strong) UILabel *firstTF;

@property (nonatomic ,strong) UILabel *secondTF;

@property (nonatomic ,strong) UILabel *thirdTF;

@property (nonatomic ,strong) UILabel *fourthTF;

@property (nonatomic ,strong) UITextField *hideTF;

@property (nonatomic ,strong) CJCErrorAletLabel *errorLabel;

@property (nonatomic ,strong) UIButton *reSendButton;

@property (nonatomic ,strong) UILabel *phoneLabel;

@end

@implementation CJCGetPhoneCodeVC

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    if (![_tempTimer isValid]){
    
        [self timerOpenFire];
    }
    
    [self resetInputCode];
    
//    [self.hideTF becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    
    if ([_tempTimer isValid]) {
        
        [_tempTimer invalidate];
        _tempTimer = nil;
    }
}

-(void)backBtnDidClick{

    if (_tempTimer) {
        
        [_tempTimer invalidate];
        _tempTimer = nil;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.keyBoardHid = YES;
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    [self setUpUI];
}

-(void)setUpUI{
    
    UIButton *bigBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    bigBackButton.frame = CGRectMake(0, 0, 64, 64);
    
    [self.view addSubview:bigBackButton];
    
    [bigBackButton addTarget:self action:@selector(backBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *loginLabel = [UIView getYYLabelWithStr:@"输入4位验证码" fontName:kFONTNAMELIGHT size:24 color:[UIColor toUIColorByStr:@"222222"]];
    
    loginLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(88.5), kAdaptedValue(158.5), kAdaptedValue(24));
    
    [self.view addSubview:loginLabel];
    
    UILabel *isSendLabel = [UIView getYYLabelWithStr:@"短信验证码已发送至" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat sendLabelWidth = [CJCTools getShortStringLength:@"短信验证码已发送至" withFont:[UIFont accodingVersionGetFont_lightWithSize:14]];
    
    isSendLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(130.5), sendLabelWidth, kAdaptedValue(14));
    
    [self.view addSubview:isSendLabel];
    
    NSString *containdouhao = [NSString stringWithFormat:@"%@,",self.phoneNum];
    
    UILabel *phoneNumLabel = [UIView getYYLabelWithStr:containdouhao fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"101010"]];
    
    self.phoneLabel = phoneNumLabel;
    
    CGFloat phoneLabelWidth = [CJCTools getShortStringLength:containdouhao withFont:[UIFont accodingVersionGetFont_lightWithSize:14]];
    
    phoneNumLabel.frame = CGRectMake(isSendLabel.right+kAdaptedValue(5), kAdaptedValue(130.5), phoneLabelWidth, kAdaptedValue(14));
    
    [self.view addSubview:phoneNumLabel];
    
    UIButton *reSendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    reSendButton.frame = CGRectMake(phoneNumLabel.right, kAdaptedValue(130.5), kAdaptedValue(56), kAdaptedValue(14));
    
    [reSendButton setTitle:@"重新发送" forState:UIControlStateNormal];
    
    self.reSendButton = reSendButton;
    
    [reSendButton addTarget:self action:@selector(reSendButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    reSendButton.titleLabel.font = [UIFont accodingVersionGetFont_lightWithSize:14];
    
    [reSendButton setTitleColor:[UIColor toUIColorByStr:@"4198F0"] forState:UIControlStateNormal];
    
    [self.view addSubview:reSendButton];
    
    [self creatFourTextFieldAndLine];
    
    //隐藏textfield是为了获得键盘的输入  赋值给四个label
    UITextField *hideTextField = [[UITextField alloc] init];
    
    hideTextField.frame = CGRectMake(0, 0, 0, 0);
    
    [hideTextField addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    hideTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    self.hideTF = hideTextField;
    
    [self.view addSubview:hideTextField];
}

-(void)reSendButtonDidClick{

    [MBManager showLoading];
    
    if (self.codeType == CJCGetCodeTypeRegist) {
        
       [self getPhoneCode];
    }else{
    
        [self forgetPassWord];
    }
    
}

-(void)forgetPassWord{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/sms/send/for_find_password"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.phoneNum;
    
    [CJCHttpTool postWithUrl:urlStr params:params success:^(id responseObject) {
        
        [MBManager hideAlert];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0) {
            
            [self timerOpenFire];
            
            self.verificationCode = responseObject[@"data"];
        }else{
            
            [self errorLabelShowWith:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)timerOpenFire{
    
    self.reSendButton.userInteractionEnabled = NO;
    
    [self.reSendButton setTitleColor:[UIColor toUIColorByStr:@"333333" andAlpha:0.5] forState:UIControlStateNormal];
    
    [self.reSendButton setTitle:@"重新发送(60s)" forState:UIControlStateNormal];
    
    self.reSendButton.x = self.phoneLabel.right;
    
    self.reSendButton.width = kAdaptedValue(89.5);
    
    __block NSInteger i = 59;
    
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        
        if (i == 0) {
            
            [timer invalidate];
            timer = nil;
            
            [self timerCloseFire];
        }else{
        
            NSString *timeStr = [NSString stringWithFormat:@"(重新发送%lds)",(long)i--];
            
            [self.reSendButton setTitle:timeStr forState:UIControlStateNormal];
        }
        
        NSLog(@"%ld",i);
        
    }];
    
    _tempTimer = timer;
    
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
}

-(void)getPhoneCode{

    [CJCHttpTool getRoxmVerificationCodeWithPhone:self.phoneNum success:^(id responseObject) {
        
        [MBManager hideAlert];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0) {
            
            [self timerOpenFire];
            
            self.verificationCode = responseObject[@"data"];
        }else{
        
            [self errorLabelShowWith:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)timerCloseFire{

    self.reSendButton.userInteractionEnabled = YES;
    
    self.reSendButton.x = self.phoneLabel.right;
    
    self.reSendButton.width = kAdaptedValue(56);
    
    [self.reSendButton setTitleColor:[UIColor toUIColorByStr:@"4198F0"] forState:UIControlStateNormal];
    
    [self.reSendButton setTitle:@"重新发送" forState:UIControlStateNormal];

    [_tempTimer invalidate];
    _tempTimer = nil;
}

-(void)creatFourTextFieldAndLine{

    for (int i=0; i<4; i++) {
        
        UILabel *codeTf = [[UILabel alloc] init];
        
        codeTf.tag = i+1;
        
        codeTf.frame = CGRectMake(kAdaptedValue(31.5)+i*kAdaptedValue(40), kAdaptedValue(188.5), kAdaptedValue(14.5), kAdaptedValue(24));
        
        codeTf.font = [UIFont accodingVersionGetFont_lightWithSize:24];
        
        codeTf.textColor = [UIColor toUIColorByStr:@"222222"];
        
        
        [self.view addSubview:codeTf];
        
        
        UIView *lineView = [UIView getLineView];
        
        lineView.frame = CGRectMake(kAdaptedValue(23)+i*kAdaptedValue(40), kAdaptedValue(220.5), kAdaptedValue(34), OnePXLineHeight);
        
        [self.view addSubview:lineView];
        
        switch (i+1) {
            case 1:{
            
                self.firstTF = codeTf;
            }
                break;
                
            case 2:{
            
                self.secondTF = codeTf;
                
            }
                break;
                
            case 3:{
            
                self.thirdTF = codeTf;
            }
                break;
                
            case 4:{
            
                self.fourthTF = codeTf;
            }
                break;
                
            default:
                break;
        }
    }

}


#pragma mark ========限制textField 只能输入数字
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [self validateNumber:string];
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}

-(void)phoneNumTFTextChanged:(UITextField *)textField{

    if (self.errorLabel.hidden == NO) {
        
        self.errorLabel.hidden = YES;
    }
    
    //是在输入
    if (phoneNUmLengh < textField.text.length){
    
        switch (textField.text.length ) {
            case 1:{
                
                self.firstTF.text = textField.text;
            }
                break;
                
            case 2:{
                
                NSString *labelStr = [textField.text substringWithRange:NSMakeRange(textField.text.length-1, 1)];
                
                self.secondTF.text = labelStr;
            }
                break;
                
            case 3:{
                
                NSString *labelStr = [textField.text substringWithRange:NSMakeRange(textField.text.length-1, 1)];
                
                self.thirdTF.text = labelStr;
            }
                break;
                
            case 4:{
                
                NSString *labelStr = [textField.text substringWithRange:NSMakeRange(textField.text.length-1, 1)];
                
                self.fourthTF.text = labelStr;
                
                [self.view endEditing:YES];
                
                [self makeSureVerificationCode];
            }
                break;
                
            default:
                break;
        }
    
        phoneNUmLengh = textField.text.length;
    }else{
    
        switch (textField.text.length ) {
            case 1:{
                
                self.secondTF.text = @"";
            }
                break;
                
            case 2:{
                
                self.thirdTF.text = @"";
            }
                break;
                
            case 3:{
                
                self.fourthTF.text = @"";
            }
                break;
                
            case 0:{
                
                self.firstTF.text = @"";
            }
                break;
                
            default:
                break;
        }
        
        phoneNUmLengh = textField.text.length;
    }
    
}

-(void)makeSureVerificationCode{

    if ([self.hideTF.text isEqualToString:self.verificationCode]) {
        
        [self showWithLabelAnimation];
        
        if (self.codeType == CJCGetCodeTypeRegist) {
            
            [self userRegister];
        }else{
        
            [self updatePassWord];
        }
        
       // [self pushToNextVC];
    }else{
    
        [MBManager hideAlert];
        [self errorLabelShowWith:@"验证码有误，请重新输入"];
        
        [self performSelector:@selector(errorLabelHid) afterDelay:0.5];
    }
}

-(void)updatePassWord{

    [self showWithLabelAnimation];
    
    NSString *updateURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"user/forget/password"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.phoneNum;
    params[@"newPassword"] = self.passWord;
    
    [CJCHttpTool postWithUrl:updateURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [self performSelector:@selector(pushToNextVC) afterDelay:0.5];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
            [self resetInputCode];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

-(void)userRegister{

    [self showWithLabelAnimation];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/register"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.phoneNum;
    params[@"password"] = self.passWord;
    
    [CJCHttpTool postWithUrl:urlStr params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            NSLog(@"back:\n%@",responseObject);
            
            [CJCpersonalInfoModel infoModel].loginInfoDict = responseObject;
           
            [kUSERDEFAULT_STAND setObject:responseObject[@"data"][@"user"][@"uid"] forKey:kUSERUID];
            [kUSERDEFAULT_STAND setObject:responseObject[@"data"][@"token"] forKey:kUSERTOKEN];
            [kUSERDEFAULT_STAND setObject:responseObject[@"data"][@"emPassword"] forKey:kHUANXINPASSWORD];
            [kUSERDEFAULT_STAND synchronize];
            
           [self performSelector:@selector(pushToNextVC) afterDelay:0.5];
        }else{
        
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)pushToNextVC{

    if (self.codeType == CJCGetCodeTypeRegist) {
        
        CJCFillInPersonalInfoVC *infoVC = [[CJCFillInPersonalInfoVC alloc] init];
        
        [self.navigationController pushViewController:infoVC animated:YES];
    }else{
    
        [[ROXMRootController shardRootController] TapTabBar];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

-(void)errorLabelShowWith:(NSString *)error{
    
    self.errorLabel.hidden = NO;
    
    self.errorLabel.text = error;
    
}

-(void)errorLabelHid{

    self.errorLabel.hidden = YES;
    
    [self resetInputCode];
}

-(void)resetInputCode{

    self.hideTF.text = @"";
    self.firstTF.text = @"";
    self.secondTF.text = @"";
    self.thirdTF.text = @"";
    self.fourthTF.text = @"";
    
    phoneNUmLengh = 0;
    
    [self.hideTF becomeFirstResponder];
}

-(CJCErrorAletLabel *)errorLabel{
    
    if (_errorLabel == nil) {
        
        _errorLabel = [[CJCErrorAletLabel alloc] init];
        
        _errorLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(233), kAdaptedValue(134), kAdaptedValue(12));
        
        [self.view addSubview:_errorLabel];
    }
    
    return _errorLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

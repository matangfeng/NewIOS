//
//  CJCBeforeTakePhotoVC.h
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef void(^changeVideoHandle)(NSDictionary *infoDict);

@interface CJCBeforeTakePhotoVC : CommonVC

@property (nonatomic ,assign) CJCTakePhotoType takePhotoType;

@property (nonatomic ,assign) CJCSexType sexType;

@property (nonatomic ,assign) CGFloat progressWidth;

@property (nonatomic ,copy) NSString *videoPath;

@property (nonatomic ,copy) NSString *imagePath;

@property (nonatomic ,copy) changeVideoHandle changeHandle;

@end

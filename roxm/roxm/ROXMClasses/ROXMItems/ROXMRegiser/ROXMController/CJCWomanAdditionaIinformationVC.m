//
//  CJCWomanAdditionaIinformationVC.m
//  roxm
//
//  Created by lfy on 2017/8/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCWomanAdditionaIinformationVC.h"
#import "CJCCommon.h"
#import "CJCBirthHeiWeightView.h"
#import "CJCpersonalInfoModel.h"
#import "CJCMaillistVC.h"
#import "CJCChooseDateGiftVC.h"

@interface CJCWomanAdditionaIinformationVC (){

    //每个按钮操作的计数  判断用户信息是否完善
    NSInteger bustButtonNum;
    NSInteger hotelLevelButtonNum;
    NSInteger threeHourButtonNum;
    NSInteger twelveHourButtonNum;

    NSInteger totalOprationNum;
    
    NSString *bustStr;
    NSString *hotelLevelStr;
    NSString *threeHourStr;
    NSString *twelveHourStr;
    NSString * threeHourStrId;
    NSString * twelveHourStrId;
}

@property (nonatomic ,strong) UIButton *BustButton;

@property (nonatomic ,strong) UIImageView *BustImageView;

@property (nonatomic ,strong) UIButton *hotelLevelButton;

@property (nonatomic ,strong) UIImageView *hotelLevelImageView;

@property (nonatomic ,strong) UIButton *threeHourButton;

@property (nonatomic ,strong) UIImageView *threeHourImageView;

@property (nonatomic ,strong) UIButton *twelveHourButton;

@property (nonatomic ,strong) UIImageView *twelveHourImageView;

@property (nonatomic ,strong) CJCOprationButton *nextStepButton;

@end

@implementation CJCWomanAdditionaIinformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpNaviView];
    
    [self setUpUI];
}

-(void)setUpNaviView{
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 61);
    
    [self setNaviViewBottom:61];
    
    self.progressViewWidth = kWOMANREGISTERPROGRESSWIDTH*4;
}

-(void)setUpUI{

    [self creatLabelAndButtonViewWithY:64 andType:@"罩杯"];
    
//    [self creatLabelAndButtonViewWithY:64+kAdaptedValue(57) andType:@"酒店偏好"];
    
    [self creatLabelAndButtonViewWithY:64+kAdaptedValue(57) andType:@"约会3小时"];
    
    [self creatLabelAndButtonViewWithY:64+kAdaptedValue(114) andType:@"约会12小时"];
    
    CJCOprationButton *nextButton = [[CJCOprationButton alloc] init];
    
    nextButton.frame = CGRectMake(kAdaptedValue(87), kAdaptedValue(582), kAdaptedValue(200), kAdaptedValue(45));
    
    [nextButton setTitle:@"完成" forState:UIControlStateNormal];
    
    self.nextStepButton = nextButton;
    
    nextButton.canOpration = NO;
    
    [nextButton addTarget:self action:@selector(nextButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:nextButton];

}

-(void)nextButtonDidClick{

    [self updateInfo];
}

-(void)updateInfo{
    
    [MBManager showLoadingInView:self.view];
    
    NSString *updateUrl = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/update"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    params[@"cupSize"] = bustStr;
    params[@"hotelLevel"] = hotelLevelStr;
    params[@"price1"] = threeHourStr;
    params[@"price2"] = twelveHourStr;
    params[@"giftId1"] = threeHourStrId;
    params[@"giftId2"] = twelveHourStrId;
    
    NSLog(@"dict\n\n  %@",params);
    [CJCHttpTool postWithUrl:updateUrl params:params success:^(id responseObject) {
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            NSLog(@"back \n %@",responseObject);
            NSLog(@"女人走了这里面");
            [CJCpersonalInfoModel infoModel].loginUserDict = responseObject[@"data"][@"user"];
            [[CJCpersonalInfoModel infoModel] userLoginWithDict:nil];
            [self pushToNextVC];
        }else{
            [MBManager hideAlert];
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)pushToNextVC{
    CJCMaillistVC *nextVC = [[CJCMaillistVC alloc] init];
    nextVC.sexType = CJCSexTypeWoman;
    [self.navigationController pushViewController:nextVC animated:YES];
}

#pragma mark ======== 一个label 一个button的view
-(void)creatLabelAndButtonViewWithY:(CGFloat)y andType:(NSString *)type{
    
    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(0, kAdaptedValue(y), SCREEN_WITDH, kAdaptedValue(57))];
    
    [self.view addSubview:containView];
    
    UIView *lineView = [self creatMarginLine];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(56), kAdaptedValue(329), OnePXLineHeight);
    
    [containView addSubview:lineView];
    
    NSString *typeStr = [NSString stringWithFormat:@"%@",type];
    
    UILabel *typeLabel = [UIView getYYLabelWithStr:typeStr fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"666666"]];
    
    typeLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(18), kAdaptedValue(75.5), kAdaptedValue(21));
    
    [containView addSubview:typeLabel];
    
    UIButton *chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    chooseButton.titleLabel.font = [UIFont accodingVersionGetFont_lightWithSize:14];
    
    [chooseButton setTitle:@"选择" forState:UIControlStateNormal];
    
    [chooseButton setTitleColor:[UIColor toUIColorByStr:@"BCBCBC"] forState:UIControlStateNormal];
    
    chooseButton.frame = CGRectMake(kAdaptedValue(113), kAdaptedValue(18), kAdaptedValue(28), kAdaptedValue(20));
    
    [containView addSubview:chooseButton];
    
    [chooseButton addTarget:self action:@selector(chooseBirthHeightWeightButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *downImageView = [[UIImageView alloc] initWithImage:kGetImage(@"list_icon_arrowdown")];
    
    downImageView.frame = CGRectMake(chooseButton.right+10, kAdaptedValue(24.5), kAdaptedValue(14.5), kAdaptedValue(8));
    
    [containView addSubview:downImageView];
    
    UIButton *containButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    containButton.frame = containView.bounds;
    
    [containButton addTarget:self action:@selector(bigButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [containView addSubview:containButton];
    
    if ([type isEqualToString:@"罩杯"]) {
        
        self.BustButton = chooseButton;
        chooseButton.tag = 1011;
        
        self.BustImageView = downImageView;
        
        containButton.tag = 1113;
    }else if ([type isEqualToString:@"酒店偏好"]){
        
        self.hotelLevelButton = chooseButton;
        chooseButton.tag = 1012;
        
        self.hotelLevelImageView = downImageView;
        
        containButton.tag = 1114;
    }else if([type isEqualToString:@"约会3小时"]){
        
        self.threeHourButton = chooseButton;
        chooseButton.tag = 1013;
        
        self.threeHourImageView = downImageView;
        
        CGAffineTransform transform = CGAffineTransformMakeRotation(-M_PI_2);
        downImageView.transform = transform;
        
        containButton.tag = 1115;
    }else if([type isEqualToString:@"约会12小时"]){
    
        self.twelveHourButton = chooseButton;
        chooseButton.tag = 1014;
        
        self.twelveHourImageView = downImageView;
        
        CGAffineTransform transform = CGAffineTransformMakeRotation(-M_PI_2);
        downImageView.transform = transform;
        
        containButton.tag = 1116;
    }
}

-(void)bigButtonDidClick:(UIButton *)button{
    
    switch (button.tag) {
            
        case 1113:{
            
            [self chooseBirthHeightWeightButtonDidClick:self.BustButton];
        }
            break;
            
        case 1114:{
            
            [self chooseBirthHeightWeightButtonDidClick:self.hotelLevelButton];
        }
            break;
            
        case 1115:{
            
//            [self chooseBirthHeightWeightButtonDidClick:self.threeHourButton];
            
            CJCChooseDateGiftVC *nextVC = [[CJCChooseDateGiftVC alloc] init];
            nextVC.chooseHandle = ^(NSString *returnString, NSString *returnIntId, NSNumber *returnIntPrice) {
              
                CGFloat buttonWidth = [CJCTools getShortStringLength:returnString withFont:[UIFont accodingVersionGetFont_lightWithSize:14]];
                
                self.threeHourButton.width = buttonWidth;
                
                [self.threeHourButton setTitleColor:[UIColor toUIColorByStr:@"222222"] forState:UIControlStateNormal];
                
                [self.threeHourButton setTitle:returnString forState:UIControlStateNormal];
                
                self.threeHourImageView.x = self.threeHourButton.right+10;
                
                threeHourStr = returnIntPrice.stringValue;
                threeHourStrId = returnIntId;
                
                if (threeHourButtonNum == 0) {
                    
                    threeHourButtonNum++;
                }
                
                [self nextButtonCanOpration];
            };
            
            [self presentViewController:nextVC animated:YES completion:nil];
        }
            break;
            
        case 1116:{
            
//            [self chooseBirthHeightWeightButtonDidClick:self.twelveHourButton];
            
            CJCChooseDateGiftVC *nextVC = [[CJCChooseDateGiftVC alloc] init];
            
            nextVC.chooseHandle = ^(NSString *returnString, NSString *returnIntId, NSNumber *returnIntPrice) {
                
                CGFloat buttonWidth = [CJCTools getShortStringLength:returnString withFont:[UIFont accodingVersionGetFont_lightWithSize:14]];
                
                self.twelveHourButton.width = buttonWidth;
                
                [self.twelveHourButton setTitleColor:[UIColor toUIColorByStr:@"222222"] forState:UIControlStateNormal];
                
                [self.twelveHourButton setTitle:returnString forState:UIControlStateNormal];
                
                self.twelveHourImageView.x = self.twelveHourButton.right+10;
                
                twelveHourStr = returnIntPrice.stringValue;
                twelveHourStrId = returnIntId;
                
                if (twelveHourButtonNum == 0) {
                    
                    twelveHourButtonNum++;
                }
                
                [self nextButtonCanOpration];
            };
            
            [self presentViewController:nextVC animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
}

-(void)chooseBirthHeightWeightButtonDidClick:(UIButton *)button{
    
    [self.view endEditing:YES];
    
    switch (button.tag) {
        case 1011:{
            
            [self creatDarePickerWithWidth:94 andSexType:0 andButton:button andPickerType:CJCPickerViewTypeBust];
        }
            break;
            
        case 1012:{
            
            [self creatDarePickerWithWidth:42.5 andSexType:0 andButton:button andPickerType:CJCPickerViewTypeHotelLevel];
            
        }
            break;
            
        case 1013:{
            
            [self creatDarePickerWithWidth:32.5 andSexType:0 andButton:button andPickerType:CJCPickerViewTypeThreeHour];
        
        }
            break;
            
        case 1014:{
            
            [self creatDarePickerWithWidth:32.5 andSexType:0 andButton:button andPickerType:CJCPickerViewTypeTwelveHoure];
            
        }
            break;
            
        default:
            break;
    }
}

-(void)creatDarePickerWithWidth:(CGFloat)width andSexType:(CJCSexType)sextype andButton:(UIButton *)button andPickerType:(CJCPickerViewType)pickType{
    
    CJCBirthHeiWeightView *pick = [[CJCBirthHeiWeightView alloc] init];
    
    pick.sexType = sextype;
    
    pick.selectStr = button.titleLabel.text;
    
    pick.pickerViewType = pickType;
    
    [pick showPickerView];
    
    pick.returnHandle = ^(NSString *returnString, NSString *returnInt) {
        
        CGFloat buttonWidth = [CJCTools getShortStringLength:returnString withFont:[UIFont accodingVersionGetFont_lightWithSize:14]];
        
        button.width = buttonWidth;
        
        [button setTitleColor:[UIColor toUIColorByStr:@"222222"] forState:UIControlStateNormal];
        
        [button setTitle:returnString forState:UIControlStateNormal];
        
        switch (button.tag) {
            case 1011:{
                
                self.BustImageView.x = button.right+10;
                
                bustStr = returnInt;
                
                if (bustButtonNum == 0) {
                    
                    bustButtonNum++;
                }
                
            }
                break;
                
            case 1012:{
                
                self.hotelLevelImageView.x = button.right+10;
                
                hotelLevelStr = returnInt;
                
                if (hotelLevelButtonNum == 0) {
                    
                    hotelLevelButtonNum++;
                }
            }
                break;
                
            case 1013:{
                
                self.threeHourImageView.x = button.right+10;
                
                threeHourStr = returnInt;
                
                if (threeHourButtonNum == 0) {
                    
                    threeHourButtonNum++;
                }
            }
                break;
                
            case 1014:{
                
                self.twelveHourImageView.x = button.right+10;
                
                twelveHourStr = returnInt;
                
                if (twelveHourButtonNum == 0) {
                    
                    twelveHourButtonNum++;
                }
            }
                break;
                
            default:
                break;
        }
        
        [self nextButtonCanOpration];
    };
    
}

-(void)nextButtonCanOpration{
    
    totalOprationNum = bustButtonNum+hotelLevelButtonNum+threeHourButtonNum+twelveHourButtonNum;
    
    if (totalOprationNum == 3) {
        
        self.nextStepButton.canOpration = YES;
    }else{
        
        self.nextStepButton.canOpration = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CJCChooseDateGiftVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCChooseDateGiftVC.h"
#import "CJCCommon.h"
#import "CJCInviteGiftCell.h"
#import "CJCInviteGiftModel.h"

#define kInviteGiftCell          @"CJCInviteGiftCell"

@interface CJCChooseDateGiftVC ()<UICollectionViewDelegate,UICollectionViewDataSource>{
    
    NSMutableArray *dataArray;
    
    NSInteger selectIndex;
}

@property (nonatomic ,strong) UICollectionView *giftCollectionView;

@property (nonatomic ,strong) UIButton *buyButton;

@end

@implementation CJCChooseDateGiftVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = [NSMutableArray array];
    selectIndex = -1;
    
    [self setUpNaviView];
    
    [self setUpUI];
    
    [self getGiftList];
}

-(void)getGiftList{
    
    [self showWithLabelAnimation];
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/list/system"];
    
    [CJCHttpTool postWithUrl:giftURL params:nil success:^(id responseObject) {
        
        [self hidHUD];
        NSLog(@"li wu list\n%@",responseObject);
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCInviteGiftModel class] json:responseObject[@"data"]];
            
            [dataArray addObjectsFromArray:tempArr];
            
            [self.giftCollectionView reloadData];
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCInviteGiftModel *model = dataArray[indexPath.item];
    
    if (selectIndex < 0) {
        
        selectIndex = indexPath.item;
        
        model.isSelect = YES;
        
        [self.giftCollectionView reloadItemsAtIndexPaths:@[indexPath]];
    }else{
        
        if (selectIndex != indexPath.item) {
            
            model.isSelect = YES;
            
            CJCInviteGiftModel *oldModel = dataArray[selectIndex];
            
            oldModel.isSelect = NO;
            
            NSIndexPath *oldIndex = [NSIndexPath indexPathForRow:selectIndex inSection:0];
            
            [self.giftCollectionView reloadItemsAtIndexPaths:@[indexPath,oldIndex]];
            
            selectIndex = indexPath.item;
        }
    }
    
    self.buyButton.userInteractionEnabled = YES;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCInviteGiftCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kInviteGiftCell forIndexPath:indexPath];
    
    CJCInviteGiftModel *model = dataArray[indexPath.item];
    
    cell.giftModel = model;
    
    return cell;
}

-(void)setUpUI{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    CGFloat itemWidth = (SCREEN_WITDH)/4;
    
    flowLayout.itemSize = CGSizeMake(itemWidth, kAdaptedValue(136));
    flowLayout.minimumInteritemSpacing = 0;
    
    flowLayout.minimumLineSpacing = 0;
    
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64-70) collectionViewLayout:flowLayout];
    
    collectionView.backgroundColor = [UIColor whiteColor];
    
    collectionView.showsVerticalScrollIndicator = NO;
    
    collectionView.alwaysBounceVertical = YES;
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    [collectionView registerClass:[CJCInviteGiftCell class] forCellWithReuseIdentifier:kInviteGiftCell];
    
    self.giftCollectionView = collectionView;
    [self.view addSubview:collectionView];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    buyButton.backgroundColor = [UIColor toUIColorByStr:@"B39B6B"];
    
    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
    [buyButton setTitle:@"确定" forState:UIControlStateNormal];
    
    buyButton.frame = CGRectMake(kAdaptedValue(18), collectionView.bottom+1, SCREEN_WITDH-kAdaptedValue(36), 49);
    
    buyButton.userInteractionEnabled = NO;
    
    [buyButton addTarget:self action:@selector(buyButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    self.buyButton = buyButton;
    [self.view addSubview:buyButton];
}

-(void)buyButtonClickHandle{
    
    if (selectIndex < 0){
    
        [self showWithLabel:@"请选择礼物"];
    }else{
    
        CJCInviteGiftModel *model = dataArray[selectIndex];
        
        if (self.chooseHandle) {
            
            self.chooseHandle(model.name, model.giftId, [NSNumber numberWithFloat:model.price]);
        }
        
        [self cancleButtonClick];
    }
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    UILabel *leftLabel = [UIView getYYLabelWithStr:@"取消" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    leftLabel.frame = CGRectMake(18, 32, 44, 22.5);
    
    leftLabel.centerY = kNAVIVIEWCENTERY;
    
    [self.view addSubview:leftLabel];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    leftButton.frame = CGRectMake(8, 8, 50, 50);
    
    leftButton.centerY = kNAVIVIEWCENTERY;
    
    [leftButton addTarget:self action:@selector(cancleButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:leftButton];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(0, 63, SCREEN_WITDH, OnePXLineHeight);
    
    [self.view addSubview:lineView];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"选择礼物" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"选择礼物" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.view addSubview:titleLabel];
}

-(void)cancleButtonClick{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

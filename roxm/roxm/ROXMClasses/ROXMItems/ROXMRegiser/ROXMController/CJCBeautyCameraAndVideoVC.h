//
//  CJCBeautyCameraAndVideoVC.h
//  roxm
//
//  Created by lfy on 2017/8/8.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"
#import "CJCBeforeTakePhotoVC.h"
#import "CJCBirthHeiWeightView.h"

typedef void(^CJCTakePhotoHandle)(UIImage * returnImage);

@interface CJCBeautyCameraAndVideoVC : CommonVC

@property (nonatomic ,assign) CJCTakePhotoType takePhotoType;

@property (nonatomic ,assign) CJCSexType sexType;

@property (nonatomic ,copy) CJCTakePhotoHandle photoHandle;

@end

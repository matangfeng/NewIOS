//
//  CJCAddMorePictureVC.h
//  roxm
//
//  Created by lfy on 2017/8/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@interface CJCAddMorePictureVC : CommonVC

@property (nonatomic ,assign) CGFloat progressWidth;

@property (nonatomic ,assign) CJCSexType sexType;

@end

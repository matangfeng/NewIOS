//
//  CJCBeautyCameraAndVideoVC.m
//  roxm
//
//  Created by lfy on 2017/8/8.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBeautyCameraAndVideoVC.h"
#import "CJCCommon.h"
#import <AVFoundation/AVFoundation.h>
#import "GPUImageBeautifyFilter.h"
#import "CJCBeautyFilter.h"
#import "LFGPUImageBeautyFilter.h"
#import "CJCVideoCrapCommond.h"
#import "CJCExportCommand.h"
#import "CJCMVideoComposeCommand.h"
#import "KWBeautyFilter.h"

//需要拍摄的视频时长
#define needVideoTime 8

@interface CJCBeautyCameraAndVideoVC (){

    BOOL beautyIsopen;
    
    CGPoint OriginalCenter;
    
    NSString *noSelectImageName;
    NSString *selectImageName;
    NSString *selectingImageName;
    NSString *stopSelectImageName;
    
    UIView *alertView;
    UIView *completView;
    UILabel *alertTitleLabel;
    
    CGFloat singleCompleteViewWidth;
    CGFloat holdSecond;
    NSTimer *holdTimer;
    
    //拍摄视频时的手势
    UILongPressGestureRecognizer *longPressGesture;
    UITapGestureRecognizer *takeVideoTap;
    
    NSInteger videoNumber;      //拍摄视频的数量  用于合成视频
    NSMutableArray *videoURLs; //保存每段视频的地址
    NSMutableArray *audioURLs;
    BOOL isFinishRecord; //判断是否拍摄够15秒  重新拍摄的时候需要重置
    
    //录制完视频之后  可以预览拍摄的视频
    AVPlayer *videoPlayer;
    AVPlayerLayer *videoplayerLayer;
    AVPlayerItem *videoitem;
    
}
//视频剪切所用的类
@property AVMutableComposition *composition;
@property AVMutableVideoComposition *videoComposition;
@property AVMutableAudioMix *audioMix;
@property AVAsset *inputAsset;

@property (nonatomic, strong) KWBeautyFilter *kwFilter;

//录制音频
@property (nonatomic ,strong) AVAudioRecorder *audioRecorder;
/*
 美颜相机的方法
 */
@property (nonatomic, strong) GPUImageStillCamera *photoCamera;
//屏幕上显示的View
@property (nonatomic, strong) GPUImageView *filterView;
//BeautifyFace美颜滤镜
@property (nonatomic ,strong) GPUImageFilter *gammaFilter;

@property (nonatomic ,strong) UIButton *openBeautyButton;

@property (nonatomic ,strong) UIButton *closeBeautyButton;
//获取拍摄视频  并保存到本地
@property (nonatomic ,strong) GPUImageMovieWriter *movieWrite;

@property (nonatomic ,strong) UIImageView *tagImageView;

@property (nonatomic ,strong) UIImage *selectImage;

@property (nonatomic ,strong) UIImageView *takePhotoView;

@property (nonatomic ,strong) UIView *reTakePhotoView;

//拍摄视频时的提示view
@property (nonatomic ,strong) UIView *VideoAlertView;

@end

@implementation CJCBeautyCameraAndVideoVC

-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [videoitem removeObserver:self forKeyPath:@"status"];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    beautyIsopen = YES;
    
    if (self.sexType == CJCSexTypeWoman) {
        
        selectImageName = @"login_icon_viedo_femaleflash_done";
    }else{
        
        selectImageName = @"login_icon_viedo_maleflash_done";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(editCommandCompletionNotificationReceiver:)
                                                 name:AVSEEditCommandCompletionNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(exportCommandCompletionNotificationReceiver:)
                                                 name:AVSEExportCommandCompletionNotification
                                               object:nil];
    
    [self setUpUI];
}

- (void)editCommandCompletionNotificationReceiver:(NSNotification*) notification
{
    if ([[notification name] isEqualToString:AVSEEditCommandCompletionNotification]) {
        // Update the document's composition, video composition etc
        self.composition = [[notification object] mutableComposition];
        self.videoComposition = [[notification object] mutableVideoComposition];
        self.audioMix = [[notification object] mutableAudioMix];
        
        CJCExportCommand *exportCommand = [[CJCExportCommand alloc] initWithComposition:self.composition videoComposition:self.videoComposition audioMix:self.audioMix];
        
        NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
        videoStr = [NSString stringWithFormat:@"%@/introduction.mp4",videoStr];
        
        exportCommand.exportPath = videoStr;
        
        [exportCommand performWithAsset:nil];
    }
}

- (void)exportCommandCompletionNotificationReceiver:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:AVSEExportCommandCompletionNotification]) {
        
        [self hidHUD];
    
        [self performSelectorOnMainThread:@selector(creatPreviewView) withObject:nil waitUntilDone:NO];
        
       // [self creatPreviewView];
        
    }
}

-(void)takeVideoDidFinish{
    
    [self oprationDidFinish:YES];
}

-(void)chooseVideoFinish{

    if (self.photoHandle) {
        
        self.photoHandle([self getVideoPreViewImage]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (UIImage*)getVideoPreViewImage
{
    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    videoStr = [NSString stringWithFormat:@"%@/introduction.mp4",videoStr];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:videoStr] options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];

    gen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    
    return img;
}

-(void)creatPreviewView{

    [UIView animateWithDuration:0.2 animations:^{
        
        self.takePhotoView.frame = CGRectMake(kAdaptedValue(260), kAdaptedValue(542.5), kAdaptedValue(74), kAdaptedValue(74));
        
        self.takePhotoView.right = SCREEN_WITDH - kAdaptedValue(42);
        self.takePhotoView.centerY = OriginalCenter.y;
        
        self.takePhotoView.image = kGetImage(selectImageName);
        
        self.reTakePhotoView.left = kAdaptedValue(42);
        
    } completion:^(BOOL finished) {
        
    }];
    
    if (takeVideoTap == nil) {
        
        takeVideoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseVideoFinish)];
        
        [_takePhotoView addGestureRecognizer:takeVideoTap];
    }else{
    
        if (takeVideoTap.enabled == NO) {
            
            takeVideoTap.enabled = YES;
        }
    }
    
    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    videoStr = [NSString stringWithFormat:@"%@/introduction.mp4",videoStr];
    
    if (videoitem == nil) {
        
        videoitem = [[AVPlayerItem alloc] initWithURL:[NSURL fileURLWithPath:videoStr]];
        
        [videoitem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    if (videoPlayer == nil) {
        
       videoPlayer = [[AVPlayer alloc] initWithPlayerItem:videoitem];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:videoitem];
    }
    
    if (videoplayerLayer == nil) {
        
        videoplayerLayer = [AVPlayerLayer playerLayerWithPlayer:videoPlayer];
        
        videoplayerLayer.frame = self.filterView.frame;
        
        [self.view.layer addSublayer:videoplayerLayer];
    }else{
    
        
    }

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{

    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status= [[change objectForKey:@"new"] intValue];
     
        if (status == AVPlayerStatusReadyToPlay) {
            
            [videoPlayer play];
        }
    }
    
    if([keyPath isEqualToString:@"adjustingFocus"]){
        BOOL adjustingFocus =[[change objectForKey:NSKeyValueChangeNewKey] isEqualToNumber:[NSNumber numberWithInt:1]];
        
        NSLog(@"adjustingFocus~~%d  change~~%@", adjustingFocus, change);
    }
}

-(void)playbackFinished:(NSNotification *)notification{
    NSLog(@"视频播放完成.");
    
    
    // 播放完成后重复播放
    // 跳到最新的时间点开始播放
    [videoPlayer seekToTime:CMTimeMake(0, 1)];
    [videoPlayer play];
}


-(void)setUpUI{

    //[self initCaptureSession];
    [self initBeautyView];
    
    [self initCoverView];
    
    [self initOprationView];
    
}

//聚焦模式   只对后置摄像头起作用
-(void)configCamera:(UITapGestureRecognizer *)tgr{

    CGPoint location = [tgr locationInView:self.filterView];
    
    AVCaptureDevice *captureDevice= self.photoCamera.inputCamera;
    AVCaptureSession *captureSession = self.photoCamera.captureSession;
    
    [captureSession addObserver:self forKeyPath:@"adjustingFocus" options:NSKeyValueObservingOptionNew context:nil];
    
    NSError *error;
    BOOL lockAcquired = [captureDevice lockForConfiguration:&error];
    if (lockAcquired) {
        
        [captureSession beginConfiguration];
        
        if ([self.photoCamera cameraPosition] == AVCaptureDevicePositionFront) {
            location.x = SCREEN_WITDH - location.x;
            
        }
        
        CGPoint pointOfInterest = CGPointMake(location.y / self.filterView.height, 1.f - (location.x / self.filterView.width));
        
        /*****必须先设定聚焦位置，在设定聚焦方式******/
        //聚焦点的位置
        if ([captureDevice isFocusPointOfInterestSupported]) {
            [captureDevice setFocusPointOfInterest:pointOfInterest];
        }
        
        // 聚焦模式
        if ([captureDevice isFocusModeSupported:AVCaptureFocusModeLocked]) {
            [captureDevice setFocusMode:AVCaptureFocusModeAutoFocus];
        }else{
            NSLog(@"聚焦模式修改失败");
        }
        
        [captureDevice unlockForConfiguration];
        [captureSession commitConfiguration];
    }
    
}


-(void)initBeautyView{

    self.photoCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:AVCaptureSessionPresetPhoto cameraPosition:AVCaptureDevicePositionFront];
    
    self.photoCamera.outputImageOrientation = UIDeviceOrientationPortrait;
    
    //[self.photoCamera addAudioInputsAndOutputs];
    
    //设置后置摄像头是否显示镜像
    //self.videoCamera.horizontallyMirrorRearFacingCamera = YES;
    
    //设置前置摄像头是否显示镜像
    self.photoCamera.horizontallyMirrorFrontFacingCamera =YES;
    
    self.filterView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 64 , SCREEN_WITDH, SCREEN_WITDH)];
    
    self.filterView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    
    [self.view insertSubview:self.filterView atIndex:1];
    
    self.kwFilter = [[KWBeautyFilter alloc] init];
    
    [self.kwFilter setParam:30 withType:KW_NEWBEAUTY_TYPE_SKINWHITENING];
    
    [self.kwFilter setParam:100 withType:KW_NEWBEAUTY_TYPE_BLEMISHREMOVAL];
    
    [self.kwFilter setParam:52 withType:KW_NEWBEAUTY_TYPE_SKINSATURATION];
    
    [self.kwFilter setParam:70 withType:KW_NEWBEAUTY_TYPE_SKINTENDERNESS];
    
    
    [self.photoCamera addTarget:self.kwFilter];
    [self.kwFilter addTarget:self.filterView];
    
    [self.photoCamera startCameraCapture];
}

-(void)initOprationView{

    UIButton *openBeautYButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [openBeautYButton setTitle:@"开启美颜" forState:UIControlStateNormal];
    
    self.openBeautyButton = openBeautYButton;
    
    openBeautYButton.frame = CGRectMake(kAdaptedValue(155.5), kAdaptedValue(492), kAdaptedValue(68), kAdaptedValue(22.5));
    
    openBeautYButton.centerX = self.view.centerX;
    openBeautYButton.centerY = self.filterView.bottom+kAdaptedValue(62);
    
    [openBeautYButton addTarget:self action:@selector(openBeauty) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:openBeautYButton];
    
    UIButton *closeBeautyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [closeBeautyButton setTitle:@"关闭美颜" forState:UIControlStateNormal];
    
    self.closeBeautyButton = closeBeautyButton;
    
    closeBeautyButton.frame = CGRectMake(openBeautYButton.right+kAdaptedValue(30), kAdaptedValue(493.5), kAdaptedValue(68), kAdaptedValue(22.5));
    
    closeBeautyButton.centerY = self.filterView.bottom+kAdaptedValue(62);
    
    [closeBeautyButton addTarget:self action:@selector(closeBeauty) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeBeautyButton];
    
    UIImage *tagImage = [UIView imageWithColor:[UIColor toUIColorByStr:@"429CF0"]];
    
    UIImageView *tagImageView = [[UIImageView alloc] initWithImage:tagImage];
    
    self.tagImageView = tagImageView;
    
    tagImageView.frame = CGRectMake(kAdaptedValue(184.5), openBeautYButton.top-kAdaptedValue(9), 6, 6);
    
    tagImageView.layer.cornerRadius = 3;
    tagImageView.layer.masksToBounds = YES;
    
    [self.view addSubview:tagImageView];
    
    if (self.sexType == CJCSexTypeWoman) {
        
        [self openBeauty];
        
        noSelectImageName = @"login_icon_photo_femaleflash";
        
    }else{
    
        [self closeBeauty];
        
        noSelectImageName = @"login_icon_photo_maleflash";
        
    }
    
    UIImageView *takePhotoView = [[UIImageView alloc] initWithImage:kGetImage(noSelectImageName)];
    
    self.takePhotoView = takePhotoView;
    
    if (self.takePhotoType == CJCTakePhotoTypeCamera) {
        
        takePhotoView.frame = CGRectMake(kAdaptedValue(152.5), kAdaptedValue(544.5), kAdaptedValue(70), kAdaptedValue(70));
    }else{
    
        takePhotoView.frame = CGRectMake(kAdaptedValue(150.5), kAdaptedValue(542.5), kAdaptedValue(74), kAdaptedValue(74));
    }
    
    OriginalCenter = takePhotoView.center;
    
    [self.view addSubview:takePhotoView];
    
    takePhotoView.userInteractionEnabled = YES;
    
    if (self.takePhotoType == CJCTakePhotoTypeCamera) {
        
        UITapGestureRecognizer *takePhotoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePhotoAction)];
        
        [takePhotoView addGestureRecognizer:takePhotoTap];
    }else{
    
        longPressGesture = [[UILongPressGestureRecognizer  alloc] initWithTarget:self action:@selector(takeVideoAction:)];
        
        longPressGesture.minimumPressDuration = 0.2;
        
        [takePhotoView addGestureRecognizer:longPressGesture];
    }
    
    if (self.takePhotoType == CJCTakePhotoTypeVideo) {
        
        self.VideoAlertView.hidden = NO;
        
        alertTitleLabel.text = [NSString stringWithFormat:@"拍摄%d秒的自我介绍吧",needVideoTime];
        
        completView.width = 0;
        
        holdSecond = 0.0;
        
        singleCompleteViewWidth = SCREEN_WITDH/needVideoTime;
        
        videoURLs = [NSMutableArray array];
        audioURLs = [NSMutableArray array];
        
        isFinishRecord = NO;
        
        if (self.sexType == CJCSexTypeWoman) {
            
            selectingImageName = @"login_icon_viedo_femaleflash_ing";
        }else{
            
            selectingImageName = @"login_icon_viedo_maleflash_ing";
        }
    }
}

#pragma mark ========拍摄视频的操作  及对视频的裁剪处理
-(void)takeVideoAction:(UILongPressGestureRecognizer *)gesture{
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        self.openBeautyButton.hidden = YES;
        self.closeBeautyButton.hidden = YES;
        self.tagImageView.hidden = YES;
        
        videoNumber ++;
        
        NSString *videoStr = [self getVideoURL];
        
        unlink([videoStr UTF8String]); // 如果已经存在文件，AVAssetWriter会有异常，删除旧文件
        // 创建写入文件的时候 可以指定视频的size
        NSDictionary *videoSettings = @{AVVideoCodecKey: AVVideoCodecH264,
                                        AVVideoWidthKey: @(480),
                                        AVVideoHeightKey: @(640),
                                        AVVideoScalingModeKey: AVVideoScalingModeResizeAspectFill};
        
        self.movieWrite = [[GPUImageMovieWriter alloc] initWithMovieURL:[NSURL fileURLWithPath:videoStr] size:CGSizeMake(480, 640) fileType:AVFileTypeQuickTimeMovie outputSettings:videoSettings];
        
        self.movieWrite.encodingLiveVideo = YES;
        
        if (beautyIsopen) {
            
            [self.kwFilter addTarget:self.movieWrite];
        }else{
        
            [self.gammaFilter addTarget:self.movieWrite];

        }
        
        //添加这一行 会引起拍摄后的视频 开头或结尾有黑幕
        self.photoCamera.audioEncodingTarget = self.movieWrite;
        
        [self.movieWrite startRecording];
        
        [self setUpAudioRecorder];
        
        [self videoShouldBegin];
        
    }else if (gesture.state == UIGestureRecognizerStateChanged){
    
        
    
    }else if (gesture.state == UIGestureRecognizerStateEnded){
    
        [self videoShouldEnd];

    }else if (gesture.state == UIGestureRecognizerStateCancelled){
    
        [self videoShouldEnd];
    }

}

-(void)takeVideoBeginAnimation{

    

}

-(void)setUpAudioRecorder{

    AVAudioSession *session =[AVAudioSession sharedInstance];
    NSError *sessionError;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    
    if (session == nil) {
        
        NSLog(@"Error creating session: %@",[sessionError description]);
        
    }else{
        [session setActive:YES error:nil];
        
    }
    
    //设置参数
    NSDictionary *recordSetting = [[NSDictionary alloc] initWithObjectsAndKeys:
                                   //采样率  8000/11025/22050/44100/96000（影响音频的质量）
                                   [NSNumber numberWithFloat: 11025],AVSampleRateKey,
                                   // 音频格式
                                   [NSNumber numberWithInt: kAudioFormatLinearPCM],AVFormatIDKey,
                                   //采样位数  8、16、24、32 默认为16
                                   [NSNumber numberWithInt:16],AVLinearPCMBitDepthKey,
                                   // 音频通道数 1 或 2
                                   [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                                   //录音质量
                                   [NSNumber numberWithInt:AVAudioQualityHigh],AVEncoderAudioQualityKey,
                                   nil];
    
    
    _audioRecorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:[self getAudoiURL]] settings:recordSetting error:nil];
    
    if (_audioRecorder) {
        
        _audioRecorder.meteringEnabled = YES;
        [_audioRecorder prepareToRecord];
        [_audioRecorder record];
        
    }else{
        NSLog(@"音频格式和文件存储格式不匹配,无法初始化Recorder");
        
    }
}

-(NSString *)getAudoiURL{

    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    videoStr = [NSString stringWithFormat:@"%@/introduction_audio%ld.caf",videoStr,(long)videoNumber];
    
    [audioURLs addObject:videoStr];
    
    return videoStr;
}

-(NSString *)getVideoURL{

    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    videoStr = [NSString stringWithFormat:@"%@/introduction%ld.mp4",videoStr,(long)videoNumber];
    
    [videoURLs addObject:videoStr];

    return videoStr;
}

-(void)videoShouldBegin{
    
    alertView.hidden = YES;
    alertTitleLabel.hidden = YES;
    
    if (holdTimer == nil) {
        
        holdTimer = [NSTimer timerWithTimeInterval:0.01 target:self selector:@selector(changeCompleteViewWidth) userInfo:nil repeats:YES];
        
        [[NSRunLoop mainRunLoop] addTimer:holdTimer forMode:NSRunLoopCommonModes];
    }else{
    
        [holdTimer setFireDate:[NSDate date]];
    }
    
    self.takePhotoView.frame = CGRectMake(kAdaptedValue(146.5), kAdaptedValue(538.5), kAdaptedValue(82.5), kAdaptedValue(82.5));
    
    self.takePhotoView.center = OriginalCenter;
    
    self.takePhotoView.image = kGetImage(selectingImageName);
}

-(void)videoShouldEnd{

    if (holdSecond<needVideoTime) {
        
        if (self.sexType == CJCSexTypeWoman) {
            
            stopSelectImageName = @"login_icon_viedo_femaleflash_stop";
        }else{
        
            stopSelectImageName = @"login_icon_photo_maleflash";
        }
        
        alertView.hidden = NO;
        alertTitleLabel.hidden = NO;
        
        alertTitleLabel.text = [NSString stringWithFormat:@"再拍摄%.1f秒松手",needVideoTime-holdSecond];
        
        [holdTimer setFireDate:[NSDate distantFuture]];
        
        self.takePhotoView.frame = CGRectMake(kAdaptedValue(150.5), kAdaptedValue(542.5), kAdaptedValue(74), kAdaptedValue(74));
        
        self.takePhotoView.center = OriginalCenter;
        
        self.takePhotoView.image = kGetImage(noSelectImageName);
    }else{
        
        [holdTimer invalidate];
        holdTimer = nil;
        
        [self.photoCamera stopCameraCapture];

    }
    

    [self.movieWrite finishRecordingWithCompletionHandler:^{
        
        if ([self.audioRecorder isRecording]) {
            [self.audioRecorder stop];
        }
        
        if (beautyIsopen) {
            
            [self.kwFilter removeTarget:self.movieWrite];
        }else{
            
            [self.gammaFilter removeTarget:self.movieWrite];
            
        }
        
        if (isFinishRecord) {
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                
                CJCMVideoComposeCommand *composeCommand = [[CJCMVideoComposeCommand alloc] init];
                
                composeCommand.videoPaths = videoURLs.copy;
                composeCommand.audioPaths = audioURLs.copy;
                
                self.composition = [composeCommand mergeVideostoOnevideo];
                
                CJCVideoCrapCommond *crapCommand = [[CJCVideoCrapCommond alloc] initWithComposition:self.composition videoComposition:self.videoComposition audioMix:self.audioMix];
                
                [crapCommand performWithAsset:nil];
                
                //CJCExportCommand *exportCommand = [[CJCExportCommand alloc] initWithComposition:self.composition videoComposition:self.videoComposition audioMix:self.audioMix];
                
                //[exportCommand performWithAsset:nil];
            });
            
            
        }
    }];
    
    
    
    //[self videoCrapAndSaveWithPath:videoURL];
}

-(void)changeCompleteViewWidth{
    
    holdSecond = holdSecond + 0.01;
    
    completView.width = holdSecond*singleCompleteViewWidth;
    
    if (holdSecond >= needVideoTime) {
        
        isFinishRecord = YES;
        
        longPressGesture.enabled = NO;
        
        [self takeVideoDidFinish];
        
        [self showWithLabelAnimation];
    }
}

#pragma  mark  拍摄照片时的调用方法 及图片的缩放 裁剪
-(void)takePhotoAction{
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.takePhotoView.frame = CGRectMake(kAdaptedValue(260), kAdaptedValue(542.5), kAdaptedValue(74), kAdaptedValue(74));
        
        self.takePhotoView.right = SCREEN_WITDH - kAdaptedValue(42);
        self.takePhotoView.centerY = OriginalCenter.y;
        
        self.takePhotoView.image = kGetImage(selectImageName);
        
        self.reTakePhotoView.left = kAdaptedValue(42);
        
    } completion:^(BOOL finished) {
    
    }];
    
    [self oprationDidFinish:YES];
    
    if (self.selectImage == nil) {
        
        if (beautyIsopen) {
            
            [self.photoCamera capturePhotoAsImageProcessedUpToFilter:self.kwFilter withCompletionHandler:^(UIImage *processedImage, NSError *error) {
                
                [self.photoCamera stopCameraCapture];
                
                UIImage *newImage = [self thumbnailWithImageWithoutScale:processedImage size:CGSizeMake(SCREEN_WITDH, SCREEN_HEIGHT)];
                
                CGFloat imageY = self.view.centerY - SCREEN_WITDH/2;
                
                self.selectImage = [self clipWithImageRect:CGRectMake(0, imageY, SCREEN_WITDH, SCREEN_WITDH) clipImage:newImage];
                //self.selectImage = newImage;
            }];
        }else{
        
            [self.photoCamera capturePhotoAsImageProcessedUpToFilter:self.gammaFilter withCompletionHandler:^(UIImage *processedImage, NSError *error) {
                
                [self.photoCamera stopCameraCapture];
                
                UIImage *newImage = [self thumbnailWithImageWithoutScale:processedImage size:CGSizeMake(SCREEN_WITDH, SCREEN_HEIGHT)];
                
                CGFloat imageY = self.view.centerY - SCREEN_WITDH/2;
                
                self.selectImage = [self clipWithImageRect:CGRectMake(0, imageY, SCREEN_WITDH, SCREEN_WITDH) clipImage:newImage];
            }];
        }
        
    }else{
    
        if (self.photoHandle) {
            
            self.photoHandle(self.selectImage);
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}

- (UIImage *)clipWithImageRect:(CGRect)clipRect clipImage:(UIImage *)clipImage;
{
    
    CGImageRef  imageRef = CGImageCreateWithImageInRect(clipImage.CGImage, clipRect);
    UIGraphicsBeginImageContext(clipRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, clipRect, imageRef);
    UIImage * clipImage1 = [UIImage imageWithCGImage:imageRef];
    UIGraphicsEndImageContext();
    
    return clipImage1;
}

-(void)openBeauty{

    beautyIsopen = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self setButtonSelect:self.openBeautyButton];
        [self setButtonNoSelect:self.closeBeautyButton];
        
        self.openBeautyButton.centerX = self.view.centerX;
        self.closeBeautyButton.x = self.openBeautyButton.right+kAdaptedValue(30);
        
    }];
    
    [self.photoCamera removeAllTargets];
    
    [self.photoCamera addTarget:self.kwFilter];
    [self.kwFilter addTarget:self.filterView];
}

-(void)closeBeauty{
    
    beautyIsopen = NO;

    [UIView animateWithDuration:0.3 animations:^{
        
        [self setButtonSelect:self.closeBeautyButton];
        [self setButtonNoSelect:self.openBeautyButton];
        
        self.closeBeautyButton.centerX = self.view.centerX;
        self.openBeautyButton.right = self.closeBeautyButton.left-kAdaptedValue(30);
        
    }];
    
    [self.photoCamera removeAllTargets];
    
    [self.photoCamera addTarget:self.gammaFilter];
    [self.gammaFilter addTarget:self.filterView];
}


-(void)setButtonSelect:(UIButton *)button{
    
    [button setTitleColor:[UIColor toUIColorByStr:@"429CF0"] forState:UIControlStateNormal];

    button.titleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:16];
    
    button.centerY = self.filterView.bottom+kAdaptedValue(62);
}

-(void)setButtonNoSelect:(UIButton *)button{

    [button setTitleColor:[UIColor toUIColorByStr:@"7A7B80"] forState:UIControlStateNormal];
    
    button.titleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
    
     button.centerY = self.filterView.bottom+kAdaptedValue(62);
}

-(void)initCoverView{

    UIView *navView = [[UIView alloc] init];
    
    navView.backgroundColor = [UIColor whiteColor];
    
    navView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    [self.view addSubview:navView];
    
    UIButton *cancleButton = [UIView getButtonWithStr:@"取消" fontName:kFONTNAMELIGHT size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    [cancleButton addTarget:self action:@selector(cancleButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    cancleButton.frame = CGRectMake(18, 31.5, 32, 22.5);
    
    [navView addSubview:cancleButton];
    
    //点击创建调节界面
    
    UIButton *hidViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    hidViewButton.frame = CGRectMake(SCREEN_WITDH - 100, 0, 100, 64);
    
    //[hidViewButton addTarget:self action:@selector(hidviewButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [navView addSubview:hidViewButton];
}


-(void)cancleButtonDidClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(UIView *)reTakePhotoView{

    if (_reTakePhotoView == nil) {
        
//        _reTakePhotoView = [[UIView alloc] initWithFrame:CGRectMake(kAdaptedValue(42), kAdaptedValue(540), kAdaptedValue(73), kAdaptedValue(73))];
        _reTakePhotoView = [[UIView alloc] initWithFrame:CGRectMake(kAdaptedValue(152.5), kAdaptedValue(544.5), kAdaptedValue(73), kAdaptedValue(73))];
        
        _reTakePhotoView.center = OriginalCenter;
        
//        [self.view addSubview:_reTakePhotoView];
        [self.view insertSubview:_reTakePhotoView belowSubview:self.takePhotoView];
        
        UIButton *circleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [circleButton setImage:kGetImage(@"login_icon_viedo_again") forState:UIControlStateNormal];
        
        circleButton.frame = _reTakePhotoView.bounds;
        
        circleButton.centerX = kAdaptedValue(30);
        
        [circleButton addTarget:self action:@selector(reTakePhoto) forControlEvents:UIControlEventTouchUpInside];
        
        [_reTakePhotoView addSubview:circleButton];
        

    }
    return _reTakePhotoView;
}

-(void)reTakePhoto{
    
    [self oprationDidFinish:NO];
    
    self.takePhotoView.image = kGetImage(noSelectImageName);
    
    if (self.takePhotoType == CJCTakePhotoTypeCamera) {
        
        self.takePhotoView.frame = CGRectMake(kAdaptedValue(152.5), kAdaptedValue(544.5), kAdaptedValue(70), kAdaptedValue(70));
        self.takePhotoView.center = OriginalCenter;
        
        self.selectImage = nil;
    }else{
    
        self.takePhotoView.frame = CGRectMake(kAdaptedValue(150.5), kAdaptedValue(542.5), kAdaptedValue(74), kAdaptedValue(74));
        self.takePhotoView.center = OriginalCenter;
        
        isFinishRecord = NO;
        [videoURLs removeAllObjects];
        [audioURLs removeAllObjects];
        videoNumber = 0;

        [videoitem removeObserver:self forKeyPath:@"status"];
        [videoplayerLayer removeFromSuperlayer];
        videoitem = nil;
        videoPlayer = nil;
        videoplayerLayer = nil;
        
        self.VideoAlertView.hidden = NO;
        
        alertView.hidden = NO;
        alertTitleLabel.hidden = NO;
        alertTitleLabel.text = [NSString stringWithFormat:@"拍摄%d秒的自我介绍吧",needVideoTime];
        
        completView.width = 0;
        
        holdSecond = 0;
        
        longPressGesture.enabled = YES;
        takeVideoTap.enabled = NO;
        
        NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
        videoStr = [NSString stringWithFormat:@"%@/introduction.mp4",videoStr];
        
        NSFileManager *manager = [NSFileManager defaultManager];
        
        [manager removeItemAtPath:videoStr error:nil];
        
        self.composition = nil;
        self.videoComposition = nil;
        self.audioMix = nil;
    }
    
    self.reTakePhotoView.center = self.takePhotoView.center;
    
    [self.photoCamera startCameraCapture];
}

//拍摄完成后 操作是YES  开始拍摄时 操作是NO
-(void)oprationDidFinish:(BOOL)finish{

    self.openBeautyButton.hidden = finish;
    self.closeBeautyButton.hidden = finish;
    self.tagImageView.hidden = finish;
    self.reTakePhotoView.hidden = !finish;
}

-(GPUImageFilter *)gammaFilter{
    
    if (_gammaFilter == nil) {
        
        _gammaFilter = [[GPUImageFilter alloc] init];
    }
    
    return _gammaFilter;
}

-(UIView *)VideoAlertView{

    if (_VideoAlertView == nil) {
        
        _VideoAlertView = [[UIView alloc] init];
        
        _VideoAlertView.frame = CGRectMake(0, SCREEN_WITDH, SCREEN_WITDH, 69);
        
        [self.view addSubview:_VideoAlertView];
        
        alertView = [[UIView alloc] init];
        
        alertView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
        
        alertView.backgroundColor = [UIColor toUIColorByStr:@"000000" andAlpha:0.3];
        
        [_VideoAlertView addSubview:alertView];
        
        alertTitleLabel = [[UILabel alloc] init];
        
        alertTitleLabel.frame = CGRectMake(0, 21.5, SCREEN_WITDH, 20);
        
        alertTitleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
        
        alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        alertTitleLabel.textColor = [UIColor toUIColorByStr:@"FFFFFF"];
        
        [_VideoAlertView addSubview:alertTitleLabel];
        
        UIView *progressView = [[UIView alloc] init];
        
        progressView.frame = CGRectMake(0, 64, SCREEN_WITDH, 5);
        
        progressView.backgroundColor = [UIColor toUIColorByStr:@"F0F0F0"];
        
        [_VideoAlertView addSubview:progressView];
        
        completView = [[UIView alloc] init];
        
        completView.frame = CGRectMake(0, 0, 30, 5);
        
        if (self.sexType == CJCSexTypeWoman) {
            
            completView.backgroundColor = [UIColor toUIColorByStr:@"FF5E75"];
        }else{
        
            completView.backgroundColor = [UIColor blackColor];
        }
        
        [progressView addSubview:completView];
    }
    
    return _VideoAlertView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

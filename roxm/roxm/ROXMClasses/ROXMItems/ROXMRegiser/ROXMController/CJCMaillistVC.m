//
//  CJCMaillistVC.m
//  roxm
//
//  Created by lfy on 2017/8/31.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMaillistVC.h"
#import "CJCCommon.h"
#import <Contacts/Contacts.h>
#import <AddressBook/AddressBook.h>
#import "CJCBlankLocationVC.h"
#import "ROXMRootController.h"

@interface CJCMaillistVC (){

    NSString *mailListStr;
}

@property (nonatomic ,strong) UILabel *mailListStatusLabel;

@property (nonatomic ,strong) UISwitch *contactSwitch;

@end

@implementation CJCMaillistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpUI];
}

-(void)setUpUI{

    NSString *topImageName,*middleImageName;
    if (self.sexType == CJCSexTypeWoman) {
        
        topImageName = @"guide_icon_people1_female";
        middleImageName = @"guide_icon_people2_female";
    }else{
    
        topImageName = @"guide_icon_people1_male";
        middleImageName = @"guide_icon_people2_male";
    }
    
    UIImageView *topImageView = [[UIImageView alloc] initWithImage:kGetImage(topImageName)];
    
    topImageView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(70), kAdaptedValue(27.5), kAdaptedValue(28.5));
    
    [self.view addSubview:topImageView];
    
    UILabel *titleLabel = [UIView getYYLabelWithStr:@"屏蔽手机联系人" fontName:kFONTNAMEREGULAR size:28 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(64.5), kAdaptedValue(70.5), kAdaptedValue(200), kAdaptedValue(28));
    
    [self.view addSubview:titleLabel];
    
    UILabel *hintLabel = [UIView getYYLabelWithStr:@"你的手机通讯录中的联系人将不会看到你，你也看不到他们" fontName:kFONTNAMELIGHT size:16 color:[UIColor toUIColorByStr:@"636363"]];
    
    hintLabel.numberOfLines = 0;
    
    hintLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(116.5), SCREEN_WITDH-kAdaptedValue(46), kAdaptedValue(46));
    
    [self.view addSubview:hintLabel];
    
    UIImageView *middleImageView = [[UIImageView alloc] initWithImage:kGetImage(middleImageName)];
    
    middleImageView.frame = CGRectMake(kAdaptedValue(126.5), kAdaptedValue(302.5), kAdaptedValue(93.5), kAdaptedValue(63.5));
    
    middleImageView.center = self.view.center;
    
    [self.view addSubview:middleImageView];
    
    UILabel *bottomLabel = [UIView getYYLabelWithStr:@"开启屏蔽联系人" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"222222"]];
    
    bottomLabel.frame = CGRectMake(kAdaptedValue(90), kAdaptedValue(505), kAdaptedValue(115), kAdaptedValue(21));
    
    self.mailListStatusLabel = bottomLabel;
    [self.view addSubview:bottomLabel];
    
    UISwitch *mailListSwitch = [[UISwitch alloc] init];
    
    mailListSwitch.frame = CGRectMake(kAdaptedValue(211), kAdaptedValue(500), kAdaptedValue(51), kAdaptedValue(31));
    
    mailListSwitch.centerY = bottomLabel.centerY;
    
    if ([[kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD] isEqualToString:@"YES"]) {
        
        mailListSwitch.on = YES;
    }else{
    
        mailListSwitch.on = NO;
    }
    
    [mailListSwitch addTarget:self action:@selector(mailListSwitch:) forControlEvents:UIControlEventValueChanged];
    
    self.contactSwitch = mailListSwitch;
    [self.view addSubview:mailListSwitch];
    
    CJCOprationButton *nextButton = [[CJCOprationButton alloc] init];
    
    nextButton.frame = CGRectMake(kAdaptedValue(87), SCREEN_HEIGHT-kAdaptedValue(90), kAdaptedValue(200), kAdaptedValue(45));
    
    nextButton.centerX = self.view.centerX;
    
    nextButton.canOpration = YES;
    
    [nextButton setTitle:@"进入首页" forState:UIControlStateNormal];
    
    [[nextButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [[ROXMRootController shardRootController] TapTabBar];
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
    
    [self.view addSubview:nextButton];
}


-(void)mailListSwitch:(UISwitch *)listSwitch{

    [self mailListAuthority];
}

-(void)mailListAuthority{

    if (GREATERIOS9) {
        
        [self mailListAuthorityIOS9Later];
        
    }else{
    
        [self mailListAuthorityIOS9Befor];
    }
}

-(void)mailListAuthorityIOS9Later{

    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    
    if (status == CNAuthorizationStatusAuthorized) {
        
        if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
            
            [self uploadContactToROXM];
        }else{
        
            [self updateContactStatus];
        }
    }else{
    
        CNContactStore *contactStore = [[CNContactStore alloc] init]; [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted) {
                
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
                        
                        [self uploadContactToROXM];
                    }else{
                    
                        [self updateContactStatus];
                    }
                });
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.contactSwitch.on = NO;
                    
                    [self alertVCShowWith:@"屏蔽手机联系人功能需要开启通讯录权限"];
                });
            }
        }];
        
    }
}

//如果上传完通讯录  只需要更新通讯录的开关状态就可
-(void)updateContactStatus{

    [self showWithLabelAnimation];
    
    NSString *statusStr,*alertStr,*saveStr;
    if (self.contactSwitch.on) {
        
        statusStr = @"1";
        alertStr = @"开启屏蔽联系人";
        saveStr = @"YES";
    }else{
    
        statusStr = @"0";
        alertStr = @"关闭屏蔽联系人";
        saveStr = @"NO";
    }
    
    NSString *cantactURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/contact/shield"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"contactShield"] = statusStr;
    
    [CJCHttpTool postWithUrl:cantactURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [kUSERDEFAULT_STAND setObject:saveStr forKey:kUSERCONTACTUPLOAD];
            
            [kUSERDEFAULT_STAND synchronize];
            
            self.mailListStatusLabel.text = alertStr;
            
            [self showWithLabel:alertStr];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
            
            self.contactSwitch.on = NO;
        }
        
    } failure:^(NSError *error) {
        
    }];
}

//如果之前没有上传过通讯录  需要先上传通讯录
-(void)uploadContactToROXM{

    [self showWithLabelAnimation];
    
    if (GREATERIOS9){
    
        [self getMailListDataIOS9Later];
    }else{
    
        [self getMailListDataIOS9Befor];
    }
    
    NSString *cantactURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/contact/upload"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"contact"] = mailListStr;
    
    [CJCHttpTool postWithUrl:cantactURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [kUSERDEFAULT_STAND setObject:@"YES" forKey:kUSERCONTACTUPLOAD];
            
            [kUSERDEFAULT_STAND synchronize];
            
            [MBManager showBriefAlert:@"屏蔽联系人成功"];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
            
            self.contactSwitch.on = NO;
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)mailListAuthorityIOS9Befor{

    ABAuthorizationStatus ABstatus = ABAddressBookGetAuthorizationStatus();
    
    if (ABstatus == kABAuthorizationStatusAuthorized) {
        
        if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
            
            [self uploadContactToROXM];
        }else{
        
            [self updateContactStatus];
        }
        
    }else{
    
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted){
                
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (![kUSERDEFAULT_STAND objectForKey:kUSERCONTACTUPLOAD]) {
                        
                        [self uploadContactToROXM];
                    }else{
                    
                        [self updateContactStatus];
                    }
                });
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.contactSwitch.on = NO;
                    
                    [self alertVCShowWith:@"拍摄照片需要开启手机的相机权限"];
                });
            }
        });
        
    }
}

-(void)getMailListDataIOS9Later{

    // 3.获取联系人
    // 3.1.创建联系人仓库
    CNContactStore *store = [[CNContactStore alloc] init];
    
    // 3.2.创建联系人的请求对象
    // keys决定这次要获取哪些信息,比如姓名/电话
    NSArray *fetchKeys = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey];
    CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:fetchKeys];
    
    NSMutableString *contactStr = [NSMutableString string];
    
    // 3.3.请求联系人
    NSError *error = nil;
    [store enumerateContactsWithFetchRequest:request error:&error usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
        
        // stop是决定是否要停止
        // 1.获取姓名
        NSString *firstname = contact.givenName;
        NSString *lastname = contact.familyName;
        
        // 2.获取电话号码
        NSArray *phones = contact.phoneNumbers;
        
        NSMutableString *tempStr = [NSMutableString string];
        
        // 3.遍历电话号码
        for (CNLabeledValue *labelValue in phones) {
            CNPhoneNumber *phoneNumber = labelValue.value;

            NSString *phone = [phoneNumber.stringValue stringByReplacingOccurrencesOfString:@"-" withString:@""];
            
            [tempStr appendFormat:@"%@%@:%@",firstname,lastname,phone];
        }
        
        [contactStr appendFormat:@"%@;",tempStr.copy];
    }];
    
    mailListStr = contactStr.copy;
}

-(void)getMailListDataIOS9Befor{

    NSMutableString *contactStr = [NSMutableString string];
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    CFArrayRef allLinkPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex num = ABAddressBookGetPersonCount(addressBook);
    for (NSInteger i = 0; i < num; i++) {
        ABRecordRef  people = CFArrayGetValueAtIndex(allLinkPeople, i);
        
        //读取firstname
        NSString *personName = (__bridge NSString*)ABRecordCopyValue(people, kABPersonFirstNameProperty);
        //读取lastname
        NSString *lastname = (__bridge NSString*)ABRecordCopyValue(people, kABPersonLastNameProperty);
        
        NSMutableString *tempStr = [NSMutableString string];
        
        ABMultiValueRef phones = ABRecordCopyValue(people, kABPersonPhoneProperty);
        
        for (int k = 0; k<ABMultiValueGetCount(phones); k++)
        {
            //获取該Label下的电话值
            NSString * personPhone = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, k);
            
            NSString *newP = [personPhone stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
            if ([CJCTools isBlankString:personName]) {
                
                [tempStr appendFormat:@"%@:%@",lastname,newP];
            }else{
            
                [tempStr appendFormat:@"%@%@:%@",personName,lastname,newP];
            }
            
        }
        
        [contactStr appendFormat:@"%@;",tempStr.copy];
    }
    
    mailListStr = contactStr.copy;
    
    NSLog(@"%@",mailListStr);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}

@end

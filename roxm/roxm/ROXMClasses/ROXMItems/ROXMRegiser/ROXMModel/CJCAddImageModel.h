//
//  CJCAddImageModel.h
//  roxm
//
//  Created by lfy on 2017/8/25.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TZAssetModel.h"

typedef NS_ENUM(NSInteger,CJCMediaType){
    
    CJCMediaTypePhoto                   = 0,
    CJCMediaTypeVideo                   = 1,
    CJCMediaTypeAsset                   = 2,
};

@interface CJCAddImageModel : NSObject

@property (nonatomic ,copy) NSString *uploadKey;

@property (nonatomic ,strong) UIImage *image;

@property (nonatomic ,assign) CJCMediaType mediaType;

@property (nonatomic ,assign) TZAssetModelMediaType assetMediaType;

@property (nonatomic ,copy) NSString *videoPath;

@property (nonatomic ,strong) PHAsset *asset;

@property (nonatomic ,copy) NSString *videoLength;

@property (nonatomic ,assign) CGFloat width;

@property (nonatomic ,assign) CGFloat height;

@end

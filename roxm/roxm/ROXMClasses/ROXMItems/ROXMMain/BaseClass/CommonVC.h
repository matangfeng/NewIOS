//
//  CJCCommonVC.h
//  roxm
//
//  Created by lfy on 2017/8/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC.h>

//是用于拍照 还是录像
typedef NS_ENUM(NSInteger,CJCTakePhotoType){
    
    CJCTakePhotoTypeCamera                   = 0,
    CJCTakePhotoTypeVideo                    = 1,
    CJCTakePhotoTypeChangeVideo              = 2,
    CJCTakePhotoTypeChangeCamera             = 3,
};

//男女性别
typedef NS_ENUM(NSInteger,CJCSexType){
    
    CJCSexTypeWoman                 = 2,
    CJCSexTypeMan                   = 1,
    CJCSexTypeNoSelect              = 0,
};

@class UILabel;

@interface CommonVC : UIViewController

//代替系统的navigationBar
@property (nonatomic ,strong) UIView *naviView;

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UILabel *mediumTitleLabel;

@property (nonatomic ,assign) CGFloat naviViewBottom;

@property (nonatomic ,strong) UIView *lineView;

@property (nonatomic ,strong) UIButton *backButton;

//导航栏下面的 注册过程中的 进度条
@property (nonatomic, assign) CGFloat progressViewWidth;

////是否需要添加手势来隐藏键盘 （默认添加）
@property (nonatomic ,assign) BOOL  keyBoardHid;

////获取分割线
-(UIView *)creatMarginLine;

//系统权限提示
-(void)alertVCShowWith:(NSString *)alertStr;
- (void)alertLoginStr:(NSString *)alertStr;
- (void)showWithLabel:(NSString *)text;
- (void)showWithLabelAnimation;
- (void)hidHUD;

- (UIImage*)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image;

@end

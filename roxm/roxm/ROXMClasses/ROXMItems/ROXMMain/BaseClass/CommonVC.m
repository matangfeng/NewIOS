//
//  CJCCommonVC.m
//  roxm
//
//  Created by lfy on 2017/8/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"
#import "CJCCommon.h"
#import "CJCGetPhoneCodeVC.h"
#import <MBProgressHUD.h>

#define tipsDelayTime    1

@interface CommonVC ()<MBProgressHUDDelegate>{
    
    MBProgressHUD *HUDPrompt;
}

@property (nonatomic ,strong) UITapGestureRecognizer *tapGesture;

@end

@implementation CommonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction)];
    
    [self.view addGestureRecognizer:self.tapGesture];

}

/**
 设置返回箭头图标  如需单独设置某个返回样式，请控制器单独复制并重写此方法
 */
- (UIBarButtonItem *)customBackItemWithTarget:(id)target action:(SEL)action
{
    UIImage * img = [UIImage imageNamed:@"nav_icon_back"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"nav_icon_back"] forState:UIControlStateNormal];
    [button setImage:[self imageByApplyingAlpha:0.5 image:[UIImage imageNamed:@"nav_icon_back"]] forState:1];
    [button setImageEdgeInsets:(UIEdgeInsetsMake(0, 0, 0, 44 - img.size.width))];
    button.frame = CGRectMake(0, 0, 44, 44);
    [button addTarget:target
               action:action
     forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

-(void)setKeyBoardHid:(BOOL)keyBoardHid{

    [self.view removeGestureRecognizer:self.tapGesture];
}

-(void)tapGestureAction{
    
    [self.view endEditing:YES];
}

-(void)setProgressViewWidth:(CGFloat )progressViewWidth{

    UIView *progressContainView = [[UIView alloc] init];
    
    progressContainView.frame = CGRectMake(0, 61, SCREEN_WITDH, 2);
    
    progressContainView.backgroundColor = [UIColor toUIColorByStr:@"D3D5DC"];
    
    [self.view addSubview:progressContainView];
    
    UIView *progressView = [[UIView alloc] init];
    
    progressView.frame = CGRectMake(0, 0, progressViewWidth, 2);
    
    progressView.backgroundColor = [UIColor blackColor];
    
    [progressContainView addSubview:progressView];
    
    UIView *lineView = [[UIView alloc] init];
    
    lineView.frame = CGRectMake(0, 2, SCREEN_WITDH, OnePXLineHeight);
    
    lineView.backgroundColor = [UIColor toUIColorByStr:@"D8D8D8"];
    
    [progressContainView addSubview:lineView];
}

#pragma mark ========设置自定义的导航栏
-(void)setNaviViewBottom:(CGFloat)naviViewBottom{

    
    self.lineView.y = naviViewBottom-1;
}

//-(UIView *)naviView{
//
//    if (_naviView == nil) {
//
//        _naviView = [[UIView alloc] init];
//
//        [self.view addSubview:_naviView];
//
//        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//
//        backButton.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(33.5), kAdaptedValue(10.5), kAdaptedValue(17.5));
//
//        backButton.centerY = kNAVIVIEWCENTERY;
//
//        [backButton setImage:kGetImage(@"nav_icon_back") forState:UIControlStateNormal];
//
//        self.backButton = backButton;
//        [_naviView addSubview:backButton];
//
//        UIButton *bigBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
//
//        bigBackButton.frame = CGRectMake(0, 0, 64, 64);
//
//        [_naviView addSubview:bigBackButton];
//
//        [bigBackButton addTarget:self action:@selector(naviViewBackBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
//
//        UIView *lineView = [[UIView alloc] init];
//
//        self.lineView = lineView;
//
//        lineView.frame = CGRectMake(0, 63, SCREEN_WITDH, OnePXLineHeight);
//
//        lineView.backgroundColor = [UIColor toUIColorByStr:@"D8D8D8"];
//
//        [_naviView addSubview:lineView];
//    }
//
//    return _naviView;
//}

//-(UILabel *)titleLabel{
//
//    if (_titleLabel == nil) {
//        
//        _titleLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:16 color:[UIColor toUIColorByStr:@"333333"]];
//        
//        _titleLabel.textAlignment = NSTextAlignmentCenter;
//        
//        [self.naviView addSubview:_titleLabel];
//    }
//    
//    return _titleLabel;
//}

//-(UILabel *)mediumTitleLabel{
//
//    if (_mediumTitleLabel == nil) {
//
//        _mediumTitleLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:17 color:[UIColor toUIColorByStr:@"222222"]];
//
//        _mediumTitleLabel.textAlignment = NSTextAlignmentCenter;
//
//        [self.naviView addSubview:_mediumTitleLabel];
//    }
//    return _mediumTitleLabel;
//}

-(UIView *)creatMarginLine{

    UIView *lineView = [[UIView alloc] init];
    
    lineView.backgroundColor = [UIColor toUIColorByStr:@"D8D8D8"];
    
    return lineView;
}

//系统权限提示
-(void)alertVCShowWith:(NSString *)alertStr{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertStr message:nil preferredStyle:  UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        
        if([[UIApplication sharedApplication] canOpenURL:url]) {
            
            NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url];
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
    
}

- (void)alertLoginStr:(NSString *)alertStr
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertStr message:nil preferredStyle:  UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
}

/////////////浮层提示
- (void)showWithLabel:(NSString *)text
{
    [self hidHUD];
    if (!HUDPrompt)
    {
        //创建菊花
        HUDPrompt = [[MBProgressHUD alloc] initWithView:self.view];
        HUDPrompt.delegate=self;
        HUDPrompt.mode=MBProgressHUDModeText;
        [self.view addSubview:HUDPrompt];
        if ([CJCTools isBlankString:text])
        {
            HUDPrompt.labelText = @"系统繁忙,请稍后再试";
        }
        else
        {
            if ([text length] > 12) {
                HUDPrompt.detailsLabelText = text;
            }
            else
            {
                HUDPrompt.labelText = text;
            }
        }
        [HUDPrompt show:YES];
        [HUDPrompt hide:YES afterDelay:tipsDelayTime];
    }
    
}

- (void)showWithLabelAnimation
{
    [self hidHUD];
    if (!HUDPrompt)
    {
        HUDPrompt = [[MBProgressHUD alloc] initWithView:self.view];
        HUDPrompt.delegate=self;
        HUDPrompt.mode=MBProgressHUDModeIndeterminate;
        [self.view addSubview:HUDPrompt];
        HUDPrompt.labelText = @"请等待";
        HUDPrompt.labelFont = [UIFont systemFontOfSize:10];
        [HUDPrompt show:YES];
    }
    
}

- (void)hidHUD
{
    if (HUDPrompt)
    {
        HUDPrompt.delegate = nil;
        [HUDPrompt removeFromSuperview];
        //        [HUDPrompt release];
        HUDPrompt = nil;
    }
}

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [self hidHUD];
}

- (UIImage*)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image {
        
        UIGraphicsBeginImageContextWithOptions(image.size,NO,0.0f);
        CGContextRef ctx =UIGraphicsGetCurrentContext();
        CGRect area =CGRectMake(0,0, image.size.width, image.size.height);
        CGContextScaleCTM(ctx,1, -1);
        CGContextTranslateCTM(ctx,0, -area.size.height);
        CGContextSetBlendMode(ctx,kCGBlendModeMultiply);
        CGContextSetAlpha(ctx, alpha);
        CGContextDrawImage(ctx, area, image.CGImage);
        UIImage*newImage =UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

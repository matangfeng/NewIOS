//
//  ROXMRootController.h
//  roxm
//
//  Created by 马棠丰 on 2017/11/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "CYLTabBarController.h"

@interface ROXMRootController : UIViewController
+ (ROXMRootController *)shardRootController;
    @property (nonatomic , strong) CYLTabBarController * customTabBar;
- (void)TapTabBar;
- (void)TapLogin;
- (void)pushIndex;
@end

//
//  ROXMRootController.m
//  roxm
//
//  Created by 马棠丰 on 2017/11/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "ROXMRootController.h"
#import "ROXMTabBarConfigThreeItem.h"
#import "CJCLoginAndRegisterVC.h"
#import "CJCFirstAnimationVC.h"
#import "CJCCommon.h"
#import "ROXMNavigationController.h"
#import "CJCpersonalInfoModel.h"

@interface ROXMRootController ()<UITabBarControllerDelegate>
    @property (nonatomic , strong) AppDelegate * appDelegate;
    @property (nonatomic , strong) UIWindow * window;
    @property (nonatomic , strong) ROXMTabBarConfigThreeItem * tabBarThreeControllerConfig;
@property (nonatomic , strong) ROXMNavigationController * loginNvCtr;
@end

@implementation ROXMRootController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createAppWindow];
    [self isLogin];
}

+ (ROXMRootController *)shardRootController
    {
        static ROXMRootController * root = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            root = [[ROXMRootController alloc] init];
        });
        return root;
    }

- (void)isLogin
{
    if ([CJCpersonalInfoModel infoModel].loginInfoDict) {
        [self loginStartView];
    }else{
        [self TapLogin];
    }
}

#pragma 登陆状态时候的启动展示
- (void)loginStartView{
    CJCFirstAnimationVC * naviVC = [[CJCFirstAnimationVC alloc] init];
    naviVC.tapStaus = ^(BOOL currentStatus) {
        if (currentStatus) {
            [self TapTabBar];
        }else{
            [self TapLogin];
        }
    };
    [self.window setRootViewController:naviVC];
}

- (void)createAppWindow
{
        self.appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
        self.window = self.appDelegate.window;
        
        self.tabBarThreeControllerConfig = [[ROXMTabBarConfigThreeItem alloc] init];
        self.customTabBar = self.tabBarThreeControllerConfig.tabBarController;
        self.customTabBar.delegate = self;
    
        CJCLoginAndRegisterVC * loginCtr = [[CJCLoginAndRegisterVC alloc] init];
        self.loginNvCtr = [[ROXMNavigationController alloc] initWithRootViewController:loginCtr];
}

- (void)TapTabBar{
    [self.window setRootViewController:self.customTabBar];
}

- (void)TapLogin{
    [self.window setRootViewController:self.loginNvCtr];
}

- (void)pushIndex
{
    self.customTabBar.selectedIndex = 0;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectControl:(UIControl *)control
{
        //    UIView * animationView = [control cyl_tabImageView];
        //    [MTF_AnimationToolClass mtf_addScaleAnimationOnView:animationView repeatCount:1];
}
    
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

//
//  CJCFistAnimationView.m
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCFistAnimationView.h"
#import "CJCCommon.h"

@interface CJCFistAnimationView ()<CAAnimationDelegate>

@property (nonatomic ,strong) UIImageView *imageScroll;

@end

@implementation CJCFistAnimationView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self setUpUI];
    }
    return self;
}

-(void)animationStart{

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        CGPoint newPoint = self.imageScroll.center;
        
        CABasicAnimation *animation =[CABasicAnimation animationWithKeyPath:@"position"];
        [animation setToValue:[NSValue valueWithCGPoint:CGPointMake(newPoint.x - 50, newPoint.y)]];
        
        animation.delegate = self;
        animation.duration = 1.0;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;//removedOnCompletion,fillMode配合使用保持动画完成效果
        animation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [self.imageScroll.layer addAnimation:animation forKey:@"position"];
        
    });
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
    [self removeFromSuperview];
    
    if (self.finishHandle) {
        
        self.finishHandle();
    }
}

-(void)setUpUI{

    NSString *fullImageName,*bottomImageName;
    
    fullImageName = @"start_user1.jpg";
    bottomImageName = @"bg4.png";
    
    UIImage *fullImage = [UIImage imageNamed:fullImageName];
    
    CGFloat imageWidth = SCREEN_WITDH+kAdaptedValue(50);
    
    CGFloat imageRatio = (CGFloat)fullImage.size.height/fullImage.size.width;
    
    UIImageView *fullImageView = [[UIImageView alloc] initWithImage:fullImage];
    
    fullImageView.frame = CGRectMake(0, 0, imageWidth, imageWidth*imageRatio);
    
    self.imageScroll = fullImageView;
    [self addSubview:fullImageView];
    
    UIImage *bottomImage = kGetImage(bottomImageName);
    
    UIImageView *bottomView = [[UIImageView alloc] initWithImage:bottomImage];
    
    CGFloat ratio = (CGFloat)bottomImage.size.height/bottomImage.size.width;
    
    bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-SCREEN_WITDH*ratio, SCREEN_WITDH, SCREEN_WITDH*ratio);
    
    [self addSubview:bottomView];

}

@end

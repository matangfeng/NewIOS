//
//  CJCLoginAndRegisterVC.h
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonVC.h"

typedef BOOL(^LoginStatus)(void);

@interface CJCLoginAndRegisterVC : CommonVC

@property (nonatomic ,assign) BOOL isVideoPlay;
@property (nonatomic , strong) LoginStatus loginSuccess;
@end

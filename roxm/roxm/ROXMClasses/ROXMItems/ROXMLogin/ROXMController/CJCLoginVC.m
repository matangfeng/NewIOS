//
//  CJCLoginVC.m
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCLoginVC.h"
#import <Hyphenate/Hyphenate.h>
#import "CJCCommon.h"
#import "CJCGetPhoneCodeVC.h"
#import "CJCForgetPassWordVC.h"
#import "CJCFillInPersonalInfoVC.h"
#import "CJCBeforeTakePhotoVC.h"
#import "CJCAddMorePictureVC.h"
#import "CJCWomanAdditionaIinformationVC.h"
#import "CJCMaillistVC.h"
#import "ROXMRootController.h"

@interface CJCLoginVC ()<UITextFieldDelegate>{

    //记录手机号的长度 用于判断是输入 还是删除
    NSInteger phoneNUmLengh;
}

@property (nonatomic ,strong) UITextField *phoneNumTF;

@property (nonatomic ,strong) UITextField *passWordTF;

@property (nonatomic ,strong) CJCOprationButton *loginButton;

@property (nonatomic ,strong) CJCErrorAletLabel *errorLabel;

@property (nonatomic ,strong) CJCErrorAletLabel *phoneErrorLabel;

@property (nonatomic ,strong) UILabel *agreeLabel;

@property (nonatomic ,strong) UIButton *protocolButton;

@end

@implementation CJCLoginVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.phoneNumTF becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self controRight];
    [self setUpUI];
    
}

- (void)controRight
{
    if ([self.isLoginOrRegiste isEqualToString:@"登录"]) {
        UILabel * label = [[UILabel alloc] init];
        label.text = @"忘记密码";
        
        
        UIButton * loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [loginButton setImageEdgeInsets:(UIEdgeInsetsMake(0, label.size.width, 0, 0))];
        loginButton.frame = CGRectMake(0, 0, label.size.width, 44);
        [loginButton setTitle:label.text forState:(UIControlStateNormal)];
        [loginButton setTitleColor:[UIColor toUIColorByStr:@"333333"] forState:(UIControlStateNormal)];
        [loginButton setTitleColor:[UIColor toUIColorByStr:@"333333" andAlpha:0.5] forState:1];
        loginButton.titleLabel.font = [UIFont accodingVersionGetFont_lightWithSize:14];
        [[loginButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
            CJCForgetPassWordVC *nextVC = [[CJCForgetPassWordVC alloc] init];
            [self.navigationController pushViewController:nextVC animated:YES];
        }];
        UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:loginButton];
        self.navigationItem.rightBarButtonItem = barButton;//为导航栏添加右侧按钮
    }
}


-(void)setUpUI{

    UILabel *loginLabel = [UIView getYYLabelWithStr:self.isLoginOrRegiste fontName:kFONTNAMELIGHT size:24 color:[UIColor toUIColorByStr:@"222222"]];
    
    if ([self.isLoginOrRegiste isEqualToString:@"登录"]) {
        
        loginLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(88), kAdaptedValue(48), kAdaptedValue(24));
        
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"忘记密码"];
        NSRange titleRange = {0,[title length]};
        
        title.font = [UIFont accodingVersionGetFont_lightWithSize:14];
        title.color = [UIColor toUIColorByStr:@"333333"];
        [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:titleRange];
    }
    
    if ([self.isLoginOrRegiste isEqualToString:@"使用手机号注册"]) {
        
        loginLabel.frame = CGRectMake(kAdaptedValue(19.5), kAdaptedValue(88), kAdaptedValue(170), kAdaptedValue(24));
    }
    
    [self.view addSubview:loginLabel];
    
    
    UILabel *phoneLabel = [UIView getYYLabelWithStr:@"手机号" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"222222"]];
    
    phoneLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(152), kAdaptedValue(42), kAdaptedValue(14));
    
    [self.view addSubview:phoneLabel];
    
    
    UILabel *eightLabel = [UIView getYYLabelWithStr:@"+86" fontName:kFONTNAMEREGULAR size:13 color:[UIColor toUIColorByStr:@"333333" andAlpha:0.5]];
    
    eightLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(180.5), kAdaptedValue(30), kAdaptedValue(18.5));
    
    [self.view addSubview:eightLabel];
    
    
    UITextField *phoneTF = [[UITextField alloc] init];
    
    self.phoneNumTF = phoneTF;
    
    phoneTF.frame = CGRectMake(eightLabel.right+kAdaptedValue(10), kAdaptedValue(177), kAdaptedValue(329), kAdaptedValue(31));
    phoneTF.font = [UIFont accodingVersionGetFont_regularWithSize:18];
    
    phoneTF.textColor = [UIColor toUIColorByStr:@"222222"];
    
    phoneTF.placeholder = @"请输入手机号";
    
    phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    
    phoneTF.delegate = self;
    
    [phoneTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    phoneTF.tag = 10001;
    
    [self.view addSubview:phoneTF];
    
    
    UIView *topLineView = [UIView getLineView];
    
    topLineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(204), kAdaptedValue(329), OnePXLineHeight);
    
    [self.view addSubview:topLineView];
    
    CJCErrorAletLabel *phoneErrorLabel = [[CJCErrorAletLabel alloc] init];
    
    phoneErrorLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(207), kAdaptedValue(200), kAdaptedValue(12));
    
    self.phoneErrorLabel = phoneErrorLabel;
    
    [self.view addSubview:phoneErrorLabel];
    
    UILabel *passWordLabel = [UIView getYYLabelWithStr:@"密码" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"333333"]];
    
    passWordLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(232.5), kAdaptedValue(28), kAdaptedValue(14));
    
    [self.view addSubview:passWordLabel];
    
    
    UIButton *showPassWordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    showPassWordBtn.frame = CGRectMake(kAdaptedValue(324), kAdaptedValue(232.5), kAdaptedValue(28), kAdaptedValue(20));
    
    [showPassWordBtn setTitle:@"显示" forState:UIControlStateNormal];
    [showPassWordBtn setTitleColor:[UIColor toUIColorByStr:@"333333"] forState:UIControlStateNormal];
    
    showPassWordBtn.titleLabel.font = [UIFont accodingVersionGetFont_lightWithSize:14];
    
    [showPassWordBtn addTarget:self action:@selector(isShowPassWord:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:showPassWordBtn];
    
    UITextField *passTF = [[UITextField alloc] initWithFrame:CGRectMake(kAdaptedValue(23), kAdaptedValue(256.5), kAdaptedValue(329), kAdaptedValue(31))];
    
    self.passWordTF = passTF;
    
    passTF.delegate = self;
    
    passTF.tag = 10002;
    
    passTF.font = [UIFont accodingVersionGetFont_regularWithSize:18];
    
    passTF.textColor = [UIColor toUIColorByStr:@"333333"];
    
    passTF.placeholder = @"请输入密码";
    
    passTF.secureTextEntry = YES;
    
    passTF.clearButtonMode = UITextFieldViewModeAlways;
    
    passTF.clearsOnBeginEditing = NO;
    
    //passTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    [passTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addSubview:passTF];
    
    UIView *BottomLineView = [UIView getLineView];
    
    BottomLineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(287.5), kAdaptedValue(329), OnePXLineHeight);
    
    [self.view addSubview:BottomLineView];
    
    CJCOprationButton *loginBtn = [[CJCOprationButton alloc] init];
    
    self.loginButton = loginBtn;
    
    loginBtn.frame = CGRectMake(kAdaptedValue(87), kAdaptedValue(364), kAdaptedValue(200), kAdaptedValue(45));
    
    loginBtn.centerX = self.view.centerX;
    loginBtn.top = BottomLineView.bottom+kAdaptedValue(32);
    
    loginBtn.canOpration = NO;
    
    [loginBtn addTarget:self action:@selector(loginButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:loginBtn];
    
    UILabel *agreLabel = [UIView getYYLabelWithStr:@"注册即表示同意" fontName:kFONTNAMEREGULAR size:12 color:[UIColor toUIColorByStr:@"B5B5B5"]];
    
    CGFloat phoneLabelWidth = [CJCTools getShortStringLength:@"注册即表示同意" withFont:[UIFont accodingVersionGetFont_regularWithSize:12]];
    
    agreLabel.frame = CGRectMake(kAdaptedValue(100), kAdaptedValue(377), phoneLabelWidth, kAdaptedValue(13));
    
    self.agreeLabel = agreLabel;
    
    agreLabel.y = loginBtn.bottom+20;
    
    [self.view addSubview:agreLabel];
    
    UIButton *protocolButton = [UIButton getButtonWithStr:@"《濡沫用户协议》" fontName:kFONTNAMEREGULAR size:12 color:[UIColor toUIColorByStr:@"4198F0"]];
    
    CGFloat phoLabelWidth = [CJCTools getShortStringLength:@"《濡沫用户协议》" withFont:[UIFont accodingVersionGetFont_regularWithSize:12]];
    
    self.protocolButton = protocolButton;
    
    CGFloat margin = SCREEN_WITDH-phoneLabelWidth-phoLabelWidth-1;
    
    agreLabel.x = margin/2;
    
    protocolButton.frame = CGRectMake(kAdaptedValue(182), kAdaptedValue(377), phoLabelWidth, kAdaptedValue(13));
    
    protocolButton.y = loginBtn.bottom+20;
    protocolButton.x = agreLabel.right+1;
    
    [self.view addSubview:protocolButton];
    
    if ([self.isLoginOrRegiste isEqualToString:@"登录"]) {
        
        [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
        agreLabel.hidden = YES;
        protocolButton.hidden = YES;
    }
    
    if ([self.isLoginOrRegiste isEqualToString:@"使用手机号注册"]) {
        
        [loginBtn setTitle:@"下一步" forState:UIControlStateNormal];
    }
    
}

//密码是否显示按钮的点击事件
-(void)isShowPassWord:(UIButton *)button{

    if (self.passWordTF.text.length > 0) {
        
        button.selected = !button.selected;
        if (button.selected) {
            
            self.passWordTF.secureTextEntry = NO;
            [button setTitle:@"隐藏" forState:UIControlStateSelected];
        }else{
            
            self.passWordTF.secureTextEntry = YES;
            [button setTitle:@"显示" forState:UIControlStateNormal];
        }
    }

}

//登陆按钮的点击事件
-(void)loginButtonDidClick{

    [self.view endEditing:YES];

    if (![CJCTools valiMobile:self.phoneNumTF.text]) {
        
        [MBManager showBriefAlert:@"请输入正确的手机号" inView:self.view];
        
        return;
    }
    
    if (self.passWordTF.text.length<1) {
        
        [MBManager showBriefAlert:@"请输入密码" inView:self.view];
        
        return;
    }
    
    if ([self.isLoginOrRegiste isEqualToString:@"登录"]) {
        
        [self userLogin];
    }
    
    if ([self.isLoginOrRegiste isEqualToString:@"使用手机号注册"]) {
        
        [self getPhoneCode];
    }
}

#pragma 用户登陆
- (void)userLogin{
    [self showWithLabelAnimation];
    NSString *loginURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/login"];
    
    NSString *phoneNum = [self.phoneNumTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = phoneNum;
    params[@"password"] = self.passWordTF.text;
    
    [CJCHttpTool postWithUrl:loginURL params:params success:^(id responseObject) {
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        NSLog(@"%@",responseObject);
        if (rtNum.integerValue == 0){
            [self chooseNextVCWith:responseObject];
        }else{
            [self showWithLabel:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [MBManager hideAlert];
    }];
}

- (void)chooseNextVCWith:(NSDictionary *)dict{
    
    [CJCpersonalInfoModel infoModel].loginInfoDict = dict;
    [CJCpersonalInfoModel infoModel].loginUserDict = dict[@"data"][@"user"];

    
    if ([CJCTools isBlankString:[CJCpersonalInfoModel infoModel].loginUserDict[@"birthDay"]]) {
        
        CJCFillInPersonalInfoVC *nextVC = [[CJCFillInPersonalInfoVC alloc] init];
        [self.navigationController pushViewController:nextVC animated:YES];
        
    }else if([CJCTools isBlankString:[CJCpersonalInfoModel infoModel].loginUserDict[@"imageUrl"]]){
        
        CJCBeforeTakePhotoVC *nextVC = [[CJCBeforeTakePhotoVC alloc] init];
        
        if ([[CJCpersonalInfoModel infoModel].loginUserDict[@"sex"] intValue] == 0) {
            
            nextVC.sexType = CJCSexTypeWoman;
        }else{
        
            nextVC.sexType = CJCSexTypeMan;
        }
        
        nextVC.takePhotoType = CJCTakePhotoTypeCamera;
        
        [self.navigationController pushViewController:nextVC animated:YES];
        
    }else{
    
        if ([[CJCpersonalInfoModel infoModel].loginUserDict[@"sex"] intValue] == 1) {
            [[CJCpersonalInfoModel infoModel] userLoginWithDict:dict];
            [self changeRootController];
        }else{
            if ([CJCTools isBlankString:[CJCpersonalInfoModel infoModel].loginUserDict[@"videoUrl"]]) {
                
                CJCBeforeTakePhotoVC *nextVC = [[CJCBeforeTakePhotoVC alloc] init];
                
                nextVC.sexType = CJCSexTypeWoman;
                
                nextVC.takePhotoType = CJCTakePhotoTypeVideo;
                
                [self.navigationController pushViewController:nextVC animated:YES];
                
            }else if([CJCTools isBlankString:[CJCpersonalInfoModel infoModel].loginUserDict[@"cupSize"]]){
                
                CJCWomanAdditionaIinformationVC *nextVC = [[CJCWomanAdditionaIinformationVC alloc] init];
                
                [self.navigationController pushViewController:nextVC animated:YES];
                
            }else{
                [[CJCpersonalInfoModel infoModel] userLoginWithDict:nil];
                [self changeRootController];
            }
        }
    }
}

- (void)changeRootController{
    [[ROXMRootController shardRootController] TapTabBar];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)getPhoneCode{
    [self showWithLabelAnimation];
    NSString *phoneNum = [self.phoneNumTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    [CJCHttpTool getRoxmVerificationCodeWithPhone:phoneNum success:^(id responseObject) {
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            NSString *phoneNum = [self.phoneNumTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            CJCGetPhoneCodeVC *codeVC = [[CJCGetPhoneCodeVC alloc] init];
            codeVC.phoneNum = phoneNum;
            codeVC.passWord = self.passWordTF.text;
            codeVC.verificationCode = responseObject[@"data"];
            codeVC.codeType = CJCGetCodeTypeRegist;
            [self.navigationController pushViewController:codeVC animated:YES];
        }else if(rtNum.integerValue == 1011){
            
            [self isAlreadyRegister];
        }else{
        
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)isAlreadyRegister{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"该手机号码已注册" message:@"请直接登录" preferredStyle:  UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"登录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        CJCLoginVC *loginVC = [[CJCLoginVC alloc] init];
        
        loginVC.isLoginOrRegiste = @"登录";
        
        [self.navigationController pushViewController:loginVC animated:YES];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
}

-(void)errorLabelShowWith:(NSString *)error{

    self.errorLabel.hidden = NO;
    
    self.errorLabel.text = error;
    
    //self.loginButton.y = kAdaptedValue(364);
    
    //self.agreeLabel.y = self.loginButton.bottom+20;
    //self.protocolButton.y = self.loginButton.bottom+20;
}

-(void)errorLabelHid{

    self.errorLabel.hidden = YES;
    
    //self.loginButton.y = kAdaptedValue(343);
    
    //self.agreeLabel.y = self.loginButton.bottom+20;
    //self.protocolButton.y = self.loginButton.bottom+20;
}

#pragma mark =======TextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    if (textField.tag == 10001) {
     
        self.phoneErrorLabel.text = @"";
    }else{
    
        if (self.errorLabel.hidden == NO) {
            
            [self errorLabelHid];
        }
    }
    
    return YES;
}


//解决密文模式下  重新输入  会删除之前内容的bug
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //得到输入框的内容
    NSString * textfieldContent = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField.tag == 10002 && textField.isSecureTextEntry ) {
        textField.text = textfieldContent;
        
        //当输入密码 并且输入完手机号  登陆按钮可以点击
        if (self.passWordTF.text.length>5&&self.phoneNumTF.text.length==13&&[CJCTools valiMobile:self.phoneNumTF.text]) {
            
            self.loginButton.canOpration = YES;
        }else{
            
            self.loginButton.canOpration = NO;
        }
        
        return NO;
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{

    if (textField.tag == 10001) {
        
        if (![CJCTools isBlankString:textField.text]) {
            
            if (![CJCTools valiMobile:textField.text]) {
                
                self.phoneErrorLabel.text = @"请填写正确的手机号";
            }else{
            
                [self errorLabelHid];
            }
        }
    }else{
    
        if (![CJCTools isBlankString:textField.text]) {
            
            if (textField.text.length <6) {
                
                [self errorLabelShowWith:@"密码长度不小于6位"];
            }else{
            
                [self errorLabelHid];
            }
        }
    }
    
}

-(void)phoneNumTFTextChanged:(UITextField *)textField{

    //手机号输入的一些限制 按照344 中间加空格的格式展示
    //需要判断手机号是否为合法手机号（正则判断）
    //多于13位时输入不了
    if (textField.tag == 10001) {
        
        if (phoneNUmLengh == 0) {
            
            phoneNUmLengh = textField.text.length;
        }
        //是在输入
        if (phoneNUmLengh < textField.text.length) {
            
            if (textField.text.length == 3 || textField.text.length == 8) {
                
                NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
                
                [tempStr appendString:@" "];
                
                textField.text = tempStr.copy;
            }
            
            if (textField.text.length == 4) {
                
                if (![textField.text containsString:@" "]) {
                    
                    NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
                    
                    [tempStr insertString:@" " atIndex:3];
                    
                    textField.text = tempStr.copy;
                }

            }
            
            if (textField.text.length == 9) {
                
                NSArray *tempArr = [textField.text componentsSeparatedByString:@" "];
                
                if (tempArr.count == 2) {
                    
                    NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
                    
                    [tempStr insertString:@" " atIndex:8];
                    
                    textField.text = tempStr.copy;
                }
            }
            
            
            phoneNUmLengh = textField.text.length;
        }else{
        
            //是在删除  需要删除空格
            
            if (textField.text.length == 4 || textField.text.length == 9) {
                
                NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
                
                [tempStr deleteCharactersInRange:NSMakeRange(textField.text.length-1, 1)];
                
                textField.text = tempStr.copy;
            }
            
            phoneNUmLengh = textField.text.length;
        }
        
        
        
        if (textField.text.length == 13) {
            
            if ([CJCTools valiMobile:textField.text]) {
                
                [self.passWordTF becomeFirstResponder];
            }
            
            if (self.errorLabel.hidden == NO) {
                
                [self errorLabelHid];
            }
        }
        
        if (textField.text.length > 13) {
            
            NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
            
            [tempStr deleteCharactersInRange:NSMakeRange(13, 1)];
            
            textField.text = tempStr.copy;
        }
    }
    //当输入密码 并且输入完手机号  登陆按钮可以点击
    if (self.passWordTF.text.length>5&&self.phoneNumTF.text.length==13&&[CJCTools valiMobile:self.phoneNumTF.text]) {
        
        self.loginButton.canOpration = YES;
    }else{
        
        self.loginButton.canOpration = NO;
    }
}

-(CJCErrorAletLabel *)errorLabel{

    if (_errorLabel == nil) {
        
        _errorLabel = [[CJCErrorAletLabel alloc] init];
        
        _errorLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(290), SCREEN_WITDH-kAdaptedValue(46), kAdaptedValue(12));
        
        
        [self.view addSubview:_errorLabel];
    }
    
    return _errorLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

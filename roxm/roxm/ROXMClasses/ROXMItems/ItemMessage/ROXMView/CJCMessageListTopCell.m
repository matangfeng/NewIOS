//
//  CJCMessageListTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/11/5.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageListTopCell.h"
#import "CJCCommon.h"

@implementation CJCMessageListTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{
    
    UILabel *titleLable = [UIView getSystemLabelWithStr:@"消息" fontName:kFONTNAMEMEDIUM size:32 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLable.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(16), kAdaptedValue(80), kAdaptedValue(32));
    
    self.titleLabel = titleLable;
    [self addSubview:titleLable];
    
    UILabel *messageNumLabel = [UIView getSystemLabelWithStr:@"您没有未读消息" fontName:kFONTNAMELIGHT size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    messageNumLabel.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(126), kAdaptedValue(200), kAdaptedValue(16));
    
    messageNumLabel.y = titleLable.bottom+kAdaptedValue(14);
    
    self.messageCountLabel = messageNumLabel;
    [self addSubview:messageNumLabel];
    
}

@end

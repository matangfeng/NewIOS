//
//  CJCFollowListVC.h
//  roxm
//
//  Created by 陈建才 on 2017/11/1.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef NS_ENUM(NSInteger,CJCFollowListVCType){
    
    CJCFollowListVCTypeDefault                  = 0,
    CJCFollowListVCTypeOpration                 = 1,
};

@interface CJCFollowListVC : CommonVC

@property (nonatomic ,assign) CJCFollowListVCType listVCType;

@end

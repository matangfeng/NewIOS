//
//  CJCBlacklistVC.m
//  roxm
//
//  Created by 陈建才 on 2017/11/1.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBlacklistVC.h"
#import "CJCCommon.h"
#import "CJCNearbyPersonCell.h"

#define NEARBYPERSONCELL     @"CJCNearbyPersonCell"

@interface CJCBlacklistVC ()<UITableViewDelegate,UITableViewDataSource>{
    
    NSMutableArray *dataArray;
    
    NSInteger pageSize;
    BOOL isRefresh;
    
    NSInteger buttonSelectTag;
}

@property (nonatomic ,strong) UITableView *listTableView;

@end

@implementation CJCBlacklistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = [NSMutableArray array];
    pageSize = 1;
    isRefresh = NO;
    
    [self setUpNaviView];
    
    [self setUpUI];
    
    [self getFollowsOrFansList];
}

-(void)getFollowsOrFansList{
    
    [self showWithLabelAnimation];
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/list/black"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"page"] = [NSString stringWithFormat:@"%ld",(long)pageSize];
    params[@"pageSize"] = [NSString stringWithFormat:@"25"];
    
    [CJCHttpTool postWithUrl:giftURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCpersonalInfoModel class] json:responseObject[@"data"]];
            
            if (isRefresh) {
                
                [dataArray removeAllObjects];
            }
            
            [dataArray addObjectsFromArray:tempArr];
            
            [self.listTableView.mj_header endRefreshing];
            [self.listTableView.mj_footer endRefreshing];
            
            [self.listTableView reloadData];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
}

-(void)setUpUI{
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT - 64);
    
    self.listTableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCNearbyPersonCell class] forCellReuseIdentifier:NEARBYPERSONCELL];
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        [weakSelf loadNewData];
    }];
    
    self.listTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        [weakSelf loadMoreData];
    }];
}

-(void)loadNewData{
    
    pageSize = 1;
    isRefresh = YES;
    
    [self getFollowsOrFansList];
}

-(void)loadMoreData{
    
    pageSize++;
    
    isRefresh = NO;
    
    [self getFollowsOrFansList];
}

#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kAdaptedValue(114);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCNearbyPersonCell *cell = [tableView dequeueReusableCellWithIdentifier:NEARBYPERSONCELL];
    
    CJCpersonalInfoModel *model = dataArray[indexPath.row];
    
    cell.infoModel = model;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self tableViewEditHandleWith:indexPath.row];
}

/**
 *  修改Delete按钮文字为“删除”
 */
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"取消拉黑";
}

-(void)tableViewEditHandleWith:(NSInteger)index{
    
    NSString *editURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/cancel/black"];
    
    CJCpersonalInfoModel *model = dataArray[index];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = model.uid;
    
    [CJCHttpTool postWithUrl:editURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            if (buttonSelectTag == 0){
                
                [MBManager showBriefAlert:@"取消关注成功"];
                
            }else{
                
                [MBManager showBriefAlert:@"拉黑成功"];
            }
            
            // 删除模型
            [dataArray removeObjectAtIndex:index];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            
            // 刷新
            [self.listTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
    
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:17 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSString *nameStr = [NSString stringWithFormat:@"黑名单"];
    
    CGFloat width = [CJCTools getShortStringLength:nameStr withFont:[UIFont accodingVersionGetFont_mediumWithSize:17]];
    
    titleLabel.text = nameStr;
    
    titleLabel.size = CGSizeMake(width, 24);
    
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

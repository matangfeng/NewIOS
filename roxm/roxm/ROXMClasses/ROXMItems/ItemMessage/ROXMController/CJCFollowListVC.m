//
//  CJCFollowListVC.m
//  roxm
//
//  Created by 陈建才 on 2017/11/1.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCFollowListVC.h"
#import "CJCCommon.h"
#import "CJCNearbyPersonCell.h"
#import "CJCPersonalInfoVC.h"

#define NEARBYPERSONCELL     @"CJCNearbyPersonCell"

@interface CJCFollowListVC ()<UITableViewDelegate,UITableViewDataSource>{

    NSMutableArray *dataArray;
    
    NSInteger pageSize;
    BOOL isRefresh;
    
    NSInteger buttonSelectTag;
}

@property (nonatomic ,strong) UITableView *listTableView;

@property (nonatomic ,strong) UILabel *followLabel;

@property (nonatomic ,strong) UILabel *fansLabel;

@property (nonatomic ,strong) UIView *hintView;

@end

@implementation CJCFollowListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = [NSMutableArray array];
    pageSize = 1;
    isRefresh = NO;
    
    buttonSelectTag = 0;
    
    [self setUpNaviView];
    
    [self setUpUI];
    
    [self getFollowsOrFansList];
}

-(void)getFollowsOrFansList{

    [self showWithLabelAnimation];
    
    NSString *giftURL;
    
    if (buttonSelectTag == 0) {
        
        giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/list/follow"];
    }else{
    
        giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/list/fans"];
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"page"] = [NSString stringWithFormat:@"%ld",(long)pageSize];
    params[@"pageSize"] = [NSString stringWithFormat:@"25"];
    
    [CJCHttpTool postWithUrl:giftURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCpersonalInfoModel class] json:responseObject[@"data"]];
            
            if (isRefresh) {
                
                [dataArray removeAllObjects];
            }
            
            [dataArray addObjectsFromArray:tempArr];
            
            [self.listTableView.mj_header endRefreshing];
            [self.listTableView.mj_footer endRefreshing];
            
            [self.listTableView reloadData];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
     
    } failure:^(NSError *error) {
        
        
    }];

}

-(void)leftButtonClickHandle{

    buttonSelectTag = 0;
    
    pageSize = 1;
    isRefresh = YES;
    
    [self getFollowsOrFansList];
    
    self.followLabel.font = [UIFont accodingVersionGetFont_mediumWithSize:14];
    self.followLabel.textColor = [UIColor toUIColorByStr:@"222222"];
    
    self.fansLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
    self.fansLabel.textColor = [UIColor toUIColorByStr:@"888888"];
    
    self.hintView.centerX = self.followLabel.centerX;
}

-(void)rightButtonClickHandle{
    
    buttonSelectTag = 1;
    
    pageSize = 1;
    isRefresh = YES;
    
    [self getFollowsOrFansList];
    
    self.followLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
    self.followLabel.textColor = [UIColor toUIColorByStr:@"888888"];
    
    self.fansLabel.font = [UIFont accodingVersionGetFont_mediumWithSize:14];
    self.fansLabel.textColor = [UIColor toUIColorByStr:@"222222"];
    
    self.hintView.centerX = self.fansLabel.centerX;
}

-(void)setUpUI{

    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    tableView.frame = CGRectMake(0, 64+49, SCREEN_WITDH, SCREEN_HEIGHT - 64-49);
    
    self.listTableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCNearbyPersonCell class] forCellReuseIdentifier:NEARBYPERSONCELL];
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    [weakSelf loadNewData];

    
    
    
    
//    self.listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//
//    }];
//
//    self.listTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//
//        [weakSelf loadMoreData];
//    }];
}

-(void)loadNewData{
    
    pageSize = 1;
    isRefresh = YES;
    
    [self getFollowsOrFansList];
}

-(void)loadMoreData{
    
    pageSize++;
    
    isRefresh = NO;
    
    [self getFollowsOrFansList];
}

#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kAdaptedValue(114);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCNearbyPersonCell *cell = [tableView dequeueReusableCellWithIdentifier:NEARBYPERSONCELL];
    
    CJCpersonalInfoModel *model = dataArray[indexPath.row];
    
    cell.infoModel = model;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCpersonalInfoModel *model = dataArray[indexPath.row];
    
    CJCPersonalInfoVC *nextVC = [[CJCPersonalInfoVC alloc] init];
    
    nextVC.sexType = CJCSexTypeWoman;
    nextVC.otherUID = model.uid;
    nextVC.infoVCType = CJCPersonalInfoVCTypeOther;
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    nextVC.location = delegate.userLocation;
    
    nextVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:nextVC animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.listVCType == CJCFollowListVCTypeOpration) {
        
        return YES;
    }
    
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    [self tableViewEditHandleWith:indexPath.row];
}

/**
 *  修改Delete按钮文字为“删除”
 */
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (buttonSelectTag == 0) {
        
        return @"取消关注";
    }else{
    
        return @"拉黑";
    }
}

-(void)tableViewEditHandleWith:(NSInteger)index{

    NSString *editURL;
    
    if (buttonSelectTag == 0) {
        
        editURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/cancel/follow"];
    }else{
    
        editURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/black"];
    }
    
    CJCpersonalInfoModel *model = dataArray[index];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = model.uid;
    
    [CJCHttpTool postWithUrl:editURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            if (buttonSelectTag == 0){
            
                [MBManager showBriefAlert:@"取消关注成功"];
                
            }else{
            
                [MBManager showBriefAlert:@"拉黑成功"];
            }
            
            // 删除模型
            [dataArray removeObjectAtIndex:index];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            
            // 刷新
            [self.listTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];

}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:17 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSString *nameStr = [NSString stringWithFormat:@"关注"];
    
    CGFloat width = [CJCTools getShortStringLength:nameStr withFont:[UIFont accodingVersionGetFont_mediumWithSize:17]];
    
    titleLabel.text = nameStr;
    
    titleLabel.size = CGSizeMake(width, 24);
    
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
    
    UIView *switchView = [[UIView alloc] init];
    
    switchView.frame = CGRectMake(0, 64, SCREEN_WITDH, 49);
    
    [self.view addSubview:switchView];
    
    UIView *bottomLineView = [UIView getLineView];
    
    bottomLineView.frame = CGRectMake(0, 48, SCREEN_WITDH, OnePXLineHeight);
    
    [switchView addSubview:bottomLineView];
    
    UILabel *leftLabel = [UIView getSystemLabelWithStr:@"关注" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"222222"]];
    
    [leftLabel sizeToFit];
    leftLabel.centerX = SCREEN_WITDH/4;
    leftLabel.centerY = 24;
    
    self.followLabel = leftLabel;
    [switchView addSubview:leftLabel];
    
    UIView *middleLineView = [UIView getLineView];
    
    middleLineView.size = CGSizeMake(OnePXLineHeight, 16);
    middleLineView.centerX = SCREEN_WITDH/2;
    middleLineView.centerY = 24;
    
    [switchView addSubview:middleLineView];
    
    UILabel *rightLabel = [UIView getSystemLabelWithStr:@"粉丝" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"888888"]];
    
    [rightLabel sizeToFit];
    rightLabel.centerX = SCREEN_WITDH*0.75;
    rightLabel.centerY = 24;
    
    self.fansLabel = rightLabel;
    [switchView addSubview:rightLabel];
    
    UIView *bottomHintView = [[UIView alloc] init];
    
    bottomHintView.backgroundColor = [UIColor toUIColorByStr:@"222222"];
    bottomHintView.size = CGSizeMake(kAdaptedValue(25), 2);
    bottomHintView.top = leftLabel.bottom+5;
    bottomHintView.centerX = leftLabel.centerX;
    
    self.hintView = bottomHintView;
    [switchView addSubview:bottomHintView];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    leftButton.frame = CGRectMake(0, 0, SCREEN_WITDH/2, 49);
    
    [leftButton addTarget:self action:@selector(leftButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    [switchView addSubview:leftButton];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    rightButton.frame = CGRectMake(SCREEN_WITDH/2, 0, SCREEN_WITDH/2, 49);
    
    [rightButton addTarget:self action:@selector(rightButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    [switchView addSubview:rightButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

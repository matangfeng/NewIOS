//
//  CJCNearbyPersonCell.h
//  roxm
//
//  Created by lfy on 2017/9/2.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCpersonalInfoModel;

@interface CJCNearbyPersonCell : UITableViewCell

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@end

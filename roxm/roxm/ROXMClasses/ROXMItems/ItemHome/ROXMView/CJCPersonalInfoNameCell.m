//
//  CJCPersonalInfoNameCell.m
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPersonalInfoNameCell.h"
#import "CJCCommon.h"

@interface CJCPersonalInfoNameCell ()

@property (nonatomic ,strong) UILabel *nameAndAgeLabel;

@property (nonatomic ,strong) UIImageView *isVipImageView;

@property (nonatomic ,strong) UILabel *signatureLabel;

@property (nonatomic ,strong) UILabel *distanceLabel;
@property (nonatomic ,strong) UILabel *hintDistanceLabel;

@property (nonatomic ,strong) UILabel *activityTimeLabel;
@property (nonatomic ,strong) UILabel *hintActivityLabel;

@end

@implementation CJCPersonalInfoNameCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{

    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:20 color:[UIColor toUIColorByStr:@"222222"]];
    
    nameLabel.frame = CGRectMake(kAdaptedValue(20.5), kAdaptedValue(16), kAdaptedValue(83), kAdaptedValue(20));
    
    self.nameAndAgeLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UIImageView *vipImageView = [[UIImageView alloc] initWithImage:kGetImage(@"preson_pic_vip")];
    
    vipImageView.frame = CGRectMake(112.5, 19.5, kAdaptedValue(19), kAdaptedValue(14));
    
    vipImageView.centerY = nameLabel.centerY;
    
    self.isVipImageView = vipImageView;
    [self.contentView addSubview:vipImageView];
    
    UILabel *qianmingLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"888888"]];
    
    qianmingLabel.frame = CGRectMake(kAdaptedValue(20), nameLabel.bottom+kAdaptedValue(12), 168, 14);
    
    self.signatureLabel = qianmingLabel;
    [self.contentView addSubview:qianmingLabel];
    
    UILabel *distanceLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"222222"]];
    
    distanceLabel.frame = CGRectMake(kAdaptedValue(20), qianmingLabel.bottom+kAdaptedValue(28), 49, kAdaptedValue(15));
    
    self.distanceLabel = distanceLabel;
    [self.contentView addSubview:distanceLabel];
    
    UILabel *activityLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"222222"]];
    
    activityLabel.frame = CGRectMake(97, qianmingLabel.bottom+kAdaptedValue(28), 39, 15);
    
    self.activityTimeLabel = activityLabel;
    [self.contentView addSubview:activityLabel];
    
    UILabel *hintDistanceLabel = [UIView getYYLabelWithStr:@"距离" fontName:kFONTNAMEREGULAR size:12 color:[UIColor toUIColorByStr:@"888888"]];
    
    hintDistanceLabel.textAlignment = NSTextAlignmentCenter;
    
    hintDistanceLabel.frame = CGRectMake(32, distanceLabel.bottom+kAdaptedValue(8), kAdaptedValue(30), kAdaptedValue(12));
    
    self.hintDistanceLabel = hintDistanceLabel;
    [self.contentView addSubview:hintDistanceLabel];
    
    UILabel *hintActivityLabel = [UIView getYYLabelWithStr:@"活跃" fontName:kFONTNAMEREGULAR size:12 color:[UIColor toUIColorByStr:@"888888"]];
    
    hintActivityLabel.textAlignment = NSTextAlignmentCenter;
    
    hintActivityLabel.frame = CGRectMake(104, activityLabel.bottom+kAdaptedValue(8), kAdaptedValue(30), kAdaptedValue(12));
    
    self.hintActivityLabel = hintActivityLabel;
    [self.contentView addSubview:hintActivityLabel];
    
    UIImageView *iconImageView = [[UIImageView alloc] init];
    
    iconImageView.frame = CGRectMake(284, kAdaptedValue(6), kAdaptedValue(66), kAdaptedValue(66));
    iconImageView.right = SCREEN_WITDH - kAdaptedValue(24.5);
    
    iconImageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    iconImageView.layer.cornerRadius = kAdaptedValue(33);
    iconImageView.layer.masksToBounds = YES;
    
    iconImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *iconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconTapHandle)];
    [iconImageView addGestureRecognizer:iconTap];
    
    self.cycleIconImageView = iconImageView;
    [self.contentView addSubview:iconImageView];
    
    UIButton *followButton = [UIView getButtonWithStr:@"关注" fontName:kFONTNAMEREGULAR size:13 color:[UIColor toUIColorByStr:@"222222"]];
    
    followButton.frame = CGRectMake(280, iconImageView.bottom+kAdaptedValue(18), kAdaptedValue(75), kAdaptedValue(34.5));
    
    followButton.centerX = iconImageView.centerX;
    
    followButton.layer.cornerRadius = kAdaptedValue(4);
    [followButton.layer setBorderColor:[UIColor toUIColorByStr:@"919191"].CGColor];
    [followButton.layer setBorderWidth:1];
    [followButton.layer setMasksToBounds:YES];
    
    [followButton addTarget:self action:@selector(followButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.followButton = followButton;
    [self.contentView addSubview:followButton];
    
}

-(void)iconTapHandle{

    if (self.tapHandle) {
        
        self.tapHandle(self);
    }
}

-(void)followButtonDidClick:(UIButton *)button{
    
    NSString *buttonStr = button.titleLabel.text;
    
    if (self.followHandle) {
        
        self.followHandle(buttonStr);
    }
}

-(void)setRelationStr:(NSString *)relationStr{

    [self.followButton setTitle:relationStr forState:UIControlStateNormal];
    
//    CGFloat width = [CJCTools getShortStringLength:relationStr withFont:[UIFont accodingVersionGetFont_regularWithSize:13]];
//    
//    self.followButton.width = width;
//    self.followButton.centerX = self.cycleIconImageView.centerX;
}

-(void)setInfoModel:(CJCpersonalInfoModel *)infoModel{

    _infoModel = infoModel;
    
    if (infoModel.infoVCType == CJCPersonalInfoVCTypeOther) {
        
        self.followButton.hidden = NO;
    }else{
    
        self.followButton.hidden = YES;
    }
    
    CGFloat nameAgeLabelWidth = [CJCTools getShortStringLength:[self nameAndAgeStr] withFont:[UIFont accodingVersionGetFont_mediumWithSize:20]];
    
    self.nameAndAgeLabel.width = nameAgeLabelWidth;
    self.nameAndAgeLabel.text = [self nameAndAgeStr];
    
    self.isVipImageView.left = self.nameAndAgeLabel.right+kAdaptedValue(9);
    
    if ([CJCTools isBlankString:infoModel.brief]) {
        
        self.signatureLabel.text = @"暂无";
    }else{
    
        self.signatureLabel.text = infoModel.brief;
    }
    
    if (![CJCTools isBlankString:infoModel.imageUrl]) {
        
        [self.cycleIconImageView setImageURL:[NSURL URLWithString:infoModel.imageUrl]];
    }
    
    NSString *distanceStr = @"2.1公里";
    
    CGFloat distanceWidth = [CJCTools getShortStringLength:distanceStr withFont:[UIFont accodingVersionGetFont_regularWithSize:15]];
    
    self.distanceLabel.width = distanceWidth;
    self.distanceLabel.text = distanceStr;
    
    self.hintDistanceLabel.centerX = self.distanceLabel.centerX;
    
    NSString *activityDay;
    
    if (infoModel.lastTime == 0) {
        
        activityDay = @"1天前";
    }else{
    
        NSInteger nowTimeStap = [CJCTools getDateTimeTOMilliSeconds].integerValue;
        
        activityDay = [self compareTwoTime:infoModel.lastTime time2:nowTimeStap];
    }
    
    CGFloat activityWidth = [CJCTools getShortStringLength:activityDay withFont:[UIFont accodingVersionGetFont_regularWithSize:15]];
    
    self.activityTimeLabel.width = activityWidth;
    self.activityTimeLabel.left = self.distanceLabel.right+kAdaptedValue(28);
    self.activityTimeLabel.text = activityDay;
    
    self.hintActivityLabel.centerX = self.activityTimeLabel.centerX;
}

-(NSString *)nameAndAgeStr{

    NSString *nameStr = self.infoModel.nickname;
    
    NSString *birthStr;
    
    if ([CJCTools isBlankString:self.infoModel.birthDay]) {
        
        birthStr = @"1997_01_01";
    }else{
    
        birthStr = self.infoModel.birthDay;
    }
    
    NSString *nameAgeStr = [NSString stringWithFormat:@"%@，%@",nameStr,[CJCTools dateToAge:birthStr]];
    
    return nameAgeStr;
}

- (NSString*)compareTwoTime:(long)time1 time2:(long)time2{
    
    NSTimeInterval balance = time2 /1000- time1 /1000;
    
    NSString *timeString = [[NSString alloc]init];
    
    timeString = [NSString stringWithFormat:@"%f",balance /60];
    
    timeString = [timeString substringToIndex:timeString.length-7];
    
    NSInteger timeInt = [timeString intValue];
    
    NSInteger hour = timeInt /60;
    
    NSInteger mint = timeInt %60;
    
    NSInteger day = timeInt/60/24;
    
    if(day == 0) {
        
        if (hour == 0) {
            
            timeString = [NSString stringWithFormat:@"%ld分钟前",(long)mint];
            
            if (mint == 0) {
                
                timeString = @"刚刚";
            }
            
        }else{
            
            timeString = [NSString stringWithFormat:@"%ld小时前",(long)hour];
        }
        
    }else{
        
        timeString = [NSString stringWithFormat:@"%ld天前",(long)day];
    }
    
    return timeString;
    
}

@end

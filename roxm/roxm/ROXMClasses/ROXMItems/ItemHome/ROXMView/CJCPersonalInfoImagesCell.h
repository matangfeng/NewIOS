//
//  CJCPersonalInfoImagesCell.h
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PersonalInfoImagesCellDelegate <NSObject>

-(void)imageCellClickHandle:(NSInteger)index;

-(void)openPhotoLibraryHandle;

@end

@class CJCpersonalInfoModel;

@interface CJCPersonalInfoImagesCell : UITableViewCell

@property (nonatomic ,weak) id<PersonalInfoImagesCellDelegate> delegate;

@property (nonatomic ,strong) UICollectionView *collectionView;

@property (nonatomic ,assign) CGFloat itemHeight;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@property (nonatomic ,copy) NSArray *imagesArr;

@end

//
//  CJCNearbyPersonCell.m
//  roxm
//
//  Created by lfy on 2017/9/2.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCNearbyPersonCell.h"
#import "CJCCommon.h"
#import "CJCpersonalInfoModel.h"

@interface CJCNearbyPersonCell ()

@property (nonatomic ,strong) UIImageView *iconImageView;

@property (nonatomic ,strong)  UILabel *nameLabel;

@property (nonatomic ,strong) UIView *sexAndAgeView;

@property (nonatomic ,strong) UIImageView *sexImageView;

@property (nonatomic ,strong) UILabel *ageLabel;

@property (nonatomic ,strong) UILabel *professionLabel;

@property (nonatomic ,strong) UILabel *heightWeightLabel;

@property (nonatomic ,strong) UILabel *distanceTimeLabel;

@property (nonatomic ,strong) UIImageView *DiamondImageView;

@property (nonatomic ,strong) UILabel *ObservantRatio;

@end

@implementation CJCNearbyPersonCell

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    self.sexAndAgeView.backgroundColor = [UIColor toUIColorByStr:@"EA78AA"];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
//        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{

    UIImageView *iconImageView = [[UIImageView alloc] init];
    
    iconImageView.frame = CGRectMake(0, kAdaptedValue(12.5), kAdaptedValue(90), kAdaptedValue(90));
    
    iconImageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    iconImageView.layer.masksToBounds = YES;
    
    self.iconImageView = iconImageView;
    [self.contentView addSubview:iconImageView];
    
    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"名字" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"2E2F33"]];
    
    nameLabel.frame = CGRectMake(iconImageView.right+kAdaptedValue(15), kAdaptedValue(18.5), kAdaptedValue(30), kAdaptedValue(16));
    
    self.nameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UIView *sexAndAgeView = [[UIView alloc] init];
    
    sexAndAgeView.frame = CGRectMake(nameLabel.right+kAdaptedValue(6), kAdaptedValue(19.5), kAdaptedValue(31.5), kAdaptedValue(14));
    
    sexAndAgeView.backgroundColor = [UIColor toUIColorByStr:@"EA78AA"];
    
    sexAndAgeView.layer.cornerRadius = kAdaptedValue(2);
    sexAndAgeView.layer.masksToBounds = YES;
    
    self.sexAndAgeView = sexAndAgeView;
    [self.contentView addSubview:sexAndAgeView];
    
    UIImageView *sexImageView = [[UIImageView alloc] initWithImage:kGetImage(@"index_female")];
    
    sexImageView.frame = CGRectMake(kAdaptedValue(3), 0, kAdaptedValue(9), kAdaptedValue(9));
    
    sexImageView.centerY = kAdaptedValue(7);
    
    self.sexImageView = sexImageView;
    [sexAndAgeView addSubview:sexImageView];
    
    UILabel *ageLabel = [UIView getSystemLabelWithStr:@"20" fontName:kFONTNAMEREGULAR size:9.5 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
     ageLabel.textAlignment = NSTextAlignmentLeft;
    
    ageLabel.frame = CGRectMake(sexImageView.right+kAdaptedValue(2), 0, sexAndAgeView.width-sexImageView.right-kAdaptedValue(2), 12);
    
    ageLabel.centerY = kAdaptedValue(7);
    
    self.ageLabel = ageLabel;
    [sexAndAgeView addSubview:ageLabel];
    
    UILabel *professionLabel = [UIView getSystemLabelWithStr:@"职业" fontName:kFONTNAMEREGULAR size:12 color:[UIColor toUIColorByStr:@"8A8C99"]];
    
    professionLabel.frame = CGRectMake(iconImageView.right+kAdaptedValue(15), nameLabel.bottom+kAdaptedValue(6), kAdaptedValue(48), kAdaptedValue(13));
    
    self.professionLabel = professionLabel;
    [self.contentView addSubview:professionLabel];
    
    UILabel *heightWeightLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:11 color:[UIColor toUIColorByStr:@"757575"]];
    
    heightWeightLabel.frame = CGRectMake(professionLabel.right+kAdaptedValue(9), 30, 30, kAdaptedValue(12));
    
    heightWeightLabel.centerY = professionLabel.centerY;
    
    self.heightWeightLabel = heightWeightLabel;
    [self.contentView addSubview:heightWeightLabel];
    
    UILabel *distanceTimeLabel = [UIView getSystemLabelWithStr:@"1.1km·15分钟前" fontName:kFONTNAMEREGULAR size:11 color:[UIColor toUIColorByStr:@"8C959F"]];
    
    distanceTimeLabel.frame = CGRectMake(iconImageView.right+kAdaptedValue(15), kAdaptedValue(84.5), 76, kAdaptedValue(11));
    
    self.distanceTimeLabel = distanceTimeLabel;
    [self.contentView addSubview:distanceTimeLabel];
    
    UIImageView *zuanshiView = [[UIImageView alloc] initWithImage:kGetImage(@"index_zuanshi")];
    
    zuanshiView.frame = CGRectMake(SCREEN_WITDH-kAdaptedValue(54), kAdaptedValue(29), kAdaptedValue(16), kAdaptedValue(14.5));
    
    self.DiamondImageView = zuanshiView;
    [self.contentView addSubview:zuanshiView];
    
    UILabel *observantLabel = [UIView getSystemLabelWithStr:@"50%" fontName:kFONTNAMEREGULAR size:11 color:[UIColor toUIColorByStr:@"8C959F"]];
    
    observantLabel.textAlignment = NSTextAlignmentRight;
    
    observantLabel.frame = CGRectMake(zuanshiView.right, kAdaptedValue(29), SCREEN_WITDH-zuanshiView.right-kAdaptedValue(11), kAdaptedValue(11));
    
    observantLabel.centerY = zuanshiView.centerY;
    
    self.ObservantRatio = observantLabel;
    [self.contentView addSubview:observantLabel];
    
    UIView *lineView = [UIView getLineView];
    lineView.frame = CGRectMake(0, kAdaptedValue(113), SCREEN_WITDH, OnePXLineHeight);
    [self.contentView addSubview:lineView];
}

-(void)setInfoModel:(CJCpersonalInfoModel *)infoModel{

    _infoModel = infoModel;
    
    [self.iconImageView setImageURL:[NSURL URLWithString:infoModel.imageUrl]];
    
    CGFloat nameLabelWidth = [CJCTools getShortStringLength:infoModel.nickname withFont:[UIFont accodingVersionGetFont_regularWithSize:15]];
    
    self.nameLabel.width = nameLabelWidth;
    
    self.nameLabel.text = infoModel.nickname;
    
    self.sexAndAgeView.left = self.nameLabel.right+kAdaptedValue(6);
    
    self.ageLabel.text = [self getAgeStr];
    
    NSString *career;
    if([CJCTools isBlankString:infoModel.career]){
        
        career = @"待业";
    }else{
    
        career = infoModel.career;
    }
    
    CGFloat careerLabelWidth = [CJCTools getShortStringLength:career withFont:[UIFont accodingVersionGetFont_regularWithSize:12]];
    
    self.professionLabel.text = career;
    self.professionLabel.width = careerLabelWidth;
    
    NSInteger height,weight;
    
    if (infoModel.height == 0) {
        
        height = 170;
    }else{
    
        height = infoModel.height;
    }
    
    if (infoModel.weight == 0) {
        
        weight = 65;
    }else{
    
        weight = infoModel.weight;
    }
    
    NSString *heightStr = [NSString stringWithFormat:@"%ldcm·%ldkg",height,weight];
    
    CGFloat heightLabelWidth = [CJCTools getShortStringLength:heightStr withFont:[UIFont accodingVersionGetFont_regularWithSize:11]];
    
    self.heightWeightLabel.width = heightLabelWidth+8;
    self.heightWeightLabel.left = self.professionLabel.right+kAdaptedValue(9);
    self.heightWeightLabel.text = heightStr;
    
    CGFloat distanceWidth = [CJCTools getShortStringLength:@"1.1km·15分钟前" withFont:[UIFont accodingVersionGetFont_regularWithSize:11]];
    
    self.distanceTimeLabel.width = distanceWidth;
}

-(NSString *)getAgeStr{

    NSString *birthStr;
    
    if ([CJCTools isBlankString:self.infoModel.birthDay]) {
        
        birthStr = @"1997_01_01";
    }else{
        
        birthStr = self.infoModel.birthDay;
    }
    
    NSString *nameAgeStr = [NSString stringWithFormat:@"%@",[CJCTools dateToAge:birthStr]];
    
    return nameAgeStr;
}

@end

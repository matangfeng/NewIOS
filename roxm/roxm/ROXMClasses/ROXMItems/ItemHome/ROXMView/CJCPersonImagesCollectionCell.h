//
//  CJCPersonImagesCollectionCell.h
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCPhotoShowModel;

@interface CJCPersonImagesCollectionCell : UICollectionViewCell

@property (nonatomic ,strong) UIImageView *personalImageView;

@property (nonatomic ,strong) CJCPhotoShowModel *showModel;

@property (nonatomic ,copy) NSString *imageURL;

@end

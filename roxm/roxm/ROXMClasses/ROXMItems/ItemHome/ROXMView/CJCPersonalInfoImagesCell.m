//
//  CJCPersonalInfoImagesCell.m
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPersonalInfoImagesCell.h"
#import "CJCCommon.h"
#import "CJCPersonImagesCollectionCell.h"
#import "CJCPhotoShowModel.h"
#import "CJCPersonalInfoImagesLayout.h"

#define kPersonImagesCollectionCell     @"CJCPersonImagesCollectionCell"

@interface CJCPersonalInfoImagesCell ()<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout>{

    CGPoint beginOffset;
    BOOL libraryOpen;
}

@property (nonatomic ,strong) UILabel *imagesNumLabel;

@property (nonatomic ,strong) UIView *noImageView;

@property (nonatomic ,strong) UIView *imagesView;

@property (nonatomic ,strong) UIPageControl *pageControl;

@property (nonatomic ,strong) CJCPersonalInfoImagesLayout *layout;

@property (nonatomic ,strong) UILabel *addMoreButton;

@end

@implementation CJCPersonalInfoImagesCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        libraryOpen = YES;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIView *containView = [[UIView alloc] init];
    
    self.imagesView = containView;
    [self.contentView addSubview:containView];
    
    UILabel *numLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:13 color:[UIColor toUIColorByStr:@"222222"]];
    
    self.imagesNumLabel = numLabel;
    [self.contentView addSubview:numLabel];
    
    UIPageControl *control = [[UIPageControl alloc] init];
    
    control.pageIndicatorTintColor = [UIColor toUIColorByStr:@"CBCBCB"];
    control.currentPageIndicatorTintColor = [UIColor toUIColorByStr:@"222222"];
    
    self.pageControl = control;
    [self.contentView addSubview:control];
}

-(void)setImagesArr:(NSArray *)imagesArr{

    if (_imagesArr.count == 0) {
        
        _imagesArr = imagesArr;
        
        if (imagesArr.count == 0) {
            
            self.imagesView.frame = CGRectMake(0, 0, SCREEN_WITDH, self.itemHeight);
            
            self.noImageView.hidden = NO;
            self.noImageView.frame = self.imagesView.bounds;
        }else if(imagesArr.count<=3){
            
            self.imagesView.frame = CGRectMake(0, 0, SCREEN_WITDH, self.itemHeight);
            
            self.collectionView.hidden = NO;
            self.collectionView.frame = self.imagesView.bounds;
        }else{
            
            self.imagesView.frame = CGRectMake(0, 0, SCREEN_WITDH, self.itemHeight*2);
            
            self.collectionView.hidden = NO;
            self.collectionView.frame = self.imagesView.bounds;
        }
    }else{
    
        _imagesArr = imagesArr;
    
        self.layout.itemsCount = imagesArr.count;
        
        [self.collectionView reloadData];
    }
    
    NSInteger imageCount=0,videoCount=0;
    
    for (CJCPhotoShowModel *model in imagesArr) {
        
        if (model.type == 21) {
            
            videoCount++;
        }else{
        
            imageCount++;
        }
    }
    
    if (imageCount == 0 && videoCount == 0) {
        
        self.imagesNumLabel.hidden = YES;
    }else{
    
        self.imagesNumLabel.hidden = NO;
        
        if (imageCount == 0) {
            
            self.imagesNumLabel.text = [NSString stringWithFormat:@"%@的%ld个视频",self.infoModel.nickname,videoCount];
        }else if(videoCount == 0){
        
            self.imagesNumLabel.text = [NSString stringWithFormat:@"%@的%ld张照片",self.infoModel.nickname,imageCount];
        }else{
        
            self.imagesNumLabel.text = [NSString stringWithFormat:@"%@的%ld张照片,%ld个视频",self.infoModel.nickname,imageCount,videoCount];
        }
    }
    
    CGFloat imageNumWidth = [CJCTools getShortStringLength:self.imagesNumLabel.text withFont:[UIFont accodingVersionGetFont_regularWithSize:13]];
    
    self.imagesNumLabel.frame = CGRectMake(kAdaptedValue(19), self.imagesView.bottom+kAdaptedValue(10), imageNumWidth, kAdaptedValue(19));
    
    if (imagesArr.count<=6) {
        
        self.pageControl.hidden = YES;
    }else{
    
        if (imagesArr.count == 18) {
            
            self.addMoreButton.hidden = NO;
            self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 100);
        }
        
        self.pageControl.hidden = NO;
        
        self.pageControl.frame = CGRectMake(100, self.imagesView.bottom+kAdaptedValue(10), 100, 19);
        self.pageControl.right = SCREEN_WITDH - 20;
        
        self.pageControl.numberOfPages = (imagesArr.count-1)/6+1;
    }
    
}



-(UIView *)noImageView{

    if (_noImageView == nil) {
        
        UIView *noimageView = [[UIView alloc] init];
        
        noimageView.frame = CGRectMake(0, 0, SCREEN_WITDH, self.itemHeight);
        
        [self.imagesView addSubview:noimageView];
        
        UILabel *noimageLabel = [UIView getYYLabelWithStr:@"上传视频图片，展示自己" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"222222"]];
        
        noimageLabel.textAlignment = NSTextAlignmentCenter;
        
        noimageLabel.frame = CGRectMake(0, 12, SCREEN_WITDH, 20);
        
        noimageLabel.centerY = self.itemHeight/2;
        
        [noimageView addSubview:noimageLabel];
        
        _noImageView = noimageView;
    }
    return _noImageView;
}

-(UICollectionView *)collectionView{

    if (_collectionView == nil) {
        
        CJCPersonalInfoImagesLayout *flowLayout = [[CJCPersonalInfoImagesLayout alloc] init];
        
        self.layout = flowLayout;
        
//        flowLayout.footerReferenceSize = CGSizeMake(100, SCREEN_WITDH/3);
        
        flowLayout.itemsCount = self.imagesArr.count;
        
        UICollectionView *imageCollectionView;
        
        if (self.imagesArr.count<=3) {
            
            imageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WITDH, self.itemHeight) collectionViewLayout:flowLayout];
        }else{
        
            imageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WITDH, 2*self.itemHeight) collectionViewLayout:flowLayout];
        }
        
        imageCollectionView.backgroundColor = [UIColor whiteColor];
        
        imageCollectionView.dataSource = self;
        imageCollectionView.delegate = self;
        
        imageCollectionView.alwaysBounceHorizontal = YES;
        imageCollectionView.showsHorizontalScrollIndicator = NO;
        
        imageCollectionView.pagingEnabled = YES;
        
        [imageCollectionView registerClass:[CJCPersonImagesCollectionCell class] forCellWithReuseIdentifier:kPersonImagesCollectionCell];
        [imageCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
        
        [self.imagesView addSubview:imageCollectionView];
        
        _collectionView = imageCollectionView;
    }
    
    return _collectionView;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{

    if (self.imagesArr.count >= 18) {
        
        if (scrollView.contentOffset.x - scrollView.contentSize.width>-325) {
            
            if ([self.delegate respondsToSelector:@selector(openPhotoLibraryHandle)]) {
                
                [self.delegate openPhotoLibraryHandle];
            }
        }
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    CGFloat scrollX = scrollView.contentOffset.x;
    
    NSInteger currentIndex = scrollX/SCREEN_WITDH;
    
    self.pageControl.currentPage = currentIndex;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    return CGSizeMake(SCREEN_WITDH/3, SCREEN_WITDH/3);
}

#pragma  mark ======collectionView 的delegate 和DataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return self.imagesArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    CJCPersonImagesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPersonImagesCollectionCell forIndexPath:indexPath];
    
    CJCPhotoShowModel *model = self.imagesArr[indexPath.item];
    cell.showModel = model;
    
    return cell;
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
//    
//    UICollectionReusableView *reusableView = nil;
//    if (kind == UICollectionElementKindSectionFooter)
//    {
//        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
//        
//        UILabel *moreLabel = [UIView getSystemLabelWithStr:@"加\n载\n更\n多" fontName:kFONTNAMEMEDIUM size:20 color:[UIColor toUIColorByStr:@"222222"]];
//        
//        moreLabel.size = CGSizeMake(30, 100);
//        moreLabel.centerY = SCREEN_WITDH/3;
//        moreLabel.x = 10;
//        
//        moreLabel.textAlignment = NSTextAlignmentCenter;
//        
//        moreLabel.layer.cornerRadius = kAdaptedValue(6);
//        [moreLabel.layer setBorderColor:[UIColor toUIColorByStr:@"979797"].CGColor];
//        [moreLabel.layer setBorderWidth:1];
//        [moreLabel.layer setMasksToBounds:YES];
//        
//        [footerview addSubview:moreLabel];
//        
//        reusableView = footerview;
//    }
//    return reusableView;
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.delegate respondsToSelector:@selector(imageCellClickHandle:)]) {
        
        [self.delegate imageCellClickHandle:indexPath.item];
    }
}

@end

//
//  CJCPersonalInfoInfoCell.m
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPersonalInfoInfoCell.h"
#import "CJCCommon.h"

@interface CJCPersonalInfoInfoCell ()

@property (nonatomic ,strong) UILabel *heightLabel;

@property (nonatomic ,strong) UILabel *weightLabel;

@property (nonatomic ,strong) UILabel *constellationLabel;

@property (nonatomic ,strong) UILabel *careerLabel;

@property (nonatomic ,strong) UILabel *bustLabel;

@property (nonatomic ,strong) UILabel *shouyueLabel;

@property (nonatomic ,strong) UILabel *hotelLevelLabel;


@end

@implementation CJCPersonalInfoInfoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIView *topLineView = [UIView getLineView];
    topLineView.frame = CGRectMake(kAdaptedValue(20), 0, SCREEN_WITDH-kAdaptedValue(40), OnePXLineHeight);
    [self.contentView addSubview:topLineView];
    
    UILabel *titleLabel = [UIView getYYLabelWithStr:@"个人信息" fontName:kFONTNAMEMEDIUM size:20 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(20), kAdaptedValue(100), kAdaptedValue(22));
    
    [self.contentView addSubview:titleLabel];
    
    UILabel *heightLabel = [self getEdgeLabel];
    
    heightLabel.text = @"身高：167cm";
    
    heightLabel.frame = CGRectMake(kAdaptedValue(20), titleLabel.bottom+kAdaptedValue(14), 107.5, kAdaptedValue(33));
    
    self.heightLabel = heightLabel;
    [self.contentView addSubview:heightLabel];
    
    UILabel *weightLabel = [self getEdgeLabel];
    
    weightLabel.text = @"体重：54kg";
    
    weightLabel.frame = CGRectMake(135, heightLabel.top, 98, kAdaptedValue(33));
    
    self.weightLabel = weightLabel;
    [self.contentView addSubview:weightLabel];
    
    UILabel *xingzuoLabel = [self getEdgeLabel];
    
    xingzuoLabel.text = @"水瓶座";
    
    xingzuoLabel.frame = CGRectMake(242, heightLabel.top, 66, kAdaptedValue(33));
    
    self.constellationLabel = xingzuoLabel;
    [self.contentView addSubview:xingzuoLabel];
    
    UILabel *careerLabel = [self getEdgeLabel];
    
    careerLabel.text = @"职业：模特";
    
    careerLabel.frame = CGRectMake(kAdaptedValue(20), heightLabel.bottom+kAdaptedValue(8), 94, kAdaptedValue(33));
    
    self.careerLabel = careerLabel;
    [self.contentView addSubview:careerLabel];
    
    UILabel *bustLabel = [self getEdgeLabel];
    
    bustLabel.text = @"罩杯：B";
    
    bustLabel.frame = CGRectMake(122, careerLabel.top, 75, kAdaptedValue(33));
    
    self.bustLabel = bustLabel;
    [self.contentView addSubview:bustLabel];
    
//    UILabel *shouyueLabel = [self getEdgeLabel];
//    
//    shouyueLabel.text = @"守约率：51%";
//    
//    shouyueLabel.frame = CGRectMake(205, careerLabel.top, 108, kAdaptedValue(33));
//    
//    self.shouyueLabel = shouyueLabel;
//    [self.contentView addSubview:shouyueLabel];
    
//    YYLabel *hotelLabel = [self getEdgeLabel];
//    
//    hotelLabel.text = @"酒店偏好：五星级或以上";
//    
//    hotelLabel.frame = CGRectMake(kAdaptedValue(20), careerLabel.bottom+kAdaptedValue(8), 108, kAdaptedValue(33));
//    
//    self.hotelLevelLabel = hotelLabel;
//    [self.contentView addSubview:hotelLabel];
    
    UIView *lineView = [UIView getLineView];
    lineView.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(150), SCREEN_WITDH-kAdaptedValue(40), OnePXLineHeight);
    self.saveBottomLine = lineView;
    [self.contentView addSubview:lineView];
}

-(void)setInfoModel:(CJCpersonalInfoModel *)infoModel{

    _infoModel = infoModel;
    
    UIFont *font = [UIFont accodingVersionGetFont_regularWithSize:15];
    
    NSInteger height,weight;
    
    if (infoModel.height == 0) {
        
        height = 165;
    }else{
        
        height = infoModel.height;
    }
    
    if (infoModel.weight == 0) {
        
        weight = 45;
    }else{
    
        weight = infoModel.weight;
    }
    
    NSString *heightStr = [NSString stringWithFormat:@"%ld%@",height,infoModel.cupSize];
    
    CGFloat heightWidth = [CJCTools getShortStringLength:heightStr withFont:font]+kAdaptedValue(10);
    
    self.heightLabel.text = heightStr;
    self.heightLabel.width = heightWidth;
    
    NSString *weightStr = [NSString stringWithFormat:@"%ldkg",weight];
    
    CGFloat weightWidth = [CJCTools getShortStringLength:weightStr withFont:font]+kAdaptedValue(10);
    
    self.weightLabel.text = weightStr;
    self.weightLabel.width = weightWidth;
    self.weightLabel.left = self.heightLabel.right+kAdaptedValue(8);
    
    NSString *monthStr,*careerStr,*bustStr,*shouyueStr;
    
    if (![CJCTools isBlankString:infoModel.birthDay]) {
        
        NSArray *tempArr = [infoModel.birthDay componentsSeparatedByString:@"_"];
        
        NSString *month = tempArr[1];
        NSString *day = tempArr[2];
        
        monthStr = [self getAstroWithMonth:month.intValue day:day.intValue];
        monthStr = [NSString stringWithFormat:@"%@座",monthStr];
    }else{
    
        monthStr = @"水瓶座";
    }
    
    CGFloat monthWidth = [CJCTools getShortStringLength:monthStr withFont:font]+kAdaptedValue(10);
    
    self.constellationLabel.width = monthWidth;
    self.constellationLabel.left = self.weightLabel.right+kAdaptedValue(8);
    
    self.constellationLabel.text = monthStr;
    
    if ([CJCTools isBlankString:infoModel.career]) {
        
        careerStr = @"模特";
    }else{
    
        careerStr = [NSString stringWithFormat:@"%@",infoModel.career];
    }
    
    CGFloat careerWidth = [CJCTools getShortStringLength:careerStr withFont:font]+kAdaptedValue(10);
    
    self.careerLabel.width = careerWidth;
    self.careerLabel.text = careerStr;
    
    if ([CJCTools isBlankString:infoModel.cupSize]) {
        
        bustStr = @"守约率51%";
    }else{
    
        bustStr = @"守约率51%";
    }
    
    CGFloat bustWidth = [CJCTools getShortStringLength:bustStr withFont:font]+kAdaptedValue(10);
    
    self.bustLabel.width = bustWidth;
    self.bustLabel.left = self.careerLabel.right + kAdaptedValue(8);
    self.bustLabel.text = bustStr;
    
    CGFloat fourWidth = heightWidth+weightWidth+monthWidth+careerWidth;
    CGFloat fiveWidth = heightWidth+weightWidth+monthWidth+careerWidth+bustWidth;
    
    if(fiveWidth <= SCREEN_WITDH-kAdaptedValue(72)){
    
//        self.heightLabel.top = kAdaptedValue(87);
//        self.weightLabel.top = kAdaptedValue(87);
//        self.constellationLabel.top = kAdaptedValue(87);
        
        self.careerLabel.x = self.constellationLabel.right+kAdaptedValue(8);
        self.careerLabel.top = self.constellationLabel.top;
        
        self.bustLabel.x = self.careerLabel.right+kAdaptedValue(8);
        self.bustLabel.top = self.constellationLabel.top;
    }else{
    
        if (fourWidth <= SCREEN_WITDH-kAdaptedValue(64)) {
            
            self.careerLabel.x = self.constellationLabel.right+kAdaptedValue(8);
            self.careerLabel.top = self.constellationLabel.top;
            
            self.bustLabel.x = kAdaptedValue(20);
            self.bustLabel.top = self.constellationLabel.bottom+kAdaptedValue(8);
        }
        
    }
    
    self.saveBottomLine.frame = CGRectMake(kAdaptedValue(20), CGRectGetMaxY(self.bustLabel.frame) + kAdaptedValue(20), SCREEN_WITDH-kAdaptedValue(40), OnePXLineHeight);
    
    shouyueStr = @"守约率51%";
    
    CGFloat shouyueWidth = [CJCTools getShortStringLength:shouyueStr withFont:font]+kAdaptedValue(10);
    
    self.shouyueLabel.left = self.bustLabel.right+kAdaptedValue(8);
    self.shouyueLabel.width = shouyueWidth;
    self.shouyueLabel.text = shouyueStr;
    
//    hotelLevlStr = [self hotelLevelStr:infoModel.hotelLevel];
//    
//    CGFloat hotelWidth = [CJCTools getShortStringLength:hotelLevlStr withFont:font]+kAdaptedValue(10);
//    
//    self.hotelLevelLabel.width = hotelWidth;
//    self.hotelLevelLabel.text = hotelLevlStr;
}

-(NSString *)hotelLevelStr:(NSInteger)level{

    NSString *hotelLevelStr;
    
    switch (level) {
        case 0:{
        
            hotelLevelStr = @"酒店偏好：不限";
        }
            break;
            
        case 3:{
            
            hotelLevelStr = @"酒店偏好：3星级或以上";
        }
            break;
            
        case 4:{
            
            hotelLevelStr = @"酒店偏好：4星级或以上";
        }
            break;
            
        case 5:{
            
            hotelLevelStr = @"酒店偏好：5星级或以上";
        }
            break;
            
        default:
            break;
    }

    return hotelLevelStr;
}

-(UILabel *)getEdgeLabel{

    UILabel *edgeLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"333333"]];
    
    edgeLabel.layer.cornerRadius = kAdaptedValue(6);
    [edgeLabel.layer setBorderColor:[UIColor toUIColorByStr:@"979797"].CGColor];
    [edgeLabel.layer setBorderWidth:1];
    [edgeLabel.layer setMasksToBounds:YES];
    
    edgeLabel.textAlignment = NSTextAlignmentCenter;
    
    return edgeLabel;
}

-(NSString *)getAstroWithMonth:(int)m day:(int)d{
    
    NSString *astroString = @"魔羯水瓶双鱼白羊金牛双子巨蟹狮子处女天秤天蝎射手魔羯";
    NSString *astroFormat = @"102123444543";
    NSString *result;
    
    if (m<1||m>12||d<1||d>31){
        return @"错误日期格式!";
    }
    
    if(m==2 && d>29)
    {
        return @"错误日期格式!!";
    }else if(m==4 || m==6 || m==9 || m==11) {
        
        if (d>30) {
            return @"错误日期格式!!!";
        }
    }
    
    result=[NSString stringWithFormat:@"%@",[astroString substringWithRange:NSMakeRange(m*2-(d < [[astroFormat substringWithRange:NSMakeRange((m-1), 1)] intValue] - (-19))*2,2)]];
    
    return result;
}

@end

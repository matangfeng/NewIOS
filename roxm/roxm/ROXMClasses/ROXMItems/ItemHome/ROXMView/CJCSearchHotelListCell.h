//
//  CJCSearchHotelListCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/12.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AMapPOI;

@interface CJCSearchHotelListCell : UITableViewCell

@property (nonatomic ,copy) NSString *keys;

@property (nonatomic ,assign) BOOL showDistance;

@property (nonatomic ,strong) AMapPOI *AmapPOI;

@end

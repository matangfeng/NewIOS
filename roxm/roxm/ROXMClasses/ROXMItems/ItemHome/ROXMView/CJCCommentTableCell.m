//
//  CJCCommentTableCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCCommentTableCell.h"
#import "CJCCommon.h"
#import "CJCCommentModel.h"
#import "CJCCommentCell.h"

#define kCommentCell            @"CJCCommentCell"

@interface CJCCommentTableCell ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic ,strong) UITableView *commentListTB;

@end

@implementation CJCCommentTableCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UITableView *tableview = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableview.showsVerticalScrollIndicator = NO;
    
    tableview.scrollEnabled = NO;
    
    tableview.dataSource = self;
    tableview.delegate = self;
    
    self.commentListTB = tableview;
    [self.contentView addSubview:tableview];
    
    [tableview registerClass:[CJCCommentCell class] forCellReuseIdentifier:kCommentCell];
}

-(void)setComments:(NSArray *)comments{
    
    _comments = comments;
    
    self.commentListTB.frame = CGRectMake(0, 0, SCREEN_WITDH, self.cellHeight);
    
    [self.commentListTB reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.comments.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    CJCCommentModel *model = self.comments[indexPath.row];
    
    return model.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    CJCCommentModel *model = self.comments[indexPath.row];
    
    CJCCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:kCommentCell];
    
    cell.commentModel = model;
    
    return cell;
}

@end

//
//  CJCCommentTableCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCCommentTableCell : UITableViewCell

@property (nonatomic ,assign) CGFloat cellHeight;

@property (nonatomic ,copy) NSArray *comments;

@end

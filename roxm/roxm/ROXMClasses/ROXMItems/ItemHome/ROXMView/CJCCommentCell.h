//
//  CJCCommentCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCCommentModel;

@interface CJCCommentCell : UITableViewCell

@property (nonatomic ,strong) CJCCommentModel *commentModel;

@end

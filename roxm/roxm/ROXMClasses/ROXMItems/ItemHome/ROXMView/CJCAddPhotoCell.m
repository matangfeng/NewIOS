//
//  CJCAddImageCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCAddPhotoCell.h"
#import "CJCCommon.h"

@implementation CJCAddPhotoCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor toUIColorByStr:@"E9EAED"];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:kGetImage(@"login_icon_photo_addmore")];
        
        imageView.size = CGSizeMake(kAdaptedValue(36), kAdaptedValue(36));
        imageView.center = CGPointMake(frame.size.width/2, frame.size.height/2);
        
        [self.contentView addSubview:imageView];
    }
    return self;
}

@end

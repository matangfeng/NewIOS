//
//  CJCPhotoOprationCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CJCPhotoOprationCellDelegate <NSObject>

-(void)cellSelectButtonClickHandle:(BOOL)isSelect andIndex:(NSInteger)index;

@end

@class CJCPhotoShowModel;

@interface CJCPhotoOprationCell : UICollectionViewCell

@property (nonatomic ,weak) id<CJCPhotoOprationCellDelegate> delegate;

@property (nonatomic ,strong) UIImageView *showImageView;

@property (nonatomic ,assign) NSInteger index;

@property (nonatomic ,strong) CJCPhotoShowModel *showModel;

@property (nonatomic ,assign) BOOL isEdit;

@end

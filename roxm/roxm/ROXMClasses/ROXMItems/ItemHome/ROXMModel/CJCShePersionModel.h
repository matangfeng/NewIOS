//
//  CJCShePersionModel.h
//  roxm
//
//  Created by 马棠丰 on 2017/11/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

//typedef NS_ENUM(NSInteger,CJCPersonalInfoVCType){
//
//    CJCPersonalInfoVCTypeOwn                    = 2,
//    CJCPersonalInfoVCTypeOther                  = 1,
//    CJCPersonalInfoVCTypeChat                   = 0,
//};

@interface CJCShePersionModel : NSObject
@property (nonatomic , assign) int statusa;
///用户id
@property (nonatomic ,copy) NSString *uid;

///用户手机  也是用户名
@property (nonatomic ,copy) NSString *mobile;

///用户昵称
@property (nonatomic ,copy) NSString *nickname;

///用户性别 0为女  1为男
@property (nonatomic ,assign) NSInteger sex;

///用户生日
@property (nonatomic ,copy) NSString *birthDay;

///用户的来源 通过哪个渠道下载的APP
@property (nonatomic ,copy) NSString *source;

///环信配置的用户ID
@property (nonatomic ,copy) NSString *emId;

///图片的url
@property (nonatomic ,copy) NSString *imageUrl;

///视频的url
@property (nonatomic ,copy) NSString *videoUrl;

///用户职业
@property (nonatomic ,copy) NSString *career;

///用户接受的酒店星级
@property (nonatomic ,assign) NSInteger hotelLevel;

///支付密码
@property (nonatomic ,copy) NSString *payPassword;

///三小时价格
@property (nonatomic ,assign) NSInteger price1;

///十二小时价格
@property (nonatomic ,assign) NSInteger price2;

///身高
@property (nonatomic ,assign) NSInteger height;

///体重
@property (nonatomic ,assign) NSInteger weight;

///胸围
@property (nonatomic ,copy) NSString *cupSize;

///简介
@property (nonatomic ,copy) NSString *brief;

///上次登录时间
@property (nonatomic ,assign) NSInteger lastTime;

///上次登录时的纬度
@property (nonatomic ,copy) NSString *lastLat;

///上次登录时的经度
@property (nonatomic ,copy) NSString *lastLng;

///上次登录时的经度
@property (nonatomic ,copy) NSString *giftId1;

///上次登录时的经度
@property (nonatomic ,copy) NSString *giftId2;

///屏幕解锁密码的 X坐标
@property (nonatomic ,copy) NSString *lockScreenX;

///屏幕解锁密码的 Y坐标
@property (nonatomic ,copy) NSString *lockScreenY;

@end

//
//  CJCPhotoOprationHandle.m
//  roxm
//
//  Created by 陈建才 on 2017/9/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPhotoOprationHandle.h"
#import "CJCCommon.h"
#import "CJCUploadManager.h"

@implementation CJCPhotoOprationHandle

+(void)deletePhotoWithPhotoId:(NSString *)photoID success:(oprationSuccessHandle)success{

    NSString *deleteURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/delete/photo"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"photoId"] = photoID;
    
    [CJCHttpTool postWithUrl:deleteURL params:params success:^(id responseObject) {
        
        if (success) {
            
            success(responseObject);
        }
        
    } failure:^(NSError *error) {
        
    }];
}

+(void)editCommentWithPhotoId:(NSString *)photoID withComment:(NSString *)commpent success:(oprationSuccessHandle)success{

    NSString *deleteURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/update/photo"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"photoId"] = photoID;
    params[@"brief"] = commpent;
    
    [CJCHttpTool postWithUrl:deleteURL params:params success:^(id responseObject) {
        
        if (success) {
            
            success(responseObject);
        }
        
    } failure:^(NSError *error) {
        
    }];
}

@end

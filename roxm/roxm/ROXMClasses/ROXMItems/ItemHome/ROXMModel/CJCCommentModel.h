//
//  CJCCommentModel.h
//  roxm
//
//  Created by 陈建才 on 2017/9/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CJCCommentModel : NSObject

@property (nonatomic ,copy) NSString *commentId;

@property (nonatomic ,copy) NSString *userId;

@property (nonatomic ,copy) NSString *authorId;

@property (nonatomic ,copy) NSString *content;

@property (nonatomic ,copy) NSString *createTime;

@property (nonatomic ,copy) NSString *profile;

@property (nonatomic ,copy) NSString *nickname;

@property (nonatomic ,assign) NSInteger score;

@property (nonatomic ,assign) CGFloat cellHeight;

@property (nonatomic ,assign) CGSize textSize;

@property (nonatomic ,assign) CGFloat nameWidth;

+(void)getFormatModel:(NSDictionary *)dict finish:(void (^)(NSArray *modelArr))finishHandle;

@end

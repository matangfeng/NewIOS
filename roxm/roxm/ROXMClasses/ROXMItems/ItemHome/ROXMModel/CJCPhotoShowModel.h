//
//  CJCPhotoShowModel.h
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CJCDownloadManger,UIImage;

typedef NS_ENUM(NSInteger,CJCPhotoShowType){
    
    CJCPhotoShowTypePhoto  = 0,
    CJCPhotoShowTypeVideo  =1,
};

@interface CJCPhotoShowModel : NSObject

@property (nonatomic ,assign) int type;

@property (nonatomic ,copy) NSString *photoId;

@property (nonatomic ,copy) NSString *url;

@property (nonatomic ,copy) NSString *text;

@property (nonatomic ,assign) NSInteger height;

@property (nonatomic ,assign) NSInteger width;

@property (nonatomic ,strong) UIImage *thumbnilImage;

@property (nonatomic ,assign) CJCPhotoShowType showType;

@property (nonatomic ,assign) BOOL isFinishLoad;

@property (nonatomic ,strong) UIImage *videoImage;

@property (nonatomic ,copy) NSString *videoPath;

@property (nonatomic ,copy) NSString *videoLength;

@property (nonatomic ,strong) CJCDownloadManger *manger;

@end

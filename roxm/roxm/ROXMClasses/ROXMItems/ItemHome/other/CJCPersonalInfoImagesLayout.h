//
//  CJCPersonalInfoImagesLayout.h
//  roxm
//
//  Created by 陈建才 on 2017/10/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCPersonalInfoImagesLayout : UICollectionViewFlowLayout

@property (nonatomic ,assign) NSInteger itemsCount;

@end

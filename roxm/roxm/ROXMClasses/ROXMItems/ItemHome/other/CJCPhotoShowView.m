//
//  CJCPhotoShowView.m
//  roxm
//
//  Created by 陈建才 on 2017/10/8.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPhotoShowView.h"
#import "CJCCommon.h"
#import "CJCPhotoShowCell.h"
#import "CJCPhotoShowModel.h"
#import "CJCPhotoOprationHandle.h"

#define kPhotoShowCell          @"CJCPhotoShowCell"

@interface CJCPhotoShowView ()<UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegate,CJCPhotoShowCellDelegate>{
    
    //当前collectionview展示的item 的index
    NSInteger currentItemIndex;
    
    BOOL isHidDescribe;
}

@property (nonatomic ,strong) UICollectionView *showCollectionView;

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UILabel *describLabel;

@property (nonatomic ,strong) CJCPhotoShowCell *selectCell;

@property (nonatomic ,strong) UITextView *editView;

@property (nonatomic ,strong) UIView *editInputView;

@property (nonatomic ,strong) UIImageView *coverImageView;

@property (nonatomic ,strong) UIView *topView;

@property (nonatomic ,strong) UIView *bottomView;

@end

@implementation CJCPhotoShowView

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)showViewShow{
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    
    [window addSubview:self];
    
    self.frame = self.originFrame;
    
    CJCPhotoShowModel *model = self.imageArr[self.currentIndex];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    
    [imageView setImageURL:[NSURL URLWithString:model.url]];
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    
    imageView.frame = self.bounds;
    
    [self addSubview:imageView];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.frame = window.bounds;
        
        if (model.width==0) {
            
            imageView.size = CGSizeMake(SCREEN_WITDH, SCREEN_WITDH);
        }else{
        
            CGFloat imageWidth = SCREEN_WITDH;
            CGFloat imageHeight = (CGFloat)model.height/model.width*imageWidth;
            
            imageView.size = CGSizeMake(imageWidth, imageHeight);
        }
        
        imageView.center = self.center;
        
    } completion:^(BOOL finished) {
        
        [self reloadData];
        [imageView removeFromSuperview];
    }];

}

-(void)showViewHid{

    [self removeAllSubviews];
    
    self.selectCell.isVideoPlayerPlay = NO;
    [self.selectCell videoStop];
    
    CJCPhotoShowModel *model = self.imageArr[self.currentIndex];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    
    [imageView setImageURL:[NSURL URLWithString:model.url]];
    
    if (model.width==0) {
        
        imageView.size = CGSizeMake(SCREEN_WITDH, SCREEN_WITDH);
    }else{
        
        CGFloat imageWidth = SCREEN_WITDH;
        CGFloat imageHeight = (CGFloat)model.height/model.width*imageWidth;
        
        imageView.size = CGSizeMake(imageWidth, imageHeight);
    }
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    
    imageView.center = self.center;
    
    [self addSubview:imageView];
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.frame = self.originFrame;
        imageView.frame = self.bounds;
        
    } completion:^(BOOL finished) {
        
        [imageView removeFromSuperview];
        [self removeFromSuperview];
    }];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor blackColor];
        
        isHidDescribe = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

-(void)scrollToIndex:(NSInteger)index{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    
    [self.showCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCPhotoShowCell *cell1 = (CJCPhotoShowCell*)cell;
    
    CJCPhotoShowModel *model = self.imageArr[indexPath.row];
    
    if (model.type == 21) {
        
        cell1.isVideoPlayerPlay = NO;
        [cell1 videoStop];
        
        self.selectCell = nil;
    }
    
    if(isHidDescribe){
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.bottomView.y = SCREEN_HEIGHT;
        }];
    }else{
        
        isHidDescribe = YES;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    CGFloat xOffset = scrollView.contentOffset.x;
    
    self.titleLabel.text = [NSString stringWithFormat:@"%ld/%ld",(NSInteger)(xOffset/SCREEN_WITDH)+1,self.imageArr.count];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat xOffset = scrollView.contentOffset.x;
    
    currentItemIndex = (NSInteger)(xOffset/SCREEN_WITDH);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:currentItemIndex inSection:0];
    
    CJCPhotoShowCell *cell1 = (CJCPhotoShowCell *)[self.showCollectionView cellForItemAtIndexPath:indexPath];
    
    CJCPhotoShowModel *model = self.imageArr[indexPath.row];
    
    if (model.type == 21) {
        
        cell1.isVideoPlayerPlay = YES;
        [cell1 videoPlay];
        
        self.selectCell = nil;
        
        self.selectCell = cell1;
    }
    
    self.currentIndex = currentItemIndex;
    
    if (self.scrollHandle) {
        
        self.scrollHandle(currentItemIndex);
    }
    
    [self changeDescribLabel];
}

-(void)reloadData{

    [self setUpUI];
    
    [self scrollToIndex:self.currentIndex];
    currentItemIndex = self.currentIndex;
    
    self.titleLabel.text = [NSString stringWithFormat:@"%ld/%ld",self.currentIndex+1,self.imageArr.count];
    
    [self changeDescribLabel];
}

-(void)cellTapHandle{

    if (self.showType == CJCPhotoShowViewTypeIcon) {
        
        [self showViewHid];
    }else{
        
        if (self.topView.y == 0) {
            
            [UIView animateWithDuration:0.2 animations:^{
                
                self.topView.y = -59;
                
                self.bottomView.y = SCREEN_HEIGHT;
            }];
        }else{
            
            [UIView animateWithDuration:0.2 animations:^{
                
                self.topView.y = 0;
                
                self.bottomView.y = SCREEN_HEIGHT - self.bottomView.height;
            }];
        }
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.imageArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCPhotoShowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPhotoShowCell forIndexPath:indexPath];
    
    CJCPhotoShowModel *model = self.imageArr[indexPath.row];
    
    if (indexPath.item == self.currentIndex) {
        
        if (model.type == 21) {
            
            cell.isVideoPlayerPlay = YES;
            
            self.selectCell = cell;
        }else{
        
            cell.isVideoPlayerPlay = NO;
        }
        
    }else{
        
        cell.isVideoPlayerPlay = NO;
    }
    
    cell.showModel = model;
    cell.delegate = self;
    
    return cell;
}

-(void)setUpUI{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    flowLayout.itemSize = CGSizeMake(SCREEN_WITDH, SCREEN_HEIGHT);
    
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    UICollectionView *imageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WITDH, SCREEN_HEIGHT) collectionViewLayout:flowLayout];;
    
    imageCollectionView.backgroundColor = [UIColor whiteColor];
    
    imageCollectionView.dataSource = self;
    imageCollectionView.delegate = self;
    
    imageCollectionView.showsHorizontalScrollIndicator = NO;
    
    imageCollectionView.pagingEnabled = YES;
    
    [imageCollectionView registerClass:[CJCPhotoShowCell class] forCellWithReuseIdentifier:kPhotoShowCell];
    
    self.showCollectionView = imageCollectionView;
    [self addSubview:imageCollectionView];
    
    if (self.showType == CJCPhotoShowViewTypeIcon) {
        
        
    }else{
    
        UIView *topView = [[UIView alloc] init];
        
        topView.backgroundColor = [UIColor toUIColorByStr:@"000000" andAlpha:0.7];
        
        topView.frame = CGRectMake(0, 0, SCREEN_WITDH, 59);
        
        self.topView = topView;
        [self insertSubview:topView aboveSubview:imageCollectionView];
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [backButton setImage:kGetImage(@"nav_icon_back_black") forState:UIControlStateNormal];
        
        backButton.frame = CGRectMake(3, 9.5, 59, 59);
        
        [backButton addTarget:self action:@selector(backButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
        
        [topView addSubview:backButton];
        
        NSString *title = [NSString stringWithFormat:@"%ld/%ld",self.imageArr.count,self.imageArr.count];
        
        UILabel *titleLabel = [UIView getSystemLabelWithStr:title fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"FFFFFF"]];
        
        titleLabel.textAlignment = NSTextAlignmentCenter;
        
        titleLabel.size = CGSizeMake(100, 17);
        titleLabel.centerX = SCREEN_WITDH/2;
        titleLabel.centerY = 30;
        
        self.titleLabel = titleLabel;
        [topView addSubview:titleLabel];
        
        UIView *bottomView = [[UIView alloc] init];
        
        bottomView.backgroundColor = [UIColor toUIColorByStr:@"000000" andAlpha:0.7];
        
        bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-80, SCREEN_WITDH, 80);
        
        self.bottomView = bottomView;
        [self insertSubview:bottomView aboveSubview:imageCollectionView];
        
        UILabel *describLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor whiteColor]];
        
        describLabel.frame = CGRectMake(14, 18, SCREEN_WITDH-28, 45);
        
        self.describLabel = describLabel;
        [bottomView addSubview:describLabel];
        
        if (self.showType == CJCPhotoShowViewTypeCanOpration) {
            
            describLabel.frame = CGRectMake(14, 18, SCREEN_WITDH-70, 45);
            
            UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            [deleteButton setImage:kGetImage(@"nav_icon_more") forState:UIControlStateNormal];
            
            deleteButton.size = CGSizeMake(40, 40);
            deleteButton.centerY = 30;
            deleteButton.right = SCREEN_WITDH - 15;
            
            [deleteButton addTarget:self action:@selector(deleteButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
            
            [topView addSubview:deleteButton];
            
            UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            [editButton setImage:kGetImage(@"nav_icon_more") forState:UIControlStateNormal];
            
            editButton.size = CGSizeMake(40, 40);
            editButton.centerY = 40;
            editButton.right = SCREEN_WITDH - 15;
            
            [editButton addTarget:self action:@selector(editButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
            
            [bottomView addSubview:editButton];
        }
    }

}

-(void)editButtonClickHandle{
    
    [self.editView becomeFirstResponder];
}

-(void)deleteButtonClickHandle{
    
    if (self.imageArr.count<6) {
        
        [MBManager showBriefAlert:@"相册不得少于5张图片"];
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"确定要删除图片吗?" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [MBManager showLoading];
        
        CJCPhotoShowModel *model = self.imageArr[currentItemIndex];
        
        [CJCPhotoOprationHandle deletePhotoWithPhotoId:model.photoId success:^(id responsObject) {
            
            [MBManager hideAlert];
            
            NSNumber *rtNum = responsObject[@"rt"];
            
            if (rtNum.intValue == 0) {
                
                NSMutableArray *tempArr = [NSMutableArray arrayWithArray:self.imageArr];
                
                [tempArr removeObject:model];
                
                self.imageArr = tempArr.copy;
                
                self.titleLabel.text = [NSString stringWithFormat:@"%ld/%ld",currentItemIndex+1,self.imageArr.count];
                
                [self.showCollectionView reloadData];
            
                
            }else{
                
                [MBManager showBriefAlert:responsObject[@"msg"]];
            }
        }];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    
}

-(void)backButtonClickHandle{
    
    [self showViewHid];
}

#pragma mark  =====监听键盘的弹起 收回
- (void)keyboardWillShow:(NSNotification *)aNotification{
    //获取键盘的高度
    NSValue *aValue = [[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    
    self.editInputView.y = SCREEN_HEIGHT - self.editInputView.height - keyboardRect.size.height;
    
}

- (void)keyboardWillHide:(NSNotification *)aNotification{
    
    self.editInputView.y = SCREEN_HEIGHT;
}

-(UIView *)editInputView{
    
    if (_editInputView == nil) {
        
        UIView *view = [[UIView alloc] init];
        
        view.backgroundColor = [UIColor whiteColor];
        
        view.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WITDH, 60);
        
        [self insertSubview:view aboveSubview:self.showCollectionView];
        
        _editInputView = view;
        
        UIButton *completeButton = [UIView getButtonWithStr:@"完成" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
        
        completeButton.size = CGSizeMake(40, 20);
        completeButton.centerY = 30;
        completeButton.right = SCREEN_WITDH - 8;
        
        [completeButton addTarget:self action:@selector(completeButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:completeButton];
    }
    return _editInputView;
}

-(void)completeButtonClickHandle{
    
    if ([CJCTools isBlankString:self.editView.text]) {
        
        [MBManager showBriefAlert:@"请输入需要添加的描述"];
        
        return ;
    }
    
    [MBManager showLoading];
    CJCPhotoShowModel *model = self.imageArr[currentItemIndex];
    
    [CJCPhotoOprationHandle editCommentWithPhotoId:model.photoId withComment:self.editView.text success:^(id responsObject) {
        
        [MBManager hideAlert];
        
        NSNumber *rtNum = responsObject[@"rt"];
        
        if (rtNum.intValue == 0) {
            
            model.text = self.editView.text;
            
            [self changeDescribLabel];
            
            [self endEditing:YES];
            
            self.editView.text = @"";
            
        }else{
            
            [MBManager showBriefAlert:responsObject[@"msg"]];
        }
    }];
    
}

-(void)changeDescribLabel{
    
    CJCPhotoShowModel *model = self.imageArr[currentItemIndex];
    
    self.describLabel.text = model.text;
    
    CGSize stringSize = CGSizeZero;
    
    if ([CJCTools isBlankString:model.text]) {
        
        self.bottomView.height = 0;
    }else{
        
        CGFloat maxTextWidth;
        
        if (self.showType == CJCPhotoShowViewTypeOnlyPreview) {
            
            maxTextWidth = SCREEN_WITDH - 28;
            
        }else{
        
            maxTextWidth = SCREEN_WITDH - 70;
        }

        UIFont *font = [UIFont accodingVersionGetFont_regularWithSize:15];
        
        NSDictionary *attributes = @{NSFontAttributeName:font};
        NSInteger options = NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin;
        CGRect stringRect = [model.text boundingRectWithSize:CGSizeMake(maxTextWidth, CGFLOAT_MAX) options:options attributes:attributes context:NULL];
        stringSize = CGSizeMake(stringRect.size.width, stringRect.size.height);
        
        self.bottomView.height = stringSize.height+kAdaptedValue(34);
        
        self.describLabel.height = stringSize.height;
        self.describLabel.top = kAdaptedValue(17);
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.bottomView.y = SCREEN_HEIGHT - self.bottomView.height;
    }];
}

-(UITextView *)editView{
    
    if (_editView == nil) {
        
        UITextView *textView = [[UITextView alloc] init];
        
        textView.frame = CGRectMake(kAdaptedValue(14), 10, SCREEN_WITDH-kAdaptedValue(60), kAdaptedValue(45));
        
        textView.textColor = [UIColor toUIColorByStr:@"333333"];
        
        textView.backgroundColor = [UIColor toUIColorByStr:@"F8F8F8"];
        
        [self.editInputView addSubview:textView];
        
        _editView = textView;
    }
    return _editView;
}


@end

//
//  CJCPersonalInfoImagesLayout.m
//  roxm
//
//  Created by 陈建才 on 2017/10/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPersonalInfoImagesLayout.h"
#import "CJCCommon.h"

@interface CJCPersonalInfoImagesLayout ()

@end

@implementation CJCPersonalInfoImagesLayout

-(void)prepareLayout{
    [super prepareLayout];
    
    self.itemSize = CGSizeMake((SCREEN_WITDH)/3, (SCREEN_WITDH)/3);
    self.minimumLineSpacing = 0;
    self.minimumInteritemSpacing = 0;
    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    
    return YES;
}

//可以自定义collectionview的contentsize
-(CGSize)collectionViewContentSize{
    
    CGFloat height;
    
    CGFloat width = ((self.itemsCount-1)/6+1)*SCREEN_WITDH;
    
    if (self.itemsCount<=3) {
        
        height = SCREEN_WITDH/3;
    }else{
    
        height = (SCREEN_WITDH)/3*2;
    }
    
    return CGSizeMake(width, height);
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    
    // 获得super已经计算好的布局属性
//    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    NSArray* array = [[NSArray alloc] initWithArray:[super layoutAttributesForElementsInRect:rect] copyItems:YES];
    
//    self.itemsCount = array.count;
    
    for (UICollectionViewLayoutAttributes *attribute in array) {
        
        NSIndexPath *index = attribute.indexPath;
        
        NSInteger row = index.row/3;
        NSInteger column = index.row%3;
        
        NSInteger page = index.row/6;
        
        CGFloat width = SCREEN_WITDH/3;
        CGFloat height = SCREEN_WITDH/3;
        
        //计算x 需要根据页数 加上屏幕宽  减去每页的高度
        CGRect newFrame = CGRectMake(page*SCREEN_WITDH+column*width, row*height-page*width*2, width, height);
        
        attribute.frame = newFrame;
    }
    
    return array;
}


@end

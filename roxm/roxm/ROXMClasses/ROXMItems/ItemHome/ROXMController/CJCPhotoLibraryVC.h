//
//  CJCPhotoLibraryVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"
#import "CJCpersonalInfoModel.h"

@class CJCpersonalInfoModel;

@interface CJCPhotoLibraryVC : CommonVC

@property (nonatomic ,copy) NSArray *imageArray;

@property (nonatomic ,copy) NSArray *uploadImageArray;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@property (nonatomic ,assign) CJCPersonalInfoVCType infoVCType;

@end

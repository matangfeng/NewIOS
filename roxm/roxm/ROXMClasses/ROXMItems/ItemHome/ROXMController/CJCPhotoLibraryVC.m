//
//  CJCPhotoLibraryVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPhotoLibraryVC.h"
#import "CJCpersonalInfoModel.h"
#import "CJCCommon.h"
#import "CJCPhotoOprationCell.h"
#import "CJCAddPhotoCell.h"
#import "CJCPhotoShowModel.h"
#import "TZImagePickerController.h"
#import <Photos/Photos.h>
#import "CJCAddImageModel.h"
#import "TZImageManager.h"
#import "CJCUploadManager.h"
#import "CJCPhotoShowVC.h"
#import "CJCBeautyCameraVC.h"
#import "CJCPhotoShowView.h"
#import <AFNetworking.h>

#define kPhotoOprationCell      @"CJCPhotoOprationCell"
#define kAddImageCell           @"CJCAddPhotoCell"

@interface CJCPhotoLibraryVC ()<UICollectionViewDataSource,UICollectionViewDelegate,TZImagePickerControllerDelegate,CJCPhotoOprationCellDelegate>{

    NSMutableArray *selectImages;
    
    NSMutableArray *cellAttributesArray;
    
    NSMutableArray *moveImagesArr;
    
    NSMutableArray *uploadImageInfoArray;
    
    BOOL isEditStatus;
}

@property (nonatomic ,strong) UICollectionView *imageCollectionView;

@property (nonatomic ,strong) UIButton *oprationButton;

@property (nonatomic ,strong) UILabel *libraryNameLable;

@property (nonatomic ,strong) UIView *bottomView;

@property (nonatomic,assign)BOOL isChange;

@end

@implementation CJCPhotoLibraryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectImages = [NSMutableArray array];
    cellAttributesArray = [NSMutableArray array];
    moveImagesArr = [NSMutableArray arrayWithArray:self.imageArray];
    uploadImageInfoArray = [NSMutableArray arrayWithArray:self.uploadImageArray];
    isEditStatus = NO;
    
    [self setUpNaviView];
    [self setUpUI];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if (self.infoVCType == CJCPersonalInfoVCTypeOwn) {
        
        return moveImagesArr.count+1;
    }
    
    return moveImagesArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (self.infoVCType == CJCPersonalInfoVCTypeOwn) {
        
        if (indexPath.item == 0) {
            
            CJCAddPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAddImageCell forIndexPath:indexPath];
            
            return cell;
        }
        
        CJCPhotoOprationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPhotoOprationCell forIndexPath:indexPath];
        
        CJCPhotoShowModel *model = moveImagesArr[indexPath.row-1];
        
        cell.index = indexPath.row-1;
        
        cell.showModel = model;
        cell.isEdit = isEditStatus;
        cell.delegate = self;
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
        [cell addGestureRecognizer:longPress];
        
        return cell;
    }
    
    CJCPhotoOprationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPhotoOprationCell forIndexPath:indexPath];
    
    CJCPhotoShowModel *model = moveImagesArr[indexPath.row];
    
    cell.index = indexPath.row;
    
    cell.showModel = model;
    cell.isEdit = isEditStatus;
    cell.delegate = self;
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
    [cell addGestureRecognizer:longPress];
    
    return cell;
}

- (void)longPressAction:(UILongPressGestureRecognizer *)longPress {
    
    if (isEditStatus) {
        
        CJCPhotoOprationCell *cell = (CJCPhotoOprationCell *)longPress.view;
        NSIndexPath *cellIndexpath = [self.imageCollectionView indexPathForCell:cell];
        
        [self.imageCollectionView bringSubviewToFront:cell];
        
        _isChange = NO;
        
        switch (longPress.state) {
            case UIGestureRecognizerStateBegan: {
                
                [cellAttributesArray removeAllObjects];
                for (int i = 1; i < moveImagesArr.count+1; i++) {
                    [cellAttributesArray addObject:[self.imageCollectionView layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]]];
                }
                
            }
                
                break;
                
            case UIGestureRecognizerStateChanged: {
                
                cell.center = [longPress locationInView:self.imageCollectionView];
                
                for (UICollectionViewLayoutAttributes *attributes in cellAttributesArray) {
                    if (CGRectContainsPoint(attributes.frame, cell.center) && cellIndexpath != attributes.indexPath) {
                        _isChange = YES;
                        
                        NSString *imgStr = moveImagesArr[cellIndexpath.row-1];
                        [moveImagesArr removeObjectAtIndex:cellIndexpath.row-1];
                        [moveImagesArr insertObject:imgStr atIndex:attributes.indexPath.row-1];
                        
                        NSDictionary *tempDict = uploadImageInfoArray[cellIndexpath.row-1];
                        [uploadImageInfoArray removeObjectAtIndex:cellIndexpath.row-1];
                        [uploadImageInfoArray insertObject:tempDict atIndex:attributes.indexPath.row-1];
                        
                        [self.imageCollectionView moveItemAtIndexPath:cellIndexpath toIndexPath:attributes.indexPath];
                    }
                }
                
            }
                
                break;
                
            case UIGestureRecognizerStateEnded: {
                
                if (!_isChange) {
                    cell.center = [self.imageCollectionView layoutAttributesForItemAtIndexPath:cellIndexpath].center;
                    
                    [self showWithLabelAnimation];
                    [self pushToROXMDataWithKey:uploadImageInfoArray.copy];
                }
            }
                
                break;
                
            default:
                break;
        }
    }
}

-(void)cellSelectButtonClickHandle:(BOOL)isSelect andIndex:(NSInteger)index{

    CJCPhotoShowModel *model = moveImagesArr[index];
    
    if (isSelect) {
        
        [selectImages addObject:model];
    }else{
    
        [selectImages removeObject:model];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (!isEditStatus) {
        
        if (self.infoVCType == CJCPersonalInfoVCTypeOwn) {
            
            if (indexPath.item == 0) {
                
                [self chooseImageWays];
                
            }else{
                
                CJCPhotoOprationCell *cell = (CJCPhotoOprationCell*)[self.imageCollectionView cellForItemAtIndexPath:indexPath];
                
                UIWindow* window = [UIApplication sharedApplication].keyWindow;
                CGRect rectInWindow = [cell convertRect:cell.showImageView.frame toView:window];
                
                CJCPhotoShowView *showView = [[CJCPhotoShowView alloc] init];
                
                showView.imageArr = _imageArray.copy;
                showView.currentIndex = indexPath.item-1;
                
                showView.showType = CJCPhotoShowViewTypeCanOpration;
                
                showView.originFrame = rectInWindow;
                
                showView.scrollHandle = ^(NSInteger scrollIndex) {
                    
                    CJCPhotoOprationCell *cell1 = (CJCPhotoOprationCell*)[self.imageCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:scrollIndex inSection:0]];
                    
                    UIWindow* window1 = [UIApplication sharedApplication].keyWindow;
                    CGRect rectInWindow1 = [cell1 convertRect:cell.showImageView.frame toView:window1];
                    
                    showView.originFrame = rectInWindow1;
                };
                
                [showView showViewShow];
                
            }
        }else{
        
            CJCPhotoOprationCell *cell = (CJCPhotoOprationCell*)[self.imageCollectionView cellForItemAtIndexPath:indexPath];
            
            UIWindow* window = [UIApplication sharedApplication].keyWindow;
            CGRect rectInWindow = [cell convertRect:cell.showImageView.frame toView:window];
            
            CJCPhotoShowView *showView = [[CJCPhotoShowView alloc] init];
            
            showView.imageArr = _imageArray.copy;
            showView.currentIndex = indexPath.item;
            
            showView.showType = CJCPhotoShowViewTypeOnlyPreview;
            
            showView.originFrame = rectInWindow;
            
            showView.scrollHandle = ^(NSInteger scrollIndex) {
                
                CJCPhotoOprationCell *cell1 = (CJCPhotoOprationCell*)[self.imageCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:scrollIndex inSection:0]];
                
                UIWindow* window1 = [UIApplication sharedApplication].keyWindow;
                CGRect rectInWindow1 = [cell1 convertRect:cell.showImageView.frame toView:window1];
                
                showView.originFrame = rectInWindow1;
            };
            
            [showView showViewShow];

        }
    }
}

-(void)chooseImageWays{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"选择操作方式" preferredStyle:  UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照、录像" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self cameraAuthorityIsOpen];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self photoLibraryAuthorityIsOpen];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
}

-(void)audioAuthorityIsOpen{
    
    AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if (authStatus == AVAuthorizationStatusAuthorized)
    {
        
        [self pushToCametaVC];
        
    }else{
        
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            
            if (granted) {
                // 用户同意获取麦克风
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self pushToCametaVC];
                });
                
            } else {
                
                // 用户不同意获取麦克风
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self alertVCShowWith:@"拍摄视频需要开启手机的麦克风权限"];
                });
            }
            
        }];
    }
}

-(void)cameraAuthorityIsOpen{
    
    
    AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusAuthorized)
    {
        
        [self audioAuthorityIsOpen];
        
    }else{
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {//相机权限
            if (granted) {
                
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self audioAuthorityIsOpen];
                });
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self alertVCShowWith:@"拍摄照片需要开启手机的相机权限"];
                });
                
            }
        }];
        
    }
    
}

-(void)pushToCametaVC{

    CJCBeautyCameraVC *cameraVC = [[CJCBeautyCameraVC alloc] init];
    
    cameraVC.takePhotoHandle = ^(UIImage *returnImage) {
        
        CJCAddImageModel *tempModel = [[CJCAddImageModel alloc] init];
        
        tempModel.image = returnImage;
        tempModel.mediaType = CJCMediaTypePhoto;
        tempModel.width = returnImage.size.width;
        tempModel.height = returnImage.size.height;
        
        [selectImages addObject:tempModel];
        
        CJCUploadManager *manger = [[CJCUploadManager alloc] init];
        
        [manger uploadGroupImagesAndVideos:selectImages.copy successHandle:^(NSArray *QiniuKeys) {
            
            [self pushToROXMDataWithKey:[self keysArrayToDictsArray:QiniuKeys]];
        }];
    };
    
    cameraVC.takeVideoHandle = ^(UIImage *returnImage, NSString *videoPath) {
        
        CJCAddImageModel *tempModel = [[CJCAddImageModel alloc] init];
        
        tempModel.image = returnImage;
        tempModel.mediaType = CJCMediaTypeVideo;
        tempModel.videoPath = videoPath;
        //            tempModel.videoLength = [self getLocalVideoLengh:videoPath];
        tempModel.width = returnImage.size.width;
        tempModel.height = returnImage.size.height;
        
        [selectImages addObject:tempModel];
        
        CJCUploadManager *manger = [[CJCUploadManager alloc] init];
        
        [manger uploadGroupImagesAndVideos:selectImages.copy successHandle:^(NSArray *QiniuKeys) {
            
            [self pushToROXMDataWithKey:[self keysArrayToDictsArray:QiniuKeys]];
        }];
    };
    
    [self.navigationController pushViewController:cameraVC animated:YES];
}

-(void)photoLibraryAuthorityIsOpen{
    
    PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
    if (authStatus == PHAuthorizationStatusAuthorized)
    {
        
        [self pushtoPhotoLibryVC];
        
    }else{
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized)
            {
                
                // 用户同意获取麦克风
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self pushtoPhotoLibryVC];
                });
                
            } else {
                
                // 用户不同意获取麦克风
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self alertVCShowWith:@"选择相册图片需要开启手机的相册权限"];
                });
            }
        }];
        
    }
}

-(void)pushtoPhotoLibryVC{
    
    TZImagePickerController *nextVC = [[TZImagePickerController alloc] initWithMaxImagesCount:10 columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    
    nextVC.allowTakePicture = NO;
    nextVC.allowPickingMultipleVideo = YES;
    nextVC.allowPickingOriginalPhoto = NO;
    nextVC.allowPreview = NO;
    nextVC.oKButtonTitleColorNormal = [UIColor toUIColorByStr:@"429CF0"];
    nextVC.oKButtonTitleColorDisabled = [UIColor toUIColorByStr:@"429CF0"];
    
    [self presentViewController:nextVC animated:YES completion:nil];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    
    [self showWithLabelAnimation];
    
    for (int i=0; i<photos.count; i++) {
        
        UIImage *image = photos[i];
        
        CJCAddImageModel *tempModel = [[CJCAddImageModel alloc] init];
        
        tempModel.image = photos[i];
        tempModel.mediaType = CJCMediaTypeAsset;
        tempModel.asset = assets[i];
        tempModel.assetMediaType = [[TZImageManager manager] getAssetType:assets[i]];
        tempModel.width = image.size.width;
        tempModel.height = image.size.height;
        
        [selectImages addObject:tempModel];
    }
    
    CJCUploadManager *manger = [[CJCUploadManager alloc] init];
    
    [manger uploadGroupImagesAndVideos:selectImages.copy successHandle:^(NSArray *QiniuKeys) {
        
        [self pushToROXMDataWithKey:[self keysArrayToDictsArray:QiniuKeys]];
    }];
    
}

-(NSArray *)keysArrayToDictsArray:(NSArray *)keys{
    
    NSMutableArray *tempArr = [NSMutableArray array];
    
    for (CJCAddImageModel *model in selectImages) {
        
        NSDictionary *tempDict;
        
        NSString *domain = [kUSERDEFAULT_STAND objectForKey:kQINIUDOMAIN];
        
        NSString *key = model.uploadKey;
        
        NSString *returnStr = [NSString stringWithFormat:@"%@/%@",domain,key];
        
        if ([key containsString:@"images"]) {
            
            tempDict = @{ @"url":returnStr,@"key":key,@"type":@"20",@"height":@(model.height),@"width":@(model.width)};
        }else{
            
            tempDict = @{ @"url":returnStr,@"key":key,@"type":@"21",@"height":@(model.height),@"width":@(model.width)};
        }
        
        [tempArr addObject:tempDict];
    }
    
    return tempArr.copy;
}

-(void)pushToROXMDataWithKey:(NSArray *)keys{
    
    NSString *pushURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/upload/file"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"files"] = keys;
    
    [CJCHttpTool postImagesAndVideosWithUrl:pushURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        [selectImages removeAllObjects];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [self getPersonalImages];
            
        }else{
        
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)getPersonalImages{
    
    NSString *imagesURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/list/photo"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"otherUserId"] = [CJCpersonalInfoModel infoModel].loginUserDict[@"uid"];
    
    [CJCHttpTool postWithUrl:imagesURL params:params success:^(id responseObject) {
        
        [MBManager hideAlert];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCPhotoShowModel class] json:responseObject[@"data"]];
            
            [moveImagesArr removeAllObjects];
            
            [moveImagesArr addObjectsFromArray:tempArr];
            
            [self.imageCollectionView reloadData];
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(UIView *)bottomView{

    if (_bottomView == nil) {
        
        UIView *view = [[UIView alloc] init];
        
        view.backgroundColor = [UIColor toUIColorByStr:@"FBFBFB"];
        
        view.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WITDH, 49);
        
        [self.view addSubview:view];
        
        _bottomView = view;
        
        UIView *lineView = [UIView getLineView];
        
        lineView.frame = CGRectMake(0, 0, SCREEN_WITDH, OnePXLineHeight);
        
        [view addSubview:lineView];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button setImage:kGetImage(@"nav_icon_more") forState:UIControlStateNormal];
        
        button.size = CGSizeMake(40, 40);
        button.centerY = 25;
        button.centerX = self.view.centerX;
        
        [button addTarget:self action:@selector(getGift) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:button];
    }
    return _bottomView;
}

-(void)getGift{
    
    NSInteger cha = moveImagesArr.count - selectImages.count;
    
    if (cha<5) {
        
        [MBManager showBriefAlert:@"相册不得少于5张图片"];
        return;
    }
    
    [self showWithLabelAnimation];
    
    NSMutableArray *tempArr = [NSMutableArray array];
    
    for (CJCPhotoShowModel *model in selectImages) {
        
        [tempArr addObject:model.photoId];
    }
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"user/delete/photos"];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:giftURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFormData:[NSKeyedArchiver archivedDataWithRootObject:tempArr.copy] name:@"photoIds"];
        
        [formData appendPartWithFormData:[[kUSERDEFAULT_STAND objectForKey:kUSERTOKEN] dataUsingEncoding:NSUTF8StringEncoding] name:@"token"];
        
        [formData appendPartWithFormData:[[kUSERDEFAULT_STAND objectForKey:kUSERUID] dataUsingEncoding:NSUTF8StringEncoding] name:@"uid"];
        
        [formData appendPartWithFormData:[@"0.1" dataUsingEncoding:NSUTF8StringEncoding] name:@"v"];
        
        [formData appendPartWithFormData:[[CJCTools getDateTimeTOMilliSeconds] dataUsingEncoding:NSUTF8StringEncoding] name:@"t"];
        
        if ([kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE]) {
            
            [formData appendPartWithFormData:[[kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE] dataUsingEncoding:NSUTF8StringEncoding] name:@"f"];
        }else{
            
            [formData appendPartWithFormData:[@"测试" dataUsingEncoding:NSUTF8StringEncoding] name:@"f"];
        }
        
        [formData appendPartWithFormData:[[[UIDevice currentDevice] systemName] dataUsingEncoding:NSUTF8StringEncoding] name:@"osv"];
        
        [formData appendPartWithFormData:[[CJCTools iphoneType] dataUsingEncoding:NSUTF8StringEncoding] name:@"pinfo"];
        
    } error:nil];
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    
    uploadTask = [manager uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            
            
        }
        
    }];
    
    [uploadTask resume];
    
}

-(void)deleteButtonClickHandle{

    NSInteger cha = moveImagesArr.count - selectImages.count;
    
    if (cha<5) {
        
        [MBManager showBriefAlert:@"相册不得少于5张图片"];
        return;
    }
    
    [MBManager showLoading];
    
    NSMutableArray *tempArr = [NSMutableArray array];
    
    for (CJCPhotoShowModel *model in selectImages) {
        
        [tempArr addObject:model.photoId];
    }
    
    NSString *deleteURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"user/delete/photos"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"photoIds"] = tempArr.copy;
    
    [CJCHttpTool postWithUrl:deleteURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        [selectImages removeAllObjects];
        
        if (rtNum.integerValue == 0){
            
            [self getPersonalImages];
            
        }else{
            
            [MBManager hideAlert];
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)setUpUI{

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    CGFloat itemWidth = (SCREEN_WITDH - 6)/3;
    
    flowLayout.itemSize = CGSizeMake(itemWidth, itemWidth);
    flowLayout.minimumInteritemSpacing = 3;
    
    flowLayout.minimumLineSpacing = kAdaptedValue(3);
    
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64) collectionViewLayout:flowLayout];
    
    collectionView.backgroundColor = [UIColor whiteColor];
    
    collectionView.showsVerticalScrollIndicator = NO;
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    [collectionView registerClass:[CJCPhotoOprationCell class] forCellWithReuseIdentifier:kPhotoOprationCell];
    [collectionView registerClass:[CJCAddPhotoCell class] forCellWithReuseIdentifier:kAddImageCell];
    
    self.imageCollectionView = collectionView;
    [self.view addSubview:collectionView];
}

-(void)setUpNaviView{

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    if (self.infoVCType == CJCPersonalInfoVCTypeOwn) {
        
        UIButton *oprationButton = [UIView getButtonWithStr:@"编辑" fontName:kFONTNAMEMEDIUM size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
        
        oprationButton.frame = CGRectMake(200, 20, kAdaptedValue(40), kAdaptedValue(40));
        oprationButton.centerY = kNAVIVIEWCENTERY;
        oprationButton.right = SCREEN_WITDH - kAdaptedValue(16);
        
        [oprationButton addTarget:self action:@selector(oprationButtonClickHandle:) forControlEvents:UIControlEventTouchUpInside];
        
        self.oprationButton = oprationButton;
        [self.naviView addSubview:oprationButton];
    }
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:17 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSString *nameStr = [NSString stringWithFormat:@"%@的相册",[CJCpersonalInfoModel infoModel].loginUserDict[@"nickname"]];
    
    CGFloat width = [CJCTools getShortStringLength:nameStr withFont:[UIFont accodingVersionGetFont_mediumWithSize:17]];
    
    titleLabel.text = nameStr;
    
    titleLabel.size = CGSizeMake(width, 24);
    
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    self.libraryNameLable = titleLabel;
    [self.naviView addSubview:titleLabel];
}

-(void)oprationButtonClickHandle:(UIButton *)button{

    [selectImages removeAllObjects];
    
    button.selected = !button.selected;
    
    if (button.selected) {
        
        [button setTitle:@"取消" forState:UIControlStateSelected];
        isEditStatus = YES;
        
        self.imageCollectionView.height = SCREEN_HEIGHT - 64 - 49;
        self.bottomView.y = SCREEN_HEIGHT - 49;
        
    }else{
    
        [button setTitle:@"编辑" forState:UIControlStateNormal];
        isEditStatus = NO;
        
        self.imageCollectionView.height = SCREEN_HEIGHT - 64;
        self.bottomView.y = SCREEN_HEIGHT;
    }
    
    [self.imageCollectionView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  CJCReportReasonVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@class CJCCommentDateVC;

@interface CJCReportReasonVC : CommonVC

@property (nonatomic ,strong) CJCCommentDateVC *preVC;

@property (nonatomic ,strong) NSString *targetUID;

@end

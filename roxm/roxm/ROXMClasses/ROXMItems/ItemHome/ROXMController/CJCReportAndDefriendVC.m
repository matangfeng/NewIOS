//
//  CJCReportAndDefriendVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCReportAndDefriendVC.h"
#import "CJCCommon.h"
#import "CJCSettingOprationCell.h"
#import "CJCReportReasonVC.h"
#import <EaseUI.h>

#define kSettingOprationCell        @"CJCSettingOprationCell"

@interface CJCReportAndDefriendVC ()<UITableViewDelegate,UITableViewDataSource,CJCSettingOprationCellDelegate>{

    NSInteger isBlackRelation;
}

@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation CJCReportAndDefriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    
    [self getUserRelation];
}

-(void)getUserRelation{

    [self showWithLabelAnimation];
    
    NSString *relationURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/is_black"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = self.targetUid;
    
    [CJCHttpTool postWithUrl:relationURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            //不是黑名单关系 0是黑名单  1不是黑名单
            NSNumber *dataNum = responseObject[@"data"];
            
            isBlackRelation = dataNum.integerValue;
            
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
        }else{
        
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        return kAdaptedValue(122);
    }
    
    return kAdaptedValue(61);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    CJCSettingOprationCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingOprationCell];
    
    cell.delegate = self;
    
    if (indexPath.row == 0) {
        
        cell.cellType = CJCSettingOprationCellTypeSwitchHigher;
        cell.title = @"黑名单";
        cell.detail = @"加入黑名单后对方将无法看到你、关注你或给你发消息";
        
        if (isBlackRelation == 0) {
            
            cell.settingSwitch.on = NO;
        }else{
        
            cell.settingSwitch.on = YES;
        }
    
        
    }else{
    
        cell.cellType = CJCSettingOprationCellTypeDefault;
        cell.title = @"举报用户";
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 1) {
        
        CJCReportReasonVC *nextVC = [[CJCReportReasonVC alloc] init];
        
        nextVC.targetUID = self.targetUid;
        
        [self.navigationController pushViewController:nextVC animated:YES];
    }
}

-(void)switchChangeHandle:(BOOL)isOpen andIndex:(CJCSettingOprationCell *)cell{
    
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    if (index.row == 0) {
        
        if (isOpen) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"拉黑后将不再收到对方的消息,对方也无法邀约你" preferredStyle:  UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self defriendHandle];
                
            }]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
                
            }]];
            
            [self presentViewController:alert animated:true completion:nil];
            
        }else{
        
            [self cancleDefriendHandle];
        }
    }
}

-(void)defriendHandle{

    [self showWithLabelAnimation];
    
    NSString *relationURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/black"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = self.targetUid;
    
    [CJCHttpTool postWithUrl:relationURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            //不是黑名单关系 0是黑名单  1不是黑名单
            isBlackRelation = 1;
            
            [[EMClient sharedClient].chatManager deleteConversation:self.targetEMID isDeleteMessages:YES completion:^(NSString *aConversationId, EMError *aError){
                //code
            }];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CJCdeFriendHandle" object:nil];
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
        }else{
            
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)cancleDefriendHandle{
    
    [self showWithLabelAnimation];
    
    NSString *relationURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/cancel/black"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = self.targetUid;
    
    [CJCHttpTool postWithUrl:relationURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            //不是黑名单关系 0不是黑名单  1是黑名单
            isBlackRelation = 0;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CJCCancledeFriendHandle" object:nil];
            
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
        }else{
            
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    self.lineView.hidden = YES;
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCSettingOprationCell class] forCellReuseIdentifier:kSettingOprationCell];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

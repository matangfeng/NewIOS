//
//  CJCReportAndDefriendVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@interface CJCReportAndDefriendVC : CommonVC

@property (nonatomic ,copy) NSString *targetUid;

@property (nonatomic ,copy) NSString *targetEMID;

@end

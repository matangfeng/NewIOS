//
//  CJCPhotoShowVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CJCpersonalInfoModel.h"

typedef void(^photosRefreshHandle)();

@interface CJCPhotoShowVC : UIViewController

@property (nonatomic ,copy) NSArray *imageArr;

@property (nonatomic ,assign) NSInteger currentIndex;

@property (nonatomic ,assign) CJCPersonalInfoVCType infoVCType;

@property (nonatomic ,copy) photosRefreshHandle refreshHandle;

@end

//
//  CJCHomeViewController.m
//  roxm
//
//  Created by lfy on 2017/9/1.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCHomeViewController.h"
#import "CJCCommon.h"
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import "CJCNearbyPersonCell.h"
#import "CJCpersonalInfoModel.h"
#import <NSObject+YYModel.h>
#import "CJCPersonalInfoVC.h"
#import "CJCCommonAuthorityView.h"
#import "CJCLoginAndRegisterVC.h"
#import "CJCNavigationController.h"
#import "CJCBlankCell.h"
#import <Hyphenate/Hyphenate.h>
#import "CJCMessageHandle.h"
#import "CJCFistAnimationView.h"
#import "CJCClickSecretView.h"
#import <AFNetworking.h>

#define NEARBYPERSONCELL     @"CJCNearbyPersonCell"
#define kBLANKCELL           @"BlankCell"

@interface CJCHomeViewController ()<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate>{

    NSMutableArray *dataArray;
    
    NSMutableArray *filterIds;
    
    NSMutableArray *blackArray;
    
    NSInteger pageSize;
    BOOL isRefresh;
    
    BOOL isDataReady;
    
    NSInteger selectIndex;
}

@property (nonatomic ,strong) CLLocationManager *manger;

@property (nonatomic ,strong) AMapLocationManager *amapManager;

@property (nonatomic ,strong) UITableView *tableView;

@property (nonatomic ,strong) CLLocation *location;

@property (nonatomic ,strong) UIView *commonView;

@property (nonatomic ,strong) CJCFistAnimationView *animationView;

@end

@implementation CJCHomeViewController

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"邀约";
//    if ([[kUSERDEFAULT_STAND objectForKey:@"secretAreaIsSet"] isEqualToString:kSWITCHON]) {
//        
//        if ([kUSERDEFAULT_STAND objectForKey:@"secretAreaX"]) {
//            
//            CJCClickSecretView *view = [[CJCClickSecretView alloc] init];
//            
//            [view showViewShow];
//        }
//    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"CJCdeFriendHandle" object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        if (dataArray.count){
            [dataArray removeObjectAtIndex:selectIndex];
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectIndex inSection:0];
        // 刷新
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }];
    
    dataArray = [NSMutableArray array];
    filterIds = [NSMutableArray arrayWithCapacity:10];
    blackArray = [NSMutableArray array];
    pageSize = 1;
    isRefresh = NO;
    isDataReady = NO;
    
    [self setUpNaviView];

    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIApplicationDidBecomeActiveNotification object:nil ] subscribeNext:^(NSNotification * _Nullable x) {
        if (self.commonView) {
            [self.commonView removeFromSuperview];
            [self configTableView];
            [self configLocationManager];
        }
    }];
    
    if ([CJCTools locationStatus] == kCLAuthorizationStatusAuthorizedAlways||[CJCTools locationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self configTableView];
        [self configLocationManager];
        
    }else if ([CJCTools locationStatus] == kCLAuthorizationStatusDenied){
        [self configBlankView];
    }else{
        CLLocationManager *manger = [[CLLocationManager alloc] init];
        self.manger = manger;
        manger.delegate = self;
        [manger requestAlwaysAuthorization];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"current status ://%d",status);
    if(status == kCLAuthorizationStatusAuthorizedAlways||[CJCTools locationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
        [self.commonView removeFromSuperview];
        [self configTableView];
        [self configLocationManager];
    }else{
        
    }
}

- (void)configBlankView{
    
    UIView *blankView = [[UIView alloc] init];
    blankView.backgroundColor = [UIColor clearColor];
    blankView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64 - 49);
    
    self.commonView = blankView;
    [self.view addSubview:blankView];

    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(174), kAdaptedValue(24), kAdaptedValue(20.5));
    [blankView addSubview:imageView];
    
    UILabel *titleLabel = [UIView getYYLabelWithStr:@"地理位置未开启" fontName:kFONTNAMEREGULAR size:28 color:[UIColor toUIColorByStr:@"222222"]];
    titleLabel.frame = CGRectMake(kAdaptedValue(60), kAdaptedValue(170), kAdaptedValue(220), kAdaptedValue(28));
    [blankView addSubview:titleLabel];
    
    NSString *hintStr = @"为给你推荐附近有趣的人，我们需要知道你的地理位置。请在iPhone\"设置-隐私-定位服务\"中允许roxm濡沫使用定位服务。";
    UILabel *tipLabel = [[UILabel alloc] init];
    tipLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(217), blankView.width - kAdaptedValue(46), kAdaptedValue(72));
    tipLabel.numberOfLines = 0;
    tipLabel.textColor = [UIColor toUIColorByStr:@"888888"];
    
    [blankView addSubview:tipLabel];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineSpacing = 9;
    
    NSDictionary *attributes = @{ NSFontAttributeName:[UIFont accodingVersionGetFont_regularWithSize:15], NSParagraphStyleAttributeName:paragraphStyle};
    tipLabel.attributedText = [[NSAttributedString alloc]initWithString:hintStr attributes:attributes];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor toUIColorByStr:@"1D1D1D"];
    bottomView.frame = CGRectMake(kAdaptedValue(108), kAdaptedValue(334), kAdaptedValue(159), kAdaptedValue(49));
    bottomView.layer.cornerRadius = kAdaptedValue(25);
    bottomView.layer.masksToBounds = YES;
    
    bottomView.centerX = blankView.centerX;
    [blankView addSubview:bottomView];
    
    UILabel *setLabel = [UIView getYYLabelWithStr:@"去设置" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    setLabel.frame = CGRectMake(kAdaptedValue(55.5), kAdaptedValue(13.5), kAdaptedValue(52), kAdaptedValue(22.5));
    setLabel.center = bottomView.center;
    [blankView addSubview:setLabel];
    
    UIButton *goSetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    goSetButton.frame = bottomView.frame;
    [[goSetButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    [blankView addSubview:goSetButton];

    UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:kGetImage(@"arrowright_white")];
    arrowImageView.frame = CGRectMake(200, 10, kAdaptedValue(6), kAdaptedValue(11));
    arrowImageView.centerY = setLabel.centerY;
    arrowImageView.x = setLabel.right+kAdaptedValue(25);
    
    [blankView addSubview:arrowImageView];
}

- (void)configTableView{
    UITableView *tableView = [[UITableView alloc] init];
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64 - 49);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCNearbyPersonCell class] forCellReuseIdentifier:NEARBYPERSONCELL];
    
    [tableView registerClass:[CJCBlankCell class] forCellReuseIdentifier:kBLANKCELL];
    
    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadNewData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMoreData];
    }];
    
    self.tableView.mj_footer.automaticallyHidden = YES;
}

- (void)loadNewData{
    pageSize = 1;
    isRefresh = YES;
    [self getBlackPersons];
}

- (void)loadMoreData{
    pageSize++;
    isRefresh = NO;
    [self getBlackPersons];
}

#pragma mark ========tableView的delegate和DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kAdaptedValue(114);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CJCNearbyPersonCell *cell = [tableView dequeueReusableCellWithIdentifier:NEARBYPERSONCELL];
    CJCpersonalInfoModel *model = dataArray[indexPath.row];
    cell.infoModel = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectIndex = indexPath.row;
    CJCpersonalInfoModel *model = dataArray[indexPath.row];
    CJCPersonalInfoVC *nextVC = [[CJCPersonalInfoVC alloc] init];
    nextVC.sexType = CJCSexTypeWoman;
    nextVC.otherUID = model.uid;
    nextVC.infoVCType = CJCPersonalInfoVCTypeOther;
    nextVC.location = self.location;
    nextVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:nextVC animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark ========获取数据
- (void)configLocationManager
{
    [self showWithLabelAnimation];
    self.amapManager = [[AMapLocationManager alloc] init];
    [self.amapManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    //   定位超时时间，最低2s，此处设置为2s
    self.amapManager.locationTimeout = 5;
    //   逆地理请求超时时间，最低2s，此处设置为2s
    self.amapManager.reGeocodeTimeout = 5;
    
    [self.amapManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        if (error){
            NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
            if (error.code == AMapLocationErrorLocateFailed){
                return;
            }
        }
        
        [kUSERDEFAULT_STAND setObject:regeocode.city forKey:kUSERCITY];
        self.location = location;
        
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        delegate.userLocation = location;
        
        CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
        handle.location = location;
        
        [self getBlackPersons];
        [self uploadUserLocation];
    }];
}

-(void)uploadUserLocation{

    NSString *uploadURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/location/add"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"lat"] = @(self.location.coordinate.latitude);
    params[@"lng"] = @(self.location.coordinate.longitude);
    
    [CJCHttpTool postWithUrl:uploadURL params:params success:^(id responseObject) {
        
    } failure:^(NSError *error) {
    
    }];
}


- (void)getBlackPersons{
    NSString *nearbyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/list/black/against_me"];
    [CJCHttpTool postWithUrl:nearbyURL params   :nil success:^(id responseObject) {
        NSNumber *rtNum = responseObject[@"rt"];
        NSLog(@"list :\n%@",responseObject);
        if (rtNum.integerValue == 0){
            isDataReady = YES;
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCpersonalInfoModel class] json:responseObject[@"data"]];
            [blackArray removeAllObjects];
            [blackArray addObjectsFromArray:tempArr];
             [self getNearbyPersons];
        }else{
            [self hidHUD];
            [self showWithLabel:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)getNearbyPersons{
    NSString *nearbyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/list/near"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"lat"] = [NSString stringWithFormat:@"%f",self.location.coordinate.latitude];
    params[@"lng"] = [NSString stringWithFormat:@"%f",self.location.coordinate.longitude];
    params[@"page"] = [NSString stringWithFormat:@"%ld",(long)pageSize];
    params[@"pageSize"] = [NSString stringWithFormat:@"25"];
    NSLog(@"ri le mlege :\n%ld",(unsigned long)filterIds.count);
    
    NSMutableString * str = [NSMutableString string];
    for (NSInteger i = 0; i < filterIds.count; i++) {
        if (i == filterIds.count -1) {
            [str appendString:[NSString stringWithFormat:@"%@",filterIds[i]]];
        }else{
            [str appendString:[NSString stringWithFormat:@"%@,",filterIds[i]]];
        }
    }
    params[@"filterIds"] = str;
   
    [CJCHttpTool postWithUrl:@"http://test.api.hojji.com/roxm/other/array/test" params:@{@"ids":str} success:^(id responseObject) {
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0){
            NSLog(@"textArray success :\n%@",responseObject);
        }else{
            NSLog(@"Error");
        }
    } failure:^(NSError *error) {
        NSLog(@"Error1");
    }];
    
    
    [CJCHttpTool postWithUrl:nearbyURL params:params success:^(id responseObject) {
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        NSLog(@"list Ont:\n%@",responseObject);
        if (rtNum.integerValue == 0){
            isDataReady = YES;
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCpersonalInfoModel class] json:responseObject[@"data"]];
            if (isRefresh) {

            }
            
            if (tempArr.count == 0) {
                [self.tableView.mj_header endRefreshing];
                [self.tableView.mj_footer endRefreshing];
                return;
            }
            
            for (CJCpersonalInfoModel *mode in tempArr) {
                [dataArray addObject:mode];
                [filterIds addObject:mode.uid];
                
                if(blackArray.count > 0){
                    for (CJCpersonalInfoModel *blackModel in blackArray.copy) {
                        if ([blackModel.uid isEqualToString:mode.uid]) {
                            [dataArray removeObject:mode];
//                            [filterIds appendString:[NSString stringWithFormat:@"%@,",mode.uid]];
                        }
                    }
                }
            }
            for (NSInteger i = 0; i < filterIds.count; i++) {
            }
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
            [self.tableView reloadData];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
    }];
}

-(void)setUpNaviView{
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    self.backButton.hidden = YES;
    self.mediumTitleLabel.text = @"邀约";
    self.mediumTitleLabel.frame = CGRectMake(kAdaptedValue(139.5), 32, kAdaptedValue(100), kAdaptedValue(22.5));
    self.titleLabel.centerY = kNAVIVIEWCENTERY;
    self.titleLabel.centerX = self.view.centerX;
    [self setKeyBoardHid:YES];
    
#pragma mark  ======测试时添加  正式版删除
//    UIButton *loginOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
//
//    [loginOutButton setTitle:@"退出登陆" forState:UIControlStateNormal];
//    [loginOutButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    
//    loginOutButton.frame = CGRectMake(SCREEN_WITDH - 100, 0, 80, 20);
//    
//    loginOutButton.centerY = kNAVIVIEWCENTERY;
//    
//    [loginOutButton addTarget:self action:@selector(loginOutButtonClick) forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.view addSubview:loginOutButton];
}

-(void)loginOutButtonClick{
    
//    [CJCpersonalInfoModel ];
    
//    EMError *error = [[EMClient sharedClient] logout:YES];
//    if (!error) {
//        NSLog(@"退出成功");
//    }
    CJCLoginAndRegisterVC *logstrVC = [[CJCLoginAndRegisterVC alloc] init];
    logstrVC.isVideoPlay = YES;
    CJCNavigationController *naviVC = [[CJCNavigationController alloc] initWithRootViewController:logstrVC];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    window.rootViewController = naviVC;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
